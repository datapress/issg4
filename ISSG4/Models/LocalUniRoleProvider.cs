﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Security;
using System.Configuration;
using System.Configuration.Provider;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using ISSG4.Models;
using NLog;

namespace ISSG4
{
    class LocalUniRoleProvider : RoleProvider
    {
        private string connectionString = null;

        #region Overrided Properties
        public override string ApplicationName { get; set; }
        #endregion

        Logger logger = LogManager.GetCurrentClassLogger();

        #region Overrided Functions
        public override void Initialize(string name, System.Collections.Specialized.NameValueCollection config)
        {

            logger.Debug("LocalUniRoleProvider initializing started");

            if (config == null) throw new ArgumentNullException("config");

            if (name == null || name.Length == 0) name = "LocalUniMembershipProvider";

            //Initialize the abstract base class.
            base.Initialize(name, config);

            /*
            applicationName = GetConfigValue(config["applicationName"], System.Web.Hosting.HostingEnvironment.ApplicationVirtualPath);
            maxInvalidPasswordAttempts = Convert.ToInt32(GetConfigValue(config["maxInvalidPasswordAttempts"], "5"));
            passwordAttemptWindow = Convert.ToInt32(GetConfigValue(config["passwordAttemptWindow"], "10"));
            minRequiredNonAlphanumericCharacters = Convert.ToInt32(GetConfigValue(config["minRequiredAlphaNumericCharacters"], "1"));
            minRequiredPasswordLength = Convert.ToInt32(GetConfigValue(config["minRequiredPasswordLength"], "7"));
            passwordStrengthRegularExpression = Convert.ToString(GetConfigValue(config["passwordStrengthRegularExpression"], String.Empty));
            enablePasswordReset = Convert.ToBoolean(GetConfigValue(config["enablePasswordReset"], "true"));
            enablePasswordRetrieval = Convert.ToBoolean(GetConfigValue(config["enablePasswordRetrieval"], "true"));
            requiresQuestionAndAnswer = Convert.ToBoolean(GetConfigValue(config["requiresQuestionAndAnswer"], "false"));
            requiresUniqueEmail = Convert.ToBoolean(GetConfigValue(config["requiresUniqueEmail"], "true"));
            */

            ConnectionStringSettings ConnectionStringSettings = ConfigurationManager.ConnectionStrings[config["connectionStringName"]];

            if ((ConnectionStringSettings == null) || (ConnectionStringSettings.ConnectionString.Trim() == String.Empty))
            {
                throw new ProviderException("Connection string cannot be blank.");
            }

            //connectionString = CreateEFConnectionString(ConnectionStringSettings.ConnectionString);
        }

        //private string CreateEFConnectionString(string dbConnectionString)
        //{
        //    //metadata=res://*/User.csdl|res://*/User.ssdl|res://*/User.msl;provider=System.Data.SqlClient;provider connection string="data source=Earl;initial catalog=ISPO2GO_gazeta;persist security info=True;user id=sa;password=m4d4g4sk4r;multipleactiveresultsets=True;App=EntityFramework"

        //    // Specify the provider name.
        //    string providerName = "System.Data.SqlClient";

        //    // Initialize the EntityConnectionStringBuilder.
        //    EntityConnectionStringBuilder entityBuilder = new EntityConnectionStringBuilder();

        //    //Set the provider name.
        //    entityBuilder.Provider = providerName;

        //    // Set the provider-specific connection string.
        //    entityBuilder.ProviderConnectionString = dbConnectionString;

        //    // Set the Metadata location.
        //    entityBuilder.Metadata = @"res://*/User.csdl|res://*/User.ssdl|res://*/User.msl";

        //    return entityBuilder.ToString();
        //}

        public override void AddUsersToRoles(string[] usernames, string[] roleNames)
        {
            //if (roleNames.Length > 1) throw new ArgumentException("Użytkownicy w LocalUniRoleProvider mogą posiadać jedną rolę.", "roleNames");

            using (Entities ue = new Entities())
            {
                var roles = ue.Grupa.Where(m => roleNames.Contains(m.Grupa_nazwa));
                var users = ue.Users.Where(m => usernames.Contains(m.UserName));
                //foreach (Users user in users)
                //{
                //    foreach(Grupa role in roles) {
                //        user.Grupa.Add(role);
                //    }
                //}
                ue.SaveChanges();
            }
        }

        public override void CreateRole(string roleName)
        {
            //throw new NotImplementedException("System ról jest zablokowany do edycji.");
            using (Entities ue = new Entities())
            {
                Grupa role = new Grupa() { Grupa_id = roleName, Grupa_nazwa = roleName };
                ue.Grupa.Add(role);
                ue.SaveChanges();
            }
        }

        public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            if (!RoleExists(roleName))
            {
                throw new ProviderException("Rola '" + roleName + "' nie istnieje.");
            }

            if (throwOnPopulatedRole && GetUsersInRole(roleName).Length > 0)
            {
                throw new ProviderException("Nie można usunąć roli, do której są przypisani użytkownicy.");
            }

            try
            {
                using (Entities ue = new Entities())
                {
                    var role = ue.Grupa.Where(m => m.Grupa_nazwa == roleName).Single();
                    ue.Grupa.Remove(role);
                    ue.SaveChanges();
                }
            }
            catch (Exception)
            {
                // Handle exception.
                return false;
            }

            return true;
        }

        public override string[] FindUsersInRole(string roleName, string usernameToMatch)
        {
            string[] usersInRole = null;
            using (Entities ue = new Entities())
            {
                usersInRole = ue.Users.Where(m => m.Grupa1.Any(n => n.Grupa_nazwa == roleName) && m.UserName.Contains(usernameToMatch))
                .Select(m => m.UserName).ToArray<string>();
            }
            if (usersInRole.Length == 0) return null;
            return usersInRole;
        }

        public override string[] GetAllRoles()
        {
            string[] allRoles = null;
            using (Entities ue = new Entities())
            {
                allRoles = ue.Grupa.Select(m => m.Grupa_nazwa).ToArray<string>();
            }
            if (allRoles.Length == 0) return null;
            return allRoles;
        }

        public override string[] GetRolesForUser(string username)
        {
            string[] rolesForUser = null;
            using (Entities ue = new Entities())
            {
                rolesForUser = ue.Users.Where(m => m.UserName == username).Single().Grupa1.Select(n => n.Grupa_nazwa).ToArray<string>();
            }
            if (rolesForUser.Length == 0) return null;
            return rolesForUser;
        }

        public override string[] GetUsersInRole(string roleName)
        {
            string[] usersInRole = null;
            using (Entities ue = new Entities())
            {
                usersInRole = ue.Users.Where(m => m.Grupa1.Any(n => n.Grupa_nazwa == roleName)).Select(m => m.UserName).ToArray<string>();
            }
            if (usersInRole.Length == 0) return null;
            return usersInRole;
        }

        public override bool IsUserInRole(string username, string roleName)
        {
            bool isUserInRole = false;
            using (Entities ue = new Entities())
            {
                isUserInRole = ue.Users.Where(m => m.Grupa1.Any(n => n.Grupa_nazwa == roleName) && m.UserName == username).Any();
            }
            return isUserInRole;
        }

        public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        {
            using (Entities ue = new Entities())
            {
                var roles = ue.Grupa.Where(m => roleNames.Contains(m.Grupa_nazwa));
                var users = ue.Users.Where(m => usernames.Contains(m.UserName));
                foreach (Users user in users)
                {
                    foreach (Grupa role in roles)
                    {
                        user.Grupa1.Remove(role);
                    }
                }
                ue.SaveChanges();
            }
        }

        public override bool RoleExists(string roleName)
        {
            bool roleExists = false;
            using (Entities ue = new Entities())
            {
                roleExists = ue.Grupa.Where(m => m.Grupa_nazwa == roleName).Any();
            }
            return roleExists;
        }
        #endregion
    }
}
