//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ISSG4.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class ProduktRodzaj
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ProduktRodzaj()
        {
            this.i3_Produkt = new HashSet<Produkt>();
        }
    
        public int ProduktRodzajId { get; set; }
        public string ProduktRodzajNazwa { get; set; }
        public bool ProduktRodzajBiezace { get; set; }
        public bool ProduktRodzajArchiwalne { get; set; }
        public bool ProduktRodzajReklamacja { get; set; }
        public bool ProduktRodzajPojedyncze { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Produkt> i3_Produkt { get; set; }
    }
}
