//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ISSG4.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class viewWydanieAktywneJoined
    {
        public int WydanieId { get; set; }
        public string WydanieOpis { get; set; }
        public string WydaniePlik { get; set; }
        public bool WydanieAktywne { get; set; }
        public System.DateTime WydanieData { get; set; }
        public string WydanieJedynka { get; set; }
        public int GazetaId { get; set; }
        public string GazetaNazwa { get; set; }
        public string GazetaSkrot { get; set; }
        public bool GazetaBezplatna { get; set; }
        public Nullable<int> GazetaKodId { get; set; }
        public string GazetaKodNumer { get; set; }
        public string GazetaKodSms { get; set; }
        public string GazetaKodCenaInfo { get; set; }
        public string GazetaKodTyp { get; set; }
        public string GazetaKodKod { get; set; }
        public Nullable<int> ZamowieniePozycjaId { get; set; }
        public Nullable<System.DateTime> ZamowieniePozycjaOd { get; set; }
        public Nullable<System.DateTime> ZamowieniePozycjaDo { get; set; }
        public Nullable<int> ZamowienieId { get; set; }
        public Nullable<int> issg_user_id { get; set; }
        public Nullable<System.DateTime> ZamowienieData { get; set; }
        public Nullable<bool> ZamowienieZablokowane { get; set; }
        public Nullable<bool> ZamowienieOplacone { get; set; }
        public Nullable<bool> ZamowienieRecznaAktywacja { get; set; }
        public int GazetaTypId { get; set; }
        public string GazetaTypNazwa { get; set; }
    }
}
