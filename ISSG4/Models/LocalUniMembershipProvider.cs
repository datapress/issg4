﻿//Adapted from: http://msdn2.microsoft.com/en-us/library/6tc47t75(VS.80).aspx
using System;
using System.Configuration;
using System.Collections.Specialized;
using System.Configuration.Provider;
using System.Data;
using System.Data.SqlClient;
using System.Security.Cryptography;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Web.Security;
using System.Linq;
//using UniMembership.Models;
using NLog;
using ISSG4.Models;

namespace Custom.Membership
{
    public enum LoginFailedCause : int { None = 0, UserLocked = 1 };

    [Serializable]
    public class LoginFailedException : System.Exception
    {
        public LoginFailedCause Cause { get; private set; }

        public LoginFailedException()
        {
            Cause = LoginFailedCause.None;
        }

        public LoginFailedException(LoginFailedCause cause)
        {
            Cause = cause;
        }

        public LoginFailedException(string message, LoginFailedCause cause)
            : base(message)
        {
            Cause = cause;
        }
    }

    public sealed class LocalUniMembershipProvider : MembershipProvider
    {

        private static Logger logger = LogManager.GetCurrentClassLogger();

        #region Class Variables

        public string connectionString { get; private set; }
        private string applicationName;
        private string providerName;
        private string passwordStrengthRegularExpression;
        private string passwordStrengthRegularExpressionInfo;

        #endregion


        #region Properties

        public override string ApplicationName
        {
            get
            {
                return applicationName;
            }
            set
            {
                applicationName = value;
            }
        }

        public override bool EnablePasswordReset
        {
            get
            {
                return true;
            }
        }

        public override bool EnablePasswordRetrieval
        {
            get
            {
                return false;
            }
        }

        public override bool RequiresQuestionAndAnswer
        {
            get
            {
                return false;
            }
        }

        public override bool RequiresUniqueEmail
        {
            get
            {
                return true;
            }
        }

        public override int MaxInvalidPasswordAttempts
        {
            get
            {
                return 5;
            }
        }

        public override int PasswordAttemptWindow
        {
            get
            {
                return 5;
            }
        }

        public override MembershipPasswordFormat PasswordFormat
        {
            get
            {
                return MembershipPasswordFormat.Hashed;
            }
        }

        public override int MinRequiredNonAlphanumericCharacters
        {
            get
            {
                return 1;
            }
        }

        public override int MinRequiredPasswordLength
        {
            get
            {
                return 8;
            }
        }

        public override string PasswordStrengthRegularExpression
        {
            get
            {
                return passwordStrengthRegularExpression;
            }
        }

        #endregion

        #region Initialization

        public override void Initialize(string name, NameValueCollection config)
        {
            //logger.Info("LocalUniMembershipProvider.Initialize(name: [" + name + "])");

            if (config == null)
            {
                throw new ArgumentNullException("config");
            }

            if (name == null || name.Length == 0)
            {
                name = "LocalUniMembershipProvider";
            }

            providerName = name;

            //Initialize the abstract base class.
            base.Initialize(name, config);

            applicationName = GetConfigValue(config["applicationName"], System.Web.Hosting.HostingEnvironment.ApplicationVirtualPath);
            //maxInvalidPasswordAttempts = Convert.ToInt32(GetConfigValue(config["maxInvalidPasswordAttempts"], "5"));
            //passwordAttemptWindow = Convert.ToInt32(GetConfigValue(config["passwordAttemptWindow"], "10"));
            //minRequiredNonAlphanumericCharacters = Convert.ToInt32(GetConfigValue(config["minRequiredAlphaNumericCharacters"], "1"));
            //minRequiredPasswordLength = Convert.ToInt32(GetConfigValue(config["minRequiredPasswordLength"], "7"));
            passwordStrengthRegularExpression = Convert.ToString(GetConfigValue(config["passwordStrengthRegularExpression"], String.Empty));
            passwordStrengthRegularExpressionInfo = Convert.ToString(GetConfigValue(config["passwordStrengthRegularExpressionInfo"], String.Empty));
            //enablePasswordReset = Convert.ToBoolean(GetConfigValue(config["enablePasswordReset"], "true"));
            //enablePasswordRetrieval = Convert.ToBoolean(GetConfigValue(config["enablePasswordRetrieval"], "true"));
            //requiresQuestionAndAnswer = Convert.ToBoolean(GetConfigValue(config["requiresQuestionAndAnswer"], "false"));
            //requiresUniqueEmail = Convert.ToBoolean(GetConfigValue(config["requiresUniqueEmail"], "true"));

            if (String.IsNullOrWhiteSpace(passwordStrengthRegularExpressionInfo)) passwordStrengthRegularExpressionInfo = "Za niski poziom skomplikowania hasła.";

            ConnectionStringSettings ConnectionStringSettings = ConfigurationManager.ConnectionStrings[config["connectionStringName"]];

            if ((ConnectionStringSettings == null) || (ConnectionStringSettings.ConnectionString.Trim() == String.Empty))
            {
                throw new ProviderException("Connection string cannot be blank.");
            }

            //connectionString = CreateEFConnectionString(ConnectionStringSettings.ConnectionString);

            logger.Trace("LocalUniMembershipProvider.Initialized:" +
                "\nproviderName: " + providerName +
                "\napplicationName: " + applicationName +
                "\npasswordStrengthRegularExpression: " + passwordStrengthRegularExpression +
                "\npasswordStrengthRegularExpressionInfo: " + passwordStrengthRegularExpressionInfo +
                "\nconnectionString: " + connectionString);

        }

        private string GetConfigValue(string configValue, string defaultValue)
        {
            if (String.IsNullOrEmpty(configValue))
            {
                return defaultValue;
            }

            return configValue;
        }

        #endregion

        #region Implemented Abstract Methods from MembershipProvider

        ///================================================
        /// <summary>
        /// Change the user password.
        /// </summary>
        /// <param name="username">UserName</param>
        /// <param name="oldPass">Old password.</param>
        /// <param name="newPass">New password.</param>
        /// <returns>T/F if password was changed.</returns>
        public override bool ChangePassword(string username, string oldPass, string newPass)
        {
            try
            {
                if (!ValidateUser(username, oldPass))
                {
                    return false;
                }
            }
            catch
            {
                return false;
            }

            if (!string.IsNullOrWhiteSpace(passwordStrengthRegularExpression))
            {
                if (!CheckPasswordRegexMatch(passwordStrengthRegularExpression, newPass))
                {
                    throw new ProviderException(passwordStrengthRegularExpressionInfo);
                }
            }

            ValidatePasswordEventArgs args = new ValidatePasswordEventArgs(username, newPass, true);

            OnValidatingPassword(args);

            if (args.Cancel)
            {
                if (args.FailureInformation != null)
                {
                    throw args.FailureInformation;
                }
                else
                {
                    throw new ProviderException("Change password canceled due to new password validation failure.");
                }
            }

            using (Entities ue = new Entities())
            {
                // Sprawdzanie czy hasło było juz uzyte
                var oldPasswordsHist = ue.Users_password_hist.Where(m => m.UserName == username);

                foreach (Users_password_hist oldPassHist in oldPasswordsHist)
                {
                    string tmpPass = CreatePasswordHash(newPass, oldPassHist.salt);
                    if (tmpPass == oldPassHist.PasswordHash) throw new ProviderException("Hasło było już użyte. Proszę wprowadzić nowe hasło.");
                }

                // Zmiana hasła + zapis do historii haseł
                Users_password_hist passHist = new Users_password_hist();
                var user = ue.Users.Where(m => m.UserName == username).SingleOrDefault();

                passHist.PasswordHash = user.PasswordHash;
                passHist.salt = user.salt;
                passHist.UserName = user.UserName;
                passHist.wpis_czas = DateTime.Now;

                ue.Users_password_hist.Add(passHist);
                //ue.Users_password_hist(passHist);
                //ue.AddToUsersPasswordHist(passHist);

                string newSalt = CreateSalt(5);
                string newHash = CreatePasswordHash(newPass, newSalt);
                user.PasswordHash = newHash;
                user.salt = newSalt;

                // Zapisanie zmian w jednej tranzakcji
                ue.SaveChanges();

                //string czy_api = ConfigurationManager.AppSettings["webapi_userchange_enabled"].FirstOrDefault().ToString();
                ////string czy_api = global::UniMembership.Properties.Settings.Default.webapi_userchange_enabled;
                //if (czy_api == "t")
                //{
                //    webapi wa = new webapi();
                //    wa.saveSendAction(2, username);
                //}

            }

            logger.Info("Zmieniono hasło (username: [" + username + "]"); 
            //oldPass: [" + oldPass + "], newPass: [" + newPass + "])");

            return true;
        }

        ///==========================================================
        /// <summary>
        /// Change the question and answer for a password validation.
        /// </summary>
        /// <param name="username">User name.</param>
        /// <param name="password">Password.</param>
        /// <param name="newPwdQuestion">New question text.</param>
        /// <param name="newPwdAnswer">New answer text.</param>
        /// <returns></returns>
        /// <remarks></remarks>
        public override bool ChangePasswordQuestionAndAnswer(string username, string password, string newPwdQuestion, string newPwdAnswer)
        {
            throw new NotSupportedException();
        }

        ///============================================================================
        /// <summary>
        /// Create a new user.
        /// </summary>
        /// <param name="username">User name.</param>
        /// <param name="password">Password.</param>
        /// <param name="email">Email address.</param>
        /// <param name="passwordQuestion">Security quesiton for password.</param>
        /// <param name="passwordAnswer">Security quesiton answer for password.</param>
        /// <param name="isApproved"></param>
        /// <param name="providerUserKey">Autogenerated - should be null.</param>
        /// <param name="status"></param>
        /// <returns>MembershipUser</returns>
        public override MembershipUser CreateUser(string username, string password, string email, string passwordQuestion, string passwordAnswer, bool isApproved, object providerUserKey, out MembershipCreateStatus status)
        {
            throw new NotSupportedException("Use LocalUniMembershipProvider.CreateUser(short) instead.");
        }

        public MembershipUser CreateUniUser(string username, string password, Users user, out MembershipCreateStatus status)
        {

            ValidatePasswordEventArgs args = new ValidatePasswordEventArgs(username, password, true);

            OnValidatingPassword(args);

            if (args.Cancel)
            {
                status = MembershipCreateStatus.InvalidPassword;
                return null;
            }

            if ((RequiresUniqueEmail && (GetUserNameByEmail(user.Email) != String.Empty)))
            {
                status = MembershipCreateStatus.DuplicateEmail;
                return null;
            }

            MembershipUser membershipUser = GetUser(username, false);

            if (membershipUser == null)
            {
                MembershipUser issgUser;

                using (Entities ue = new Entities())
                {
                    Grupa grupa = ue.Grupa.Where(m => m.Grupa_default == true).Single();

                    user.UserName = username;
                    user.Grupa_id = grupa.Grupa_id;
                    user.salt = CreateSalt(5);
                    user.PasswordHash = CreatePasswordHash(password, user.salt);
                    user.czy_zablokowane = "n";
                    user.add_time = DateTime.Now;

                    ue.Users.Add(user);

                    ue.SaveChanges();

                    issgUser = new UniMembershipUser(providerName,
                                                      user.UserName,
                                                      (user.ispo_user_id as object),
                                                      user.Email,
                                                      "",
                                                      "",
                                                      true,
                                                      (user.czy_zablokowane != "t"),
                                                      user.add_time.GetValueOrDefault(DateTime.MinValue),
                                                      DateTime.MinValue,
                                                      DateTime.MinValue,
                                                      DateTime.MinValue,
                                                      user.czy_zablokowane_kiedy.GetValueOrDefault(DateTime.MinValue)
                                                      , user
                                                      );
                }

                status = MembershipCreateStatus.Success;

                logger.Info("Dodano użytkownika [" + username + "].");

                return issgUser;
            }
            else
            {
                status = MembershipCreateStatus.DuplicateUserName;
            }

            return null;
        }

        ///=========================================================
        /// <summary>
        /// Creates new Issg User.
        /// </summary>
        /// <param name="username">Username</param>
        /// <param name="password">Password</param>
        /// <param name="user">Urzytkownik entity to insert.</param>
        /// <param name="status"></param>
        /// <returns>UniMembershipUser</returns>
        //public UniMembershipUser CreateUniUser(string username, string password, Users user, out MembershipCreateStatus status)
        //{
        //    ValidatePasswordEventArgs args = new ValidatePasswordEventArgs(username, password, true);

        //    OnValidatingPassword(args);

        //    if (args.Cancel)
        //    {
        //        status = MembershipCreateStatus.InvalidPassword;
        //        return null;
        //    }

        //    if ((RequiresUniqueEmail && (GetUserNameByEmail(user.Email) != String.Empty)))
        //    {
        //        status = MembershipCreateStatus.DuplicateEmail;
        //        return null;
        //    }

        //    MembershipUser membershipUser = GetUser(username, false);

        //    if (membershipUser == null)
        //    {
        //        Users issgUser;

        //        using (Entities ue = new Entities())
        //        {
        //            Grupa grupa = ue.Grupa.Where(m => m.Grupa_default == true).Single();

        //            user.UserName = username;
        //            user.Grupa_id = grupa.Grupa_id;
        //            user.salt = CreateSalt(5);
        //            user.PasswordHash = CreatePasswordHash(password, user.salt);
        //            user.czy_zablokowane = "n";
        //            user.add_time = DateTime.Now;

        //            //ue.AddToUsers(user);
        //            ue.Users.Add(user);
        //            //user.Grupa.Add(grupa);

        //            ue.SaveChanges();

        //            issgUser = new UniMembershipUser(providerName,
        //                                              user.UserName,
        //                                              (user.ispo_user_id as object),
        //                                              user.Email,
        //                                              "",
        //                                              "",
        //                                              true,
        //                                              (user.czy_zablokowane != "t"),
        //                                              user.add_time.GetValueOrDefault(DateTime.MinValue),
        //                                              DateTime.MinValue,
        //                                              DateTime.MinValue,
        //                                              DateTime.MinValue,
        //                                              user.czy_zablokowane_kiedy.GetValueOrDefault(DateTime.MinValue)
        //                                              , user
        //                                              );
        //        }

        //        status = MembershipCreateStatus.Success;

        //        logger.Info("Dodano użytkownika [" + username + "].");

        //        return issgUser;
        //    }
        //    else
        //    {
        //        status = MembershipCreateStatus.DuplicateUserName;
        //    }

        //    return null;
        //}

        ///===============================================================================
        /// <summary>
        /// Delete a user.
        /// </summary>
        /// <param name="username">User name.</param>
        /// <param name="deleteAllRelatedData">Whether to delete all related data.</param>
        /// <returns>T/F if the user was deleted.</returns>
        public override bool DeleteUser(string username, bool deleteAllRelatedData)
        {
            //throw new NotSupportedException("Urzytkownik nie może zostać usunięty!");

            try
            {
                using (Entities ue = new Entities())
                {
                    var user = ue.Users.Where(m => m.UserName == username).Single();

                    //Grupa[] role = user.Grupa.ToArray();

                    //foreach (Grupa g in role)
                    //{
                    //    user.Grupa.Remove(g);
                    //}

                    ue.Users.Remove(user);
                    ue.SaveChanges();

                    logger.Info("Usunięto użytkownika [" + username + "].");
                    return true;
                }
            }
            catch (Exception)
            {
                logger.Info("Próba usunięcia użytkownika [" + username + "] zakończyła się niepowodzeniem.");
                return false;
            }
        }

        ///===========================
        /// <summary>
        /// Get a collection of users.
        /// </summary>
        /// <param name="pageIndex">Page index.</param>
        /// <param name="pageSize">Page size.</param>
        /// <param name="totalRecords">Total # of records to retrieve.</param>
        /// <returns>Collection of MembershipUser objects.</returns>
        public override MembershipUserCollection GetAllUsers(int pageIndex, int pageSize, out int totalRecords)
        {
            MembershipUserCollection UserCollection = new MembershipUserCollection();

            using (Entities ue = new Entities())
            {
                var selectedUsers = ue.Users;
                totalRecords = selectedUsers.Count();

                var filteredUsers = selectedUsers.OrderBy(m => m.UserName)
                                                 .Skip(pageIndex * pageSize)
                                                 .Take(pageSize)
                                                 .Select(m => new
                                                 {
                                                     m.UserName,
                                                     m.ispo_user_id,
                                                     m.Email,
                                                     czy_zablokowane = (m.czy_zablokowane != "t"),
                                                     add_time = m.add_time.HasValue ? m.add_time.Value : DateTime.MinValue,
                                                     czy_zablokowane_kiedy = m.czy_zablokowane_kiedy.HasValue ? m.czy_zablokowane_kiedy.Value : DateTime.MinValue,
                                                     users  = m
                                                 }).ToArray();

                foreach (var user in filteredUsers) UserCollection.Add(new UniMembershipUser(providerName,
                                                                                                user.UserName,
                                                                                                (user.ispo_user_id as object),
                                                                                                user.Email,
                                                                                                "",
                                                                                                "",
                                                                                                true,
                                                                                                user.czy_zablokowane,
                                                                                                user.add_time,
                                                                                                DateTime.MinValue,
                                                                                                DateTime.MinValue,
                                                                                                DateTime.MinValue,
                                                                                                user.czy_zablokowane_kiedy,
                                                                                                user.users));
            }

            return UserCollection;
        }

        ///============================================
        /// <summary>
        /// Gets the number of users currently on-line.
        /// </summary>
        /// <returns>  /// # of users on-line.</returns>
        public override int GetNumberOfUsersOnline()
        {
            return 0;
        }

        ///==========================================================
        /// <summary>
        /// Get password.
        /// </summary>
        /// <param name="username">User name.</param>
        /// <param name="answer">Answer to security question.</param>
        /// <returns>Password for the user.</returns>
        public override string GetPassword(string username, string answer)
        {
            throw new NotSupportedException("Hasła nie można odzyskać.");
        }

        ///========================================================
        /// <summary>
        /// Get a user based upon username and if they are on-line.
        /// </summary>
        /// <param name="username">Username.</param>
        /// <param name="userIsOnline">T/F whether the user is on-line.</param>
        /// <returns></returns>
        public override MembershipUser GetUser(string username, bool userIsOnline)
        {
            UniMembershipUser IssgUser = null;

            using (Entities ue = new Entities())
            {
                var user = ue.Users.Where(m => m.UserName == username)
                           .Select(m => new
                           {
                               UserName = m.UserName,
                               Id = m.ispo_user_id,
                               Email = m.Email,
                               czy_zablokowane = (m.czy_zablokowane != "t"),
                               add_time = m.add_time.HasValue ? m.add_time.Value : DateTime.MinValue,
                               czy_zablokowane_kiedy = m.czy_zablokowane_kiedy.HasValue ? m.czy_zablokowane_kiedy.Value : DateTime.MinValue,
                               users  = m
                           }).SingleOrDefault();

                if (user != null)
                {
                    IssgUser = new UniMembershipUser(providerName,
                                                      user.UserName,
                                                      (user.Id as object),
                                                      user.Email,
                                                      "",
                                                      "",
                                                      true,
                                                      user.czy_zablokowane,
                                                      user.add_time,
                                                      DateTime.MinValue,
                                                      DateTime.MinValue,
                                                      DateTime.MinValue,
                                                      user.czy_zablokowane_kiedy,
                                                      user.users);
                }
            }

            return IssgUser;
        }

        ///============================================================
        /// <summary>
        /// Get a user based upon provider key and if they are on-line.
        /// </summary>
        /// <param name="userID">Provider key.</param>
        /// <param name="userIsOnline">T/F whether the user is on-line.</param>
        /// <returns></returns>
        public override MembershipUser GetUser(object userID, bool userIsOnline)
        {
            UniMembershipUser IssgUser = null;

            if (!(userID is int)) return null;
            int id = (int)userID;

            using (Entities ue = new Entities())
            {
                var user = ue.Users.Where(m => m.ispo_user_id == id)
                           .Select(m => new
                           {
                               UserName = m.UserName,
                               Id = m.ispo_user_id,
                               Email = m.Email,
                               czy_zablokowane = (m.czy_zablokowane != "t"),
                               add_time = m.add_time.HasValue ? m.add_time.Value : DateTime.MinValue,
                               czy_zablokowane_kiedy = m.czy_zablokowane_kiedy.HasValue ? m.czy_zablokowane_kiedy.Value : DateTime.MinValue,
                               users  = m
                           }).SingleOrDefault();

                if (user != null)
                {
                    IssgUser = new UniMembershipUser(providerName,
                                                      user.UserName,
                                                      (user.Id as object),
                                                      user.Email,
                                                      "",
                                                      "",
                                                      true,
                                                      user.czy_zablokowane,
                                                      user.add_time,
                                                      DateTime.MinValue,
                                                      DateTime.MinValue,
                                                      DateTime.MinValue,
                                                      user.czy_zablokowane_kiedy,
                                                      user.users);
                }
            }

            return IssgUser;
        }

        ///==========================================
        /// <summary>
        /// Unlock a user.
        /// </summary>
        /// <param name="username">User name.</param>
        /// <returns>T/F if unlocked.</returns>
        public override bool UnlockUser(string username)
        {
            using (Entities ue = new Entities())
            {
                var user = ue.Users.Where(m => m.UserName == username).SingleOrDefault();
                user.czy_zablokowane = "n";
                ue.SaveChanges();
            }

            return true;
        }

        ///===========================================
        /// <summary>
        /// Returns user name by email.
        /// </summary>
        /// <param name="email">Email address.</param>
        /// <returns>User name.</returns>
        public override string GetUserNameByEmail(string email)
        {
            string username = String.Empty;

            using (Entities ue = new Entities())
            {
                username = ue.Users.Where(m => m.Email == email).Select(m => m.UserName).DefaultIfEmpty("").Single();
            }

            return username;
        }

        ///==========================================================
        /// <summary>
        /// Reset the user password.
        /// </summary>
        /// <param name="username">User name.</param>
        /// <param name="answer">Answer to security question.</param>
        /// <returns></returns>
        /// <remarks></remarks>
        public override string ResetPassword(string username, string answer)
        {
            string newPass = GetRandomPasswordUsingGUID();
            string newSalt = CreateSalt(5);
            string newHash = CreatePasswordHash(newPass, newSalt);

            ValidatePasswordEventArgs args = new ValidatePasswordEventArgs(username, newPass, true);

            OnValidatingPassword(args);

            if (args.Cancel)
            {
                if (args.FailureInformation != null)
                {
                    throw args.FailureInformation;
                }
                else
                {
                    throw new MembershipPasswordException("Reset password canceled due to password validation failure.");
                }
            }

            using (Entities ue = new Entities())
            {
                var user = ue.Users.Where(m => m.UserName == username).SingleOrDefault();

                if (user == null)
                {
                    throw new MembershipPasswordException("The supplied user name is not found.");
                }

                if (user.czy_zablokowane == "t")
                {
                    throw new MembershipPasswordException("The supplied user is locked out.");
                }

                user.PasswordHash = newHash;
                user.salt = newSalt;
                ue.SaveChanges();
            }

            return newPass;
        }

        ///=============================================================================
        /// <summary>
        /// Update the user information.
        /// </summary>
        /// <param name="_membershipUser">MembershipUser object containing data.</param>
        public override void UpdateUser(MembershipUser membershipUser)
        {
            throw new NotSupportedException("Use LocalUniMembershipProvider.UpdateUniUser() instead.");
        }

        public void UpdateUniUser(UniMembershipUser issgUser)
        {
            using (Entities db = new Entities())
            {
                ObjectContext objectContext = ((IObjectContextAdapter)db).ObjectContext;
                Users us = db.Users
                    .Where(m => m.ispo_user_id == issgUser.uzytkownik.ispo_user_id)
                    .Single();

                //ue.Users.Attach(issgUser.uzytkownik);
                //ue.Users.ApplyCurrentValues(issgUser.uzytkownik); TODO
                ((IObjectContextAdapter)objectContext).ObjectContext.ApplyCurrentValues("Users", issgUser);
                db.SaveChanges();

                //string czy_api = ConfigurationManager.AppSettings["webapi_userchange_enabled"].FirstOrDefault().ToString();
                ////string czy_api = global::UniMembership.Properties.Settings.Default.webapi_userchange_enabled;
                //if (czy_api == "t")
                //{
                //    webapi wa = new webapi();
                //    wa.saveSendAction(2, issgUser.UserName);
                //}

                logger.Info("Wprowadzono zmiany w profilu użytkownika [" + issgUser.uzytkownik.UserName + "].");
            }
        }

        ///================================================================
        /// <summary>
        /// Validate the user based upon username and password.
        /// </summary>
        /// <param name="username">User name.</param>
        /// <param name="password">Password.</param>
        /// <returns>T/F if the user is valid.</returns>
        public override bool ValidateUser(string username, string password)
        {
            bool isValid = false;

            using (Entities ue = new Entities())
            {
                var users = ue.Users.Where(m => m.UserName == username).ToList();

                if (users.Count != 1) return false;

                Users  user = users[0];

                if (user.czy_zablokowane == "t") throw new LoginFailedException("Użytkownik [" + user.UserName + "] jest zablokowany.", LoginFailedCause.UserLocked);

                string EnteredPasswordHash = CreatePasswordHash(password, user.salt);
                isValid = EnteredPasswordHash.Equals(user.PasswordHash);

                string status = isValid ? "t" : "n";
                ue.addUserLoginHistInfo(username, "", "", status, "i", "");
            }

            return isValid;
        }

        ///===========================================================================
        /// <summary>
        /// Find all users matching a search string.
        /// </summary>
        /// <param name="usernameToMatch">Search string of user name to match.</param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalRecords">Total records found.</param>
        /// <returns>Collection of MembershipUser objects.</returns>
        public override MembershipUserCollection FindUsersByName(string usernameToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            MembershipUserCollection UserCollection = new MembershipUserCollection();

            using (Entities ue = new Entities())
            {
                var selectedUsers = ue.Users.Where(m => m.UserName.Contains(usernameToMatch) 
                                                     || m.Imie.Contains(usernameToMatch)
                                                     || m.Nazwisko.Contains(usernameToMatch)
                                                     || m.Email.Contains(usernameToMatch));
                totalRecords = selectedUsers.Count();

                var filteredUsers = selectedUsers.OrderBy(m => m.UserName)
                                                 .Skip(pageIndex * pageSize)
                                                 .Take(pageSize)
                                                 .Select(m => new
                                                 {
                                                     m.UserName,
                                                     m.ispo_user_id,
                                                     m.Email,
                                                     czy_zablokowane = (m.czy_zablokowane != "t"),
                                                     add_time = m.add_time.HasValue ? m.add_time.Value : DateTime.MinValue,
                                                     czy_zablokowane_kiedy = m.czy_zablokowane_kiedy.HasValue ? m.czy_zablokowane_kiedy.Value : DateTime.MinValue,
                                                     users  = m
                                                 }).ToArray();

                foreach (var user in filteredUsers) UserCollection.Add(new UniMembershipUser(providerName,
                                                                                                user.UserName,
                                                                                                (user.ispo_user_id as object),
                                                                                                user.Email,
                                                                                                "",
                                                                                                "",
                                                                                                true,
                                                                                                user.czy_zablokowane,
                                                                                                user.add_time,
                                                                                                DateTime.MinValue,
                                                                                                DateTime.MinValue,
                                                                                                DateTime.MinValue,
                                                                                                user.czy_zablokowane_kiedy,
                                                                                                user.users));
            }

            return UserCollection;
        }

        ///====================================================================
        /// <summary>
        /// Find all users matching a search string of their email.
        /// </summary>
        /// <param name="emailToMatch">Search string of email to match.</param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalRecords">Total records found.</param>
        /// <returns>Collection of MembershipUser objects.</returns>
        public override MembershipUserCollection FindUsersByEmail(string emailToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            MembershipUserCollection UserCollection = new MembershipUserCollection();

            using (Entities ue = new Entities())
            {
                var selectedUsers = ue.Users.Where(m => m.Email.Contains(emailToMatch));
                totalRecords = selectedUsers.Count();

                var filteredUsers = selectedUsers.OrderBy(m => m.UserName)
                                                 .Skip(pageIndex * pageSize)
                                                 .Take(pageSize)
                                                 .Select(m => new
                                                 {
                                                     m.UserName,
                                                     m.ispo_user_id,
                                                     m.Email,
                                                     czy_zablokowane = (m.czy_zablokowane != "t"),
                                                     add_time = m.add_time.HasValue ? m.add_time.Value : DateTime.MinValue,
                                                     czy_zablokowane_kiedy = m.czy_zablokowane_kiedy.HasValue ? m.czy_zablokowane_kiedy.Value : DateTime.MinValue,
                                                     users  = m
                                                 }).ToArray();

                foreach (var user in filteredUsers) UserCollection.Add(new UniMembershipUser(providerName,
                                                                                                user.UserName,
                                                                                                (user.ispo_user_id as object),
                                                                                                user.Email,
                                                                                                "",
                                                                                                "",
                                                                                                true,
                                                                                                user.czy_zablokowane,
                                                                                                user.add_time,
                                                                                                DateTime.MinValue,
                                                                                                DateTime.MinValue,
                                                                                                DateTime.MinValue,
                                                                                                user.czy_zablokowane_kiedy,
                                                                                                user.users));
            }

            return UserCollection;
        }

        #endregion

        #region "Utility Functions"

        private string GetRandomPasswordUsingGUID()
        {
            string guidResult = System.Guid.NewGuid().ToString();
            guidResult = guidResult.Replace("-", string.Empty);
            return guidResult.Substring(0, 8);
        }

        private string CreateSalt(int size)
        {
            // Generate a cryptographic random number using the cryptographic
            // service provider
            RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
            byte[] buff = new byte[size];
            rng.GetBytes(buff);
            // Return a Base64 string representation of the random number
            return Convert.ToBase64String(buff);
        }

        private string CreatePasswordHash(string pwd, string salt)
        {
            string saltAndPwd = String.Concat(pwd, salt);
            string hashedPwd = FormsAuthentication.HashPasswordForStoringInConfigFile(saltAndPwd, "SHA1");
            return hashedPwd;
        }

        private bool CheckPasswordRegexMatch(string regex, string pwd)
        {
            return System.Text.RegularExpressions.Regex.IsMatch(pwd, regex);
        }

        //private string CreateEFConnectionString(string dbConnectionString)
        //{
        //    //metadata=res://*/User.csdl|res://*/User.ssdl|res://*/User.msl;provider=System.Data.SqlClient;provider connection string="data source=Earl;initial catalog=ISPO2GO_gazeta;persist security info=True;user id=sa;password=m4d4g4sk4r;multipleactiveresultsets=True;App=EntityFramework"

        //    // Specify the provider name.
        //    string providerName = "System.Data.SqlClient";

        //    // Initialize the EntityConnectionStringBuilder.
        //    EntityConnectionStringBuilder entityBuilder = new EntityConnectionStringBuilder();

        //    //Set the provider name.
        //    entityBuilder.Provider = providerName;

        //    // Set the provider-specific connection string.
        //    entityBuilder.ProviderConnectionString = dbConnectionString;

        //    // Set the Metadata location.
        //    entityBuilder.Metadata = @"res://*/User.csdl|res://*/User.ssdl|res://*/User.msl";

        //    return entityBuilder.ToString();
        //}
        #endregion

    }
}
