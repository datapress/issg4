﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ISSG4.Models
{
    public partial class Cennik
    {
        public static List<viewCennikJoined> getCachedList()
        {
            string key = "Cennik.getCachedList()";

            var val = HttpContext.Current.Cache.Get(key);
            if (val != null)
            {
                return (List<viewCennikJoined>)val;
            }
            else
            {
                List<viewCennikJoined> cennik;
                using (Entities ie = new Entities() ) {
                    cennik = ie.viewCennikJoined.ToList();
                    HttpContext.Current.Cache.Insert(key, cennik, null, DateTime.Now.AddMinutes(10), System.Web.Caching.Cache.NoSlidingExpiration);
                }
                return cennik;
            }
        }
    }
}