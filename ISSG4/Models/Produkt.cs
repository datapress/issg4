//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ISSG4.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Produkt
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Produkt()
        {
            this.i3_Cennik = new HashSet<Cennik>();
        }
    
        public int ProduktId { get; set; }
        public int RodzajOkresuId { get; set; }
        public int ProduktIlosc { get; set; }
        public string ProduktNazwa { get; set; }
        public int ProduktRodzajId { get; set; }
        public Nullable<int> ProduktStandardId { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Cennik> i3_Cennik { get; set; }
        public virtual RodzajOkresu i3_RodzajOkresu { get; set; }
        public virtual ProduktRodzaj i3_ProduktRodzaj { get; set; }
        public virtual ProduktStandard i3_ProduktStandard { get; set; }
    }
}
