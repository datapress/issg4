﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using DotNetOpenMail;
using DotNetOpenMail.SmtpAuth;
using System.Security.Cryptography;
using System.Transactions;
using System.Configuration;
using NLog;
using System.Web.Security;

namespace ISSG4.Models
{
    public class ResetPasswordModel
    {
        [Required(ErrorMessage = "Pole wymagane.")]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Adres e-mail")]
        public string Email { get; set; }

        public bool resetPassword(string username, string email)
        {
            bool ret = false;
            Logger logger = LogManager.GetCurrentClassLogger();
            //Entities db = new Entities();

            string newpass = GetRandomPasswordUsingGUID();
            string salt = CreateSalt(5);
            string hash = CreatePasswordHash(newpass, salt);

            using (TransactionScope transScope = new TransactionScope())
            {
                using (Entities db = new Entities())
                {
                    logger.Debug("Ustawiam nowe hasło: " + username + " na email: " + email);
                    db.SetNewPassword(username, hash, salt);
                    logger.Debug("Ustawiłem nowe hasło: " + username + " na email: " + email);
                    SendNewPassByMail(username, newpass, email);
                    logger.Debug("Wysłałem nowe hasło: " + username + " na email: " + email);
                    int ispouserid = db.Users.Where(u => u.UserName == username).First().ispo_user_id;
                   // logger.Debug("newpass: "+newpass+" salt: "+salt+" hash: "+hash);

                    db.addUserEventHist(ispouserid, DateTime.Now, username, "Reset hasła", "Wysłano na adres: " + email);
                }

                transScope.Complete();

                ret = true;
                logger.Info("Zakończono reset hasła: " + username + " na email: " + email);
            }

            return ret;
        }


        public void SendNewPassByMail(string username, string as_newpass, string as_email)
        {
            Logger logger = LogManager.GetCurrentClassLogger();
            SmtpServer smtpServer;
            AppSettingsReader apr = new AppSettingsReader();

            string ls_mailserver = apr.GetValue("MailServer", typeof(string)).ToString();
            string ls_mailaddress = apr.GetValue("MailSenderAddress", typeof(string)).ToString();
            string ls_mailfrom = apr.GetValue("MailSenderFrom", typeof(string)).ToString();
            string ls_urllink = apr.GetValue("MailLink", typeof(string)).ToString();
            string ls_serverport = apr.GetValue("MailServerPort", typeof(string)).ToString();

            smtpServer = new SmtpServer(ls_mailserver, int.Parse(ls_serverport));

            if (ConfigurationManager.AppSettings["MailAuth"].ToString() == "yes")
            {
                string ls_mailuser = apr.GetValue("MailUser", typeof(string)).ToString();
                string ls_mailpwd = apr.GetValue("MailPass", typeof(string)).ToString();
                smtpServer.SmtpAuthToken = new SmtpAuthToken(ls_mailuser, ls_mailpwd);
            }

            try
            {
                EmailMessage emailMessage = new DotNetOpenMail.EmailMessage();
                emailMessage.HeaderCharSet = System.Text.Encoding.GetEncoding("UTF-8");
                emailMessage.HeaderEncoding = DotNetOpenMail.Encoding.EncodingType.QuotedPrintable;

                emailMessage.FromAddress = new DotNetOpenMail.EmailAddress(ls_mailaddress, ls_mailfrom,
                    DotNetOpenMail.Encoding.EncodingType.QuotedPrintable, System.Text.Encoding.GetEncoding("UTF-8"));
                emailMessage.AddToAddress(new EmailAddress(as_email));
                emailMessage.Subject = "Reset hasla.";

                string ls_send_time = DateTime.Now.ToString("dd-MM-yyyy HH:mm");

                String scriptString = "\n Dzien dobry! \n";
                scriptString += "\n";
                scriptString += "Osoba poslugujaca sie emailem " + as_email + "\n";
                scriptString += "w serwisie gazety uruchomila o godzinie " + ls_send_time + "\n";
                scriptString += "funkcje generowania nowego hasla.\n";
                scriptString += "\n";
                scriptString += "\n" + "Nowe haslo uzytkownika " + username + " to: " + as_newpass;
                scriptString += "\n\nProsimy, po zalogowaniu sie ustawic wlasne nowe haslo.";

                emailMessage.BodyText = scriptString;

                emailMessage.Send(smtpServer);

                logger.Info("Wysłano email z resetem hasła dla użytkownika "+username + " na adres: "+as_email);

            }
            catch (DotNetOpenMail.SmtpException ex)
            {
                logger.Error("SmtpException: " + ex.Message);

            }
            catch (DotNetOpenMail.MailException ex)
            {
                logger.Error("MailException : " + ex.Message);

            }
            catch (Exception ex)
            {
                logger.Error("Exception : " + ex.Message);

            }

        }

        public string GetRandomPasswordUsingGUID()
        {
            string guidResult = System.Guid.NewGuid().ToString();
            guidResult = guidResult.Replace("-", string.Empty);
            return guidResult.Substring(0, 8);
        }

        public static string CreateSalt(int size)
        {
            RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
            byte[] buff = new byte[size];
            rng.GetBytes(buff);
            return Convert.ToBase64String(buff);
        }

        static public string CreatePasswordHash(string pwd, string salt)
        {
            string saltAndPwd = String.Concat(pwd, salt);
            string hashedPwd =
                FormsAuthentication.HashPasswordForStoringInConfigFile(
                saltAndPwd, "SHA1");
            return hashedPwd;
        }
    }

}