﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Security;
using ISSG4.Models;

namespace ISSG4.Models
{
    public class UniMembershipUser : MembershipUser
    {

        public Users uzytkownik { get; private set; }

        public UniMembershipUser(string providerName,
                                  string name,
                                  object providerUserKey,
                                  string email,
                                  string passwordQuestion,
                                  string comment,
                                  bool isApproved,
                                  bool isLockedOut,
                                  DateTime creationDate,
                                  DateTime lastLoginDate,
                                  DateTime lastActivityDate,
                                  DateTime lastPasswordChangedDate,
                                  DateTime lastLockoutDate,
                                  Users uzytkownik)
                                  : base(providerName, name, providerUserKey, email, passwordQuestion, comment, isApproved, isLockedOut, creationDate, lastLoginDate, lastActivityDate, lastPasswordChangedDate, lastLockoutDate)
        {
            this.uzytkownik = uzytkownik;
        }

    }
}
