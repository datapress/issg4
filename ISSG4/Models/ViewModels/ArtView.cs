﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ISSG4.Models;

namespace ISSG4.Models.ViewModels
{

    public class KoszykWykupModel
    {
        public ISSG4.Controllers.PrenumerataController.NowaViewModel nowaModel { get; set; }
        public WydanieJoinedResult wydanieModel {get; set;}
    }

    public class ArtViewList
    {
        public Wydanie wydanie { get; set; }

        public List<i3_ArtArtykul> art_lista { get; set; }
        public List<i3_ArtDzial> dzial_lista { get; set; }
        public i3_ArtArtykul art_czolo { get; set; }
        public i3_ArtFoto art_foto_czolo { get; set; }
        public List<i3_ArtFoto> art_foto_list { get; set; }
        public int dzialId { get; set; }

        public string termin_string { get; set; }

        public List<ArtView> artView_list { get; set; }

    }

    public class ArtView
    {
        public i3_ArtArtykul art { get; set; }
        public List<i3_ArtFoto> art_foto_list { get; set; }

        public Wydanie wydanie { get; set; }
        public List<i3_ArtDzial> dzial_lista { get; set; }

        public int prevId { get; set; }
        public int nextId { get; set; }
    }

    public class ArtFotoListView
    {
        public List<i3_ArtArtykul> art_list { get; set; }
        public List<i3_ArtFoto> art_foto_list { get; set; }

        public Wydanie wydanie { get; set; }
        public List<i3_ArtDzial> dzial_lista { get; set; }

        public int prevId { get; set; }
        public int nextId { get; set; }
    }

    public class ArtFotoView
    {
        public i3_ArtArtykul art { get; set; }
        public i3_ArtFoto art_foto { get; set; }

        public string crop_data { get; set; }

        public int prevId { get; set; }
        public int nextId { get; set; }
    }
}