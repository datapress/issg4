﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ISSG3.Models.ViewModels
{
    public class StatusPlatnosciViewModel
    {
        public string Status { get; set; }

        public StatusPlatnosciViewModel(string status)
        {
            Status = status;
        }
    }
}