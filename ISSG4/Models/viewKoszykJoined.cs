//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ISSG4.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class viewKoszykJoined
    {
        public int KoszykId { get; set; }
        public Nullable<int> ispo_user_id { get; set; }
        public string KoszykSesja { get; set; }
        public System.DateTime KoszykOd { get; set; }
        public System.DateTime KoszykData { get; set; }
        public int CennikId { get; set; }
        public int CennikNaglowekId { get; set; }
        public decimal CennikNetto { get; set; }
        public decimal CennikBrutto { get; set; }
        public decimal CennikVat { get; set; }
        public int ProduktId { get; set; }
        public int ProduktIlosc { get; set; }
        public string ProduktNazwa { get; set; }
        public int GazetaId { get; set; }
        public string GazetaNazwa { get; set; }
        public string GazetaSkrot { get; set; }
        public int GazetaTypId { get; set; }
        public int WydawcaId { get; set; }
        public bool GazetaBezplatna { get; set; }
        public Nullable<int> GazetaKodId { get; set; }
        public int RodzajOkresuId { get; set; }
        public string RodzajOkresuNazwa { get; set; }
        public int RodzajOkresuMiesiac { get; set; }
        public int RodzajOkresuDzien { get; set; }
        public string VatId { get; set; }
        public decimal VatProc { get; set; }
    }
}
