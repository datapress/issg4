﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Configuration;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;

namespace ISSG4.Models
{
    public partial class Koszyk
    {
        public static int iloscProduktow()
        {
            using (Entities ie = new Entities())
            {
                if (HttpContext.Current.User.Identity.IsAuthenticated)
                {
                    int userId = (int)Membership.GetUser().ProviderUserKey;
                    return ie.Koszyk.Where(m => m.ispo_user_id == userId).Count();
                }
                else
                {
                    return ie.Koszyk.Where(m => m.KoszykSesja == HttpContext.Current.Session.SessionID).Count();
                }
            }
        }

        public static void PrzypiszSesjeDoUzytkownika(string UserName)
        {
            using (Entities ie = new Entities())
            {
                IEnumerable<Koszyk> items = null;

                int userId = (int)Membership.GetUser(UserName).ProviderUserKey;
                items = ie.Koszyk.Where(m => m.KoszykSesja == HttpContext.Current.Session.SessionID);
                foreach (var item in items)
                {
                    item.ispo_user_id = userId;
                    item.KoszykSesja = null;
                }

                ie.SaveChanges();
            }
        }

        public static void DodajDoKoszyka(int CennikId, DateTime Od)
        {
            DodajDoKoszyka(CennikId, Od, 0);
        }

        public static void DodajDoKoszyka(int CennikId, DateTime Od, int DodatekId)
        {
            using (Entities ie = new Entities())
            {
                ObjectContext objectContext = ((IObjectContextAdapter)ie).ObjectContext;

                Koszyk k = new Koszyk();
                k.CennikId = CennikId;
                k.KoszykOd = Od;
                k.KoszykData = DateTime.Now;


                if (DodatekId > 0)
                {
                    Dodatek d = ie.Dodatek.SingleOrDefault(f=>f.DodatekId == DodatekId);
                    var ek = objectContext.ObjectStateManager.GetObjectStateEntry(d).EntityKey;

                    //    Dodatek dodatek = new Dodatek() { EntityKey = new System.Data.EntityKey("Entities.Dodatek", "DodatekId", DodatekId), DodatekId = DodatekId };

                    //ie.Attach(dodatek);
                    //ie.
                    k.i3_Dodatek.Add(d);
                }

                int userId = 0;
                if (HttpContext.Current.User.Identity.IsAuthenticated)
                {
                    userId = (int)Membership.GetUser().ProviderUserKey;
                    k.ispo_user_id = userId;
                }
                else
                {
                    k.KoszykSesja = HttpContext.Current.Session.SessionID;
                }

                ie.Koszyk.Add(k);
                ie.SaveChanges();
            }
        }

        public static string getFooter()
        {

            string ret = string.Empty;

            //using (Entities ie = new Entities())
            //{
            //    OPCJE op = ie.OPCJE.SingleOrDefault(c => c.opcje_nazwa == "layout_footer");
            //    if (op != null)
            //        ret = op.opcje_wartosc;
            //}

            AppSettingsReader apr = new AppSettingsReader();
            ret = apr.GetValue("layout_footer", typeof(string)).ToString();

            return ret;
        }
    }
}