//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ISSG4.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class KodBufor
    {
        public int KodBuforId { get; set; }
        public int KodBuforGrupaId { get; set; }
        public string Kod { get; set; }
    
        public virtual KodBuforGrupa i3_KodBuforGrupa { get; set; }
    }
}
