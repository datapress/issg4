﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;


namespace ISSG4.Models
{

    public class UserAdminCreatedModel
    {
        public string username { get; set; }
        public string passwd { get; set; }
        public string admin_email { get; set; }
        public List<ISSG4.Models.viewZamowienieJoined> prenumeraty { get; set; }
    }

    public class ChangePasswordModel
    {
        [Required(ErrorMessage = "Pole wymagane.")]
        [DataType(DataType.Password)]
        [Display(Name = "Dotychczasowe hasło")]
        public string OldPassword { get; set; }

        [Required(ErrorMessage = "Pole wymagane.")]
        [StringLength(100, ErrorMessage = "{0} musi zawierać conajmniej {2} znaków.", MinimumLength = 8)]
        //[PasswordStrength]
        [DataType(DataType.Password)]
        [Display(Name = "Nowe hasło")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Powtórz hasło")]
        [System.ComponentModel.DataAnnotations.Compare("NewPassword", ErrorMessage = "Hasła nie zagadzają się.")]
        public string ConfirmPassword { get; set; }
    }

    public class LogOnModel
    {
        [Required(ErrorMessage = "Pole wymagane.")]
        [Display(Name = "Użytkownik")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Pole wymagane.")]
        [DataType(DataType.Password)]
        [Display(Name = "Hasło")]
        public string Password { get; set; }

        [Display(Name = "Zapamiętaj mnie")]
        public bool RememberMe { get; set; }
    }

    public class RegisterModel
    {
        [Required(ErrorMessage = "Pole wymagane.")]
        [Display(Name = "Login")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Pole wymagane.")]
        [Display(Name = "E-mail")]
        [RegularExpression(@"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?",
        ErrorMessage = "Niepoprawny adress.")]
        //[MXRecordLookup(ErrorMessage = "Podany serwer nie istnieje.")]
        [DataType(DataType.EmailAddress)]
        public string AdresPocztyElektronicznej { get; set; }

        //[Required(ErrorMessage="Pole wymagane.")]
        //[RegularExpression(@"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?",
        //ErrorMessage = "Niepoprawny adress.")]
        //[MXRecordLookup(ErrorMessage = "Podany serwer nie istnieje.")]
        //[DataType(DataType.EmailAddress)]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Pole wymagane.")]
        //[PasswordStrength]
        [DataType(DataType.Password)]
        [Display(Name = "Hasło")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Powtórz hasło")]
        [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessage = "Hasła nie zagadzają się.")]
        public string ConfirmPassword { get; set; }


        [Required(ErrorMessage = "Pole wymagane.")]
        [Display(Name = "Imię")]
        public string Imie { get; set; }

        [Required(ErrorMessage = "Pole wymagane.")]
        [Display(Name = "Nazwisko")]
        public string Nazwisko { get; set; }


        /*[Required(ErrorMessage = "Pole wymagane.")]*/
        [Display(Name = "Telefon")]
        [RegularExpression(@"\+?[0-9 -]*", ErrorMessage = "Niepoprawny nr telefonu.")]
        public string Telefon { get; set; }
        
        // ********* //
        // Dodatkowe //

        [Display(Name = "Adres")]
        public string Adres { get; set; }

        [Display(Name = "Miejscowość")]
        public string Miejscowosc { get; set; }

        [Display(Name = "Kod pocztowy")]
        [RegularExpression("[0-9]{2}-[0-9]{3}", ErrorMessage = "Kod musi być w formacie 12-345")]
        public string Kod { get; set; }

        [Display(Name = "Konto firmowe")]
        public bool CzyFirma { get; set; }

        [Display(Name = "Ulica")]
        public string Ulica { get; set; }

        [Display(Name = "Numer budynku")]
        public string Budynek { get; set; }

        [Display(Name = "Numer mieszkania")]
        public string Mieszkanie { get; set; }

        [Display(Name = "Kraj")]
        public string Kraj { get; set; }

        //[Nip(ErrorMessage = "Podany NIP jest nieprawidłowy.")]
        [Display(Name = "NIP")]
        public string NIP { get; set; }

        [Display(Name = "Nazwa firmy")]
        public string NazwaFirmy { get; set; }

        [Display(Name = "Proszę o wystawienie faktury VAT")]
        public bool CzyFaktura { get; set; }

        [Display(Name = "Adres1")]
        public string AdresDokorenspodencji1 { get; set; }
        [Display(Name = "Adres2")]
        public string AdresDokorenspodencji2 { get; set; }
        [Display(Name = "Adres3")]
        public string AdresDokorenspodencji3 { get; set; }
        [Display(Name = "Adres4")]
        public string AdresDokorenspodencji4 { get; set; }

        [Display(Name = "Wyrażam zgodę na przetwarzanie moich danych osobowych")]
        public bool CzyZgodaNaPrzetwarzanie { get; set; }
        [Display(Name = "Wyrażam zgodę na przyjmowanie treści marketingowych")]
        public bool CzyZgodaNaReklamy { get; set; }

        // ********** //
        // Pomocnicze //

        public Dictionary<string, string> Kraje { get { return InMemoryCache.Get("RegisterModel.Kraje", 15, GetKraje); } }

        [Display(Name = "Akceptuję regulamin")]
        public bool CzyAkceptacjaRegulaminu { get; set; }

        public Dictionary<string, string> GetKraje()
        {
            Dictionary<string, string> k = null;

            using (Entities ie = new Entities())
            {
                k = ie.KRAJ.Where(m => m.kraj_kod != "PL").OrderBy(m => m.kraj_nazwa).ToDictionary(m => m.kraj_kod, m => m.kraj_nazwa);
            }

            return k;
        }
    }

    public class UpdateModel
    {
        [Display(Name = "Login")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Pole wymagane.")]
        [Display(Name = "Imię")]
        public string Imie { get; set; }

        [Required(ErrorMessage = "Pole wymagane.")]
        [Display(Name = "Nazwisko")]
        public string Nazwisko { get; set; }

        [Required(ErrorMessage = "Pole wymagane.")]
        [RegularExpression(@"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?",
                           ErrorMessage = "Niepoprawny adress.")]
        //[MXRecordLookup(ErrorMessage = "Podany serwer nie istnieje.")]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Pole wymagane.")]
        [Display(Name = "Telefon")]
        [RegularExpression(@"\+?[0-9 -]*", ErrorMessage = "Niepoprawny nr telefonu.")]
        public string Telefon { get; set; }

        // ********* //
        // Dodatkowe //

        [Display(Name = "Adres")]
        public string Adres { get; set; }

        [Display(Name = "Miejscowość")]
        public string Miejscowosc { get; set; }

        [Display(Name = "Kod pocztowy")]
        [RegularExpression("[0-9]{2}-[0-9]{3}", ErrorMessage = "Kod musi być w formacie 12-345")]
        public string Kod { get; set; }

        [Display(Name = "Konto firmowe")]
        public bool CzyFirma { get; set; }

        [Display(Name = "Ulica")]
        public string Ulica { get; set; }

        [Display(Name = "Numer budynku")]
        public string Budynek { get; set; }

        [Display(Name = "Numer mieszkania")]
        public string Mieszkanie { get; set; }

        [Display(Name = "Kraj")]
        public string Kraj { get; set; }

        //[Nip(ErrorMessage = "Podany NIP jest nieprawidłowy.")]
        [Display(Name = "NIP")]
        public string NIP { get; set; }

        [Display(Name = "Nazwa firmy")]
        public string NazwaFirmy { get; set; }

        [Display(Name = "Proszę o wystawienie faktury VAT")]
        public bool CzyFaktura { get; set; }

        [Display(Name = "Adres1")]
        public string AdresDokorenspodencji1 { get; set; }
        [Display(Name = "Adres2")]
        public string AdresDokorenspodencji2 { get; set; }
        [Display(Name = "Adres3")]
        public string AdresDokorenspodencji3 { get; set; }
        [Display(Name = "Adres4")]
        public string AdresDokorenspodencji4 { get; set; }

        [Display(Name = "Wyrażam zgodę na przetwarzanie moich danych osobowych")]
        public bool CzyZgodaNaPrzetwarzanie { get; set; }
        [Display(Name = "Wyrażam zgodę na przyjmowanie treści maretingowych")]
        public bool CzyZgodaNaReklamy { get; set; }

        // ********** //
        // Pomocnicze //

       public Dictionary<string, string> Kraje { get { return InMemoryCache.Get("RegisterModel.Kraje", 15, GetKraje); } }

        public Dictionary<string, string> GetKraje()
        {
            Dictionary<string, string> k = null;

            using (Entities ie = new Entities())
            {
                k = ie.KRAJ.Where(m => m.kraj_kod != "PL").OrderBy(m => m.kraj_nazwa).ToDictionary(m => m.kraj_kod, m => m.kraj_nazwa);
            }

            return k;
        }
    }


}
