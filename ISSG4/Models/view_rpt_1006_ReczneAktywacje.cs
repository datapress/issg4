//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ISSG4.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class view_rpt_1006_ReczneAktywacje
    {
        public string GazetaNazwa { get; set; }
        public int ZamowienieId { get; set; }
        public int ZamowieniePozycjaId { get; set; }
        public System.DateTime ZamowieniePozycjaOd { get; set; }
        public System.DateTime ZamowieniePozycjaDo { get; set; }
        public decimal ZamowieniePozycjaNetto { get; set; }
        public decimal ZamowieniePozycjaBrutto { get; set; }
        public decimal ZamowieniePozycjaVat { get; set; }
        public string UserName { get; set; }
        public string Imie { get; set; }
        public string Nazwisko { get; set; }
        public string salt { get; set; }
        public string Adres { get; set; }
        public string Miejscowosc { get; set; }
        public string Kod { get; set; }
        public string Email { get; set; }
        public Nullable<bool> czy_firma { get; set; }
        public string ulica { get; set; }
        public string budynek { get; set; }
        public string mieszkanie { get; set; }
        public string telefon { get; set; }
        public string kraj { get; set; }
        public string nip { get; set; }
        public string nazwa_firmy { get; set; }
        public string kod_external { get; set; }
        public string klient_czy_kredyt { get; set; }
        public Nullable<bool> czy_faktura { get; set; }
        public string c_faktura_username { get; set; }
        public System.DateTime PlatnoscCzas { get; set; }
        public string ProduktNazwa { get; set; }
        public string PlatnoscData { get; set; }
        public string Grupa4 { get; set; }
        public int RodzajOkresuMiesiac { get; set; }
        public int RodzajOkresuDzien { get; set; }
        public string DodatekNazwa { get; set; }
        public string DodatekSkrot { get; set; }
    }
}
