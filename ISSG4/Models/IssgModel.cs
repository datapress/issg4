﻿using System;
using System.Reflection;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Transactions;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity.Infrastructure;
using System.Collections.Generic;

namespace ISSG4.Models
{

    public partial class IssgEntities : DbContext
    {
        public override int SaveChanges()
        {

            string usr = HttpContext.Current.User.Identity.Name;
            DateTime czas = DateTime.Now;

            /* transakcja ze względu na dwuetapowy zapis */
            var option = new TransactionOptions
            {
                IsolationLevel = IsolationLevel.ReadCommitted,
                Timeout = TimeSpan.FromSeconds(120)
            };
            using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required, option))
            {
                //using (var ts = new TransactionScope())
                //{
                /* przechowanie oryginalnych statusów i zmiana insert, update na unchanged by wykonały się w drugim etapie*/

                //  Dictionary<DbEntityEntry, EntityState> entityStateBackUpDictionary = new Dictionary<DbEntityEntry, EntityState>();
                ObjectContext objectContext = ((IObjectContextAdapter)this).ObjectContext;

                var added = this.ChangeTracker.Entries().Where(e => e.State == EntityState.Added);
                var modified = this.ChangeTracker.Entries().Where(e => e.State == EntityState.Modified);
                var deleted = this.ChangeTracker.Entries().Where(e => e.State == EntityState.Deleted);

                //foreach (var item in added)
                //{
                //    var oc = ((IObjectContextAdapter)this).ObjectContext;

                //    entityStateBackUpDictionary.Add(item, item.State);

                //    item.State = EntityState.Unchanged; /* inserty później */
                //}

                //foreach (var item in modified)
                //{
                //    entityStateBackUpDictionary.Add(item, item.State);

                //    item.State = EntityState.Unchanged;

                //}

                /* delete najpierw idą jako update z uzupełnieniem prac_modyf */
                //foreach (var entry in deleted)
                //{
                //    entityStateBackUpDictionary.Add(entry, entry.State);
                //    //entry.ChangeState(EntityState.Modified); /* update before delete! */
                //    entry.State = EntityState.Modified; /* inserty później */

                //    entry.Property("prac_modyf").CurrentValue = usr;
                //    entry.Property("data_modyf").CurrentValue = DateTime.Now;
                //}

                //base.SaveChanges();

                /* przywracamy statusy przechowywane do właściwych operacji w zapisie nr 2*/
                //foreach (DbEntityEntry ent in entityStateBackUpDictionary.Keys)
                //{
                //    //ObjectStateManager.ChangeObjectState(ent, entityStateBackUpDictionary[ent]);
                //    //ent.EntityState = EntityState.Unchanged; /* inserty później */
                //    ent.State = entityStateBackUpDictionary[ent];
                //}

                /* i normalny tracking operacji insert, update */
                /* nowe rekordy */
                var newEntities = objectContext.ObjectStateManager.GetObjectStateEntries(EntityState.Added);
                foreach (var entry in newEntities)
                {
                    PropertyInfo _prac_wpis = entry.Entity.GetType().GetProperty("prac_wpis");
                    if (_prac_wpis != null) _prac_wpis.SetValue(entry.Entity, usr, null);

                    PropertyInfo _data_wpis = entry.Entity.GetType().GetProperty("data_wpis");
                    if (_data_wpis != null) _data_wpis.SetValue(entry.Entity, czas, null);

                    PropertyInfo _prac_modyf = entry.Entity.GetType().GetProperty("prac_modyf");
                    if (_prac_modyf != null) _prac_modyf.SetValue(entry.Entity, usr, null);

                    PropertyInfo _data_modyf = entry.Entity.GetType().GetProperty("data_modyf");
                    if (_data_modyf != null) _data_modyf.SetValue(entry.Entity, czas, null);

                }

                /* zmieniane rekordy */
                var updatedEntities = objectContext.ObjectStateManager.GetObjectStateEntries(EntityState.Modified);
                foreach (var entry in updatedEntities)
                {
                    if (entry.State == EntityState.Modified)
                    {
                        PropertyInfo _prac_modyf = entry.Entity.GetType().GetProperty("prac_modyf");
                        if (_prac_modyf != null) _prac_modyf.SetValue(entry.Entity, usr, null);

                        PropertyInfo _data_modyf = entry.Entity.GetType().GetProperty("data_modyf");
                        if (_data_modyf != null) _data_modyf.SetValue(entry.Entity, czas, null);
                    }
                }

                /* usuwane rekordy  loguje sie z poziomu aplikacji by ominąć kombinacje z pierwszej wersji overrida 
                var deletedEntities = objectContext.ObjectStateManager.GetObjectStateEntries(EntityState.Deleted);
                foreach (var entry in deletedEntities)
                {
                    if (entry.State == EntityState.Deleted)
                    {
                        var item = entry.Entity;
                        var originalValues = this.Entry(item).OriginalValues;

                        foreach (string propertyName in originalValues.PropertyNames)
                        {
                            var original = originalValues[propertyName];

                                audyt a = new Models.audyt();
                                a.audyt_czas = czas;
                                a.kolumna_nazwa = propertyName;

                            var propertyInfo = item.GetType().GetProperty(propertyName);
                            var propertyType = propertyInfo.PropertyType;

                            a.kolumna_typ = propertyType.Name;
                                a.nowa_wartosc = null;
                                if (original != null)
                                    a.stara_wartosc = original.ToString();
                                a.tabela_pk_nazwa = GetPrimaryKeyName(this.Entry(item)).ToString();

                                string klucz = entry.EntityKey.EntityKeyValues[0].ToString();
                                klucz = klucz.Replace('[',' ' ).Replace(']', ' ').Trim();
                                string[] arr = klucz.Split(',');
                                a.tabela_pk_nazwa = arr[0];
                                a.tabela_pk_wartosc = arr[1];

                                var entityType = ObjectContext.GetObjectType(entry.Entity.GetType());

                                a.tabela_nazwa = entityType.Name.ToString();
                                a.username = usr;
                                a.typ = "D";

                                this.audyt.Add(a);
                        }

                    }
                }*/


                int i = 0;
                try
                {
                    i = base.SaveChanges();
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException ve)
                {
                    var errors = new List<string>();
                    foreach (var e in ve.EntityValidationErrors)
                    {
                        errors.AddRange(e.ValidationErrors.Select(e2 => string.Join("Validation Error :: ", e2.PropertyName, " : ", e2.ErrorMessage)));
                    }
                    var error = string.Join("\r\n", errors);
                    var betterException = new Exception(error, ve);

                    throw betterException;
                }

                ts.Complete();

                return i;

            }
        }

        public RecordInfoModel GetRecordInfo(DbEntityEntry eo)
        {
            RecordInfoModel ri = new RecordInfoModel();

            string hh = eo.Property("data_wpis").CurrentValue.ToString();

            ri.data_wpis = DateTime.Parse(hh);
            ri.data_modyf = DateTime.Parse(eo.Property("data_modyf").CurrentValue.ToString());
            ri.prac_wpis = eo.Property("prac_wpis").CurrentValue.ToString();
            ri.prac_modyf = eo.Property("prac_modyf").CurrentValue.ToString();

            return ri;
        }
    }

    public class RecordInfoModel
    {
        [Display(Name = "wpisano")]
        [DataType(DataType.DateTime)]
        public DateTime data_wpis { get; set; }

        [Display(Name = "modyfikacja")]
        [DataType(DataType.DateTime)]
        public DateTime data_modyf { get; set; }

        [Display(Name = "przez")]
        public string prac_wpis { get; set; }

        [Display(Name = "przez")]
        public string prac_modyf { get; set; }
    }




    public class NotFoundModel : HandleErrorInfo
    {
        public NotFoundModel(Exception exception, string controllerName, string actionName)
            : base(exception, controllerName, actionName)
        {
        }
        public string RequestedUrl { get; set; }
        public string ReferrerUrl { get; set; }
    }

    public class InternalServerErrorModel : HandleErrorInfo
    {
        public InternalServerErrorModel(Exception exception, string controllerName, string actionName)
            : base(exception, controllerName, actionName)
        {
        }
        public string RequestedUrl { get; set; }
        public string Message { get; set; }
    }

    [MetadataType(typeof(wartosci_Validation))]
    public class wartosci
    {
        public decimal cena_przed { get; set; }
        public decimal cena_rab { get; set; }
        public decimal cena_dop { get; set; }
        public decimal cena_netto { get; set; }
        public decimal cena_vat { get; set; }
        public decimal cena_brutto { get; set; }

        public void getZerosRecord()
        {
            this.cena_przed = 0;
            this.cena_rab = 0;
            this.cena_dop = 0;
            this.cena_netto = 0;
            this.cena_vat = 0;
            this.cena_brutto = 0;
        }

        public void Add(wartosci w)
        {
            this.cena_przed += w.cena_przed;
            this.cena_rab += w.cena_rab;
            this.cena_dop += w.cena_dop;
            this.cena_netto += w.cena_netto;
            this.cena_vat += w.cena_vat;
            this.cena_brutto += w.cena_brutto;
        }
    }

    public class wartosci_Validation
    {

        [Display(Name = "Cennik")]
        public decimal cena_przed { get; set; }

        [Display(Name = "Rabaty")]
        public decimal cena_rab { get; set; }

        [Display(Name = "Dopłaty")]
        public decimal cena_dop { get; set; }

        [Display(Name = "Netto")]
        public decimal cena_netto { get; set; }

        [Display(Name = "Vat")]
        public decimal cena_vat { get; set; }

        [Display(Name = "Brutto")]
        public decimal cena_brutto { get; set; }

    }

    public class ogl_export
    {
        public int ogl_id { get; set; }
        public int wyd_id { get; set; }
    }

    public class retIdNazwa
    {
        public int id { get; set; }
        public string nazwa { get; set; }
    }
    
}

