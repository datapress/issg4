//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ISSG4.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Post2
    {
        public long PostId { get; set; }
        public string id { get; set; }
        public string operation_number { get; set; }
        public string operation_type { get; set; }
        public string operation_status { get; set; }
        public string operation_amount { get; set; }
        public string operation_currency { get; set; }
        public string operation_original_amount { get; set; }
        public string operation_original_currency { get; set; }
        public string control { get; set; }
        public string email { get; set; }
        public string signature { get; set; }
        public string description { get; set; }
        public string wpis_error { get; set; }
        public Nullable<System.DateTime> wpis_dataczas { get; set; }
    }
}
