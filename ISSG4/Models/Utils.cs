﻿using System;
using System.Collections.Generic;
//using System.Data;
using System.Data.Entity;
//using System.Data.Objects;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Text;
using NLog;
using System.Runtime.InteropServices;
using System.Security.Principal;
using System.ComponentModel;
using ISSG4.Models;
using System.Web.Script.Serialization;
using Microsoft.VisualBasic.FileIO;
using System.Transactions;
using System.IO;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.ComponentModel.DataAnnotations;
using System.Reflection;
using System.Xml;
using HtmlAgilityPack;

namespace ISSG3
{
    public class Utils
    {

        public static string[] formats = { "dd-MM-yyyy", "dd-MM-yy", "yyyy-MM-dd" };

        public static string GetNumberFromStr(string str)
        {

            if (string.IsNullOrEmpty(str)) return string.Empty;

            return string.Join(null, System.Text.RegularExpressions.Regex.Split(str, "[^\\d]"));
        }

        public static SelectList CreateNumberList(int minpos, int maxpos, int steppos, int valtoset)
        {

            List<SelectListItem> liczby = new List<SelectListItem>();

            for (int i = minpos; i <= maxpos; i += steppos)
            {
                liczby.Add(new SelectListItem { Text = i.ToString(), Value = i.ToString(), Selected = i.Equals(valtoset) ? true : false });
            }

            return new SelectList(liczby, "Value", "Text", new { value = valtoset.ToString() });

        }

        public static string getOpcjeValue(string opcja_id)
        {

            Entities db = new Entities();

            return db.OPCJE
            .Where(o => o.opcje_nazwa == opcja_id)
            .Select(o => o.opcje_wartosc)
            .SingleOrDefault();

        }

        public object GetListOfType(Type t)
        {
            Type listType = typeof(List<>);
            var listOfType = listType.MakeGenericType(t);

            var listOfMyClassInstance = Activator.CreateInstance(listOfType);

            return listOfMyClassInstance;
        }

        public class KwotaSlownie
        {
            private static string zero = "zero";
            private static string[] jednosci = { "", " jeden ", " dwa ", " trzy ", " cztery ", " pięć ", " sześć ", " siedem ", " osiem ", " dziewięć " };
            private static string[] dziesiatki = { "", " dziesięć ", " dwadzieścia ", " trzydzieści ", " czterdzieści ", " pięćdziesiąt ", " sześćdziesiąt ", " siedemdziesiąt ", " osiemdziesiąt ", " dziewięćdziesiąt " };
            private static string[] nascie = { "dziesięć", " jedenaście ", " dwanaście ", " trzynaście ", " czternaście ", " piętnaście ", " szesnaście ", " siedemnaście ", " osiemnaście ", " dziewiętnaście " };
            private static string[] setki = { "", " sto ", " dwieście ", " trzysta ", " czterysta ", " pięćset ", " sześćset ", " siedemset ", " osiemset ", " dziewięćset " };
            private static string[] tysiacePojedyncze = { "", " tysiąc ", " tysiące ", " tysiące ", " tysiące ", " tysięcy ", " tysięcy ", " tysięcy ", " tysięcy ", " tysięcy " };
            private static string tysiaceNascie = " tysięcy ";
            private static string[] tysiaceMnogie = { " tysięcy ", " tysięcy ", " tysiące ", " tysiące ", " tysiące ", " tysięcy ", " tysięcy ", " tysięcy ", " tysięcy ", " tysięcy " };
            public static string getKwotaSlownie(decimal liczba)
            {
                string tekst = getKwotaSlownieBase(liczba).Replace(" ", " ").Trim();
                string poprzecinku = " i " + ((liczba - Math.Truncate(liczba)) * 100).ToString("0") + "/100";
                return tekst + poprzecinku;
            }
            private static string getKwotaSlownieBase(decimal liczba)
            {
                StringBuilder sb = new StringBuilder();
                //0-999
                int wartosc = (int)liczba;
                if (wartosc == 0)
                    return zero;
                int jednosc = wartosc % 10;
                int para = wartosc % 100;
                int set = (wartosc % 1000) / 100;
                if (para > 10 && para < 20)
                    sb.Insert(0, nascie[jednosc]);
                else
                {
                    sb.Insert(0, jednosci[jednosc]);
                    sb.Insert(0, dziesiatki[para / 10]);
                }
                sb.Insert(0, setki[set]);

                //1000-999999
                wartosc = wartosc / 1000;
                jednosc = wartosc % 10;
                para = wartosc % 100;
                set = (wartosc % 1000) / 100;
                if ((wartosc % 1000) / 10 == 0)
                {
                    sb.Insert(0, tysiacePojedyncze[jednosc]);
                    if (jednosc > 1)
                        sb.Insert(0, jednosci[jednosc]);
                    return sb.ToString();
                }
                if (para >= 10 && para < 20)
                {
                    sb.Insert(0, tysiaceNascie);
                    sb.Insert(0, nascie[para % 10]);
                }
                else
                {
                    sb.Insert(0, tysiaceMnogie[jednosc]);
                    sb.Insert(0, jednosci[jednosc]);
                    sb.Insert(0, dziesiatki[para / 10]);
                }
                sb.Insert(0, setki[set]);
                return sb.ToString();
            }
        }

        public static SelectList getSelectListTakNieWszystkie()
        {
            return new SelectList(new[]
                                          {
                                              new {id="" ,nazwa="<wszystkie>"},
                                              new {id="n",nazwa="Nie"},
                                              new {id="t",nazwa="Tak"},
                                          },
                                                "id", "nazwa", "");
        }
        
        public static bool ReadFile()
        {
            bool ret = false;
            Entities db = new Entities();

            string path_import = Utils.getOpcjeValue("ImportKodPath");
            string path_import_done = Utils.getOpcjeValue("ImportKodPathDone");

            DirectoryInfo dirInfo = new DirectoryInfo(path_import);
            if (!dirInfo.Exists) dirInfo.Create();
            FileInfo[] dirList = dirInfo.GetFiles();

            /* po plikach w folderze */
            using (TransactionScope ts = new TransactionScope())
            {
                foreach (FileInfo fil in dirList)
                {

                    ImportKodPlik plik = new ImportKodPlik();
                    plik.PlikNazwa = fil.FullName;
                    plik.ImportCzas = DateTime.Now;

                    Guid g = Guid.NewGuid();
                    plik.ImportKodPlikId = g.ToString();

                    string content = string.Empty;


                    if (fil.Length > 1) /* puste przerzucamy */
                    {
                        TextFieldParser parser = new TextFieldParser(fil.FullName);
                        parser.TextFieldType = FieldType.Delimited;
                        parser.SetDelimiters("\t");
                        while (!parser.EndOfData)
                        {

                            string[] fields = parser.ReadFields();/* expecting pięć pola: data yyyy-MM-dd, kod, tyt1 GO/DE, tyt2, email */
                            string pole1;
                            /* dane do pola backupu w rekordzie pliku */
                            plik.ImportPlikContent += string.Join(",", fields) + System.Environment.NewLine;

                            /* najpierw sprawdzamy czy już jest żeby był jeden */
                            if (string.IsNullOrEmpty(fields[0]) || string.IsNullOrEmpty(fields[1]) || string.IsNullOrEmpty(fields[2])) continue; /* brak pola */
                            if (string.IsNullOrEmpty(fields[1])) continue; /* brak pola */
                            pole1 = fields[1];
                            if (db.ImportKod.Any(m => m.Kod == pole1))
                                continue; /* istnieje */

                            /* każda linia to jeden kod */
                            ImportKod kod = new ImportKod();
                            //kod.ImportKodId = i;
                            kod.ImportKodPlikId = plik.ImportKodPlikId;
                            kod.SprawdzanyStatus = "n";
                            kod.SprawdzanyWynik = "n";
                            kod.PobranyPdfStatus = "n";

                            /* 1 */
                            DateTime dt;
                            DateTime.TryParseExact(fields[0], Utils.formats, new CultureInfo("pl-PL"), DateTimeStyles.None, out dt);
                            kod.Wydanie = dt;

                            /* 2 */
                            kod.Kod = fields[1];
                            
                            /* 3 */
                            kod.TytulKod1 = fields[2];
                            /* trzeba przemapować od razu na gazetę*/
                            var mapowanie = db.ImportKodTytul.SingleOrDefault(m => m.TytulKod1 == kod.TytulKod1);
                            if (mapowanie != null)
                            {
                                kod.GazetaId = mapowanie.GazetaId;
                            }
                            else /* domyślnie mapowanie dla pierwszej gazety jeśli mapowania brak. */
                            {
                                kod.GazetaId = 1;
                            }

                            /* 4 */
                            if (fields.Length > 3)
                                if (!string.IsNullOrEmpty(fields[3])) kod.TytulKod2 = fields[3];

                            /* 5 */
                            if (fields.Length > 4)
                                if (!string.IsNullOrEmpty(fields[4])) kod.Email = fields[4];

                            //i++;
                            db.ImportKod.Add(kod);
                        }
                        parser.Close();
                    }

                    db.ImportKodPlik.Add(plik);
                    db.SaveChanges();

                    fil.MoveTo(path_import_done + "\\" + fil.Name.Replace("." + fil.Extension, string.Empty) + "_" + g.ToString() + fil.Extension);

                }

                ts.Complete();
                ret = true;
            }

            return ret;
        }


        private static void ConvertContentTo(HtmlNode node, TextWriter outText)
        {
            foreach (HtmlNode subnode in node.ChildNodes)
            {
                ConvertTo(subnode, outText);
            }
        }

        private static void ConvertTo(HtmlNode node, TextWriter outText)
        {
            string html;
            switch (node.NodeType)
            {
                case HtmlNodeType.Comment:
                    // don't output comments
                    break;

                case HtmlNodeType.Document:
                    ConvertContentTo(node, outText);
                    break;

                case HtmlNodeType.Text:
                    // script and style must not be output
                    string parentName = node.ParentNode.Name;
                    if ((parentName == "script") || (parentName == "style"))
                        break;

                    // get text
                    html = ((HtmlTextNode)node).Text;

                    // is it in fact a special closing node output as text?
                    if (HtmlNode.IsOverlappedClosingElement(html))
                        break;

                    // check the text is meaningful and not a bunch of whitespaces
                    if (html.Trim().Length > 0)
                    {
                        outText.Write(HtmlEntity.DeEntitize(html));
                    }
                    break;

                case HtmlNodeType.Element:
                    switch (node.Name)
                    {
                        case "p":
                            // treat paragraphs as crlf
                            outText.Write("\r\n");
                            break;
                        case "br":
                            outText.Write("\r\n");
                            break;
                    }

                    if (node.HasChildNodes)
                    {
                        ConvertContentTo(node, outText);
                    }
                    break;
            }
        }

        public static string ConvertToPlainText(string html)
        {
            HtmlDocument doc = new HtmlDocument();
            doc.LoadHtml(html);

            StringWriter sw = new StringWriter();
            ConvertTo(doc.DocumentNode, sw);
            sw.Flush();
            return sw.ToString();
        }

        public static bool IndexTresci(int wydanieId)
        {
            bool ret = false;

            NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
           

            using (Entities db = new Entities())
            {
                //czyścimy przed ponownym indeksowaniem
                string sql = "delete from i3_ArtArtykulTresciIndex where wydanieid=" + wydanieId.ToString();

                logger.Debug("Usuwam słowa z indeksu dla wydania: "+wydanieId.ToString());
                ObjectContext objectContext = ((IObjectContextAdapter)db).ObjectContext;
                var query = objectContext.ExecuteStoreCommand(sql);
                logger.Debug("Po usunięciu słów z indeksu: "+query.ToString());

                logger.Debug("Start indeksacji wyrazów treści zaimportowanych artykułów wydania id=" + wydanieId.ToString());
                List<i3_ArtArtykul> arts = db.i3_ArtArtykul.Where(d => d.WydanieId == wydanieId).ToList();
                logger.Debug("Artykułów: "+arts.Count.ToString());
                int i = 0;

                foreach(i3_ArtArtykul art in arts)
                {
                    i++;

                    //string tytul = art.ArtArtykulTytul;
                    //var punctuation_tytul = tytul.Where(Char.IsPunctuation).Distinct().ToArray();
                    //IEnumerable<string> words_tytul= tytul.ToLower().Split().Select(x => x.Trim(punctuation_tytul)).Distinct();

                    string tresc = art.ArtArtykulNadytul+" "+art.ArtArtykulTytul+" "+ConvertToPlainText(art.ArtArtykulTresc);
                    var punctuation = tresc.Where(Char.IsPunctuation).Distinct().ToArray();
                    IEnumerable<string> words = tresc.ToLower().Split().Select(x => x.Trim(punctuation)).Distinct();

                    logger.Debug("Artykuł id="+art.ArtArtykulId.ToString()+"("+i.ToString()+"/"+arts.Count().ToString()+")"+" słów: "+words.Count().ToString());

                    foreach(string word in words)
                    {
                        if ( word.Length < 4 || word.Length > 49 )
                            continue;

                        i3_ArtArtykulTresciIndex idx = new i3_ArtArtykulTresciIndex();

                        idx.ArtArtykulId = art.ArtArtykulId;
                        idx.ArtArtykulTresc = word;
                        idx.ArtAutorId = art.ArtAutorId;
                        idx.ArtDzialId = art.ArtDzialId;
                        idx.WydanieId = art.WydanieId;
                        idx.PracWpis= "import";
                        idx.PracModyf = "import";
                        idx.DataWpis = DateTime.Now;
                        idx.DataModyf = DateTime.Now;

                        db.i3_ArtArtykulTresciIndex.Add(idx);
                    }

                    logger.Debug("Przed zapisem słów z artykułu " + i.ToString());

                    db.SaveChanges();

                    logger.Debug("Po zapisie słów z artykułu " + i.ToString());
                }

            }

                return ret;
        }

        public static string NoHtmlTruncate(string val, int len)
        {
            string res = StripTagsRegex(val);
            res = TextTruncate(res, len);
            res = SignReplace(res);
            return res;
        }

        public static string ArtTruncate(string val, int len)
        {
            //string res = StripTagsRegex(val);

            Logger logger = LogManager.GetCurrentClassLogger();
            logger.Debug("ArtTruncate VAL: "+val);

            string res = val;
            res = TextTruncate(res, len);
            res = SignReplace(res);
            res = res + "</p>"; /* trochę na sztywno UWAGA kończymy paragraf, bo trunkacja treści z reguły obcina koniec.*/

            logger.Debug("ArtTruncate RES: " + res);

            return res;
        }


        public static string StripTagsRegex(string source)
        {
            return Regex.Replace(source, "<.*?>", string.Empty);
        }

        public static string TextTruncate(string val, int len)
        {
            if (val == null || val.Length < len || val.IndexOf(" ", len) == -1)
                return val;

            char[] charsToTrim = { ',', '.', '!', '?', '(', ')', '-' };
            return val.Substring(0, val.IndexOf(" ", len)).TrimEnd(charsToTrim) + "...";
        }

        public static string SignReplace(string text)
        {
            if (text != null && text.Length > 0)
            {
                text = text.Replace("&nbsp;", " ");
                //text = Regex.Replace(text, "&lt;|&gt;", ""); // < >
                text = Regex.Replace(text, "&oacute;|&#243;", "ó");
                text = Regex.Replace(text, "&Oacute;|&#211;", "Ó");
                text = Regex.Replace(text, "&apos;|&#39;", "'");
                text = Regex.Replace(text, "&ndash;", "-");
                text = Regex.Replace(text, "&quot;|&#34;", "\"");
                text = Regex.Replace(text, "&#187;|&raquo;", "»");
                text = Regex.Replace(text, "&#171;|&laquo;", "«");
                text = Regex.Replace(text, "&#8222;|&bdquo;", "\"");
                text = Regex.Replace(text, "&#8221;|&rdquo;", "\"");
                text = Regex.Replace(text, "&#133;|&hellip;", "...");
                text = Regex.Replace(text, "&#200;|&Uuml;", "Ü");
                text = Regex.Replace(text, "&#252;|&uuml;", "ü");
                text = Regex.Replace(text, "&#353;|&scaron;", "š");
                text = Regex.Replace(text, "&#352;|&Scaron;", "Š");
                text = Regex.Replace(text, "&#8217;|&rsquo;", "’");
                text = Regex.Replace(text, "&Eacute;", "É");
                text = Regex.Replace(text, "&eacute;", "é");
                text = Regex.Replace(text, "&minus;", "-");
                text = Regex.Replace(text, "&ouml;", "ö");
                text = Regex.Replace(text, "&Ouml;", "Ö");
                text = Regex.Replace(text, "&deg;", "°");
                text = Regex.Replace(text, "&copy;|&#0169;", "©");
                text = Regex.Replace(text, "&#8471;", "℗");
                text = Regex.Replace(text, "&auml;", "ä");
                
                text = text.Replace("\\b", "");
                text = text.Replace((char)0x08, ' ');
            }
            return text;
        }

        public static ISSG4.Models.ViewModels.ArtViewList getSpisTresci(int wydanieId, int dzialId)
        {

            Logger logger = LogManager.GetCurrentClassLogger();
            ISSG4.Models.ViewModels.ArtViewList spis = new ISSG4.Models.ViewModels.ArtViewList();

            List<i3_ArtArtykul> art_lista = new List<i3_ArtArtykul>();

            using (Entities db = new Entities())
            {
                ObjectContext objectContext = ((IObjectContextAdapter)db).ObjectContext;

                ObjectQuery<i3_ArtArtykul> arts = objectContext.CreateObjectSet<i3_ArtArtykul>("i3_ArtArtykul")
                .Include("i3_ArtDzial")
                .Where("it.WydanieId == " + wydanieId.ToString())
                .OrderBy("it.ArtArtykulFoto desc, it.ArtArtykulUtworzono");

                if (dzialId > 0)
                {
                    arts = objectContext.CreateObjectSet<i3_ArtArtykul>("i3_ArtArtykul")
                    .Include("i3_ArtDzial")
                    .Where("it.WydanieId == " + wydanieId.ToString())
                    .Where("it.ArtDzialid == " + dzialId.ToString())
                    .OrderBy("it.ArtArtykulFoto desc");

                }
                spis.dzialId = dzialId;

                spis.art_lista = arts.ToList();
                spis.wydanie = db.Wydanie.SingleOrDefault(d => d.WydanieId == wydanieId);

                ObjectQuery<i3_ArtDzial> dzials = objectContext
                    .CreateObjectSet<i3_ArtDzial>("i3_ArtDzial");
                    //.Include("i3_ArtArtykul");
                spis.dzial_lista = dzials.ToList();

                ObjectQuery<i3_ArtArtykul> art_czolo = objectContext
                    .CreateObjectSet<i3_ArtArtykul>("i3_ArtArtykul")
                    .Where("it.WydanieId == " + wydanieId.ToString())
                      .Where("it.ArtArtykulFoto == true")
                     .Where("it.ArtJedynka == true");

                spis.art_czolo = art_czolo.FirstOrDefault();
                if (spis.art_czolo == null)
                {
                    //brak artykułu czoła, bierzemy pierwszy ze zdjęciem z pięciu.
                    art_czolo = arts.Where("it.ArtPierwsza == true")
                        .Where("it.ArtArtykulFoto == true");
                    spis.art_czolo = art_czolo.FirstOrDefault(); 

                }
                // else
                // {
                spis.art_foto_czolo = db.i3_ArtFoto.Where(d => d.ArtArtykulId == art_czolo.FirstOrDefault().ArtArtykulId).FirstOrDefault();
                //spis.art_foto_czolo = db.i3_ArtFoto.Where(d => d.ArtArtykulId == spis.art_czolo.ArtArtykulId).FirstOrDefault();

                spis.art_foto_list = db.i3_ArtFoto
                    //.Include("i3_ArtArtykul")
                    .Where(d => d.i3_ArtArtykul.WydanieId == wydanieId).ToList();

                DateTime termin = spis.wydanie.WydanieData;
                spis.termin_string = termin.Year.ToString() + termin.Month.ToString("d2") + termin.Day.ToString("d2");
              //  }

                /* dla kart z artykułem */
                List<ISSG4.Models.ViewModels.ArtView> artView_list = new List<ISSG4.Models.ViewModels.ArtView>();
                foreach (i3_ArtArtykul art in spis.art_lista)
                {
                    ISSG4.Models.ViewModels.ArtView mod = new ISSG4.Models.ViewModels.ArtView();
                    mod.art = art;
                    mod.art_foto_list = spis.art_foto_list.Where(d => d.ArtArtykulId == art.ArtArtykulId).ToList();
                    mod.wydanie = spis.wydanie;

                    artView_list.Add(mod);
                }

                spis.artView_list = artView_list;
            }

            return spis;
        }

    }

    public static class HtmlTruncator
    {

        public static string LimitOnWordBoundary(string str, int maxLength, string ellipses = "...")
        {
            XmlDocument doc = new XmlDocument();

            XmlParserContext context = new XmlParserContext(doc.NameTable, new XmlNamespaceManager(doc.NameTable), null, XmlSpace.Preserve);
            XmlTextReader reader = new XmlTextReader("<xml>" + str + "</xml>", XmlNodeType.Document, context);
            bool shouldWriteEllipses;
            using (var writer = doc.CreateNavigator().AppendChild())
            {
                LimitOnWordBoundary(writer, reader, maxLength, out shouldWriteEllipses);
                writer.Flush();
            }

            return doc.DocumentElement.InnerXml + (shouldWriteEllipses ? ellipses : "");
        }

        public static void LimitOnWordBoundary(XmlWriter writer, XmlReader reader, int maxLength, out bool shouldWriteEllipses)
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }
            if (writer == null)
            {
                throw new ArgumentNullException("writer");
            }

            int elementCount = 0;

            int currentLength = 0;
            shouldWriteEllipses = false;

            int magicMinimumLength = Math.Min(5, (maxLength + 1) / 2);

            int num = (reader.NodeType == XmlNodeType.None) ? -1 : reader.Depth;
            do
            {
                bool done = false;
                switch (reader.NodeType)
                {
                    case XmlNodeType.Element:
                        elementCount++;
                        writer.WriteStartElement(reader.Prefix, reader.LocalName, reader.NamespaceURI);
                        writer.WriteAttributes(reader, false);
                        if (reader.IsEmptyElement)
                        {
                            elementCount--;
                            writer.WriteEndElement();
                        }
                        break;

                    case XmlNodeType.Text:
                        string value = reader.Value;
                        int strLen = value.Length;

                        if (currentLength + strLen > maxLength && (maxLength - currentLength + 1 >0))
                        {
                            string almost = value.Substring(0, maxLength - currentLength + 1);
                            int lastSpace = almost.LastIndexOf(' ');
                            if (lastSpace < 0)
                            {
                                if (currentLength < magicMinimumLength)
                                {
                                    value = value.Substring(0, maxLength - currentLength);
                                }
                                else
                                {
                                    value = null;
                                }
                            }
                            else if (lastSpace + currentLength < magicMinimumLength)
                            {
                                value = value.Substring(0, maxLength - currentLength);
                            }
                            else
                            {
                                value = value.Substring(0, lastSpace);
                            }
                            shouldWriteEllipses = true;
                            done = true;
                        }
                        if (value != null)
                        {
                            writer.WriteString(value);
                            currentLength += value.Length;
                        }
                        break;

                    case XmlNodeType.Whitespace:
                    case XmlNodeType.SignificantWhitespace:
                        writer.WriteString(reader.Value);
                        currentLength += reader.Value.Length;
                        break;

                    case XmlNodeType.EndElement:
                        elementCount--;
                        writer.WriteFullEndElement();
                        break;

                    case XmlNodeType.CDATA:
                        //writer.WriteCData(reader.Value);
                        break;

                    case XmlNodeType.EntityReference:
                        writer.WriteEntityRef(reader.Name);
                        currentLength++;
                        break;

                    case XmlNodeType.ProcessingInstruction:
                    case XmlNodeType.XmlDeclaration:
                        //writer.WriteProcessingInstruction(reader.Name, reader.Value);
                        break;

                    case XmlNodeType.Comment:
                        //writer.WriteComment(reader.Value);
                        break;

                    case XmlNodeType.DocumentType:
                        //writer.WriteDocType(reader.Name, reader.GetAttribute("PUBLIC"), reader.GetAttribute("SYSTEM"), reader.Value);
                        break;
                }
                if (done) break;
            }
            while (reader.Read() && ((num < reader.Depth) || ((num == reader.Depth) && (reader.NodeType == XmlNodeType.EndElement))));

            while (elementCount > 0)
            {
                writer.WriteFullEndElement();
                elementCount--;
            }
        }

    }

    //public partial class ISSG3Entities : DbContext
    //{


    //    public override int SaveChanges()
    //    {

    //        ISSG3Entities db = new ISSG3Entities();

    //        ObjectContext objectContext = ((IObjectContextAdapter)db).ObjectContext;

    //        string usr = HttpContext.Current.User.Identity.Name;
    //        DateTime czas = DateTime.Now;

    //        /* transakcja ze względu na dwuetapowy zapis */
    //        var option = new TransactionOptions
    //        {
    //            IsolationLevel = IsolationLevel.ReadCommitted,
    //            Timeout = TimeSpan.FromSeconds(120)
    //        };
    //        using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required, option))
    //        {
    //            //using (var ts = new TransactionScope())
    //            //{
    //            /* przechowanie oryginalnych statusów i zmiana insert, update na unchanged by wykonały się w drugim etapie*/

    //            //  Dictionary<DbEntityEntry, EntityState> entityStateBackUpDictionary = new Dictionary<DbEntityEntry, EntityState>();
    //           // ObjectContext objectContext = ((IObjectContextAdapter)this).ObjectContext;

    //            var added = this.ChangeTracker.Entries().Where(e => e.State == EntityState.Added);
    //            var modified = this.ChangeTracker.Entries().Where(e => e.State == EntityState.Modified);
    //            var deleted = this.ChangeTracker.Entries().Where(e => e.State == EntityState.Deleted);

    //            //foreach (var item in added)
    //            //{
    //            //    var oc = ((IObjectContextAdapter)this).ObjectContext;

    //            //    entityStateBackUpDictionary.Add(item, item.State);

    //            //    item.State = EntityState.Unchanged; /* inserty później */
    //            //}

    //            //foreach (var item in modified)
    //            //{
    //            //    entityStateBackUpDictionary.Add(item, item.State);

    //            //    item.State = EntityState.Unchanged;

    //            //}

    //            /* delete najpierw idą jako update z uzupełnieniem prac_modyf */
    //            //foreach (var entry in deleted)
    //            //{
    //            //    entityStateBackUpDictionary.Add(entry, entry.State);
    //            //    //entry.ChangeState(EntityState.Modified); /* update before delete! */
    //            //    entry.State = EntityState.Modified; /* inserty później */

    //            //    entry.Property("prac_modyf").CurrentValue = usr;
    //            //    entry.Property("data_modyf").CurrentValue = DateTime.Now;
    //            //}

    //            //base.SaveChanges();

    //            /* przywracamy statusy przechowywane do właściwych operacji w zapisie nr 2*/
    //            //foreach (DbEntityEntry ent in entityStateBackUpDictionary.Keys)
    //            //{
    //            //    //ObjectStateManager.ChangeObjectState(ent, entityStateBackUpDictionary[ent]);
    //            //    //ent.EntityState = EntityState.Unchanged; /* inserty później */
    //            //    ent.State = entityStateBackUpDictionary[ent];
    //            //}

    //            /* i normalny tracking operacji insert, update */
    //            /* nowe rekordy */
    //            var newEntities = objectContext.ObjectStateManager.GetObjectStateEntries(EntityState.Added);
    //            foreach (var entry in newEntities)
    //            {
    //                PropertyInfo _prac_wpis = entry.Entity.GetType().GetProperty("prac_wpis");
    //                if (_prac_wpis != null) _prac_wpis.SetValue(entry.Entity, usr, null);

    //                PropertyInfo _data_wpis = entry.Entity.GetType().GetProperty("data_wpis");
    //                if (_data_wpis != null) _data_wpis.SetValue(entry.Entity, czas, null);

    //                PropertyInfo _prac_modyf = entry.Entity.GetType().GetProperty("prac_modyf");
    //                if (_prac_modyf != null) _prac_modyf.SetValue(entry.Entity, usr, null);

    //                PropertyInfo _data_modyf = entry.Entity.GetType().GetProperty("data_modyf");
    //                if (_data_modyf != null) _data_modyf.SetValue(entry.Entity, czas, null);

    //            }

    //            /* zmieniane rekordy */
    //            var updatedEntities = objectContext.ObjectStateManager.GetObjectStateEntries(EntityState.Modified);
    //            foreach (var entry in updatedEntities)
    //            {
    //                if (entry.State == EntityState.Modified)
    //                {
    //                    PropertyInfo _prac_modyf = entry.Entity.GetType().GetProperty("prac_modyf");
    //                    if (_prac_modyf != null) _prac_modyf.SetValue(entry.Entity, usr, null);

    //                    PropertyInfo _data_modyf = entry.Entity.GetType().GetProperty("data_modyf");
    //                    if (_data_modyf != null) _data_modyf.SetValue(entry.Entity, czas, null);
    //                }
    //            }

    //            /* usuwane rekordy  loguje sie z poziomu aplikacji by ominąć kombinacje z pierwszej wersji overrida 
    //            var deletedEntities = objectContext.ObjectStateManager.GetObjectStateEntries(EntityState.Deleted);
    //            foreach (var entry in deletedEntities)
    //            {
    //                if (entry.State == EntityState.Deleted)
    //                {
    //                    var item = entry.Entity;
    //                    var originalValues = this.Entry(item).OriginalValues;

    //                    foreach (string propertyName in originalValues.PropertyNames)
    //                    {
    //                        var original = originalValues[propertyName];

    //                            audyt a = new Models.audyt();
    //                            a.audyt_czas = czas;
    //                            a.kolumna_nazwa = propertyName;

    //                        var propertyInfo = item.GetType().GetProperty(propertyName);
    //                        var propertyType = propertyInfo.PropertyType;

    //                        a.kolumna_typ = propertyType.Name;
    //                            a.nowa_wartosc = null;
    //                            if (original != null)
    //                                a.stara_wartosc = original.ToString();
    //                            a.tabela_pk_nazwa = GetPrimaryKeyName(this.Entry(item)).ToString();

    //                            string klucz = entry.EntityKey.EntityKeyValues[0].ToString();
    //                            klucz = klucz.Replace('[',' ' ).Replace(']', ' ').Trim();
    //                            string[] arr = klucz.Split(',');
    //                            a.tabela_pk_nazwa = arr[0];
    //                            a.tabela_pk_wartosc = arr[1];

    //                            var entityType = ObjectContext.GetObjectType(entry.Entity.GetType());

    //                            a.tabela_nazwa = entityType.Name.ToString();
    //                            a.username = usr;
    //                            a.typ = "D";

    //                            this.audyt.Add(a);
    //                    }

    //                }
    //            }*/


    //            int i = 0;
    //            try
    //            {
    //                i = base.SaveChanges();
    //            }
    //            catch (System.Data.Entity.Validation.DbEntityValidationException ve)
    //            {
    //                var errors = new List<string>();
    //                foreach (var e in ve.EntityValidationErrors)
    //                {
    //                    errors.AddRange(e.ValidationErrors.Select(e2 => string.Join("Validation Error :: ", e2.PropertyName, " : ", e2.ErrorMessage)));
    //                }
    //                var error = string.Join("\r\n", errors);
    //                var betterException = new Exception(error, ve);

    //                throw betterException;
    //            }

    //            ts.Complete();

    //            return i;

    //        }
    //    }

    //    public RecordInfoModel GetRecordInfo(DbEntityEntry eo)
    //    {
    //        RecordInfoModel ri = new RecordInfoModel();

    //        string hh = eo.Property("data_wpis").CurrentValue.ToString();

    //        ri.data_wpis = DateTime.Parse(hh);
    //        ri.data_modyf = DateTime.Parse(eo.Property("data_modyf").CurrentValue.ToString());
    //        ri.prac_wpis = eo.Property("prac_wpis").CurrentValue.ToString();
    //        ri.prac_modyf = eo.Property("prac_modyf").CurrentValue.ToString();

    //        return ri;
    //    }
    //}

}