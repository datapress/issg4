﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.IO;

namespace Simplid.Images
{
    public class ImageManipulation
    {
        public static Image Resize(Image img, int newWidth, int newHeight)
        {
            Image imgFromBitmap = new Bitmap(newWidth, newHeight);
            Graphics graphics = Graphics.FromImage(imgFromBitmap);
            graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
            graphics.SmoothingMode = SmoothingMode.HighQuality;
            graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;
            graphics.CompositingQuality = CompositingQuality.HighQuality;
            graphics.DrawImage(img, 0, 0, newWidth, newHeight);
            return imgFromBitmap;
        }
        
        public static Image ResizeFixedRatio(Image img, int maxWidth, int maxHeight)
        {
            int width = 0, height = 0;

            int origWidth = img.Width;
            int origHeight = img.Height;

            float nPercent;
            float nPercentW = 0;
            float nPercentH = 0;

            nPercentW = ((float)maxWidth / (float)origWidth);
            nPercentH = ((float)maxHeight / (float)origHeight);
            if (nPercentH < nPercentW)
            {
                nPercent = nPercentH;
            }
            else
            {
                nPercent = nPercentW;
            }

            width = (int)(origWidth * nPercent);
            height = (int)(origHeight * nPercent);

            return Resize(img, width, height);
        }

        public static Image ResizeCropFixedRatio(Image img, int maxWidth, int maxHeight, decimal fx, decimal fy)
        {
            /*int width = 0, height = 0;

            int origWidth = img.Width;
            int origHeight = img.Height;

            float nPercent;
            float nPercentW = 0;
            float nPercentH = 0;

            nPercentW = ((float)maxWidth / (float)origWidth);
            nPercentH = ((float)maxHeight / (float)origHeight);
            if (nPercentH < nPercentW)
            {
                nPercent = nPercentH;
            }
            else
            {
                nPercent = nPercentW;
            }

            width = (int)(origWidth * nPercent);
            height = (int)(origHeight * nPercent);*/

            //return Resize(img, width, height);

            decimal result_ratio = (decimal)maxHeight / (decimal)maxWidth;
            decimal current_ratio = (decimal)img.Height / (decimal)img.Width;

            Boolean preserve_width = false;
            if (current_ratio > result_ratio)
            {
                preserve_width = true;
            }
            int new_width = 0;
            int new_height = 0;
            if (preserve_width)
            {
                new_width = maxWidth;
                new_height = (int)Math.Round((decimal)(current_ratio * new_width));
            }
            else
            {
                new_height = maxHeight;
                new_width = (int)Math.Round((decimal)(new_height / current_ratio));
            }

            Image resized = Resize(img, new_width, new_height);

            int startX = 0;
            int startY = 0;

            int half_height = (int)Math.Round((decimal)(maxHeight / 2));
            int half_width = (int)Math.Round((decimal)(maxWidth / 2));

            //przejście ze skali -1..1 / -1..1 do 0..1 
            decimal fxs = fx + ((1 - fx) / 2);
            decimal fys = 1 - (fy + ((1 - fy) / 2)); //odwrócenie skali dla igreka, bo image zaczyna się od początku

            int focusXpx = (int)Math.Round((decimal)(resized.Width * fxs));
            int focusYpx = (int)Math.Round((decimal)(resized.Height * fys));

            startX = focusXpx - half_width >= 0 ? (focusXpx - half_width) : 0;
            startY = focusYpx - half_height >= 0 ? (focusYpx - half_height) : 0;

            int width_to_crop = maxWidth > resized.Width ? resized.Width : maxWidth;
            int height_to_crop = maxHeight > resized.Height ? resized.Height : maxHeight;

            if ((width_to_crop + startX) > resized.Width)
                startX = 0;
            if ((height_to_crop + startY) > resized.Height)
                startY = resized.Height - height_to_crop;


            Bitmap btm = new Bitmap(resized);
            Rectangle rct = new Rectangle(startX, startY, width_to_crop, height_to_crop);
            Image img2 = btm.Clone(rct, PixelFormat.DontCare);

            //return Resize(img2, width, height);
            return img2;


        }

        public static Image ResizeCropFixedRatioOld(Image img, int maxWidth, int maxHeight, decimal fx, decimal fy)
        {
            //fx, fy in the range -1..1 on x and y axis give the focus point to crop arround

            int width = 0, height = 0;

            int origWidth = img.Width;
            int origHeight = img.Height;

            float nPercent;
            float nPercentW = 0;
            float nPercentH = 0;

            nPercentW = ((float)maxWidth / (float)origWidth);
            nPercentH = ((float)maxHeight / (float)origHeight);
            if (nPercentH < nPercentW)
            {
                nPercent = nPercentH; //pionowe
            }
            else
            {
                nPercent = nPercentW; //poziome
            }

            width = (int)(origWidth * nPercent);
            height = (int)(origHeight * nPercent);

            int startX = 0;
            int startY = 0;

            //text focus point
            fx = decimal.Parse("-0,5");
            fy = decimal.Parse("0");

            decimal mnoznikX = (fx +1 ) / 2; 
            decimal mnoznikY = (fy +1 ) / 2;

            decimal cropX = (int) mnoznikX * origWidth; //175
            decimal cropY = (int) mnoznikY * origHeight; //155

            //decimal new_ratio = 

            //startX = 
            //startY = 

            //calc x,y to fit the crop area

            //Image img2 = Resize(img, width, height);
            img = Resize(img, width, height);

            Bitmap btm = new Bitmap(img);
            Rectangle rct = new Rectangle(startX, startY, maxWidth, maxHeight);
            Image img2 = btm.Clone(rct, PixelFormat.DontCare);

            //return Resize(img2, width, height);
            return img2;
        }

        public static Image ResizeCropFixedRatioOld2(Image img, int maxWidth, int maxHeight, decimal fx, decimal fy)
        {
            //fx, fy in the range -1..1 on x and y axis give the focus point to crop arround

            if (img.Height != maxHeight || img.Width != maxWidth)
            {
                decimal result_ratio = (decimal)maxHeight / (decimal)maxWidth;
                decimal current_ratio = (decimal)img.Height / (decimal)img.Width;

                int startX = 0;
                int startY = 0;

                Boolean preserve_width = false;
                if (current_ratio > result_ratio)
                {
                    preserve_width = true;
                }
                int new_width = 0;
                int new_height = 0;
                if (preserve_width)
                {
                    new_width = maxWidth;
                    new_height = (int)Math.Round((decimal)(current_ratio * new_width));
                }
                else
                {
                    new_height = maxHeight;
                    new_width = (int)Math.Round((decimal)(new_height / current_ratio));
                }

                // String newGeomStr = new_width.ToString() + "x" + new_height.ToString();
                // String geomStr = width.ToString() + "x" + height.ToString();
                // ImageMagickNET.Geometry intermediate_geo = new ImageMagickNET.Geometry(newGeomStr);
                // ImageMagickNET.Geometry final_geo = new ImageMagickNET.Geometry(geomStr);
                // img.Resize(intermediate_geo);
                // img.Crop(final_geo);

                img = Resize(img, new_width, new_height);

                //text focus point
                //fx = decimal.Parse("-1");
                //fy = decimal.Parse("-1");

                decimal fxs = fx + 1;
                decimal fys = (fy + 1) * (-1);

                fxs = fxs / 2;
                fys = fys / 2;

                int focusPointX = (int)(fxs * new_width); //focuspointx 375
                int focusPointY = (int)(fys * new_height); //focuspointy 166

                int width_half = (int)new_width / 2;
                int height_half = (int)new_height / 2;

                startX = focusPointX - width_half/2;
                startY = focusPointY - height_half/2;

                if (startX < 0) startX = 0;
                if (startY < 0) startY = 0;

                int endX = maxWidth > new_width ? maxWidth : new_width;
                int endY = maxHeight > new_height ? maxHeight : new_height;

                if (endX > maxWidth) endX = maxWidth;
                //if (endY > maxHeight) endY = (maxHeight+startY);
                if (endY > maxHeight) endY = maxHeight;

                Bitmap btm = new Bitmap(img);

                if (startX + maxWidth > img.Width)
                    startX = img.Width - maxWidth;

                Rectangle rct = new Rectangle(startX, startY, maxWidth, endY);

                //Rectangle rct = new Rectangle(startX, startY, endX, endY);
                //Rectangle rct = new Rectangle(startX, startY, maxWidth, maxHeight);

                Image img2 = btm.Clone(rct, PixelFormat.DontCare);

                return img2;

            }

            return img;
        }


        public static Image ResizeCropFixedRatioNew1(Image img, int maxWidth, int maxHeight, decimal fx, decimal fy)
        {
            //fx, fy in the range -1..1 on x and y axis give the focus point to crop arround
            if (img.Height != maxHeight || img.Width != maxWidth)
            {
                decimal result_ratio = (decimal)maxHeight / (decimal)maxWidth;
                decimal current_ratio = (decimal)img.Height / (decimal)img.Width;

                int startX = 0;
                int startY = 0;

                Boolean preserve_width = false;
                if (current_ratio > result_ratio)
                {
                    preserve_width = true;
                }
                int new_width = 0;
                int new_height = 0;

                if (preserve_width) 
                {
                    new_width = maxWidth;
                    new_height = (int)Math.Round((decimal)(current_ratio * new_width));
                }
                else
                {
                    new_height = maxHeight;
                    new_width = (int)Math.Round((decimal)(new_height / current_ratio));
                }

                img = Resize(img, new_width, new_height);

                int width_half = (int)new_width / 2;
                int height_half = (int)new_height / 2;

                startY = img.Height - height_half; 
                startY = startY - (maxHeight / 2); /* centrowanie, odpowiednik fy=0*/

                decimal fxs = fx + 1;
                //decimal fys = (fy + 1);// * (-1);
                decimal fys = fx + ((1 - fx) / 2);

                fxs = fxs / 2;
                //fys = fys / 2;

                //startY = (int)(startY * fys);
               

                // int focusPointX = (int)(fxs * new_width); //focuspointx 375
                // int focusPointY = (int)(fys * new_height); //focuspointy 166

                Bitmap btm = new Bitmap(img);
                new_width = new_width > maxWidth ? maxWidth : new_width;
                startY = (int)(new_width * fys);

                Rectangle rct = new Rectangle(startX, startY, new_width, maxHeight);

                Image img2 = btm.Clone(rct, PixelFormat.DontCare);

                return img2;

            }

            return img;
        }





        public static Image ResizeFixedRatioAndWidth(Image img, int Width)
        {
            int width = 0, height = 0;

            int origWidth = img.Width;
            int origHeight = img.Height;

            float nPercent = ((float)Width / (float)origWidth);

            width = (int)(origWidth * nPercent);
            height = (int)(origHeight * nPercent);

            return Resize(img, width, height);
        }

        public static Image ResizeFixedRatioAndHeight(Image img, int Height)
        {
            int width = 0, height = 0;

            int origWidth = img.Width;
            int origHeight = img.Height;

            float nPercent = ((float)Height / (float)origHeight);

            width = (int)(origWidth * nPercent);
            height = (int)(origHeight * nPercent);

            return Resize(img, width, height);
        }

        public static Image ResizePercent(Image img, float percent)
        {
            return ResizePercent(img, percent, percent);
        }

        public static Image ResizePercent(Image img, float percentWidth, float percentHeight)
        {
            int origWidth = img.Width;
            int origHeight = img.Height;

            int width = (int)(origWidth * percentWidth);
            int height = (int)(origHeight * percentHeight);

            return Resize(img, width, height);
        }

        public static void SaveAsJPG(Image img, string filename)
        {
            SaveAsJPG(img, filename, 75L);
        }

        public static void SaveAsJPG(Image img, string filename, long quality)
        {
            ImageCodecInfo codecInfo = GetEncoderInfo("image/jpeg");
            System.Drawing.Imaging.EncoderParameters encoderParameters = new System.Drawing.Imaging.EncoderParameters(1);
            encoderParameters.Param[0] = new System.Drawing.Imaging.EncoderParameter(System.Drawing.Imaging.Encoder.Quality, quality);
            
            img.Save(filename, codecInfo, encoderParameters);
        }

        public static byte[] SaveAsJPG(Image img)
        {
            return SaveAsJPG(img, 75L);
        }

        public static byte[] SaveAsJPG(Image img, long quality)
        {
            ImageCodecInfo codecInfo = GetEncoderInfo("image/jpeg");
            System.Drawing.Imaging.EncoderParameters encoderParameters = new System.Drawing.Imaging.EncoderParameters(1);
            encoderParameters.Param[0] = new System.Drawing.Imaging.EncoderParameter(System.Drawing.Imaging.Encoder.Quality, quality);
            
            MemoryStream ms = new MemoryStream();
            img.Save(ms, codecInfo, encoderParameters);
            return ms.GetBuffer();
        }

        private static ImageCodecInfo GetEncoderInfo(String mimeType)
        {
            int j;
            ImageCodecInfo[] encoders;
            encoders = ImageCodecInfo.GetImageEncoders();
            for (j = 0; j < encoders.Length; ++j)
            {
                if (encoders[j].MimeType == mimeType)
                    return encoders[j];
            }
            return null;
        }
    }
}