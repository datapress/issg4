﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;
using System.IO;

namespace ISSG4.Helpers
{
    public class StringHelper
    {
        public static string SerializeToString(object obj)
        {
            XmlSerializer serializer = new XmlSerializer(obj.GetType());

            using (StringWriter writer = new StringWriter())
            {
                serializer.Serialize(writer, obj);

                return writer.ToString();
            }
        }

        public static string DeleteSpecialSigns(string cSource, bool cSpecialSings, bool cPolishSings)
        //Usuwa znaki specjalne, np. w nazwie pliku
        {
            string cSourceOut;
            cSourceOut = cSource;

            if (cSpecialSings == true)
            {
                cSourceOut = cSourceOut.Replace(":", "");
                cSourceOut = cSourceOut.Replace("!", "");
                cSourceOut = cSourceOut.Replace("\"", "");
                cSourceOut = cSourceOut.Replace("#", "");
                cSourceOut = cSourceOut.Replace("$", "");
                cSourceOut = cSourceOut.Replace("%", "");
                cSourceOut = cSourceOut.Replace("&", "");
                cSourceOut = cSourceOut.Replace("'", "");
                cSourceOut = cSourceOut.Replace("(", "");
                cSourceOut = cSourceOut.Replace(")", "");
                cSourceOut = cSourceOut.Replace("*", "");
                cSourceOut = cSourceOut.Replace(".", "");
                cSourceOut = cSourceOut.Replace("/", "");
                cSourceOut = cSourceOut.Replace(";", "");
                cSourceOut = cSourceOut.Replace("<", "");
                cSourceOut = cSourceOut.Replace("=", "");
                cSourceOut = cSourceOut.Replace(">", "");
                cSourceOut = cSourceOut.Replace("?", "");
                cSourceOut = cSourceOut.Replace("@", "");
                cSourceOut = cSourceOut.Replace("\\", "");
            }
            if (cPolishSings == true)
            {
                cSourceOut = cSourceOut.Replace("Ą", "A");    //Ą
                cSourceOut = cSourceOut.Replace("Ś", "S");    //Ś
                cSourceOut = cSourceOut.Replace("Ę", "E");    //Ę
                cSourceOut = cSourceOut.Replace("Ń", "N");    //Ń
                cSourceOut = cSourceOut.Replace("Ó", "O");    //Ó
                cSourceOut = cSourceOut.Replace("Ć", "C");    //Ć
                cSourceOut = cSourceOut.Replace("Ż", "Z");    //Ź
                cSourceOut = cSourceOut.Replace("Ż", "Z");    //Ż
                cSourceOut = cSourceOut.Replace("ą", "a");    //ą
                cSourceOut = cSourceOut.Replace("ć", "c");    //ć
                cSourceOut = cSourceOut.Replace("ę", "e");    //ę
                cSourceOut = cSourceOut.Replace("ł", "l");    //ł
                cSourceOut = cSourceOut.Replace("ó", "o");    //ó
                cSourceOut = cSourceOut.Replace("ś", "s");    //ś
                cSourceOut = cSourceOut.Replace("ź", "z");    //ź
                cSourceOut = cSourceOut.Replace("ż", "z");    //ż
                cSourceOut = cSourceOut.Replace("Ä", "A");    //Ä
                cSourceOut = cSourceOut.Replace("ë", "e");    //ë
                cSourceOut = cSourceOut.Replace("Ö", "O");    //Ö
                cSourceOut = cSourceOut.Replace("Ü", "U");    //Ü
                cSourceOut = cSourceOut.Replace("ß", "SS");    //ß
                cSourceOut = cSourceOut.Replace("ä", "a");    //ä
                cSourceOut = cSourceOut.Replace("Ë", "E");    //Ë
                cSourceOut = cSourceOut.Replace("ö", "o");    //ö
                cSourceOut = cSourceOut.Replace("ü", "u");    //ü
                cSourceOut = cSourceOut.Replace("Ł", "L");    //Ł
                cSourceOut = cSourceOut.Replace("ń", "n");    //ń
            }
            return cSourceOut;
        }
    }
}