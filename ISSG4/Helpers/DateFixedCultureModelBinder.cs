﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Reflection;

namespace ISSG3.Helpers
{
    public class DateFiexedCultureModelBinder : DefaultModelBinder
    {
        protected override void BindProperty(ControllerContext controllerContext, ModelBindingContext bindingContext, System.ComponentModel.PropertyDescriptor propertyDescriptor)
        {
            if (propertyDescriptor.PropertyType == typeof(DateTime) || 
                propertyDescriptor.PropertyType == typeof(DateTime?))
            {
                try
                {
                    var model = bindingContext.Model;
                    PropertyInfo property = model.GetType().GetProperty(propertyDescriptor.Name);

                    var value = bindingContext.ValueProvider.GetValue(propertyDescriptor.Name);

                    if (value != null)
                    {
                        //System.Globalization.CultureInfo cultureinfo = new System.Globalization.CultureInfo("pl-PL");
                        //var date = DateTime.Parse(value.AttemptedValue, cultureinfo);

                        IFormatProvider plFormatProvider = new System.Globalization.CultureInfo("pl-PL", true);

                        string[] formats = { "dd-MM-yyyy", 
                                             "dd-MM-yyyy hh:mm:ss" };

                        var date = DateTime.ParseExact(value.AttemptedValue, formats, plFormatProvider, System.Globalization.DateTimeStyles.AllowWhiteSpaces);

                        property.SetValue(model, date, null);
                    }
                }
                catch
                {
                    //If something wrong, validation should take care
                }
            }
            else
            {
                base.BindProperty(controllerContext, bindingContext, propertyDescriptor);
            }
        }
    }
}