﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IssgHtmlHelpers
{
    public static class IssgHtmlHelperExtensions
    {
        public static MvcHtmlString SpacesToBreaks(this HtmlHelper helper, string value)
        {
            return new MvcHtmlString(value.Replace(" ", "<br />"));
        }

    }
}