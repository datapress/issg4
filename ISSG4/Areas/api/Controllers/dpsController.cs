﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Xml.Linq;
using System.Web.Security;
using System.Globalization;
using NLog;
using ISSG4.Models;

namespace ISSG4.Areas.api.Controllers
{

    public class dpsController : Controller
    {
        public int CONST_GAZETA_ID = 1;
        public string CONST_subscriberType = "web";

        Logger logger = LogManager.GetCurrentClassLogger();

        [HttpGet]
        public ActionResult Test(string giegie)
        {
            return View(giegie);
        }

        
        [HttpPost]
        public ActionResult SignInWithCredentials(string appId="", string appVersion="", string uuid="")
        {
            bool isAuthenticated = false;
            string authToken = string.Empty;
            /* Request Body: Will contain the following xml document specifiying the user credentials
                <credentials>
                    <emailAddress>joeblank@smooth.com</emailAddress>
                <password>stupid</password>
                </credentials>
             */

            //load xml from request body
            StreamReader stream = new StreamReader(Request.InputStream);
            string x = stream.ReadToEnd();  // added to view content of input stream

            logger.Debug(x);

            XDocument xmlInput = XDocument.Parse(x);

            string emailAddress = xmlInput.Root.Element("emailAddress").Value;
            logger.Debug(emailAddress);
            string password = xmlInput.Root.Element("password").Value;
           // logger.Debug(password);

            if (Membership.ValidateUser(emailAddress, password))
            {
                authToken = createToken();

                saveToken(emailAddress, authToken);

                ViewBag.authToken = authToken;
               
                return View();

            }
            else
            {
                return UnauthorizedAction();
            }
        }


        [HttpGet]
        public ActionResult RenewAuthToken(string authToken, string appId = "", string appVersion = "", string uuid = "")
        {
            /* If an authentication token is present, use it to obtain a new auth token. This eliminates the need for storing
                the user’s account id and password on the device.
             */

            logger.Debug("RenewAuthToken: " + authToken);

            string newToken = renewToken(authToken);

            if (!string.IsNullOrEmpty(newToken))
            {
                ViewBag.authToken = authToken;
                return View();
            }
            else
            {
               return UnauthorizedAction();
            }
        }

        [HttpPost]
        public ActionResult entitlements(string authToken, string appId = "", string appVersion = "", string uuid = "")
        {
            /*Get the list of issues that the user is entitled to.*/

            logger.Debug("entitlements: " + authToken);

            api_tokens oldToken = getToken(authToken);

            if (oldToken != null)
            {
                /* read from post xml */
                StreamReader stream = new StreamReader(Request.InputStream);
                string x = stream.ReadToEnd();  // added to view content of input stream

                logger.Debug(x);

                XDocument xmlInput = XDocument.Parse(x);

                List<FolioViewModel> folios = new List<FolioViewModel>();

                foreach (XElement folio in xmlInput.Root.Elements("folio"))
                {
                    foreach (XElement product in folio.Elements("productId"))
                    {
                        string pid = product.Value;
                        //string coverDateString = product.Value;

                        FolioViewModel f = new FolioViewModel();
                        f.productId = pid;

                        folios.Add(f);
                    }
                }

                //mamy wczytanego xmla do modelu Fol, można na jego podsawie sprawdzić praw do wydań i wydać model Ent

                int userId = getUserIdByName(oldToken.username);

                EntitlementsViewModel model = getEntitlements(oldToken.username, userId, folios);

                return View(model);
            }
            else
            {
                return UnauthorizedAction();
            }
        }

        [HttpGet]
        public ActionResult verifyEntitlement(string authToken, string productId, string coverDate="", string appId = "", string appVersion = "", string uuid = "")
        {
            /*Get the list of issues that the user is entitled to.*/

            logger.Debug("verifyEntitlement: " + authToken+"; productId: "+productId+"; coverDate: "+coverDate);

            api_tokens oldToken = getToken(authToken);

            if (oldToken != null)
            {
                int userId = getUserIdByName(oldToken.username);

                ViewBag.entitled = verifyEntitlement(oldToken.username, userId, productId);

                logger.Debug("verifyEntitlement: " + authToken +"; user: " +oldToken.username+"; entitled: " + ViewBag.entitled);

                return View();
            }
            else
            {
                return UnauthorizedAction();
            }
        }

        public static string[] formats = { "dd-MM-yyyy", "dd-MM-yy", "yyyyMMdd" };

        private bool verifyEntitlement(string username, int userId, string productId)
        {
            int WydanieId = 0;
            int naDzial = 0;
            

            using (Entities ie = new Entities())
            {
             
                Wydanie wyd = ie.Wydanie
                          .Where(d => d.ApiDpsProductId == productId)
                          .FirstOrDefault();

                if (wyd != null)
                {
                    WydanieId = wyd.WydanieId;
                    naDzial = ie.ZamowieniePozycja.Where(m => m.i3_Wydanie.Any(n => n.WydanieId == WydanieId) && m.i3_Zamowienie.issg_user_id == userId).Count();
                }
            }

            return naDzial > 0;
        }

        private int getUserIdByName(string userName)
        {

            int userId = 0;
            using (Entities ie = new Entities())
            {
                userId = ie.Users.SingleOrDefault(d => d.UserName == userName).ispo_user_id;
            }

            return userId;

        }

        private EntitlementsViewModel getEntitlements(string username, int userId, List<FolioViewModel> folios)
        {
            EntitlementsViewModel model = new EntitlementsViewModel();

            List<EntitlementViewModel> list = new List<EntitlementViewModel>();

            using (Entities ie = new Entities())
            {

                foreach (FolioViewModel f in folios)
                {
                    
                    int naDzial = 0;
                    int WydanieId = 0;

                    Wydanie wyd = ie.Wydanie
                        .Where(d=>d.ApiDpsProductId == f.productId)
                        .FirstOrDefault();

                    if (wyd != null)
                        WydanieId = wyd.WydanieId;

                    naDzial = ie.ZamowieniePozycja.Where(m => m.i3_Wydanie.Any(n => n.WydanieId == WydanieId) && m.i3_Zamowienie.issg_user_id == userId).Count();
                    logger.Debug("Sprawdzam getEntitlement f.coverDate: "+ f.coverDate.ToString("yyyyMMdd")+ "; f.productId: "+f.productId + "; WydanieId: "+WydanieId.ToString());

                    if (naDzial > 0)
                    {
                        EntitlementViewModel e = new EntitlementViewModel();
                        e.productid = f.productId;
                        e.subscriberId = username;
                        e.subscriberType = CONST_subscriberType;

                        list.Add(e);

                        logger.Debug("Jest nadział, dodaję "+e.productid+" "+ e.subscriberId+" "+ e.subscriberType);
                    }

                }
            }

            model.entitlements = list;

            return model;

        }

      

        private api_tokens getToken(string authToken)
        {
            using (Entities ie = new Entities())
            {
                return ie.api_tokens.SingleOrDefault(d => d.authToken == authToken);
            }
        }

        private ActionResult UnauthorizedAction()
        {
            string error = "<result httpResponseCode=\"401\" errorCode=\"\"/>";
            Response.AddHeader("Status Code", "401");
            Response.StatusCode = 401;
            Response.ClearContent();
            Response.Write(error);
            Response.End();

            return new HttpStatusCodeResult(401, error);
        }

        private string renewToken(string authToken)
        {
            string newToken = string.Empty;
            using (Entities ie = new Entities())
            {
                api_tokens oldToken = ie.api_tokens.SingleOrDefault(d => d.authToken == authToken);
                if (oldToken != null)
                {
                    newToken = createToken();
                    saveToken(oldToken.username, newToken);
                }
            }

            return newToken;
        }

        private void saveToken(string emailAddress, string authToken)
        {
            using (Entities ie = new Entities())
            {
                api_tokens at = new api_tokens();
                at.username = emailAddress;
                at.authToken = authToken;
                at.data_wpis = DateTime.Now;

                ie.api_tokens.Add(at);
                ie.SaveChanges();
            }
        }

        private string createToken()
        {
            return Guid.NewGuid().ToString();
        }
   
    }



    
}
