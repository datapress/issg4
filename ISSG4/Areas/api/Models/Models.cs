﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Xml.Linq;
using System.Web.Security;
using NLog;
using ISSG4.Models;

namespace ISSG4.Models
{
    public class FolioViewModel
    {
        public string productId { get; set; }
        public DateTime coverDate { get; set; }
    }

    //public class FoliosViewModel
    //{
    //    public List<FolioViewModel> folios { get; set; }
    //}



    public class EntitlementViewModel
    {
        public string productid { get; set; }
        public string subscriberType { get; set; }
        public string subscriberId { get; set; }
    }

    public class EntitlementsViewModel
    {
        public List<EntitlementViewModel> entitlements { get; set; }
    }
}