﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ISSG4
{
    //public class InMemoryCache : ICacheService
    public class InMemoryCache
    {
        static public int DefaultExpirationMinutes { get; set; }

        static public T Get<T>(string cacheID, Func<T> getItemCallback) where T : class
        {
            return Get(cacheID, DefaultExpirationMinutes, getItemCallback);
        }

        static public T Get<T>(string cacheID, int MinutesToExpire, Func<T> getItemCallback) where T : class
        {
            T item = HttpRuntime.Cache.Get(cacheID) as T;
            if (item == null)
            {
                item = getItemCallback();
                HttpContext.Current.Cache.Insert(cacheID, item, null, DateTime.Now.AddMinutes(MinutesToExpire), System.Web.Caching.Cache.NoSlidingExpiration);
            }
            return item;
        }
    }

    //interface ICacheService
    //{
    //    static T Get<T>(string cacheID, Func<T> getItemCallback) where T : class;
    //}
}