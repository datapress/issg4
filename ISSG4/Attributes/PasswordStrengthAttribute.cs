﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace ISSG3
{
    public class PasswordStrengthAttribute : RegularExpressionAttribute
    {

        public PasswordStrengthAttribute() : base(getPasswordStrengthRegex())
        {
            if (System.Configuration.ConfigurationManager.AppSettings["PASSWORD_INFO"] != null)
                ErrorMessage = System.Configuration.ConfigurationManager.AppSettings["PASSWORD_INFO"].ToString();
        }

        private static string getPasswordStrengthRegex()
        {
            if (System.Configuration.ConfigurationManager.AppSettings["PASSWORD_REGEX"] == null) return "";
            return System.Configuration.ConfigurationManager.AppSettings["PASSWORD_REGEX"].ToString();
        }

    }
}