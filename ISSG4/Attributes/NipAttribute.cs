﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace ISSG3
{
    public class NipAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            if (value == null)
            {
                return true;
            }

            var nip = (string)value;
            nip = nip.Replace(" ", "").Replace("-", "");

            return ValidateNip(nip);
        }

        private bool ValidateNip(string nip)
        {
            if (nip.Length != 10) return false;

            try { Int64.Parse(nip); }
            catch { return false; }

            int[] weights = new int[] { 6, 5, 7, 2, 3, 4, 5, 6, 7 };
            int sum = 0;
            for (int i = 0; i < weights.Length; i++)
                sum += int.Parse(nip.Substring(i, 1)) * weights[i];

            return (sum % 11) == int.Parse(nip.Substring(9, 1));
        }
    }
}