﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace ISSG3
{
    public class MXRecordLookupAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            if (value == null)
            {
                return true;
            }

            var email = (string)value;

            return isEmailMXValid(email);
        }

        private bool isEmailMXValid(string email)
        {
            bool ret = false;
            try
            {
                string[] mx_dns_records = TensaiLabs.DNS.MXLookUp.GetMXRecords(email);
                if (!mx_dns_records[0].Equals("MX Record Not Found")) ret = true;
            }
            catch
            {
                // Do nothing
            }
            return ret;
        }
    }
}