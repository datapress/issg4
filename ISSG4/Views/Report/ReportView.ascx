﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" CodeBehind="ReportView.ascx.cs" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
 <form runat="server" id="frm">
 <script runat="server">
     protected void Page_Load(object sender, EventArgs e)
     {
         string rpt_id = ViewBag.RouteDataPartId;
         ReportViewer1.ProcessingMode = ProcessingMode.Local;

         using (var context = new ISSG4.Models.Entities())
         {

             /* wstupna deklaracja zmiennych */
             string rptname = "1001eKomisy";
             string rpt_def = "~/Reports/Report" + rptname + ".rdlc";

             ReportDataSource datasource;
             DateTime dtod;
             DateTime dtdo;
             DateTime dtdo_param_linq;
             string KodOperator = string.Empty;
             
             IFormatProvider plformat = new System.Globalization.CultureInfo("pl-PL", true);
             string[] expectedFormats = { "d-MM-yyyy", "d-MM-yy", "yyyy-MM-d" };

             switch (rpt_id)
             {
                 /* *******************************************************************/
                 /* ******************** [1001] eKomisy ******************************/
                 /* *******************************************************************/                     
                 case "eKomisy":
                     rptname = "1001eKomisy_2.rdlc";
                     //int gazeta_id = int.Parse(TempData["gazeta_id"].ToString());
                     try
                     {
                         dtod = DateTime.ParseExact(TempData["data_od"].ToString(), expectedFormats, plformat, System.Globalization.DateTimeStyles.AllowWhiteSpaces);
                         dtdo = DateTime.ParseExact(TempData["data_do"].ToString(), expectedFormats, plformat, System.Globalization.DateTimeStyles.AllowWhiteSpaces);
                         dtdo_param_linq = DateTime.ParseExact(TempData["data_do"].ToString(), expectedFormats, plformat, System.Globalization.DateTimeStyles.AllowWhiteSpaces).AddDays(1);
                         KodOperator = TempData["operator"].ToString();
                     }
                     catch (Exception)
                     {
                         return;
                     }
                     
                     var dane1 = (from v in context.view_rpt_1001_eKomisy
                                  where v.KodCzas >= dtod && v.KodCzas <= dtdo_param_linq
                                 // && System.Data.Objects.EntityFunctions.TruncateTime(v.KodCzas) <= v.WydanieData
                                    && System.Data.Entity.DbFunctions.TruncateTime(v.KodCzas) <= v.WydanieData
                                  && v.KodOperator == KodOperator
                                  
                                 select v).ToList();

                     rpt_def = "~/Reports/" + rptname;
                     ReportViewer1.LocalReport.ReportPath = Server.MapPath(rpt_def);
                     ReportViewer1.LocalReport.DataSources.Clear();
                     datasource = new ReportDataSource("DataSet1", dane1);
                     ReportViewer1.LocalReport.DataSources.Add(datasource);

                     ReportParameter[] param = new ReportParameter[4];
                     param[0] = new ReportParameter("data_od", dtod.ToShortDateString(), false);
                     param[1] = new ReportParameter("data_do", dtdo.ToShortDateString(), false);
                     param[2] = new ReportParameter("login", HttpContext.Current.User.Identity.Name, false);
                     param[3] = new ReportParameter("operator", KodOperator, false);
                     ReportViewer1.LocalReport.SetParameters(param);
                   

                     ReportViewer1.LocalReport.Refresh();
                     break;
                 /* *******************************************************************/
                 /* ******************** [1002] ePrenumeraty**************************/
                 /* *******************************************************************/                     
                 case "ePrenumeraty":
                     rptname = "1002ePrenumeraty_2.rdlc";
                    try {
                            dtod = DateTime.ParseExact(TempData["data_od"].ToString(), expectedFormats, plformat, System.Globalization.DateTimeStyles.AllowWhiteSpaces);
                            dtdo = DateTime.ParseExact(TempData["data_do"].ToString(), expectedFormats, plformat, System.Globalization.DateTimeStyles.AllowWhiteSpaces);
                            dtdo_param_linq = DateTime.ParseExact(TempData["data_do"].ToString(), expectedFormats, plformat, System.Globalization.DateTimeStyles.AllowWhiteSpaces).AddDays(1);
                        }
                         catch (Exception){
                             return;
                        }

                    var dane2 = (from v in context.view_rpt_1002_ePrenumeraty
                                 where (v.PlatnoscCzas >= dtod && v.PlatnoscCzas <= dtdo_param_linq)
                                 /*&& (v.RodzajOkresuMiesiac > 0 || v.RodzajOkresuDzien > 1)*/
                                 select v).ToList();

                     rpt_def = "~/Reports/" + rptname;
                     ReportViewer1.LocalReport.ReportPath = Server.MapPath(rpt_def);
                     ReportViewer1.LocalReport.DataSources.Clear();
                     datasource = new ReportDataSource("DataSet1", dane2);
                     ReportViewer1.LocalReport.DataSources.Add(datasource);

                     ReportParameter[] param2 = new ReportParameter[3];
                     param2[0] = new ReportParameter("data_od", dtod.ToShortDateString(), false);
                     param2[1] = new ReportParameter("data_do", dtdo.ToShortDateString(), false);
                     param2[2] = new ReportParameter("login", HttpContext.Current.User.Identity.Name, false);
                     ReportViewer1.LocalReport.SetParameters(param2);

                     ReportViewer1.LocalReport.Refresh();
                     break;
                     
                    /********************************************************************/                     
                    /********************* [1003] Szczegolowy ***************************/
                   /********************************************************************/                     
                 case "eSzczegolowy":
                     rptname = "1003eSzczegolowy_2.rdlc";
                     try
                     {
                         dtod = DateTime.ParseExact(TempData["data_od"].ToString(), expectedFormats, plformat, System.Globalization.DateTimeStyles.AllowWhiteSpaces);
                         dtdo = DateTime.ParseExact(TempData["data_do"].ToString(), expectedFormats, plformat, System.Globalization.DateTimeStyles.AllowWhiteSpaces);
                         dtdo_param_linq = DateTime.ParseExact(TempData["data_do"].ToString(), expectedFormats, plformat, System.Globalization.DateTimeStyles.AllowWhiteSpaces).AddDays(1);
                     }
                     catch (Exception)
                     {
                         return;
                     }

                     var dane3 = (from v in context.view_rpt_1002_ePrenumeraty
                                  where v.PlatnoscCzas >= dtod && v.PlatnoscCzas <= dtdo_param_linq 
                                  select v).ToList();

                     rpt_def = "~/Reports/" + rptname;
                     ReportViewer1.LocalReport.ReportPath = Server.MapPath(rpt_def);
                     ReportViewer1.LocalReport.DataSources.Clear();
                     datasource = new ReportDataSource("DataSet1", dane3);
                     ReportViewer1.LocalReport.DataSources.Add(datasource);

                     ReportParameter[] param3 = new ReportParameter[3];
                     param3[0] = new ReportParameter("data_od", dtod.ToShortDateString(), false);
                     param3[1] = new ReportParameter("data_do", dtdo.ToShortDateString(), false);
                     param3[2] = new ReportParameter("login", HttpContext.Current.User.Identity.Name, false);
                     ReportViewer1.LocalReport.SetParameters(param3);

                     ReportViewer1.LocalReport.Refresh();
                     break;

                 /********************************************************************/
                 /********************* [1004] Darmowe ***************************/
                 /********************************************************************/
                 case "eDarmowe":
                     rptname = "1004eDarmowe_2.rdlc";
                     //int gazeta_id = int.Parse(TempData["gazeta_id"].ToString());
                     try
                     {
                         dtod = DateTime.ParseExact(TempData["data_od"].ToString(), expectedFormats, plformat, System.Globalization.DateTimeStyles.AllowWhiteSpaces);
                         dtdo = DateTime.ParseExact(TempData["data_do"].ToString(), expectedFormats, plformat, System.Globalization.DateTimeStyles.AllowWhiteSpaces);
                         dtdo_param_linq = DateTime.ParseExact(TempData["data_do"].ToString(), expectedFormats, plformat, System.Globalization.DateTimeStyles.AllowWhiteSpaces).AddDays(1);
                     }
                     catch (Exception)
                     {
                         return;
                     }
                     
                     var dane4 = (from v in context.view_rpt_1004_ePobrania
                                  where v.PobranieData >= dtod && v.PobranieData <= dtdo_param_linq
                                 select v).ToList();

                     rpt_def = "~/Reports/" + rptname;
                     ReportViewer1.LocalReport.ReportPath = Server.MapPath(rpt_def);
                     ReportViewer1.LocalReport.DataSources.Clear();
                     datasource = new ReportDataSource("DataSet1", dane4);
                     ReportViewer1.LocalReport.DataSources.Add(datasource);

                     ReportParameter[] param4 = new ReportParameter[3];
                     param4[0] = new ReportParameter("data_od", dtod.ToShortDateString(), false);
                     param4[1] = new ReportParameter("data_do", dtdo.ToShortDateString(), false);
                     param4[2] = new ReportParameter("login", HttpContext.Current.User.Identity.Name, false);
                     ReportViewer1.LocalReport.SetParameters(param4);
                   

                     ReportViewer1.LocalReport.Refresh();
                     
                     break;

                 /********************************************************************/
                 /********************* [1004] Darmowe ***************************/
                 /********************************************************************/
                 case "eImportowane":
                     rptname = "1005eImportowane.rdlc";
                     //int gazeta_id = int.Parse(TempData["gazeta_id"].ToString());
                     try
                     {
                         dtod = DateTime.ParseExact(TempData["data_od"].ToString(), expectedFormats, plformat, System.Globalization.DateTimeStyles.AllowWhiteSpaces);
                         dtdo = DateTime.ParseExact(TempData["data_do"].ToString(), expectedFormats, plformat, System.Globalization.DateTimeStyles.AllowWhiteSpaces);
                         dtdo_param_linq = DateTime.ParseExact(TempData["data_do"].ToString(), expectedFormats, plformat, System.Globalization.DateTimeStyles.AllowWhiteSpaces).AddDays(1);
                     }
                     catch (Exception)
                     {
                         return;
                     }

                     var dane5 = (from v in context.view_rpt_1005_eImportowane
                                  where v.PobranyPdfCzas >= dtod && v.PobranyPdfCzas <= dtdo_param_linq
                                  select v).ToList();

                     rpt_def = "~/Reports/" + rptname;
                     ReportViewer1.LocalReport.ReportPath = Server.MapPath(rpt_def);
                     ReportViewer1.LocalReport.DataSources.Clear();
                     datasource = new ReportDataSource("DataSet1", dane5);
                     ReportViewer1.LocalReport.DataSources.Add(datasource);

                     ReportParameter[] param5 = new ReportParameter[3];
                     param5[0] = new ReportParameter("data_od", dtod.ToShortDateString(), false);
                     param5[1] = new ReportParameter("data_do", dtdo.ToShortDateString(), false);
                     param5[2] = new ReportParameter("login", HttpContext.Current.User.Identity.Name, false);
                     ReportViewer1.LocalReport.SetParameters(param5);


                     ReportViewer1.LocalReport.Refresh();

                     break;

                 /* *******************************************************************/
                 /* ******************** [1006] ReczneAktywacje**************************/
                 /* *******************************************************************/
                 case "ReczneAktywacje":
                     rptname = "1006ReczneAktywacje.rdlc";
                     try
                     {
                         dtod = DateTime.ParseExact(TempData["data_od"].ToString(), expectedFormats, plformat, System.Globalization.DateTimeStyles.AllowWhiteSpaces);
                         dtdo = DateTime.ParseExact(TempData["data_do"].ToString(), expectedFormats, plformat, System.Globalization.DateTimeStyles.AllowWhiteSpaces);
                         dtdo_param_linq = DateTime.ParseExact(TempData["data_do"].ToString(), expectedFormats, plformat, System.Globalization.DateTimeStyles.AllowWhiteSpaces).AddDays(1);
                     }
                     catch (Exception)
                     {
                         return;
                     }

                     var dane6 = (from v in context.view_rpt_1006_ReczneAktywacje
                                  where (v.PlatnoscCzas >= dtod && v.PlatnoscCzas <= dtdo_param_linq)
                                  /*&& (v.RodzajOkresuMiesiac > 0 || v.RodzajOkresuDzien > 1)*/
                                  select v).ToList();

                     rpt_def = "~/Reports/" + rptname;
                     ReportViewer1.LocalReport.ReportPath = Server.MapPath(rpt_def);
                     ReportViewer1.LocalReport.DataSources.Clear();
                     datasource = new ReportDataSource("DataSet1", dane6);
                     ReportViewer1.LocalReport.DataSources.Add(datasource);

                     ReportParameter[] param6 = new ReportParameter[3];
                     param6[0] = new ReportParameter("data_od", dtod.ToShortDateString(), false);
                     param6[1] = new ReportParameter("data_do", dtdo.ToShortDateString(), false);
                     param6[2] = new ReportParameter("login", HttpContext.Current.User.Identity.Name, false);
                     ReportViewer1.LocalReport.SetParameters(param6);

                     ReportViewer1.LocalReport.Refresh();
                     break;

                 /* *******************************************************************/
                 /* ******************** [1001] komisy Arch  **************************/
                 /* *******************************************************************/

                 case "eKomisyArch":
                     rptname = "1001eKomisyArch_2.rdlc";
                     //int gazeta_id = int.Parse(TempData["gazeta_id"].ToString());
                     try
                     {
                         dtod = DateTime.ParseExact(TempData["data_od"].ToString(), expectedFormats, plformat, System.Globalization.DateTimeStyles.AllowWhiteSpaces);
                         dtdo = DateTime.ParseExact(TempData["data_do"].ToString(), expectedFormats, plformat, System.Globalization.DateTimeStyles.AllowWhiteSpaces);
                         dtdo_param_linq = DateTime.ParseExact(TempData["data_do"].ToString(), expectedFormats, plformat, System.Globalization.DateTimeStyles.AllowWhiteSpaces).AddDays(1);
                         KodOperator = TempData["operator"].ToString();
                     }
                     catch (Exception)
                     {
                         return;
                     }

                     var dane1Arch = (from v in context.view_rpt_1001_eKomisy
                                  where v.KodCzas >= dtod && v.KodCzas <= dtdo_param_linq
                                 // && System.Data.Objects.EntityFunctions.TruncateTime(v.KodCzas) > v.WydanieData
                                    && System.Data.Entity.DbFunctions.TruncateTime(v.KodCzas) > v.WydanieData
                                  && v.KodOperator == KodOperator

                                  select v).ToList();

                     rpt_def = "~/Reports/" + rptname;
                     ReportViewer1.LocalReport.ReportPath = Server.MapPath(rpt_def);
                     ReportViewer1.LocalReport.DataSources.Clear();
                     datasource = new ReportDataSource("DataSet1", dane1Arch);
                     ReportViewer1.LocalReport.DataSources.Add(datasource);

                     ReportParameter[] param1Arch = new ReportParameter[4];
                     param1Arch[0] = new ReportParameter("data_od", dtod.ToShortDateString(), false);
                     param1Arch[1] = new ReportParameter("data_do", dtdo.ToShortDateString(), false);
                     param1Arch[2] = new ReportParameter("login", HttpContext.Current.User.Identity.Name, false);
                     param1Arch[3] = new ReportParameter("operator", KodOperator, false);
                     ReportViewer1.LocalReport.SetParameters(param1Arch);


                     ReportViewer1.LocalReport.Refresh();
                     break;

                 default:
                     break;
             }
         }
     }
        </script>

    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <rsweb:ReportViewer ID="ReportViewer1" runat="server" AsyncRendering="false"
     Width="100%" Height="100%" ShowRefreshButton="false" ShowBackButton="false" 
     ShowFindControls="false" ShowPageNavigationControls="false"
     InteractivityPostBackMode="AlwaysAsynchronous" >
    </rsweb:ReportViewer>

</form>
