﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ISSG4.Models;
using System.Web.Security;
using System.ComponentModel.DataAnnotations;

namespace ISSG4.Controllers
{
    public class PrenumerataController : Controller
    {
        //
        // GET: /Prenumerata/
        //
        public ActionResult Index()
        {
            List<viewZamowienieJoined> zamowienia = new List<viewZamowienieJoined>();

            using (Entities ie = new Entities())
            {
                if (User.Identity.IsAuthenticated) {
                    int userId = (int)Membership.GetUser().ProviderUserKey;
                    zamowienia = ie.viewZamowienieJoined.Where(m => m.issg_user_id == userId).OrderBy(m => m.ZamowienieData).ToList();
                }
            }

            return View(zamowienia);
        }

        public class NowaViewModel
        {
            public List<SelectListItem> Gazety { get; set; }
            public List<SelectListItem> Produkty { get; set; }
            public List<SelectListItem> Dodatki { get; set; }
            public List<SelectListItem> Standardy { get; set; }

            //public List<Produkt> produkt_list { get; set; }

            [Required(ErrorMessage="Musi zostać wybrana data początku prenumeraty")]
            //[RegularExpression(@"20[0-5]\d-((0[1-9])|(1[0-2]))-((0[1-9])|([1-2][0-9])|(3[0-1]))", ErrorMessage = "Data musi być w formacie yyyy-mm-dd.")]
            public string Od { get; set; }
            
            [Required]
            public int GazetaId { get; set; }

            [Required]
            public int ProduktId { get; set; }
            public int DodatekId { get; set; }

            public void FillLists()
            {
                Gazety = Cennik.getCachedList().Select(m => new SelectListItem { Text = m.GazetaNazwa, Value = m.GazetaId.ToString(), Selected = false })
                    .Distinct(new SelectListItemComparer())
                    .OrderBy(d=>d.Value)
                    .ToList();
                int gazetaId = int.Parse(Gazety.ElementAt(0).Value);

                Produkty = Cennik.getCachedList()
                    .Where(m => m.GazetaId == gazetaId && m.ProduktRodzajBiezace == true)
                    .OrderBy(m=>m.CennikBrutto)
                    .Select(m => new SelectListItem { Value = m.ProduktId.ToString(), Text = m.ProduktNazwa +" - "+ m.CennikBrutto.ToString("0.00 zł"),
                        Selected = false })
                    .ToList();

                using (Entities ie = new Entities())
                {
                    var gazeta = ie.Gazeta.Include("i3_Dodatek").Where(m => m.GazetaId == gazetaId).Single();
                    //Dodatki = gazeta.i3_Dodatek.Where(m => m.DodatekAktywny == true)
                     //   .Select(m => new SelectListItem { Value = m.DodatekId.ToString(), Text = m.DodatekNazwa, Selected = false }).ToList();
                    
                    //Dodatki = ie.Dodatek.Where(m => m.DodatekAktywny == true).Select(m => new SelectListItem { Value = m.DodatekId.ToString(), Text = m.DodatekNazwa, Selected = false }).ToList();
                    List<Dodatek> dodatek_list = ie.Dodatek
                        .Where(m => m.DodatekAktywny == true)
                        .ToList();

                       Dodatki = dodatek_list
                        .Select(m => new SelectListItem { Value = m.DodatekId.ToString(), Text = m.DodatekNazwa, Selected = false })
                        .ToList();

                //    produkt_list = ie.Produkt
                }

                if (Dodatki.Count() == 0) Dodatki.Add(new SelectListItem { Text = "---", Value = "0" });
            }
        }

        public class SelectListItemComparer : EqualityComparer<SelectListItem>
        {
            public override bool Equals(SelectListItem x, SelectListItem y)
            {
                return x.Value.Equals(y.Value);
            }

            public override int GetHashCode(SelectListItem obj)
            {
                return obj.Value.GetHashCode();
            }
        }

        //
        // GET: /Prenumerata/Nowa
        //
        public ActionResult Nowa()
        {
            NowaViewModel model = new NowaViewModel();
            //model.Gazety = Cennik.getCachedList().Select(m => new SelectListItem { Text = m.GazetaNazwa, Value = m.GazetaId.ToString(), Selected = false }).Distinct(new SelectListItemComparer()).ToList();
            //int gazetaId = int.Parse(model.Gazety.ElementAt(0).Value);
            //model.Produkty = Cennik.getCachedList().Where(m => m.GazetaId == gazetaId).Select(m => new SelectListItem { Value = m.ProduktId.ToString(), Text = m.ProduktNazwa, Selected = false }).ToList();
            //using (Entities ie = new Entities())
            //{
            //    var gazeta = ie.Gazeta.Include("i3_Dodatek").Where(m => m.GazetaId == gazetaId).Single();
            //    model.Dodatki = gazeta.i3_Dodatek.Where(m => m.DodatekAktywny == true).Select(m => new SelectListItem { Value = m.DodatekId.ToString(), Text = m.DodatekNazwa, Selected = false }).ToList();
            //}

            //if (model.Dodatki.Count() == 0) model.Dodatki.Add(new SelectListItem { Text = "---", Value = "0" });

            model.FillLists();

            //model.Od = DateTime.Today.ToString("yyyy-MM-dd");
            model.Od = DateTime.Today.ToString("dd-MM-yyyy");

            return View(model);
        }

        //
        // GET: /Prenumerata/Ukryj/xx
        //
        public ActionResult Ukryj()
        {
            if (RouteData.Values["id"] == null )
                return RedirectToAction("Index");

            string id = RouteData.Values["id"].ToString();
            int prenum_id=0;

            if (int.TryParse(RouteData.Values["id"].ToString(), out prenum_id))
            {
                using (Entities ie = new Entities())
                {
                    ZamowieniePozycja prenum = ie.ZamowieniePozycja.Include("i3_Zamowienie").SingleOrDefault(m => m.ZamowieniePozycjaId == prenum_id);

                    if (prenum != null)
                    {
                        int userId = (int)Membership.GetUser().ProviderUserKey;
                        if (prenum.i3_Zamowienie.issg_user_id == userId) /* na wszelki wypadek by ktoś nie ukrył obcej prenumeraty */
                        {
                            prenum.ZamowieniePozycjaWidoczna = "n";
                            ie.SaveChanges();
                        }
                    }
                }
            }
            return RedirectToAction("Index");
        }

        [HttpPost]
        //public ActionResult Nowa(int GazetaId, int ProduktId, string Od, int DodatekId)
        public ActionResult Nowa(NowaViewModel model)
        {
            if (!ModelState.IsValid)
            {
                model.FillLists();
                return View(model);
            }

            //DateTime od = DateTime.ParseExact(model.Od, "yyyy-MM-dd", new System.Globalization.CultureInfo("pl-PL"));
            DateTime od = DateTime.ParseExact(model.Od, "dd-MM-yyyy", new System.Globalization.CultureInfo("pl-PL"));

            int cennikId = Cennik.getCachedList().Where(m => m.ProduktId == model.ProduktId && m.GazetaId == model.GazetaId).Select(m => m.CennikId).Single();

            Koszyk.DodajDoKoszyka(cennikId, od, model.DodatekId);
            
            string issg_layout_ver = ISSG3.Utils.getOpcjeValue("issg_layout_ver");

            if (issg_layout_ver == "kurier")
            {
                return RedirectToAction("Index", "Koszyk");
            }
            else
            {
                return View("Potwierdzenie");
            }
            
        }

        [HttpPost]
        public ActionResult Skopiuj(int zp_id, string username)
        {
            /* wywoływane po tworzeniu nowego usera w backendzie, by mu naszą prenumeratę przepisać. */
            Zamowienie zam_new = new Zamowienie();
            ZamowieniePozycja zampoz_new = new ZamowieniePozycja();

            int userId = (int)Membership.GetUser(username).ProviderUserKey;

            using (Entities db = new Entities())
            {
                ZamowieniePozycja zampoz = db.ZamowieniePozycja.SingleOrDefault(m=>m.ZamowieniePozycjaId == zp_id);
                Zamowienie zam;
                if(zampoz != null)
                {
                    zam = db.Zamowienie.Single(d => d.ZamowienieId == zampoz.ZamowienieId);
                    
                    //zam_new = zam;
                    zam_new.ZamowienieBrutto = zam.ZamowienieBrutto;
                    zam_new.ZamowienieNetto = zam.ZamowienieNetto;
                    zam_new.ZamowienieProcRd = zam.ZamowienieProcRd;
                    zam_new.ZamowieniePrzedRd = zam.ZamowieniePrzedRd;
                    zam_new.ZamowienieVat = zam.ZamowienieVat;
                    zam_new.ZamowienieWartoscRd = zam.ZamowienieWartoscRd;
                    zam_new.ZamowienieZablokowane = false;
                    zam_new.ZamowienieRecznaAktywacja = false;

                    zam_new.issg_user_id = userId;
                    zam_new.ZamowienieData = System.DateTime.Now;

                    //zampoz_new = zampoz;
                    zampoz_new.ZamowienieId = zam_new.ZamowienieId;
                    zampoz_new.ZamowieniePozycjaWidoczna = "t";
                    
                    zampoz_new.ZamowieniePozycjaOd = zampoz.ZamowieniePozycjaOd;
                    zampoz_new.ZamowieniePozycjaDo = zampoz.ZamowieniePozycjaDo;
                    zampoz_new.ZamowieniePozycjaNetto = zampoz.ZamowieniePozycjaNetto;
                    zampoz_new.ZamowieniePozycjaProcRD = zampoz.ZamowieniePozycjaProcRD;
                    zampoz_new.ZamowieniePozycjaVat = zampoz_new.ZamowieniePozycjaVat;
                    zampoz_new.ZamowieniePozycjaBrutto = zampoz.ZamowieniePozycjaBrutto;
                    zampoz_new.VatId = zampoz.VatId;
                    zampoz_new.ZamowieniePozycjaProcRD = zampoz.ZamowieniePozycjaProcRD;
                    zampoz_new.ZamowieniePozycjaWartoscRD = zampoz.ZamowieniePozycjaWartoscRD;
                    zampoz_new.CennikId = zampoz.CennikId;
                    

                    db.Zamowienie.Add(zam_new);
                    db.ZamowieniePozycja.Add(zampoz_new);

                    db.SaveChanges();

                    /*trigger na update*/

                    zam_new.ZamowienieRecznaAktywacja = true;
                    db.SaveChanges();

                    
                }
            }

           // JsonResult js = new JsonResult();
           // js.Data = "{responseText:\"Ok\"}";

            return Json(new
            {
                responseText = "OK",
                color = "xxx"
            }
          , JsonRequestBehavior.AllowGet);
        }

        public class GetProduktViewModel
        {
            public List<ProduktShort> Produkty { get; set; }
            public List<DodatekShort> Dodatki { get; set; }
        }

        public class ProduktShort
        {
            public int ProduktId { get; set; }
            public string ProduktNazwa { get; set; }
        }

        public class DodatekShort
        {
            public int DodatekId { get; set; }
            public string DodatekNazwa { get; set; }
        }

        [HttpPost]
        public ActionResult GetProdukt(int id)
        {
            if (Request.IsAjaxRequest())
            {
                GetProduktViewModel model = new GetProduktViewModel();
                model.Produkty = Cennik.getCachedList()
                    .Where(m => m.GazetaId == id && m.ProduktRodzajBiezace == true)
                    .Select(m => new ProduktShort { ProduktId = m.ProduktId, 
                        ProduktNazwa = m.ProduktNazwa + " - " + m.CennikBrutto.ToString("0.00 zł") }).ToList();

                using (Entities ie = new Entities()) {
                    var gazeta = ie.Gazeta.Include("i3_Dodatek").Where(m => m.GazetaId == id).Single();

                    //model.Dodatki = gazeta.i3_Dodatek.Where(m => m.DodatekAktywny == true).Select(m => new DodatekShort { DodatekId = m.DodatekId, DodatekNazwa = m.DodatekNazwa }).ToList();
                    //od 2018-05-30 dodatki są dostępne we wszystkich gazetach
                    
                    List<Dodatek> DodatekList = ie.Dodatek.Where(m => m.DodatekAktywny == true).ToList();
                    model.Dodatki = DodatekList.Select(m => new DodatekShort { DodatekId = m.DodatekId, DodatekNazwa = m.DodatekNazwa }).ToList();
                }
                if (model.Dodatki.Count() == 0) model.Dodatki.Add(new DodatekShort { DodatekNazwa = "---", DodatekId = 0 });
                return Json(model);
            }

            return new EmptyResult();
        }

    }
}
