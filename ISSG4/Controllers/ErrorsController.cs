﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ISSG3.Controllers
{
    public class ErrorController : Controller
    {
        public ActionResult Static404()
        {
            Response.StatusCode = 404;
            Response.TrySkipIisCustomErrors = true;
            return View();
        }

        public ActionResult DefaultError()
        {
            return View();
        }
    }


 /*public sealed class ErrorsController : Controller
{
    ActionResult result;

    object model =  Request.Url.PathAndQuery;

    if (!Request.IsAjaxRequest())
        result = View(model);
    else
        result = PartialView("_NotFound", model);

    return result;
}*/
}
