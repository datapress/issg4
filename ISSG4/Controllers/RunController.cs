﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ISSG4.Models;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using NLog;

namespace ISSG3.Controllers
{
    public class RunController : Controller
    {
        public string ImportKod()
        {
            string ret = "Import kodów z pliku ";
            try
            {
                Utils.ReadFile();
            }
            catch (Exception ex)
            {
                ret = ret+" "+ ex.Message;
            }
            return ret;
        }

        public string IndeksujTresciWydania(int id)
        {
            string ret = "Indeksacja treści atykułów do późniejszgo wyszukiwania.";
            try
            {
                Utils.IndexTresci(id);
            }
            catch (Exception ex)
            {
                ret = ret + " " + ex.Message;
            }
            return ret;
        }

        public string IndeksujTresciOtwarcie(string param)
        {
            /* akcja hurtowej indeksacji na początek */
            string ret = "Start hurtowej indeksacji ";

            if (param != "tajne_h_a_s_l_o")
                return "ups";

            NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
            logger.Debug(ret);

            /* lista wydań */
            List<int> wyd_ids = new List<int>();

            using (Entities db = new Entities())
            {
                wyd_ids = db.i3_ArtArtykul.Select(d => d.WydanieId).Distinct().ToList();
            }

            string lista_id = string.Join(",", wyd_ids);
            logger.Debug("Start indeksu wydań: "+lista_id);

            try
            {
                foreach (int id in wyd_ids)
                {
                    logger.Debug("START Indeksowanie wydania z pętli: "+id.ToString());
                    Utils.IndexTresci(id);
                    logger.Debug("STOP Indeksowanie wydania z pętli: " + id.ToString());
                }
            }

            catch (Exception ex)
            {
                ret = ret + " " + ex.Message;
            }

            logger.Debug("Koniec hurtowej indeksacji na otwarcie.");

            return ret;
        }

    }
}
