﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ISSG3.Controllers
{
    public class PrzyciskController : Controller
    {
        //
        // GET: /Przycisk/{Text}
        [OutputCache(Duration = 1800, VaryByParam = "Text")] // 30min
        public ActionResult Index(string Text)
        {
            return View("Przycisk", new { Text = Text });
        }

        //
        // GET: /Przycisk/PrzyciskLight/{Text}
        [OutputCache(Duration = 1800, VaryByParam = "Text")] // 30min
        public ActionResult Yellow(string Text)
        {
            return View("PrzyciskLight", new { Text = Text });
        }

    }
}
