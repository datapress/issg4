﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ISSG3.Models;

namespace ISSG3.Controllers
{
    [Authorize(Roles = "Administratorzy,Kolportaż")]
    public class ReportController : Controller
    {
        //
        // GET: /Report/

        
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Param()
        {
            ViewBag.RouteDataPartId = RouteData.Values["id"];

            switch (RouteData.Values["id"].ToString())
            {
                case "eKomisy":
                    ViewBag.RaportNazwa = "[1001] Sprzedaż - płatności SMS (bieżące)";
                    break;
                case "ePrenumeraty":
                    ViewBag.RaportNazwa = "[1002] Sprzedaż - płatności on-line";
                    break;
                case "eSzczegolowy":
                    ViewBag.RaportNazwa = "[1003] Sprzedaż - płatności on-line (szczegółowy)";
                    break;
                case "eDarmowe":
                    ViewBag.RaportNazwa = "[1004] Pobrania Darmowe";
                    break;
                case "eImportowane":
                    ViewBag.RaportNazwa = "[1005] Kody importowane";
                    break;
                case "ReczneAktywacje":
                    ViewBag.RaportNazwa = "[1006] Ręczne aktywacje";
                    break;
                case "eKomisyArch":
                    ViewBag.RaportNazwa = "[1001] Sprzedaż - płatności SMS (archiwalne)";
                    break;
                default:
                    break;
            }

            return View();
        }
        /*
        [HttpPost]
        public ActionResult Param(string data_od, string data_do)
        {
            DateTime dt_od, dt_do;
            IFormatProvider plformat = new System.Globalization.CultureInfo("pl-PL", true);
            string[] expectedFormats = { "d-MM-yyyy", "d-MM-yy","yyyy-MM-d" };
            try {
                dt_od = DateTime.ParseExact(data_od, expectedFormats, plformat, System.Globalization.DateTimeStyles.AllowWhiteSpaces);
                dt_do = DateTime.ParseExact(data_do, expectedFormats, plformat, System.Globalization.DateTimeStyles.AllowWhiteSpaces);
            }
             catch {
                 ViewBag.Komunikat = "Wystąpił błąd podczas konwersji daty. Popraw dane.";
                 return RedirectToAction("Param", "Report", RouteData.Values);
            }

            ViewBag.RouteDataPartId = RouteData.Values["id"]; 
            TempData["gazeta_id"] = Gazeta.ToString(); 
            TempData["data_od"] = data_od.ToString(); 
            TempData["data_do"] = data_do.ToString(); 
            

            return RedirectToAction("Show", "Report",RouteData.Values);
           // return Show(data_od.ToString(), data_do.ToString());
        }*/


        public ActionResult Show()
        {
            ViewBag.RouteDataPartId = RouteData.Values["id"];

            if (Request.QueryString["data_od"] != null && Request.QueryString["data_do"] != null)
            {
                TempData["data_od"] = Request.QueryString["data_od"].ToString();
                TempData["data_do"] = Request.QueryString["data_do"].ToString();
                TempData["operator"] = Request.QueryString["operator"].ToString();
            }

            return View();
        }

    }
}
