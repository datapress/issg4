﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using ISSG4.Models;
using System.Web.Services.Protocols;
using NLog;
using DotNetOpenMail;
using DotNetOpenMail.SmtpAuth;
using System.Transactions;
using System.Configuration;
using Custom.Membership;


namespace ISSG4.Controllers
{
    public class KontoController : Controller
    {
        Logger logger = LogManager.GetCurrentClassLogger();
        //private LocalUniMembershipProvider um = (LocalUniMembershipProvider)Membership.Provider;
        //
        // GET: /Konto/Zaloguj
        //
        public ActionResult Zaloguj(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;

            return View();
        }

        //
        // POST: /Konto/Zaloguj
        //
        [HttpPost]
        public ActionResult Zaloguj(LogOnModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    if (Membership.ValidateUser(model.UserName, model.Password))
                    {
                        FormsAuthentication.SetAuthCookie(model.UserName, model.RememberMe);
                        // Przypisujemy produkty z koszyka nowo zalogowanemu uzytkownikowi
                        Koszyk.PrzypiszSesjeDoUzytkownika(model.UserName);

                        //jeśli user jest z issg_users_auto i ma flagę password_changed na false to odsyłaj do zmiany hasła
                        //jak powrót nie jest udany, to odautentykuj usera.
                        /* na chwilę obecną nie proponujemy przekierowania do zmiany hasła 
                        if (doesUserNeedsToChangePassword(model.UserName))
                        {
                            return RedirectToAction("ZmienHaslo", "Konto");
                            // Tam trzeba ustawic flage ze haslo zmienione - nie, standardowy wpis do historii zmiany haseł
                            // pytanie jak kontrolowac na innych adresach, że hasło zostało zmienione dla auto usera, ktory ma wstępną autoryzację.
                            // to na deser.
                        }
                        else {* */

                            if (Url.IsLocalUrl(returnUrl) && returnUrl.Length > 1 && returnUrl.StartsWith("/")
                                && !returnUrl.StartsWith("//") && !returnUrl.StartsWith("/\\"))
                            {
                                return Redirect(returnUrl);
                            }
                            else
                            {

                                if (User.IsInRole("Administratorzy")) return RedirectToAction("Zamowienia", "Home");
                                var userRoles = Roles.GetRolesForUser(model.UserName);
                                if (userRoles.Contains("Prenum")) return RedirectToAction("Index", "Prenumerata");
                                    
                                return RedirectToAction("Index", "Home");
                            }
                        //} doesUserNeedsToChangePassword
                    }
                    else /* brak autoryzacji, ale sprawdźmy czy to nie uwm */
                    {
                        //if (model.UserName.Contains("@uwm.edu.pl") || model.UserName.Contains("@gazetaolsztynska.pl")
                        //    || model.UserName.Contains("@datapress.pl"))
                        //{ /* magic dla uwm, gdzie wzorzec hasła leży w issg_users_auto */

                        //    if (!string.IsNullOrEmpty(Membership.GetUserNameByEmail(model.UserName))) /* jeśli taki user istnieje to wywal */
                        //    {
                        //        ModelState.AddModelError("",
                        //            "Błędny login i/lub hasło. Sprawdź pocztę email z przypisanym hasłem do konta "
                        //            + model.UserName);
                        //    }

                        //    return CreateUserAuto(model);
                        //}
                        //else
                        //{
                            ModelState.AddModelError("", "Login lub Hasło jest niepoprawne!");
                        //}
                    }
                }
                catch (SoapException ex)
                {
                    //logger.Info("Próba logowania zablokowanego konta: " + model.UserName + ex.Cause.ToString());
                    logger.Info("SOAP ERROR: "+ex.ToString());
                    ModelState.AddModelError("", "Użytkownik jest zablokowany. Prosimy o kontakt z Działem Sprzedaży.");
                }
                //catch (UniMembership.LoginFailedException fex)
                //{
                //    logger.Info("Próba logowania zablokowanego konta: " + model.UserName + fex.Cause.ToString());
                //    ModelState.AddModelError("", "Użytkownik jest zablokowany. Prosimy o kontakt z Działem Sprzedaży.");
                //}

                
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //private ActionResult CreateUserAuto(LogOnModel model)
        //{

        //    /* najpierw sprawdzamy czy w puli 300 kont jest jeszcze coś wolnego */
        //    //issg_users_auto iua;
        //    //using (Entities db = new Entities())
        //    //{
        //        /* pobieramy pierwszy pusty wpis z tabeli */
        //        //iua = db.issg_users_auto
        //        //    .Where(d=> string.IsNullOrEmpty(d.login))
        //        //    .OrderBy(d=>d.lp)
        //        //    .FirstOrDefault();

        //        //if (iua == null) /* ZABRAKŁO KONT */
        //        //{
        //        //    ModelState.AddModelError("", "Limit kont dla danej domeny został wyczerpany.");
        //        //    return View(model);
        //        //}

        //        Users user = new Users();

        //        /* przepisujemy dane z auto */
        //        //user.PasswordHash = iua.passwordhash;
        //        //user.salt = iua.salt;
        //        user.Grupa_id = "Prenum"; /* Ważne */
        //        //iua.login = model.UserName; /* tutaj wiązemy konto auto z puli z danym userem */


        //        // Dane podstawowe
        //        user.UserName = model.UserName;
        //        user.Email = model.UserName;
        //        user.Imie = "---";
        //        user.Nazwisko = "---";
        //        user.telefon = "---";

        //        // Dane firmowe
        //        user.czy_firma = false;
        //        user.czy_faktura = false;
        //        user.nazwa_firmy = "---";
        //        user.nip = "0000000000";

        //        // Dane teleadresowe
        //        user.Adres = null;
        //        user.ulica = "---";
        //        user.budynek = "---";
        //        user.mieszkanie = "---";
        //        user.Kod = "---";
        //        user.Miejscowosc = "---";
        //        user.kraj = "---";

        //        // Korespodencja
        //        user.koresp1 = "---";
        //        user.koresp2 = "---";
        //        user.koresp3 = "---";
        //        user.koresp4 = "---";

        //        // Zgody
        //        user.czy_zgoda_przetwarzanie = false;
        //        user.czy_zgoda_reklamy = false;

        //        user.audyt_kiedy = DateTime.Now;
        //        user.audyt_kto = "create_auto";

        //        // Attempt to register the user
        //        MembershipCreateStatus createStatus;
        //        //((RemoteUniMembershipProvider)Membership.Provider).CreateUniUser(model.UserName, model.Password, user, out createStatus);
        //            (Membership.Provider).CreateUser((model.UserName, model.Password, string.Empty, string.Empty, out createStatus);

        //    if (createStatus == MembershipCreateStatus.Success)
        //        {

        //            //db.Users.AddObject(user);
        //            //tutaj wkracza trigger na tabeli Users, który wpisuje odpowiednie hasło itd.


        //            FormsAuthentication.SetAuthCookie(model.UserName, false /* createPersistentCookie */);
        //            // Przypisujemy produkty z koszyka nowo zalogowanemu urzytkownikowi
        //            Koszyk.PrzypiszSesjeDoUzytkownika(model.UserName);

        //          //  db.ObjectStateManager.ChangeObjectState(iua, System.Data.EntityState.Modified);
        //            //db.SaveChanges();
        //            sendEmailAutoUser(model.UserName);

        //            return RedirectToAction("Index", "Prenumerata");

        //        }

        //        return View(model);
        //        //}



            

             
        //}

        //private void sendEmailAutoUser(string username)
        //{

        //    issg_users_auto iua;

        //    using (Entities db = new Entities())
        //    {
        //        iua = db.issg_users_auto.FirstOrDefault(d => d.login == username);
        //        if (iua != null)
        //        {


        //            SendPassByMailForAutoUser(username, iua.haslo);

        //        }
        //        else
        //        {
        //            logger.Error("Brak loginu "+username +" w issg_users_auto po założeniu usera z email uwm.edu.pl");
        //        }
        //    }


        //}

        //private bool doesUserNeedsToChangePassword(string username)
        //{
        //    UniMembershipUser issgUser = (UniMembershipUser)Membership.GetUser();

        //    //Uzytkownik user = issgUser.uzytkownik;
        //    //string username = user.UserName;
        //    short? zmiana_hasla_cnt = 0;

        //    using (Entities db = new Entities())
        //    {
        //        //ile razy user zmieniał hasło
        //        zmiana_hasla_cnt = db.CheckUserPasswordChange(username).Single();
        //        //jeśli więcej niż zero, to ok, ale jeśli zero to kierjemy na stronę zmiany hasła.
        //    }

        //    /* może na poziomie procedury w db sprawdzać jakąś flagę. 
        //     * Może zaglądać do tabeli issg_users_auto i password hist by ocenić czy zmiana miała miejsce */

        //    return zmiana_hasla_cnt == 0 ? true : false;
        //}

        //
        // GET: /Konto/LogOff

        public ActionResult Wyloguj()
        {
            FormsAuthentication.SignOut();

            return RedirectToAction("Index", "Home");
        }

        //
        // GET: /Konto/Nowe

        public ActionResult Nowe()
        {
            RegisterModel model = new RegisterModel();

            model.Kraj = "POL";

            return View(model);
        }

        //
        // POST: /Konto/Nowe

        [HttpPost]
        //[CaptchaMVC.Attribute.CaptchaVerify("Błędnie przepisane znaki z obrazka.")]
        public ActionResult Nowe(RegisterModel model, string returnUrl)
        {
            //string issg_layout_ver = ISSG3.Utils.getOpcjeValue("issg_layout_ver");
            if (!string.IsNullOrEmpty(model.Email)) /* custom anty bot captcha */
            {
                return RedirectToAction("Index", "Home");
            }


            if (ModelState.IsValid)
            {
                Users user = new Users();

                // Dane podstawowe
                user.Email = model.AdresPocztyElektronicznej;
                user.Imie = model.Imie;
                user.Nazwisko = model.Nazwisko;
                user.telefon = model.Telefon;
                user.UserName = model.UserName;

                // Dane firmowe
                user.czy_firma = model.CzyFirma;
                user.czy_faktura = model.CzyFaktura;
                user.nazwa_firmy = model.NazwaFirmy;
                user.nip = model.NIP;

                // Dane teleadresowe
                user.Adres = null;
                user.ulica = model.Ulica;
                user.budynek = model.Budynek;
                user.mieszkanie = model.Mieszkanie;
                user.Kod = model.Kod;
                user.Miejscowosc = model.Miejscowosc;
                user.kraj = model.Kraj;

                // Korespodencja
                user.koresp1 = model.AdresDokorenspodencji1;
                user.koresp2 = model.AdresDokorenspodencji2;
                user.koresp3 = model.AdresDokorenspodencji3;
                user.koresp4 = model.AdresDokorenspodencji4;

                // Zgody
                user.czy_zgoda_przetwarzanie = model.CzyZgodaNaPrzetwarzanie;
                user.czy_zgoda_reklamy = model.CzyZgodaNaReklamy;

                user.audyt_kiedy = DateTime.Now;
                user.audyt_kto = HttpContext.User.Identity.Name;

                object user_key = Guid.NewGuid();

                // Attempt to register the user
                MembershipCreateStatus createStatus;
                ((LocalUniMembershipProvider)Membership.Provider).CreateUniUser(
                      model.UserName
                    , model.Password
                    , user
                   // , model.Email
                  //  , string.Empty
                  //  , string.Empty
                 //   , true
                //    , user_key
                    , out createStatus);

                if (createStatus == MembershipCreateStatus.Success)
                {
                    FormsAuthentication.SetAuthCookie(model.UserName, false /* createPersistentCookie */);

                    // Przypisujemy produkty z koszyka nowo zalogowanemu urzytkownikowi
                    Koszyk.PrzypiszSesjeDoUzytkownika(model.UserName);

                    //jeszcze ewentualna akcja do aw ze zmianą danych klienta
                    /* moved to UniMembership
                    string czy_api = string.Empty;
                    using (Entities db = new Entities())
                    {
                        czy_api = db.OPCJE.SingleOrDefault(m => m.opcje_nazwa == "webapi_register_enabled").opcje_wartosc;
                    }

                    if (czy_api == "t")
                    {
                        webapi wa = new webapi();
                        wa.saveSendAction(1, user.UserName);
                    }
                     */

                    SendMailAfterRegister(model.UserName, model.AdresPocztyElektronicznej);

                    if (Url.IsLocalUrl(returnUrl) && returnUrl.Length > 1 && returnUrl.StartsWith("/")
                        && !returnUrl.StartsWith("//") && !returnUrl.StartsWith("/\\"))
                    {
                        return Redirect(returnUrl);
                    }
                    else
                    {
                        return RedirectToAction("Index", "Home");
                    }
                }
                else
                {
                    ModelState.AddModelError("", ErrorCodeToString(createStatus));
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Konto/Edytuj
        [Authorize]
        public ActionResult Edytuj()
        {
            UpdateModel model = new UpdateModel();

            MembershipUser issgUser = (MembershipUser)Membership.GetUser();

            Users user = new Users();
            using (Entities db = new Entities())
            {
                user = db.Users.SingleOrDefault(d=>d.UserName==issgUser.UserName);
            }
             
            // Dane podstawowe
            model.Email = user.Email;
            model.Imie = user.Imie;
            model.Nazwisko = user.Nazwisko;
            model.Telefon = user.telefon;
            model.UserName = user.UserName;

            // Dane firmowe
            model.CzyFirma = user.czy_firma.GetValueOrDefault(false);
            model.CzyFaktura = user.czy_faktura.GetValueOrDefault(false);
            model.NazwaFirmy = user.nazwa_firmy;
            model.NIP = user.nip;

            // Dane teleadresowe
            model.Ulica = user.ulica;
            model.Budynek = user.budynek;
            model.Mieszkanie = user.mieszkanie;
            model.Kod = user.Kod;
            model.Miejscowosc = user.Miejscowosc;
            model.Kraj = user.kraj;

            // Korespodencja
            model.AdresDokorenspodencji1 = user.koresp1;
            model.AdresDokorenspodencji2 = user.koresp2;
            model.AdresDokorenspodencji3 = user.koresp3;
            model.AdresDokorenspodencji4 = user.koresp4;

            // Zgody
            model.CzyZgodaNaPrzetwarzanie = user.czy_zgoda_przetwarzanie.GetValueOrDefault(false);
            model.CzyZgodaNaReklamy = user.czy_zgoda_reklamy.GetValueOrDefault(false);

            model.Kraj = user.kraj == "PL" ? "POL" : user.kraj;

            return View(model);
        }

        //
        // POST: /Konto/Edytuj
        [Authorize]
        [HttpPost]
        [CaptchaMVC.Attribute.CaptchaVerify("Błędnie przepisane znaki z obrazka.")]
        public ActionResult Edytuj(UpdateModel model, string returnUrl)
        {

            if (ModelState.IsValid)
            {
                MembershipUser issgUser = (MembershipUser)Membership.GetUser();

                //Users user = new Users();
                //using (Entities db = new Entities())
                //{
                //    user = db.Users.SingleOrDefault(d => d.UserName == issgUser.UserName);
                //}

                Entities db = new Entities();
                Users user = db.Users.SingleOrDefault(d => d.UserName == issgUser.UserName);


                // Dane podstawowe
                user.Email = model.Email;
                user.Imie = model.Imie;
                user.Nazwisko = model.Nazwisko;
                user.telefon = model.Telefon;

                // Dane firmowe
                user.czy_firma = model.CzyFirma;
                user.czy_faktura = model.CzyFaktura;
                user.nazwa_firmy = model.NazwaFirmy;
                user.nip = model.NIP;

                // Dane teleadresowe
                user.Adres = null;
                user.ulica = model.Ulica;
                user.budynek = model.Budynek;
                user.mieszkanie = model.Mieszkanie;
                user.Kod = model.Kod;
                user.Miejscowosc = model.Miejscowosc;
                user.kraj = model.Kraj;

                // Korespodencja
                user.koresp1 = model.AdresDokorenspodencji1;
                user.koresp2 = model.AdresDokorenspodencji2;
                user.koresp3 = model.AdresDokorenspodencji3;
                user.koresp4 = model.AdresDokorenspodencji4;

                // Zgody
                user.czy_zgoda_przetwarzanie = model.CzyZgodaNaPrzetwarzanie;
                user.czy_zgoda_reklamy = model.CzyZgodaNaReklamy;

                //Audyt
                user.audyt_kiedy = DateTime.Now;
                user.audyt_kto = HttpContext.User.Identity.Name;

                ViewBag.Success = true;
                try
                {
                    // (Membership.Provider).UpdateUniUser(issgUser);
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    logger.Debug("RemoteUniMembershipProvider Error:" + ex.Message);
                    ViewBag.Success = false;
                }

                /* moved to UniUserMembership
                 * if (ViewBag.Success)
                {
                    string czy_api = string.Empty;
                    //jeszcze ewentualna akcja do aw ze zmianą danych klienta
                    using (Entities db = new Entities())
                    {
                        czy_api = db.OPCJE.SingleOrDefault(m => m.opcje_nazwa == "webapi_userchange_enabled").opcje_wartosc;
                    }

                    if (czy_api == "t")
                    {
                        webapi wa = new webapi();
                        wa.saveSendAction(2, user.UserName);
                    }
                } */

                if (ViewBag.Success && Url.IsLocalUrl(returnUrl) && returnUrl.Length > 1 && returnUrl.StartsWith("/")
                        && !returnUrl.StartsWith("//") && !returnUrl.StartsWith("/\\"))
                {
                    return Redirect(returnUrl);
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Konto/ChangePassword

        [Authorize]
        public ActionResult ZmienHaslo()
        {
            return View();
        }

        //
        // POST: /Konto/ChangePassword

        [Authorize]
        [HttpPost]
        /*[CaptchaMVC.Attribute.CaptchaVerify("Błędnie przepisane znaki z obrazka.")]*/
        public ActionResult ZmienHaslo(ChangePasswordModel model)
        {
            if (ModelState.IsValid)
            {
                // ChangePassword will throw an exception rather
                // than return false in certain failure scenarios.
                bool changePasswordSucceeded = true;
                try
                {
                    MembershipUser currentUser = Membership.GetUser(User.Identity.Name, true /* userIsOnline */);
                    changePasswordSucceeded = currentUser.ChangePassword(model.OldPassword, model.NewPassword);
                }
                catch (System.Configuration.Provider.ProviderException ex)
                {
                    ModelState.AddModelError("", ex.Message);
                    changePasswordSucceeded = false;
                }
                catch (Exception)
                {
                    ModelState.AddModelError("", "Dotychczasowe hsało jest niepoprawne lub nowe hasło nie spełnia kryteriów.");
                    changePasswordSucceeded = false;
                }

                if (changePasswordSucceeded) return RedirectToAction("ZmienHasloSukces");
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Konto/ChangePasswordSuccess

        public ActionResult ZmienHasloSukces()
        {
            return View();
        }

        //
        // GET: /Konto/ResetPassword
        public ActionResult ResetujHaslo()
        {
            return View();
        }

        //
        // POST: /Konto/ResetPassword

        [HttpPost]
        [CaptchaMVC.Attribute.CaptchaVerify("Błędnie przepisane znaki z obrazka.")]
        public ActionResult ResetujHaslo(ResetPasswordModel model)
        {

            string username = string.Empty;

            logger.Debug("Reset hasła dla email: " + model.Email);
            if (ModelState.IsValid)
            {
                bool resetPasswordSucceeded = true;
                try
                {
                    using (Entities ie = new Entities())
                    {
                        //string username = Membership.GetUserNameByEmail(model.Email);
                        username = ie.Users.SingleOrDefault(m => m.Email == model.Email).UserName;
                        if (!string.IsNullOrEmpty(username))
                        {
                            logger.Debug("Reset hasła: znaleziono user: " + username + " dla email: " + model.Email);
                            //MembershipUser currentUser = Membership.GetUser(username, false /* userIsNotOnline */);
                            //resetPasswordSucceeded = currentUser.ResetPassword(); /*TODO Reset hasła - zrobione w modeli ISSG, powinno być w UniUser ? */
                            ResetPasswordModel resetmod = new ResetPasswordModel();
                            //resetPasswordSucceeded = resetmod.resetPassword(currentUser.UserName, model.Email);
                            resetPasswordSucceeded = resetmod.resetPassword(username, model.Email);
                            logger.Debug("Po resecie hasła: " + model.Email);
                        }
                        else
                        {
                            logger.Debug("Nie znaleziono usera dla email: "+model.Email);
                        }
                    }

                }
                catch (System.Configuration.Provider.ProviderException ex)
                {
                    ModelState.AddModelError("", ex.Message);
                    resetPasswordSucceeded = false;
                    logger.Error("Wystąpił błąd podczas resetowania hasła ProviderException: " + ex.Message);
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", "Wystąpił błąd podczas resetowania hasła.");
                    resetPasswordSucceeded = false;
                    logger.Error("Wystąpił błąd podczas resetowania hasła: "+ex.Message);
                }

                if (resetPasswordSucceeded && username!="")
                {
                    /* moved to UniMemberShip
                    string czy_api = string.Empty;
                    //jeszcze ewentualna akcja do aw ze zmianą danych klienta
                    using (Entities db = new Entities())
                    {
                        czy_api = db.OPCJE.SingleOrDefault(m => m.opcje_nazwa == "webapi_userchange_enabled").opcje_wartosc;
                    }

                    if (czy_api == "t")
                    {
                        webapi wa = new webapi();
                        wa.saveSendAction(2, username);
                    }
                     * */

                    return RedirectToAction("ResetujHasloSukces");
                }
            }

            return View(model);
        }

        //
        // GET: /Konto/ChangePasswordSuccess

        public ActionResult ResetujHasloSukces()
        {
            return View();
        }

        public void SendMailAfterRegister(string username, string email)
        {
            Logger logger = LogManager.GetCurrentClassLogger();
            SmtpServer smtpServer;
            AppSettingsReader apr = new AppSettingsReader();

            string ls_mailserver = apr.GetValue("MailServer", typeof(string)).ToString();
            string ls_mailaddress = apr.GetValue("MailSenderAddress", typeof(string)).ToString();
            string ls_mailfrom = apr.GetValue("MailSenderFrom", typeof(string)).ToString();
            string ls_urllink = apr.GetValue("MailLink", typeof(string)).ToString();
            string ls_serverport = apr.GetValue("MailServerPort", typeof(string)).ToString();

            smtpServer = new SmtpServer(ls_mailserver, int.Parse(ls_serverport));

            if (ConfigurationManager.AppSettings["MailAuth"].ToString() == "yes")
            {
                string ls_mailuser = apr.GetValue("MailUser", typeof(string)).ToString();
                string ls_mailpwd = apr.GetValue("MailPass", typeof(string)).ToString();
                smtpServer.SmtpAuthToken = new SmtpAuthToken(ls_mailuser, ls_mailpwd);
            }

            try
            {
                EmailMessage emailMessage = new DotNetOpenMail.EmailMessage();
                emailMessage.HeaderCharSet = System.Text.Encoding.GetEncoding("UTF-8");
                emailMessage.HeaderEncoding = DotNetOpenMail.Encoding.EncodingType.QuotedPrintable;

                emailMessage.FromAddress = new DotNetOpenMail.EmailAddress(ls_mailaddress, ls_mailfrom,
                    DotNetOpenMail.Encoding.EncodingType.QuotedPrintable, System.Text.Encoding.GetEncoding("UTF-8"));
                emailMessage.AddToAddress(new EmailAddress(email));
                emailMessage.Subject = "eKurier - nowe konto";

                string ls_send_time = DateTime.Now.ToString("dd-MM-yyyy HH:mm");

                String scriptString = "\nDzień dobry! \n";
                scriptString += "\n";
                scriptString += "Osoba posługująca się adresem email " + email + "\n";
                scriptString += "utworzyła konto w serwisie eKurier.24kurier.pl .\n";
                scriptString += "Twój login w serwisie to: \n\n";
                scriptString += username + "\n"; ;
                scriptString += "\n"; ;
                scriptString += "Zapraszamy do lektury prenumeraty elektronicznej naszej Gazety.\n";


                TextAttachment ta = new TextAttachment(scriptString);
                ta.CharSet = System.Text.Encoding.GetEncoding("UTF-8");
                emailMessage.TextPart = ta;

                emailMessage.Send(smtpServer);

                logger.Info("Wysłano email z po rejestracji dla:" + username + " na adres: " + email);

            }
            catch (DotNetOpenMail.SmtpException ex)
            {
                logger.Error("SmtpException: " + ex.Message);

            }
            catch (DotNetOpenMail.MailException ex)
            {
                logger.Error("MailException : " + ex.Message);

            }
            catch (Exception ex)
            {
                logger.Error("Exception : " + ex.Message);

            }

        }


        #region Status Codes
        private static string ErrorCodeToString(MembershipCreateStatus createStatus)
        {
            // See http://go.microsoft.com/fwlink/?LinkID=177550 for
            // a full list of status codes.
            switch (createStatus)
            {
                case MembershipCreateStatus.DuplicateUserName:
                    return "Podany login już istnieje. Proszę podać inny login.";

                case MembershipCreateStatus.DuplicateEmail:
                    return "Podany adres email juz istnieje. Proszę podać inny email.";

                case MembershipCreateStatus.InvalidPassword:
                    return "Podane hasło jest niezgodne z wymogami bezpieczeństwa. Proszę wprowadzić nowe hasło.";

                case MembershipCreateStatus.InvalidEmail:
                    return "Podany adres email jest niepoprawny. Proszę sprawdzić poprawność adresu.";

                case MembershipCreateStatus.InvalidAnswer:
                    return "The password retrieval answer provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidQuestion:
                    return "The password retrieval question provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidUserName:
                    return "The user name provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.ProviderError:
                    return "The authentication provider returned an error. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                case MembershipCreateStatus.UserRejected:
                    return "The user creation request has been canceled. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                default:
                    return "An unknown error occurred. Please verify your entry and try again. If the problem persists, please contact your system administrator.";
            }
        }
        #endregion
    }
}
