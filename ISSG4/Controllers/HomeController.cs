﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ISSG4.Models;
using System.IO;
using System.Drawing;
using Simplid.Images;
using System.Web.Security;
using ISSG4.Models.ViewModels;

namespace ISSG4.Controllers
{
    public class HomeController : Controller
    {
        //[OutputCache(Duration=900, VaryByParam="none"))] // 15min
        public ActionResult Index()
        {
            using (Entities ie = new Entities())
            {
                List<NoweWydaniaByType> gazety = ie.getNoweWydaniaByType(1).OrderByDescending(d=>d.GazetaId).ToList();
                ViewBag.Dzienniki = gazety;
                ViewBag.DziennikiDodatki = ie.getNoweWydaniaByType(0).ToList(); //uwaga hardcoded na sztywno
                ViewBag.Tygodniki = ie.getNoweWydaniaByType(2).ToList();
                ViewBag.Bezplatne = ie.getNoweWydaniaByType(3).ToList();
                ViewBag.Miesieczniki = ie.getNoweWydaniaByType(4).ToList();

                if (gazety.Count == 1)/* przypadek Kuriera */
                {
                    int wydanie_nr = gazety.First().WydanieId;
                    return RedirectToAction("Wydanie", "Wydanie", new { WydanieId = wydanie_nr });
                }
            }

            return View();
        }

        //[OutputCache(Duration=900, VaryByParam="none")] // 15min
        public ActionResult Archiwalne()
        {
            using (Entities ie = new Entities())
            {
                ViewBag.Gazety = ie.getGazetyZWydaniami().ToList();
            }

            return View();
        }

        [HttpGet]
        public ActionResult Szukaj(string term)
        {
            return View("Search");
        }

        [HttpPost]
        public ActionResult SearchResult(string term)
        {
            List<i3_ArtArtykul> arts = new List<i3_ArtArtykul>();
            List<i3_ArtFoto> fotos = new List<i3_ArtFoto>();

            using (Entities db = new Entities())
            {
                //ViewBag.Gazety = ie.getGazetyZWydaniami().ToList();
                //List<int> art_ids = db.i3_ArtArtykulTresciIndex
                //    .Where(d=>d.ArtArtykulTresc.Contains(term))
                //    .Select(d=>d.ArtArtykulId)
                //    .Take(100)
                //    .ToList();

                //List<i3_ArtArtykulTresciIndex> indks = db.i3_ArtArtykulTresciIndex
                //    .Where(d => d.ArtArtykulTresc.Contains(term))
                //    .Take(100)
                //    .ToList();

                List<int> art_ids = db.i3_ArtArtykulTresciIndex
                    .Where(d => d.ArtArtykulTresc.Contains(term))
                    .Select(d => d.ArtArtykulId)
                    .Take(100)
                    .ToList();

                arts = db.i3_ArtArtykul
                    .Include("i3_Wydanie")
                    .Where(d => art_ids.Contains(d.ArtArtykulId))
                    .OrderByDescending(d=>d.ArtArtykulId)
                    .ToList();

                fotos = db.i3_ArtFoto
                    .Where(d => art_ids.Contains(d.ArtArtykulId))
                    .ToList();

            }

            ArtViewList lista = new ArtViewList();
            lista.art_lista = arts;
            lista.art_foto_list = fotos;
            lista.termin_string = term;

            return View(lista);
        }

        [OutputCache(Duration = 900, VaryByParam = "id;tylkoMoje;tylkoDodatki", VaryByCustom = "User")] // 15min
        public ActionResult Archiwum(int? id, bool? tylkoMoje, bool? tylkoDodatki)
        {
            //List<viewWydanieAktywneJoined> Wydania = new List<viewWydanieAktywneJoined>();

            List<getWydanieAktywneNaDzial_Result> Wyd = new List<getWydanieAktywneNaDzial_Result>();
            //List<getWydanieAktywneNaDzialDodatki_Result> WydDod = new List<getWydanieAktywneNaDzialDodatki_Result>();

            int userId = 0;
            if (User.Identity.IsAuthenticated)
            {
                userId = (int)Membership.GetUser().ProviderUserKey;
            }

            ViewBag.userId = userId;
            ViewBag.tylkoMoje = tylkoMoje.GetValueOrDefault(false);
            ViewBag.tylkoDodatki = tylkoDodatki.GetValueOrDefault(false);

            int idg = 1;
            if (id.HasValue) idg = id.Value;

            using (Entities ie = new Entities())
            {
                ViewBag.MiesiacRok = ie.getMiesiacRokWydanByGazetaId(id).ToList();
                ViewBag.Gazeta = ie.Gazeta.Where(m => m.GazetaId == idg).Single().GazetaNazwa;
                //if (tylkoMoje.HasValue && tylkoMoje.Value == true)
                //{
                    //Wydania = ie.viewWydanieAktywneJoined.Where(m => m.GazetaId == id && m.issg_user_id == userId).OrderByDescending(m => m.WydanieData).Take(20).ToList();
                    //Wyd = ie.Wydanie
                    //        .Include("i3_ZamowieniePozycja")
                    //        .Include("i3_ZamowieniePozycja.i3_Zamowienie")
                    //        .Where(m => m.i3_ZamowieniePozycja.Any(n => n.i3_Zamowienie.issg_user_id == userId && (n.i3_Zamowienie.ZamowienieOplacone == true || n.i3_Zamowienie.ZamowienieRecznaAktywacja == true))
                    //            && m.WydanieAktywne == true
                    //            && m.GazetaId == id)
                    //        .OrderByDescending(m => m.WydanieData)
                    //        .AsEnumerable()
                    //        .Distinct(new PropertyComparer<Wydanie>("WydanieId"))
                    //        .Take(20)
                    //        .ToList();
                if (tylkoDodatki.HasValue)
                {
                    if (tylkoDodatki == true)
                    {
                        Wyd = ie.getWydanieAktywneNaDzialDodatki(idg, userId, null, null, 20, tylkoMoje, 1).ToList();
                    }
                    else
                    {
                        Wyd = ie.getWydanieAktywneNaDzial(idg, userId, null, null, 20, tylkoMoje).ToList();
                    }
                }
                else
                {
                    Wyd = ie.getWydanieAktywneNaDzial(idg, userId, null, null, 20, tylkoMoje).ToList();
                }
                //}
                //else
                //{
                //    //Wyd = ie.Wydanie.Where(m => m.GazetaId == id && m.WydanieAktywne == true).OrderByDescending(m => m.WydanieData).Take(20).ToList();
                //    Wyd = ie.getWydanieAktywneNaDzial(id, userId, null, null, 20).ToList();
                //}
            }

            return View(Wyd);
        }

        [OutputCache(Duration = 900, VaryByParam = "id;Miesiac;tylkoMoje;tylkoDodatki", VaryByCustom = "User")] // 15min
        [HttpPost]
        public ActionResult Archiwum(int id, DateTime Miesiac, bool? tylkoMoje, bool? tylkoDodatki)
        {
            //List<viewWydanieAktywneJoined> Wydania = new List<viewWydanieAktywneJoined>();

            List<getWydanieAktywneNaDzial_Result> Wyd = new List<getWydanieAktywneNaDzial_Result>();

            int userId = 0;
            if (User.Identity.IsAuthenticated)
            {
                userId = (int)Membership.GetUser().ProviderUserKey;
            }
            ViewBag.userId = userId;
            ViewBag.Miesiac = Miesiac;
            if (User.IsInRole("Prenum"))
            {
                ViewBag.tylkoMoje = true;
                ViewBag.tylkoDodatki = false;
            }
            else
            {
                ViewBag.tylkoMoje = tylkoMoje.GetValueOrDefault(false);
                ViewBag.tylkoDodatki = tylkoDodatki.GetValueOrDefault(false);
            }

            using (Entities ie = new Entities())
            {
                ViewBag.MiesiacRok = ie.getMiesiacRokWydanByGazetaId(id).ToList();
                ViewBag.Gazeta = ie.Gazeta.Where(m => m.GazetaId == id).Single().GazetaNazwa;

                if (tylkoDodatki.HasValue && tylkoDodatki.Value == true)
                {
                    if (Miesiac.Year == 1000)
                    {
                        //Wydania = ie.viewWydanieAktywneJoined.Where(m => m.GazetaId == id && m.issg_user_id == userId).OrderByDescending(m => m.WydanieData).Take(20).ToList();
                        Wyd = ie.getWydanieAktywneNaDzialDodatki(id, userId, null, null, 20, tylkoMoje,1).ToList();
                    }
                    else
                    {
                        //Wydania = ie.viewWydanieAktywneJoined.Where(m => m.GazetaId == id && m.issg_user_id == userId && m.WydanieData.Month == Miesiac.Month && m.WydanieData.Year == Miesiac.Year).OrderByDescending(m => m.WydanieData).ToList();
                        Wyd = ie.getWydanieAktywneNaDzialDodatki(id, userId, Miesiac.Month, Miesiac.Year, null, tylkoMoje, 1).ToList();
                    }
                }
                else
                {
                    if (Miesiac.Year == 1000)
                    {
                        //Wydania = ie.viewWydanieAktywneJoined.Where(m => m.GazetaId == id && m.issg_user_id == userId).OrderByDescending(m => m.WydanieData).Take(20).ToList();
                        Wyd = ie.getWydanieAktywneNaDzial(id, userId, null, null, 20, tylkoMoje).ToList();
                    }
                    else
                    {
                        //Wydania = ie.viewWydanieAktywneJoined.Where(m => m.GazetaId == id && m.issg_user_id == userId && m.WydanieData.Month == Miesiac.Month && m.WydanieData.Year == Miesiac.Year).OrderByDescending(m => m.WydanieData).ToList();
                        Wyd = ie.getWydanieAktywneNaDzial(id, userId, Miesiac.Month, Miesiac.Year, null, tylkoMoje).ToList();
                    }
                }
            }

            return View(Wyd);
        }

        public ActionResult Regulamin()
        {
            string wydawca = string.Empty;
            using (Entities db = new Entities())
            {
                wydawca = db.Wydawca.FirstOrDefault().WydawcaNazwa;
            }
            object model = new object();
            model = wydawca;
            return View(model);
        }

        public ActionResult Cennik()
        {
            string wydawca = string.Empty;
            using (Entities db = new Entities())
            {
                wydawca = db.Wydawca.FirstOrDefault().WydawcaNazwa;
            }
            object model = new object();
            model = wydawca;
            return View(model);
        }

        public ActionResult Kontakt()
        {
            string wydawca = string.Empty;
            using (Entities db = new Entities())
            {
                wydawca = db.Wydawca.FirstOrDefault().WydawcaNazwa;
            }
            object model = new object();
            model = wydawca;
            return View(model);
        }

        [HttpGet]
        [OutputCache(Duration = 1800, VaryByParam = "src;w;h")] // 30min
        public ActionResult Jedynka(string src, int? w, int? h)
        {
            int width = w.GetValueOrDefault(0);
            int height = h.GetValueOrDefault(0);
            string dir = System.Configuration.ConfigurationManager.AppSettings["PDFStore"];

            byte[] result = null;

            try
            {
                if (w.HasValue && h.HasValue)
                {
                    Image Img = Image.FromFile(dir + src);
                    result = ImageManipulation.SaveAsJPG(ImageManipulation.ResizeFixedRatio(Img, w.Value, h.Value), 60L);
                    Img.Dispose();
                }

                if (w.HasValue && !h.HasValue)
                {
                    Image Img = Image.FromFile(dir + src);
                    result = ImageManipulation.SaveAsJPG(ImageManipulation.ResizeFixedRatioAndWidth(Img, w.Value), 60L);
                    Img.Dispose();
                }

                if (!w.HasValue && h.HasValue)
                {
                    Image Img = Image.FromFile(dir + src);
                    result = ImageManipulation.SaveAsJPG(ImageManipulation.ResizeFixedRatioAndHeight(Img, h.Value), 60L);
                    Img.Dispose();
                }

                if (!w.HasValue && !h.HasValue)
                    result = System.IO.File.ReadAllBytes(dir + src);
            }
            catch
            {
                result = System.IO.File.ReadAllBytes(System.Web.HttpRuntime.AppDomainAppPath + "Content/BrakObrazka.png");
            }

            return new FileContentResult(result, "image/jpeg");
        }
    }
}
