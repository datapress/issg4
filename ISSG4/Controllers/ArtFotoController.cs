﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ISSG4.Models;
using ISSG4.Models.ViewModels;
using System.Drawing;
using Simplid.Images;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Web.Security;
using NLog;

namespace ISSG4.Controllers
{
    public class ArtFotoController : Controller
    {

        Logger logger = LogManager.GetCurrentClassLogger();

        // GET: Art
        public ActionResult Index(int id)
        {
            ArtFotoListView model = new ArtFotoListView();

            using (Entities db = new Entities())
            {

                ObjectContext objectContext = ((IObjectContextAdapter)db).ObjectContext;

                model.art_list = db.i3_ArtArtykul.Where(d => d.WydanieId == id)
                    //.OrderBy("it.ArtArtykulFoto desc, it.ArtArtykulUtworzono");
                    .OrderByDescending(d=>d.ArtArtykulFoto)
                    .ThenBy(d=>d.ArtArtykulUtworzono)
                    .ToList();

                List<int> artidlist = model.art_list.Select(f => f.ArtArtykulId).ToList();
                model.art_foto_list = new List<i3_ArtFoto>();

                foreach (int artid in artidlist)
                {
                    List<i3_ArtFoto> lst = db.i3_ArtFoto.Where(d => d.ArtArtykulId == artid).ToList();
                    model.art_foto_list.AddRange(lst);
                }

                Wydanie wyd = db.Wydanie.SingleOrDefault(d => d.WydanieId == id);
                DateTime termin = wyd.WydanieData;
                ViewBag.termin_string = termin.Year.ToString() + termin.Month.ToString("d2") + termin.Day.ToString("d2");

                model.wydanie = wyd;

                ObjectQuery<i3_ArtDzial> dzials = objectContext
                  .CreateObjectSet<i3_ArtDzial>("i3_ArtDzial");
                  //.Include("i3_ArtArtykul");

                model.dzial_lista = dzials.ToList();

                //next / prev 
                //ObjectQuery<i3_ArtArtykul> arts = objectContext.CreateObjectSet<i3_ArtArtykul>("i3_ArtArtykul")
                //.Include("i3_ArtDzial")
                //.Where("it.WydanieId == " + wyd.WydanieId.ToString())
                //.OrderBy("it.ArtDzialId,it.ArtArtykulId");

                //List<i3_ArtArtykul> lista = arts.ToList();
                //int poz_artu = lista.FindIndex(d => d.ArtArtykulId == id);
                ////sprawdzamy czy nie pierwszy
                //if (poz_artu == 0)
                //    model.prevId = 0;

                //if (poz_artu == lista.Count - 1)
                //    model.nextId = 0;

                //if (poz_artu > 0) //czyli nie pierwszy
                //    model.prevId = lista[poz_artu - 1].ArtArtykulId;

                //if (poz_artu < lista.Count - 1) //czyli nie ostatni
                //    model.nextId = lista[poz_artu + 1].ArtArtykulId;

                //if (Request.IsAuthenticated)
                //{
                //    int userId = (int)Membership.GetUser().ProviderUserKey;
                //    naDzial = db.ZamowieniePozycja.Where(m => m.i3_Wydanie.Any(n => n.WydanieId == wyd.WydanieId) && m.i3_Zamowienie.issg_user_id == userId).Count();
                //}

            }


            return View(model);
        }

        [OutputCache(Duration = 60, VaryByParam = "id", VaryByCustom = "User")]
        public ActionResult Show(int id)
        {
            i3_ArtArtykul art = new i3_ArtArtykul();
            ArtView model = new ArtView();
            DateTime termin;
            int naDzial = 0;
            using (Entities db = new Entities())
            {

                ObjectContext objectContext = ((IObjectContextAdapter)db).ObjectContext;


                model.art = db.i3_ArtArtykul.SingleOrDefault(d=>d.ArtArtykulId == id);

                if(model.art == null)
                    return RedirectToAction("Index", "Wydanie");

                model.art_foto_list = db.i3_ArtFoto.Where(d => d.ArtArtykulId == id).ToList();
                Wydanie wyd = db.Wydanie.SingleOrDefault(d => d.WydanieId == model.art.WydanieId);
                termin = wyd.WydanieData;

                model.wydanie = wyd;

                ObjectQuery<i3_ArtDzial> dzials = objectContext
                  .CreateObjectSet<i3_ArtDzial>("i3_ArtDzial")
                  .Include("i3_ArtArtykul");

                model.dzial_lista = dzials.ToList();

                //next / prev 
                ObjectQuery<i3_ArtArtykul> arts = objectContext.CreateObjectSet<i3_ArtArtykul>("i3_ArtArtykul")
                .Include("i3_ArtDzial")
                .Where("it.WydanieId == " + wyd.WydanieId.ToString())
                .OrderBy("it.ArtDzialId,it.ArtArtykulId");

                List<i3_ArtArtykul> lista = arts.ToList();
                int poz_artu = lista.FindIndex(d => d.ArtArtykulId == id);
                //sprawdzamy czy nie pierwszy
                if (poz_artu == 0)
                    model.prevId = 0;

                if (poz_artu == lista.Count-1)
                    model.nextId = 0;

                if (poz_artu > 0) //czyli nie pierwszy
                    model.prevId = lista[poz_artu - 1].ArtArtykulId;

                if (poz_artu < lista.Count-1) //czyli nie ostatni
                    model.nextId = lista[poz_artu + 1].ArtArtykulId;

                if (Request.IsAuthenticated)
                {
                    int userId = (int)Membership.GetUser().ProviderUserKey;
                    naDzial = db.ZamowieniePozycja.Where(m => m.i3_Wydanie.Any(n => n.WydanieId == wyd.WydanieId) && m.i3_Zamowienie.issg_user_id == userId).Count();
                }

            }
            ViewBag.NaDzial = naDzial;

            ViewBag.termin_string = termin.Year.ToString() + termin.Month.ToString("d2") + termin.Day.ToString("d2");

            ViewBag.SpisTresci = ISSG3.Utils.getSpisTresci(model.wydanie.WydanieId, model.art.ArtDzialId);

            return View(model);
        }

        [HttpGet]
        [OutputCache(Duration = 180, VaryByParam = "src;w;h;fx;fy")] // 3min
        public ActionResult ShowFoto(string src, int? w, int? h, decimal? fx, decimal? fy)
        {
            int width = w.GetValueOrDefault(0);
            int height = h.GetValueOrDefault(0);

            decimal focusX = fx.GetValueOrDefault(0);
            decimal focusY = fy.GetValueOrDefault(-1);

            string dir = System.Configuration.ConfigurationManager.AppSettings["EgazetaPhotoStore"];

            byte[] result = null;

            try
            {
                if (w.HasValue && h.HasValue)
                {
                    Image Img = Image.FromFile(dir + src);
                    result = ImageManipulation.SaveAsJPG(ImageManipulation.ResizeCropFixedRatio(
                        Img, 
                        w.Value, 
                        h.Value,
                        fx.GetValueOrDefault(-1),
                        fy.GetValueOrDefault(1)), 
                        60L);
                    Img.Dispose();
                }

                if (w.HasValue && !h.HasValue)
                {
                    Image Img = Image.FromFile(dir + src);
                    result = ImageManipulation.SaveAsJPG(ImageManipulation.ResizeFixedRatioAndWidth(Img, w.Value), 60L);
                    Img.Dispose();
                }

                if (!w.HasValue && h.HasValue)
                {
                    Image Img = Image.FromFile(dir + src);
                    result = ImageManipulation.SaveAsJPG(ImageManipulation.ResizeFixedRatioAndHeight(Img, h.Value), 60L);
                    Img.Dispose();
                }

                if (!w.HasValue && !h.HasValue)
                    result = System.IO.File.ReadAllBytes(dir + src);
            }
            catch (Exception ex)
            {
                logger.Error("Błąd przetwarzania obrazu: "+ex.Message);
                result = System.IO.File.ReadAllBytes(System.Web.HttpRuntime.AppDomainAppPath + "Content/BrakObrazka.png");
            }

            return new FileContentResult(result, "image/jpeg");
        }

        public ActionResult Edit(int id)
        {
            ArtFotoView model = new ArtFotoView();

            using (Entities db = new Entities())
            {
                ObjectContext objectContext = ((IObjectContextAdapter)db).ObjectContext;

                model.art_foto = db.i3_ArtFoto
                    .Include("i3_ArtArtykul")
                    .SingleOrDefault(d=>d.ArtFotoId==id);

                if(model.art_foto != null)
                {
                    i3_ArtArtykul art = model.art_foto.i3_ArtArtykul;

                    Wydanie wyd = db.Wydanie.SingleOrDefault(d => d.WydanieId == art.i3_Wydanie.WydanieId);
                    DateTime termin = wyd.WydanieData;
                    ViewBag.termin_string = termin.Year.ToString() + termin.Month.ToString("d2") + termin.Day.ToString("d2");
                }

            }

                return View(model);
        }

        public ActionResult Edit2(int id)
        {
            ArtFotoView model = new ArtFotoView();

            using (Entities db = new Entities())
            {
                ObjectContext objectContext = ((IObjectContextAdapter)db).ObjectContext;

                model.art_foto = db.i3_ArtFoto
                    .Include("i3_ArtArtykul")
                    .SingleOrDefault(d => d.ArtFotoId == id);

                if (model.art_foto != null)
                {
                    i3_ArtArtykul art = model.art_foto.i3_ArtArtykul;

                    Wydanie wyd = db.Wydanie.SingleOrDefault(d => d.WydanieId == art.i3_Wydanie.WydanieId);
                    DateTime termin = wyd.WydanieData;
                    ViewBag.termin_string = termin.Year.ToString() + termin.Month.ToString("d2") + termin.Day.ToString("d2");
                }

            }

            return View(model);
        }

        public string SaveFocus(int ArtFotoId, string ArtFotoCropData)
        {
            string str_result = "Zapisano";

            using (Entities db = new Entities())
            {
                i3_ArtFoto foto = db.i3_ArtFoto.SingleOrDefault(d => d.ArtFotoId == ArtFotoId);

                if(foto != null && string.IsNullOrEmpty(ArtFotoCropData) == false )
                {
                    foto.ArtFotoCropData = ArtFotoCropData;
                    db.SaveChanges();
                }

               
            }

            return str_result;
        }

    }

}