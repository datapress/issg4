﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ISSG4.Models;
using ISSG3.Models.ViewModels;
using System.Web.Security;
using ISSG4.Helpers;
using NLog;
using System.Configuration;
using System.Transactions;
using ISSG4.Models.ViewModels;
using System.Security.Cryptography;
using System.Text;
using System.Net;

namespace ISSG3.Controllers
{
    public class KoszykController : Controller
    {

        Logger logger = LogManager.GetCurrentClassLogger();

        //
        // GET: /Koszyk/
        //
        public ActionResult Index()
        {
            IEnumerable<viewKoszykJoined> koszyk = null;

            using (Entities ie = new Entities())
            {
                if (User.Identity.IsAuthenticated)
                {
                    int userId = (int)Membership.GetUser().ProviderUserKey;
                    koszyk = ie.viewKoszykJoined.Where(m => m.ispo_user_id == userId).OrderBy(m => m.KoszykData).ToList();
                    //string sql = ((ObjectQuery)koszyk).ToTraceString();
                }
                else
                {
                    koszyk = ie.viewKoszykJoined.Where(m => m.KoszykSesja == Session.SessionID).OrderBy(m => m.KoszykData).ToList();
                }
            }

            return View(koszyk);
        }

        //
        // GET: /Koszyk/Usun/{id}
        //
        public ActionResult Usun(int? id)
        {
            using (Entities ie = new Entities())
            {
                if (id.HasValue)
                {
                    // Kasujemy pojedynczy wpis z koszyka
                    var item = ie.Koszyk.Where(m => m.KoszykId == id).Single();
                    ie.Koszyk.Remove(item);
                    ie.SaveChanges();
                }
                else
                {
                    // Kasujemy szystkie wspisy z koszyka

                    IEnumerable<Koszyk> items = null;

                    if(User.Identity.IsAuthenticated)
                    {
                        int userId = (int)Membership.GetUser().ProviderUserKey;
                        items = ie.Koszyk.Where(m => m.ispo_user_id == userId);
                    }
                    else
                    {
                        items = ie.Koszyk.Where(m => m.KoszykSesja == Session.SessionID);
                    }

                    foreach (var item in items)
                    {
                        ie.Koszyk.Remove(item);
                    }

                    ie.SaveChanges();

                }
            }

            return RedirectToAction("Index");
        }

        //
        // GET /Koszyk/Dodaj
        //
        [HttpPost]
        public ActionResult Dodaj(int CennikId, DateTime Od, int? DodatekId)
        {
            if (DodatekId.HasValue)
                Koszyk.DodajDoKoszyka(CennikId, Od, DodatekId.Value);
            else
                Koszyk.DodajDoKoszyka(CennikId, Od);

            // Wracamy do miejsca z ktorego został wywołany ten adres
            //return Redirect(Request.UrlReferrer.ToString());
            return Content(Koszyk.iloscProduktow().ToString());
        }

        //
        // GET: /Koszyk/Zamawiam
        //
        [Authorize]
        public ActionResult Zamawiam(string promoKod)
        {
            IEnumerable<viewKoszykJoined> koszyk = null;
            ViewBag.User = (Membership.GetUser() as MembershipUser);

            Users user = null; 

            using (Entities ie = new Entities())
            {
                var uzer = Membership.GetUser();
                user = ie.Users.SingleOrDefault(d => d.UserName == uzer.UserName);

                int userId = (int)uzer.ProviderUserKey;
                koszyk = ie.viewKoszykJoined.Where(m => m.ispo_user_id == userId).OrderBy(m => m.KoszykData).ToList();
                //string sql = ((ObjectQuery)koszyk).ToTraceString();

                var kod = ie.PromoKod
                            .Include("i3_PromoKodGrupa")
                            .Include("i3_PromoKodGrupa.i3_PromoKodTyp")
                            .Where(m => m.PromoKodKod == promoKod
                                     && m.PromoKodWykorzystany == false
                                     && m.i3_PromoKodGrupa.PromoKodWaznyDo > DateTime.Now)
                            .SingleOrDefault();
                ViewBag.PromoKod = kod;
                //ViewBag.User = user;

                List<Decimal> BruttoPR = new List<decimal>();
                List<Decimal> NettoPR = new List<decimal>();

                if (kod != null)
                {
                    //if (kod.i3_PromoKodGrupa.PromoKodTypId < 13) /* procent */
                    //{
                    //    decimal RabatProc = kod != null ? Math.Round(kod.i3_PromoKodGrupa.i3_PromoKodTyp.Wartosc / 100, 5, MidpointRounding.AwayFromZero) : 0;

                    //    foreach (var item in koszyk)
                    //    {
                    //        decimal RabatWartosc = Math.Round(item.CennikNetto * RabatProc, 2, MidpointRounding.AwayFromZero);

                    //        decimal NettoPoRabacie = Math.Round(item.CennikNetto - RabatWartosc, 2, MidpointRounding.AwayFromZero);
                    //        decimal VatWartosc = Math.Round(NettoPoRabacie * item.VatProc, 2, MidpointRounding.AwayFromZero);
                    //        decimal BruttoPoRabacie = Math.Round(NettoPoRabacie + VatWartosc, 2, MidpointRounding.AwayFromZero);
                    //        NettoPR.Add(NettoPoRabacie);
                    //        BruttoPR.Add(BruttoPoRabacie);
                    //    }
                    //}
                    //else
                    //{
                        decimal RabatWart = kod.i3_PromoKodGrupa.i3_PromoKodTyp.Wartosc; /* złote */

                        foreach (var item in koszyk)
                        {
                            decimal RabatWartosc = RabatWart;// Math.Round(RabatWart, 3, MidpointRounding.AwayFromZero);
                            
                            //decimal NettoPoRabacie = Math.Round(item.CennikNetto - RabatWartosc, 2, MidpointRounding.AwayFromZero);
                            //logger.Debug("NettoPoRabacie: " + NettoPoRabacie.ToString());
                            //decimal VatWartosc = Math.Round(NettoPoRabacie * item.VatProc, 4, MidpointRounding.AwayFromZero);
                            //logger.Debug("VatWartosc: " + VatWartosc.ToString());
                            //decimal BruttoPoRabacie = Math.Round(NettoPoRabacie + VatWartosc, 2, MidpointRounding.AwayFromZero);
                            //logger.Debug("BruttoPoRabacie: " + BruttoPoRabacie.ToString());

                            decimal BruttoPoRabacie = Math.Round(item.CennikBrutto - RabatWart, 2, MidpointRounding.AwayFromZero);
                            logger.Debug("BruttoPoRabacie: " + BruttoPoRabacie.ToString());
                            decimal NettoPoRabacie = Math.Round(BruttoPoRabacie / (1 + item.VatProc), 2, MidpointRounding.AwayFromZero);
                            logger.Debug("NettoPoRabacie: " + NettoPoRabacie.ToString());
                            decimal VatWartosc = Math.Round(BruttoPoRabacie - NettoPoRabacie, 2, MidpointRounding.AwayFromZero);
                            logger.Debug("VatWartosc:  " + VatWartosc.ToString());
                            
                            NettoPR.Add(NettoPoRabacie);
                            BruttoPR.Add(BruttoPoRabacie);
                        }
//                    }
                }
                else
                {

                    foreach (var item in koszyk)
                    {
                        decimal RabatWartosc = 0;
                        
                        decimal NettoPoRabacie = Math.Round(item.CennikNetto - RabatWartosc, 2, MidpointRounding.AwayFromZero);
                        //decimal VatWartosc = Math.Round(NettoPoRabacie * item.VatProc, 2, MidpointRounding.AwayFromZero);
                        decimal VatWartosc = item.CennikVat;
                        decimal BruttoPoRabacie = item.CennikBrutto;//Math.Round(NettoPoRabacie + VatWartosc, 2, MidpointRounding.AwayFromZero);
                        NettoPR.Add(NettoPoRabacie);
                        BruttoPR.Add(BruttoPoRabacie);
                    }

                }

                ViewBag.NettoPR = NettoPR;
                ViewBag.BruttoPR = BruttoPR;
                ViewBag.BruttoPRSuma = BruttoPR.Sum();

                int zamid = 0;
                var k = koszyk.FirstOrDefault();
                if (k != null)
                    zamid = k.KoszykId;

                //ViewBag.dotpay_link_T2 = submitDotPay_T2(ie, ViewBag.User, zamid, k.CennikBrutto, k.WydawcaId) ;
            }

            return View(koszyk);
        }

        //
        // GET: /Koszyk/StatusPlatnosci
        //
        public ActionResult StatusPlatnosci(String status)
        {
            logger.Info("Status płatności: " + status);

            return View(new StatusPlatnosciViewModel(status));
        }

        //
        // POST: /Koszyk/OdbierzDotPay
        //
        [HttpPost]
        public ActionResult OdbierzDotPay(Post DotPayPost)
        {
            try
            {
                DotPayPost.wpis_dataczas = DateTime.Now;

                logger.Info("Wywoałno OdbierzDotPay(): " + System.Environment.NewLine + StringHelper.SerializeToString(DotPayPost));

                string dotpay_klient_id = ConfigurationManager.AppSettings["DotPayKlientId"];

                if (DotPayPost.id != dotpay_klient_id) return Content("WRONG_ID"); // text/html | text/plain

                using (Entities ie = new Entities())
                {
                    ie.Post.Add(DotPayPost);
                    ie.SaveChanges();
                }
            }
            catch(Exception ex)
            {
                logger.ErrorException("Wystąpił błąd w OdbierzDotPay()", ex);
            }

            return Content("OK");
        }

        [HttpPost]
        public ActionResult OdbierzDotPay2(Post2 DotPayPost)
        {
            try
            {
                DotPayPost.wpis_dataczas = DateTime.Now;
                DotPayPost.control = DotPayPost.control.ToUpper();

                logger.Info("Wywoałno OdbierzDotPay2(): " + System.Environment.NewLine + StringHelper.SerializeToString(DotPayPost));

                string dotpay_klient_id = ConfigurationManager.AppSettings["DotPayKlientId"];

                if (DotPayPost.id != dotpay_klient_id) return Content("WRONG_ID"); // text/html | text/plain

                using (Entities ie = new Entities())
                {
                    ie.Post2.Add(DotPayPost);
                    ie.SaveChanges();
                }
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    logger.Debug("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        logger.Debug("- Property: \"{0}\", Error: \"{1}\"",
                                ve.PropertyName, ve.ErrorMessage);
                    }
                }
               // throw;
            }
            catch (Exception ex)
            {
                //logger.ErrorException("Wystąpił błąd w OdbierzDotPay2()", ex);
                logger.Error(ex, "OdbierzDotpay2 Error:"+ex.Message);
                if (ex.InnerException != null)
                    logger.Error(ex.InnerException, "Inner:" + ex.InnerException.Message);
            }

            return Content("OK");
        }

        //
        // POST: /Koszyk/Blokada
        //
        [Authorize]
        [HttpPost]
        public ActionResult Blokada(string promoKod)
        {
            if (Request.IsAjaxRequest())
            {
                string FormaDotPay = "";
                string Error = "";

                using (Entities ie = new Entities())
                {
                    var kod = ie.PromoKod
                                .Include("i3_PromoKodGrupa")
                                .Include("i3_PromoKodGrupa.i3_PromoKodTyp")
                                .Where(m => m.PromoKodKod == promoKod
                                         && m.PromoKodWykorzystany == false
                                         && m.i3_PromoKodGrupa.PromoKodWaznyDo > DateTime.Now)
                                .SingleOrDefault();

                    // Sprawdzamy poprawność promoKodu
                    if (!string.IsNullOrWhiteSpace(promoKod) && kod == null)
                    {
                        Error = "Błędny kod rabatowy!";
                    }
                    else
                    {
                        int userId = 0; ;
                        MembershipUser user = (MembershipUser)Membership.GetUser();
                        using (Entities db = new Entities())
                        {
                            Users user_pay = db.Users.SingleOrDefault(d => d.UserName == user.UserName);
                            userId = user_pay.ispo_user_id;
                        }
                            
                        var koszyk = ie.viewKoszykJoined.Where(m => m.ispo_user_id == userId).OrderBy(m => m.KoszykData).ToList();
                        //string sql = ((ObjectQuery)koszyk).ToTraceString();

                        var koszykLista = koszyk.ToList();

                        if (koszykLista.Count > 0)
                        {
                            // Jeżeli koszyk nie jest pusty zapisujemy ...

                            var koszykIds = koszyk.Select(m => m.KoszykId);
                            //Dodatek dodatki = ie.Koszyk.Where(m => koszykIds.Contains(m.KoszykId)).Select(m => m.i3_Dodatek);
                            //var dodatki = ie.Koszyk.Where(m => koszykIds.Contains(m.KoszykId)).SelectMany(m => m.i3_Dodatek).ToList();
                            var dodatki = ie.Koszyk.Where(m => koszykIds.Contains(m.KoszykId)).ToDictionary(m => m.KoszykId, n => n.i3_Dodatek);

                            //decimal RabatProc = kod != null ? Math.Round(kod.i3_PromoKodGrupa.i3_PromoKodTyp.Wartosc / 100, 4, MidpointRounding.AwayFromZero) : 0;
                            decimal RabatWart = kod != null ? Math.Round(kod.i3_PromoKodGrupa.i3_PromoKodTyp.Wartosc, 2, MidpointRounding.AwayFromZero) : 0;

                            using (TransactionScope scope = new TransactionScope())
                            {
                                Zamowienie zamowienie = new Zamowienie();
                                zamowienie.issg_user_id = userId;
                                zamowienie.ZamowienieData = DateTime.Now;
                                zamowienie.ZamowienieOplacone = false;
                                zamowienie.ZamowienieRecznaAktywacja = false;
                                zamowienie.ZamowienieZablokowane = true;

                                if (kod != null )
                                {
                                    zamowienie.ZamowienieProcRd = kod.i3_PromoKodGrupa.i3_PromoKodTyp.Wartosc;
                                    if (kod != null)
                                    {
                                        zamowienie.ZamowieniePromoKodId = kod.PromoKodId;
                                        zamowienie.ZamowieniePromoKodKod = kod.PromoKodKod;
                                        zamowienie.ZamowieniePromoKodTypNazwa = kod.i3_PromoKodGrupa.i3_PromoKodTyp.Nazwa;

                                        kod.PromoKodWykorzystany = true;
                                    }
                                }

                                // Te pola dodajemy narazie jako 0
                                // zostaną one uzupenione poniżej, po dodaniu wszystkich pozycji
                                zamowienie.ZamowieniePrzedRd = 0;
                                zamowienie.ZamowienieWartoscRd = 0;
                                zamowienie.ZamowienieBrutto = 0; //koszykLista.Sum(m => m.CennikBrutto);
                                zamowienie.ZamowienieNetto = 0;  //koszykLista.Sum(m => m.CennikNetto);
                                zamowienie.ZamowienieVat = 0;    //koszykLista.Sum(m => m.CennikVat);

                                ie.Zamowienie.Add(zamowienie);
                                ie.SaveChanges();

                                decimal BruttoPoRabacieSuma = 0;
                                decimal NettoPoRabacieSuma = 0;
                                decimal VatWartoscSuma = 0;
                                decimal RabatWartoscSuma = 0;
                                foreach (var item in koszyk)
                                {
                                    // Obliczamy poszczególne wartości
                                    decimal BruttoPoRabacie = Math.Round(item.CennikBrutto - RabatWart, 2, MidpointRounding.AwayFromZero);
                                    decimal NettoPoRabacie = Math.Round(BruttoPoRabacie /(1+item.VatProc), 2, MidpointRounding.AwayFromZero);
                                    decimal VatWartosc = Math.Round(BruttoPoRabacie - NettoPoRabacie, 2, MidpointRounding.AwayFromZero);
                                    
                                    NettoPoRabacieSuma += NettoPoRabacie;
                                    BruttoPoRabacieSuma += BruttoPoRabacie;
                                    VatWartoscSuma += VatWartosc;
                                    RabatWartoscSuma += RabatWart;

                                    ZamowieniePozycja pozycja = new ZamowieniePozycja();
                                    // Pola ogolne
                                    pozycja.CennikId = item.CennikId;
                                    pozycja.ZamowienieId = zamowienie.ZamowienieId;
                                    pozycja.VatId = item.VatId;
                                    pozycja.ZamowieniePozycjaOd = item.KoszykOd;
                                    pozycja.ZamowieniePozycjaDo = item.KoszykOd.AddMonths(item.RodzajOkresuMiesiac).AddDays(item.RodzajOkresuDzien).AddDays(-1);
                                    // Pola rabatowe
                                    pozycja.ZamowieniePozycjaPrzedRD = item.CennikNetto;
                                    pozycja.ZamowieniePozycjaProcRD = 0;//kod.i3_PromoKodGrupa.i3_PromoKodTyp.Wartosc;
                                    pozycja.ZamowieniePozycjaWartoscRD = RabatWart;
                                    // Ceny
                                    pozycja.ZamowieniePozycjaNetto = NettoPoRabacie;
                                    pozycja.ZamowieniePozycjaVat = VatWartosc;
                                    pozycja.ZamowieniePozycjaBrutto = BruttoPoRabacie;
                                    pozycja.ZamowieniePozycjaWidoczna = "t";

                                    foreach (Dodatek d in dodatki[item.KoszykId])
                                    {
                                        pozycja.i3_Dodatek.Add(d);
                                    }

                                    ie.ZamowieniePozycja.Add(pozycja);
                                }

                                // uzupełniamy zamowienie sumami
                                zamowienie.ZamowieniePrzedRd = koszykLista.Sum(m => m.CennikNetto);
                                zamowienie.ZamowienieWartoscRd = RabatWartoscSuma;
                                zamowienie.ZamowienieBrutto = BruttoPoRabacieSuma;
                                zamowienie.ZamowienieNetto = NettoPoRabacieSuma;
                                zamowienie.ZamowienieVat = VatWartoscSuma;

                                int[] ids = koszykLista.Select(n => n.KoszykId).ToArray();

                                var items = ie.Koszyk.Where(m => ids.Contains(m.KoszykId));

                                foreach (var item in items)
                                {
                                    ie.Koszyk.Remove(item);
                                }

                                ie.SaveChanges();

                                FormaDotPay = submitDotPay(ie, user, zamowienie.ZamowienieId, zamowienie.ZamowienieBrutto, 1);

                                scope.Complete();
                            }
                        }
                        else
                        {
                            // Jeżeli jest pusty zwracamy błąd
                            Error = "Twoj koszyk jest pusty. Jeżeli dokonywane było przed chwilą zamowienie, zostało ono zablokowane i przekirowane do systemu płatności. Aby ponowić próbę płaności, należy złożyć nowe zamówienie.";
                        }
                    }

                    return Json(new { forma = FormaDotPay, error = Error});
                }
            }

            return new EmptyResult();
        }

        [Authorize]
        [HttpPost]
        public ActionResult Blokada2(string promoKod)
        {
            if (Request.IsAjaxRequest())
            {
                string FormaDotPay = "";
                //string dotpay_link_T2 = string.Empty;
                string Error = "";

                using (Entities ie = new Entities())
                {
                    var kod = ie.PromoKod
                                .Include("i3_PromoKodGrupa")
                                .Include("i3_PromoKodGrupa.i3_PromoKodTyp")
                                .Where(m => m.PromoKodKod == promoKod
                                         && m.PromoKodWykorzystany == false
                                         && m.i3_PromoKodGrupa.PromoKodWaznyDo > DateTime.Now)
                                .SingleOrDefault();

                    // Sprawdzamy poprawność promoKodu
                    if (!string.IsNullOrWhiteSpace(promoKod) && kod == null)
                    {
                        Error = "Błędny kod rabatowy!";
                    }
                    else
                    {
                        int userId = 0; ;
                        MembershipUser user = (MembershipUser)Membership.GetUser();
                        using (Entities db = new Entities())
                        {
                            Users user_pay = db.Users.SingleOrDefault(d => d.UserName == user.UserName);
                            userId = user_pay.ispo_user_id;
                        }

                        var koszyk = ie.viewKoszykJoined.Where(m => m.ispo_user_id == userId).OrderBy(m => m.KoszykData).ToList();
                        //string sql = ((ObjectQuery)koszyk).ToTraceString();

                        var koszykLista = koszyk.ToList();

                        if (koszykLista.Count > 0)
                        {
                            // Jeżeli koszyk nie jest pusty zapisujemy ...

                            var koszykIds = koszyk.Select(m => m.KoszykId);
                            //Dodatek dodatki = ie.Koszyk.Where(m => koszykIds.Contains(m.KoszykId)).Select(m => m.i3_Dodatek);
                            //var dodatki = ie.Koszyk.Where(m => koszykIds.Contains(m.KoszykId)).SelectMany(m => m.i3_Dodatek).ToList();
                            var dodatki = ie.Koszyk.Where(m => koszykIds.Contains(m.KoszykId)).ToDictionary(m => m.KoszykId, n => n.i3_Dodatek);

                            //decimal RabatProc = kod != null ? Math.Round(kod.i3_PromoKodGrupa.i3_PromoKodTyp.Wartosc / 100, 4, MidpointRounding.AwayFromZero) : 0;
                            decimal RabatWart = kod != null ? Math.Round(kod.i3_PromoKodGrupa.i3_PromoKodTyp.Wartosc, 2, MidpointRounding.AwayFromZero) : 0;

                            using (TransactionScope scope = new TransactionScope())
                            {
                                Zamowienie zamowienie = new Zamowienie();
                                zamowienie.issg_user_id = userId;
                                zamowienie.ZamowienieData = DateTime.Now;
                                zamowienie.ZamowienieOplacone = false;
                                zamowienie.ZamowienieRecznaAktywacja = false;
                                zamowienie.ZamowienieZablokowane = true;

                                if (kod != null)
                                {
                                    zamowienie.ZamowienieProcRd = kod.i3_PromoKodGrupa.i3_PromoKodTyp.Wartosc;
                                    if (kod != null)
                                    {
                                        zamowienie.ZamowieniePromoKodId = kod.PromoKodId;
                                        zamowienie.ZamowieniePromoKodKod = kod.PromoKodKod;
                                        zamowienie.ZamowieniePromoKodTypNazwa = kod.i3_PromoKodGrupa.i3_PromoKodTyp.Nazwa;

                                        kod.PromoKodWykorzystany = true;
                                    }
                                }

                                // Te pola dodajemy narazie jako 0
                                // zostaną one uzupenione poniżej, po dodaniu wszystkich pozycji
                                zamowienie.ZamowieniePrzedRd = 0;
                                zamowienie.ZamowienieWartoscRd = 0;
                                zamowienie.ZamowienieBrutto = 0; //koszykLista.Sum(m => m.CennikBrutto);
                                zamowienie.ZamowienieNetto = 0;  //koszykLista.Sum(m => m.CennikNetto);
                                zamowienie.ZamowienieVat = 0;    //koszykLista.Sum(m => m.CennikVat);

                                ie.Zamowienie.Add(zamowienie);
                                ie.SaveChanges();

                                logger.Debug("ZAM ID: "+zamowienie.ZamowienieId.ToString());

                                decimal BruttoPoRabacieSuma = 0;
                                decimal NettoPoRabacieSuma = 0;
                                decimal VatWartoscSuma = 0;
                                decimal RabatWartoscSuma = 0;
                                foreach (var item in koszyk)
                                {
                                    // Obliczamy poszczególne wartości
                                    decimal BruttoPoRabacie = Math.Round(item.CennikBrutto - RabatWart, 2, MidpointRounding.AwayFromZero);
                                    decimal NettoPoRabacie = Math.Round(BruttoPoRabacie / (1 + item.VatProc), 2, MidpointRounding.AwayFromZero);
                                    decimal VatWartosc = Math.Round(BruttoPoRabacie - NettoPoRabacie, 2, MidpointRounding.AwayFromZero);

                                    NettoPoRabacieSuma += NettoPoRabacie;
                                    BruttoPoRabacieSuma += BruttoPoRabacie;
                                    VatWartoscSuma += VatWartosc;
                                    RabatWartoscSuma += RabatWart;

                                    ZamowieniePozycja pozycja = new ZamowieniePozycja();
                                    // Pola ogolne
                                    pozycja.CennikId = item.CennikId;
                                    pozycja.ZamowienieId = zamowienie.ZamowienieId;
                                    pozycja.VatId = item.VatId;
                                    pozycja.ZamowieniePozycjaOd = item.KoszykOd;
                                    pozycja.ZamowieniePozycjaDo = item.KoszykOd.AddMonths(item.RodzajOkresuMiesiac).AddDays(item.RodzajOkresuDzien).AddDays(-1);
                                    // Pola rabatowe
                                    pozycja.ZamowieniePozycjaPrzedRD = item.CennikNetto;
                                    pozycja.ZamowieniePozycjaProcRD = 0;//kod.i3_PromoKodGrupa.i3_PromoKodTyp.Wartosc;
                                    pozycja.ZamowieniePozycjaWartoscRD = RabatWart;
                                    // Ceny
                                    pozycja.ZamowieniePozycjaNetto = NettoPoRabacie;
                                    pozycja.ZamowieniePozycjaVat = VatWartosc;
                                    pozycja.ZamowieniePozycjaBrutto = BruttoPoRabacie;
                                    pozycja.ZamowieniePozycjaWidoczna = "t";

                                    foreach (Dodatek d in dodatki[item.KoszykId])
                                    {
                                        pozycja.i3_Dodatek.Add(d);
                                    }

                                    ie.ZamowieniePozycja.Add(pozycja);
                                }

                                // uzupełniamy zamowienie sumami
                                zamowienie.ZamowieniePrzedRd = koszykLista.Sum(m => m.CennikNetto);
                                zamowienie.ZamowienieWartoscRd = RabatWartoscSuma;
                                zamowienie.ZamowienieBrutto = BruttoPoRabacieSuma;
                                zamowienie.ZamowienieNetto = NettoPoRabacieSuma;
                                zamowienie.ZamowienieVat = VatWartoscSuma;

                                int[] ids = koszykLista.Select(n => n.KoszykId).ToArray();

                                var items = ie.Koszyk.Where(m => ids.Contains(m.KoszykId));

                                foreach (var item in items)
                                {
                                    ie.Koszyk.Remove(item);
                                }

                                ie.SaveChanges();

                                logger.Debug("Blokada2 - submitDotpay_T2: " +
                                   zamowienie.ZamowienieId.ToString(),
                                   zamowienie.ZamowienieBrutto.ToString());

                                FormaDotPay = submitDotPay_T2_Post(ie, user, zamowienie.ZamowienieId, zamowienie.ZamowienieBrutto, 1);
                                //dotpay_link_T2 = submitDotPay_T2(ie, user, zamowienie.ZamowienieId, zamowienie.ZamowienieBrutto, 1);
                              

                                scope.Complete();
                            }
                        }
                        else
                        {
                            // Jeżeli jest pusty zwracamy błąd
                            Error = "Twoj koszyk jest pusty. Jeżeli dokonywane było przed chwilą zamowienie, zostało ono zablokowane i przekirowane do systemu płatności. Aby ponowić próbę płaności, należy złożyć nowe zamówienie.";
                        }
                    }

                    return Json(new { forma = FormaDotPay, error = Error});
                    //return Json(new { error = Error, dotpay_link = dotpay_link_T2 });


                }
            }

            return new EmptyResult();
        }


        private string submitDotPay(Entities ie, MembershipUser user, int li_zamid, decimal ld_kwota, int li_wydawca_id)
        {
            
            Entities db = new Entities();
            
            Users user_pay = db.Users.SingleOrDefault(d=>d.UserName == user.UserName);
            int li_ispo_user_id = user_pay.ispo_user_id;
           
            Guid control = Guid.NewGuid();

            try
            {
                Payment payment = new Payment();
                payment.ispo_user_id = li_ispo_user_id;
                payment.PaymentBlokada = "t";
                payment.PaymentControl = control;
                payment.PaymentDataczas = DateTime.Now;
                payment.PaymentKwota = ld_kwota;
                payment.PaymentMd5 = "";
                payment.PaymentStatus = "";
                payment.WydawcaId = li_wydawca_id;
                payment.ZamowienieId = li_zamid;

                ie.Payment.Add(payment);
                ie.SaveChanges();

                var wydawca = ie.Wydawca.Where(m => m.WydawcaId == li_wydawca_id).Single();

                          //string ls_allpay_klient_id = GetOpcjeValue("ALLPAY_ID");
                string ls_allpay_klient_id = ConfigurationManager.AppSettings["DotPayKlientId"];
                
                string ls_description = "Opłata za zamówienie nr: " + li_zamid.ToString();
                    
                string ls_amount = ld_kwota.ToString();//.Replace(",",".");
                
                string ls_URLC = Request.Url.AbsoluteUri.Replace(Request.Url.PathAndQuery, string.Empty) + @"/Koszyk/OdbierzDotPay/";

                string ls_lang = "pl";

              
                string ls_onlinetransfer = "";
                string ls_firstname = user_pay.Imie;
                string ls_lastname = user_pay.Nazwisko;
                string ls_email = user_pay.Email;
                string ls_street = string.IsNullOrWhiteSpace(user_pay.ulica) ? string.Empty : user_pay.ulica;
                string ls_street_n1 = string.IsNullOrWhiteSpace(user_pay.budynek) ? string.Empty : user_pay.budynek;
                string ls_street_n2 = string.IsNullOrWhiteSpace(user_pay.mieszkanie) ? string.Empty : user_pay.mieszkanie;
                string ls_city = string.IsNullOrWhiteSpace(user_pay.Miejscowosc) ? string.Empty : user_pay.Miejscowosc;
                string ls_postcode  = string.IsNullOrWhiteSpace(user_pay.Kod) ? string.Empty : user_pay.Kod;

                string ls_type = "3";
                string ls_URL = Request.Url.AbsoluteUri.Replace(Request.Url.PathAndQuery, string.Empty) + @"/Koszyk/StatusPlatnosci";
                string ls_p_info = "#B; #FF0022;" + wydawca.WydawcaNazwa + " =B;";
                string ls_txt_guzik = "Powrót do serwisu";

                string url = ConfigurationManager.AppSettings["DotPayLink"];

                //string data = String.Format("parameter1={0}&parameter2={1}&parameter3={2}", parameter1, parameter2, parameter3);
                string data = String.Format("<INPUT name='URL' id='URL' type='hidden' value='{0}'>" +
                                            "<INPUT name='type' id='type' type='hidden' value='{1}'>" +
                                            "<INPUT name='amount' id='amount' type='hidden' value='{2}'>" +
                                            "<INPUT name='description' id='description' type='hidden' value='{3}'>" +
                                            "<INPUT name='id' id='id' type='hidden' value='{4}'>" +
                                            "<INPUT name='lang' id='lang' type='hidden' value='{5}'>" +
                                            "<INPUT name='URLC' id='URLC' type='hidden' value='{6}'>" +
                                            "<INPUT name='control' id='control' type='hidden' value='{7}'>" +
                                            "<INPUT name='firstname' id='firstname' type='hidden' value='{8}'>" +
                                            "<INPUT name='postcode' id='postcode' type='hidden' value='{9}'>" +
                                            "<INPUT name='onlinetransfer' id='onlinetransfer' type='hidden' value='{10}'>" +
                                            "<INPUT name='city' id='city' type='hidden' value='{11}'>" +
                                            "<INPUT name='street_n2' id='street_n2' type='hidden' value='{12}'>" +
                                            "<INPUT name='street_n1' id='street_n1' type='hidden' value='{13}'>" +
                                            "<INPUT name='street' id='street' type='hidden' value='{14}'>" +
                                            "<INPUT name='email' id='email' type='hidden' value='{15}'>" +
                                            "<INPUT name='lastname' id='lastname' type='hidden' value='{16}'>" +
                                            "<INPUT name='p_info' id='p_info' type='hidden' value='{17}'>" +
                                            "<INPUT name='buttontext' id='buttontext' type='hidden' value='{18}'>",
                                            ls_URL,
                                            ls_type,
                                            ls_amount,
                                            ls_description,
                                            ls_allpay_klient_id,
                                            ls_lang,
                                            ls_URLC,
                                            control.ToString(),
                                            ls_firstname,
                                            ls_postcode,
                                            ls_onlinetransfer,
                                            ls_city,
                                            ls_street_n2,
                                            ls_street_n1,
                                            ls_street,
                                            ls_email,
                                            ls_lastname,
                                            ls_p_info,
                                            ls_txt_guzik);

                //System.Web.UI.LiteralControl lc = new System.Web.UI.LiteralControl();
                string ret = "";
                ret += "<form id='FormaDotPay' name='FormaDotPay' action='" + url + "' method='post'>";
                ret += data;
                ret += "</form>";
                //lc.Text += "</div></form><script type='text/javascript'>document.forms['formallpay'].submit();</script>";
                //the_page.Controls.AddAt(0, lc); //na początku, inaczej startskrypt nie widzi formy
                //the_page.Controls.Add(lc);
                logger.Debug("formAllpay: " + ret);
                return ret;
            }
            catch (Exception ex)
            {
                logger.Error("submitDotPay error: " + ex.Message);
                throw ex;
            }
        }

        public static string getHashSha256(string text)
        {
            byte[] bytes = Encoding.Unicode.GetBytes(text);
            SHA256Managed hashstring = new SHA256Managed();
            byte[] hash = hashstring.ComputeHash(bytes);
            string hashString = string.Empty;
            foreach (byte x in hash)
            {
                hashString += String.Format("{0:x2}", x);
            }
            return hashString;
        }

        public static String sha256_hash(String value)
        {
            StringBuilder Sb = new StringBuilder();

            using (SHA256 hash = SHA256Managed.Create())
            {
                Encoding enc = Encoding.UTF8;
                Byte[] result = hash.ComputeHash(enc.GetBytes(value));

                foreach (Byte b in result)
                    Sb.Append(b.ToString("x2"));
            }

            return Sb.ToString();
        }

        public static string myHash(string str)
        {
            string res = string.Empty;
            using (SHA256 sha256Hash = SHA256.Create())
            {
                byte[] hashedBytes = sha256Hash.ComputeHash(Encoding.UTF8.GetBytes(str));

                for (int i = 0; i < hashedBytes.Length; i++)
                {
                    res += hashedBytes[i].ToString("x2");
                }
            }

            return res;
        }

        static string hash3(string rawData)
        {
            // Create a SHA256   
            using (SHA256 sha256Hash = SHA256.Create())
            {
                // ComputeHash - returns byte array  
                byte[] bytes = sha256Hash.ComputeHash(Encoding.UTF8.GetBytes(rawData));

                // Convert byte array to a string   
                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < bytes.Length; i++)
                {
                    builder.Append(bytes[i].ToString("x2"));
                }
                return builder.ToString();
            }
        }

        public static string hash4(string str)
        {
            System.Security.Cryptography.SHA256Managed hm = new System.Security.Cryptography.SHA256Managed();
            byte[] hashValue = hm.ComputeHash(System.Text.Encoding.UTF8.GetBytes(str));

            return System.BitConverter.ToString(hashValue).Replace("-", "").ToLower();
        }


        public static string hash5(string str)
        {

            //Convert.ToBase64String(str);

            System.Security.Cryptography.SHA256Managed hm = new System.Security.Cryptography.SHA256Managed();
            byte[] hashValue = hm.ComputeHash(System.Text.Encoding.UTF8.GetBytes(str));

            return  System.BitConverter.ToString(hashValue).Replace("-", "").ToLower();

        }

        private string submitDotPay_T2(Entities ie, MembershipUser user, int li_zamid, decimal ld_kwota, int li_wydawca_id)
        {
            /* w odróżnieniu od wersji T1 gdzie generuje się formę dla posta, wersja T2 
             generuje link dla metody GET */

            Entities db = new Entities();

            Users user_pay = db.Users.SingleOrDefault(d => d.UserName == user.UserName);
            int li_ispo_user_id = user_pay.ispo_user_id;

            Guid control = Guid.NewGuid();

            try
            {
                Payment payment = new Payment();
                payment.ispo_user_id = li_ispo_user_id;
                payment.PaymentBlokada = "t";
                payment.PaymentControl = control;
                payment.PaymentDataczas = DateTime.Now;
                payment.PaymentKwota = ld_kwota;
                payment.PaymentMd5 = "";
                payment.PaymentStatus = "";
                payment.WydawcaId = li_wydawca_id;
                payment.ZamowienieId = li_zamid;

                ie.Payment.Add(payment);
                ie.SaveChanges();

                var wydawca = ie.Wydawca.Where(m => m.WydawcaId == li_wydawca_id).Single();

                string ls_allpay_klient_id = ConfigurationManager.AppSettings["DotPayKlientId"];
                string ls_description = "Opłata za zamówienie nr: " + li_zamid.ToString();
                string ls_amount = ld_kwota.ToString().Replace(",",".");
                string ls_URLC = Request.Url.AbsoluteUri.Replace(Request.Url.PathAndQuery, string.Empty) + @"/Koszyk/OdbierzDotPay2/";
                string ls_lang = "pl";
                string ls_currency = "PLN";

                string ls_firstname = user_pay.Imie;
                string ls_lastname = user_pay.Nazwisko;
                string ls_email = user_pay.Email;
                string ls_street = string.IsNullOrWhiteSpace(user_pay.ulica) ? string.Empty : user_pay.ulica;
                string ls_street_n1 = string.IsNullOrWhiteSpace(user_pay.budynek) ? string.Empty : user_pay.budynek;
                string ls_street_n2 = string.IsNullOrWhiteSpace(user_pay.mieszkanie) ? string.Empty : user_pay.mieszkanie;
                string ls_city = string.IsNullOrWhiteSpace(user_pay.Miejscowosc) ? string.Empty : user_pay.Miejscowosc;
                string ls_postcode = string.IsNullOrWhiteSpace(user_pay.Kod) ? string.Empty : user_pay.Kod;
                string ls_country = "POL";

                string ls_type = "0";
                string ls_URL = Request.Url.AbsoluteUri.Replace(Request.Url.PathAndQuery, string.Empty) + @"/Koszyk/StatusPlatnosci";
                string ls_p_info = "#B; #FF0022;" + wydawca.WydawcaNazwa + " =B;";
                string ls_txt_guzik = "Powrót do serwisu";

                string url = ConfigurationManager.AppSettings["DotPayLink"];
                
                string ls_pin = string.Empty;
                if (string.IsNullOrEmpty(ls_pin))
                    ls_pin = "YniyFZyHSYmAIRQe8jj2hwJvSDjYmHcL";

                string api_version = string.Empty;//GetOpcjeValue("api_version"); //dev
                if (string.IsNullOrEmpty(api_version))
                    api_version = "dev";

                string ls_control = control.ToString();

                string ls_inputs = string.Concat(
                                             ls_pin,
                                             api_version,
                                             ls_lang,
                                             ls_allpay_klient_id,
                                             ls_amount,
                                             ls_currency,
                                             ls_description,
                                             ls_control,
                                             ls_URL,
                                             ls_type,
                                             ls_txt_guzik,
                                             ls_URLC,
                                             ls_firstname,
                                             ls_postcode,
                                             ls_lastname,
                                             ls_email,
                                             ls_street,
                                             ls_street_n1,
                                             ls_street_n2,
                                             ls_city,
                                             ls_postcode,
                                             ls_country,
                                             ls_p_info
                                            );

                logger.Debug("dotpay_inputs: " + ls_inputs);

                string chk = getHashSha256(ls_inputs);
               // string chk2 = myHash(ls_inputs);
                //string chk3 = 

                logger.Debug("chk: " + chk);

                StringBuilder sb = new StringBuilder(url);

                sb.Append("?url="+ls_URL);
                sb.Append("&type="+ ls_type);
                sb.Append("&currency="+ ls_currency);
                sb.Append("&amount="+ ls_amount);
                sb.Append("&description="+ ls_description);
                sb.Append("&id="+ ls_allpay_klient_id);
                sb.Append("&lang="+ ls_lang);
                sb.Append("&URLC="+ ls_URLC);
                sb.Append("&control="+ ls_control);
                sb.Append("&firstname="+ ls_firstname);
                sb.Append("&postcode="+ ls_postcode);
                //sb.Append("&onlinetransfer="+ ls_onlinetransfer);
                sb.Append("&city="+ ls_city);
                sb.Append("&street_n2="+ ls_street_n2);
                sb.Append("&street_n1="+ ls_street_n1);
                sb.Append("&street="+ ls_street);
                sb.Append("&email="+ ls_email);
                sb.Append("&lastname="+ ls_lastname);
                sb.Append("&p_info="+ ls_p_info);
                sb.Append("&buttontext="+ ls_txt_guzik);
                sb.Append("&chk="+ chk);

                ServicePointManager.Expect100Continue = true;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                logger.Debug("DOTPAY T2 RESULT: "+sb.ToString());

                return sb.ToString();
            }
            catch (Exception ex)
            {
                logger.Error("submitDotPayT2 error: " + ex.Message);
                throw ex;
            }
        }

        private string submitDotPay_T2_Post(Entities ie, MembershipUser user, int li_zamid, decimal ld_kwota, int li_wydawca_id)
        {
            /* w odróżnieniu od wersji T1 gdzie generuje się formę dla posta, wersja T2 
             generuje link dla metody GET */

            Entities db = new Entities();

            Users user_pay = db.Users.SingleOrDefault(d => d.UserName == user.UserName);
            int li_ispo_user_id = user_pay.ispo_user_id;

            Guid control = Guid.NewGuid();
            //Guid control = Guid.Parse("f23e3c63-533b-4bdc-a197-790662badb54"); test

            try
            {
                Payment payment = new Payment();
                payment.ispo_user_id = li_ispo_user_id;
                payment.PaymentBlokada = "t";
                payment.PaymentControl = control;
                payment.PaymentDataczas = DateTime.Now;
                payment.PaymentKwota = ld_kwota;
                payment.PaymentMd5 = "";
                payment.PaymentStatus = "";
                payment.WydawcaId = li_wydawca_id;
                payment.ZamowienieId = li_zamid;

                ie.Payment.Add(payment);
                ie.SaveChanges();

                var wydawca = ie.Wydawca.Where(m => m.WydawcaId == li_wydawca_id).Single();

                string ls_allpay_klient_id = ConfigurationManager.AppSettings["DotPayKlientId"];
                string ls_pin = ConfigurationManager.AppSettings["DotPayPin"];
                string api_version = ConfigurationManager.AppSettings["DotPayApiVersion"]; //dev
                string url = ConfigurationManager.AppSettings["DotPayLink"];

                ls_pin = string.IsNullOrEmpty(ls_pin) ? "YniyFZyHSYmAIRQe8jj2hwJvSDjYmHcL" : ls_pin;
                api_version = string.IsNullOrEmpty(api_version) ? "dev" : api_version;

                string ls_description = "Zamowienie nr "+ li_zamid.ToString();
                string ls_amount = ld_kwota.ToString().Replace(",", ".");
                string ls_URLC = Request.Url.AbsoluteUri.Replace(Request.Url.PathAndQuery, string.Empty) + @"/Koszyk/OdbierzDotPay2/";
                string ls_lang = "pl";
                string ls_currency = "PLN";

                string ls_firstname = user_pay.Imie;
                string ls_lastname = user_pay.Nazwisko;
                string ls_email = user_pay.Email;
                string ls_street = string.IsNullOrWhiteSpace(user_pay.ulica) ? string.Empty : user_pay.ulica;
                string ls_street_n1 = string.IsNullOrWhiteSpace(user_pay.budynek) ? string.Empty : user_pay.budynek;
                string ls_street_n2 = string.IsNullOrWhiteSpace(user_pay.mieszkanie) ? string.Empty : user_pay.mieszkanie;
                string ls_city = string.IsNullOrWhiteSpace(user_pay.Miejscowosc) ? string.Empty : user_pay.Miejscowosc;
                string ls_postcode = string.IsNullOrWhiteSpace(user_pay.Kod) ? string.Empty : user_pay.Kod;
                string ls_country = "POL";

                string ls_type = "0";
                string ls_URL = Request.Url.AbsoluteUri.Replace(Request.Url.PathAndQuery, string.Empty) + @"/Koszyk/StatusPlatnosci";
                string ls_p_info =  wydawca.WydawcaNazwa;
                string ls_txt_guzik = "Powrót do serwisu";

                string ls_control = control.ToString();

                string ls_inputs = string.Concat(
                                             ls_pin,
                                             api_version,
                                             ls_lang,
                                             ls_allpay_klient_id,
                                             ls_amount,
                                             ls_currency,
                                             ls_description,
                                             ls_control,
                                             ls_URL,
                                             ls_type,
                                             ls_txt_guzik,
                                             ls_URLC,
                                             ls_firstname,
                                             ls_lastname,
                                             ls_email,
                                             ls_street,
                                             ls_street_n1,
                                             ls_street_n2,
                                             ls_city,
                                             ls_postcode,
                                             ls_country,
                                             ls_p_info
                                            );

                logger.Debug("Inputs=" + ls_inputs);

                string chk1 = getHashSha256(ls_inputs);
                string chk2 = sha256_hash(ls_inputs);
                string chk3 = hash3(ls_inputs);
                string chk4 = hash4(ls_inputs);
                string chk5 = hash5(ls_inputs);

                logger.Debug("Inputs hashed="+ ls_inputs 
                    + "\r\n chk1=" + chk1
                    + "\r\n chk2=" + chk2
                    + "\r\n chk3=" + chk3
                    + "\r\n chk4=" + chk4
                    + "\r\n chk5=" + chk5);

               string data = String.Format("<INPUT name='url' id='url' type='hidden' value='{0}'>\r\n" +
                                           "<INPUT name='type' id='type' type='hidden' value='{1}'>\r\n" +
                                           "<INPUT name='amount' id='amount' type='hidden' value='{2}'>\r\n" +
                                           "<INPUT name='description' id='description' type='hidden' value='{3}'>\r\n" +
                                           "<INPUT name='id' id='id' type='hidden' value='{4}'>\r\n" +
                                           "<INPUT name='lang' id='lang' type='hidden' value='{5}'>\r\n" +
                                           "<INPUT name='urlc' id='urlc' type='hidden' value='{6}'>\r\n" +
                                           "<INPUT name='control' id='control' type='hidden' value='{7}'>\r\n" +
                                           "<INPUT name='firstname' id='firstname' type='hidden' value='{8}'>\r\n" +
                                           "<INPUT name='postcode' id='postcode' type='hidden' value='{9}'>\r\n" +
                                           "<INPUT name='city' id='city' type='hidden' value='{10}'>\r\n" +
                                           "<INPUT name='street_n2' id='street_n2' type='hidden' value='{11}'>\r\n" +
                                           "<INPUT name='street_n1' id='street_n1' type='hidden' value='{12}'>\r\n" +
                                           "<INPUT name='street' id='street' type='hidden' value='{13}'>\r\n" +
                                           "<INPUT name='email' id='email' type='hidden' value='{14}'>\r\n" +
                                           "<INPUT name='lastname' id='lastname' type='hidden' value='{15}'>\r\n" +
                                           "<INPUT name='p_info' id='p_info' type='hidden' value='{16}'>\r\n" +
                                           "<INPUT name='buttontext' id='buttontext' type='hidden' value='{17}'>\r\n" +
                                           "<INPUT name='currency' id='currency' type='hidden' value='{18}'>\r\n" +
                                           "<INPUT name='country' id='country' type='hidden' value='{19}'>\r\n"+
                                           "<INPUT name='api_version' id='api_version' type='hidden' value='{20}'>\r\n" +
                                           "<INPUT name='chk' id='chk' type='hidden' value='{21}'>\r\n",
                                           ls_URL,
                                           ls_type,
                                           ls_amount,
                                           ls_description,
                                           ls_allpay_klient_id,
                                           ls_lang,
                                           ls_URLC,
                                           ls_control,
                                           ls_firstname,
                                           ls_postcode,
                                           ls_city,
                                           ls_street_n2,
                                           ls_street_n1,
                                           ls_street,
                                           ls_email,
                                           ls_lastname,
                                           ls_p_info,
                                           ls_txt_guzik,
                                           ls_currency,
                                           ls_country,
                                           api_version,
                                           chk5);

                string ret = "";
                ret += "<form id='FormaDotPay' name='FormaDotPay' action='" + url + "' method='post' accept-charset='UTF-8'>\r\n";
                ret += data;
                ret += "</form>";

                ServicePointManager.Expect100Continue = true;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                logger.Debug("DOTPAY T2 RESULT: " + ret);

                return ret;
            }
            catch (Exception ex)
            {
                logger.Error("submitDotPayT2_Post error: " + ex.Message);
                throw ex;
            }
        }


        public ActionResult Wykup(int id = 0)
        {

            ISSG4.Controllers.PrenumerataController.NowaViewModel nowa = new ISSG4.Controllers.PrenumerataController.NowaViewModel();
            nowa.FillLists();
            //nowa.Od = DateTime.Today.ToString("yyyy-MM-dd");
            nowa.Od = DateTime.Today.ToString("dd-MM-yyyy");

            KoszykWykupModel kwm = new KoszykWykupModel();
            kwm.nowaModel = nowa;

            if (id == 0)
            {
                id = getBiezaceWydanie();
            }


            WydanieJoinedResult wydanie = null;
            using (Entities ie = new Entities())
            {
                wydanie = ie.getWydanieJoined(id).Single();

                //nowa.Od = wydanie.WydanieData.ToString("yyyy-MM-dd");
                nowa.Od = wydanie.WydanieData.ToString("dd-MM-yyyy");

                ViewBag.CennikId = ie.viewCennikJoined.Where(m => m.GazetaId == wydanie.GazetaId &&
                                                                m.RodzajOkresuDzien == 1 &&
                                                                m.RodzajOkresuMiesiac == 0 &&
                                                                m.ProduktRodzajPojedyncze == true)
                                                        .Select(m => m.CennikId)
                                                        .FirstOrDefault();
            }
            kwm.wydanieModel = wydanie;

            return View("Wykup",kwm);
        }

        private int getBiezaceWydanie()
        {
            int wydanie_nr = 0;
            using (Entities ie = new Entities())
            {
                List<NoweWydaniaByType> gazety = ie.getNoweWydaniaByType(1).OrderByDescending(d => d.GazetaId).ToList();
                wydanie_nr = gazety.First().WydanieId;
            }

            return wydanie_nr;
        }
    }
}
