﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ISSG4.Models;

namespace ISSG3.Controllers.Backend
{
    [Authorize(Roles = "Administratorzy,Kolportaż")]
    public class ZamowienieLogController : Controller
    {
        private Entities db = new Entities();

        //
        // GET: /ZamowienieLog/

        public ViewResult Index(int start = 0, int itemsPerPage = 20, string orderBy = "ZamowienieLogData", bool desc = true)
        {
            ObjectContext objectContext = ((IObjectContextAdapter)db).ObjectContext;
            ViewBag.Count = objectContext.CreateObjectSet<ZamowienieLog>("ZamowienieLog").Count();
            ViewBag.Start = start;
            ViewBag.ItemsPerPage = itemsPerPage;
            ViewBag.OrderBy = orderBy;
            ViewBag.Desc = desc;

            return View();
        }

        //
        // GET: /ZamowienieLog/GridData/?start=0&itemsPerPage=20&orderBy=ZamowienieLogId&desc=true

        public ActionResult GridData(int start = 0, int itemsPerPage = 20, string orderBy = "ZamowienieLogData", bool desc = true)
        {
            Response.AppendHeader("X-Total-Row-Count", db.ZamowienieLog.Count().ToString());
            ObjectContext objectContext = ((IObjectContextAdapter)db).ObjectContext;
            ObjectQuery<ZamowienieLog> zamowienielog = objectContext.CreateObjectSet<ZamowienieLog>("ZamowienieLog")
                .Include("i3_Zamowienie")
                .Include("Users");
            zamowienielog = zamowienielog.OrderBy("it." + orderBy + (desc ? " desc" : ""));

            return PartialView(zamowienielog.Skip(start).Take(itemsPerPage));
        }

        //
        // GET: /Default5/RowData/5

        public ActionResult RowData(int id)
        {
            ZamowienieLog zamowienielog = db.ZamowienieLog.Single(z => z.ZamowienieLogId == id);
            return PartialView("GridData", new ZamowienieLog[] { zamowienielog });
        }

        //
        // GET: /ZamowienieLog/Create

        //public ActionResult Create()
        //{
        //    ViewBag.ZamowienieId = new SelectList(db.Zamowienie, "ZamowienieId", "ZamowienieId");
        //    ViewBag.issg_user_id = new SelectList(db.Users, "ispo_user_id", "UserName");
        //    return PartialView("Edit");
        //}

        ////
        //// POST: /ZamowienieLog/Create

        //[HttpPost]
        //public ActionResult Create(ZamowienieLog zamowienielog)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.ZamowienieLog.AddObject(zamowienielog);
        //        db.SaveChanges();
        //        return PartialView("GridData", new ZamowienieLog[] { zamowienielog });
        //    }

        //    ViewBag.ZamowienieId = new SelectList(db.Zamowienie, "ZamowienieId", "ZamowienieId", zamowienielog.ZamowienieId);
        //    ViewBag.issg_user_id = new SelectList(db.Users, "ispo_user_id", "UserName", zamowienielog.issg_user_id);
        //    return PartialView("Edit", zamowienielog);
        //}

        ////
        //// GET: /ZamowienieLog/Edit/5

        //public ActionResult Edit(int id)
        //{
        //    ZamowienieLog zamowienielog = db.ZamowienieLog.Single(z => z.ZamowienieLogId == id);
        //    ViewBag.ZamowienieId = new SelectList(db.Zamowienie, "ZamowienieId", "ZamowienieId", zamowienielog.ZamowienieId);
        //    ViewBag.issg_user_id = new SelectList(db.Users, "ispo_user_id", "UserName", zamowienielog.issg_user_id);
        //    return PartialView(zamowienielog);
        //}

        ////
        //// POST: /ZamowienieLog/Edit/5

        //[HttpPost]
        //public ActionResult Edit(ZamowienieLog zamowienielog)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.ZamowienieLog.Attach(zamowienielog);
        //        db.ObjectStateManager.ChangeObjectState(zamowienielog, EntityState.Modified);
        //        db.SaveChanges();
        //        return PartialView("GridData", new ZamowienieLog[] { zamowienielog });
        //    }

        //    ViewBag.ZamowienieId = new SelectList(db.Zamowienie, "ZamowienieId", "ZamowienieId", zamowienielog.ZamowienieId);
        //    ViewBag.issg_user_id = new SelectList(db.Users, "ispo_user_id", "UserName", zamowienielog.issg_user_id);
        //    return PartialView(zamowienielog);
        //}

        ////
        //// POST: /ZamowienieLog/Delete/5

        //[HttpPost]
        //public void Delete(int id)
        //{
        //    ZamowienieLog zamowienielog = db.ZamowienieLog.Single(z => z.ZamowienieLogId == id);
        //    db.ZamowienieLog.DeleteObject(zamowienielog);
        //    db.SaveChanges();
        //}

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}
