﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ISSG4.Models;
using System.Web.Script.Serialization;
using Microsoft.VisualBasic.FileIO;
using System.Transactions;
using System.Globalization;

namespace ISSG3.Controllers.Backend
{
    [Authorize(Roles = "Administratorzy,Kolportaż")]
    public class ImportKodController : Controller
    {
        private Entities db = new Entities();

        public ViewResult Index(int start = 0, int itemsPerPage = 20, string orderBy = "ImportCzas", bool desc = true)
        {
            ViewBag.Count = db.ImportKodPlik.Count();
            ViewBag.Start = start;
            ViewBag.ItemsPerPage = itemsPerPage;
            ViewBag.OrderBy = orderBy;
            ViewBag.Desc = desc;

            string path_import = Utils.getOpcjeValue("ImportKodPath");

            DirectoryInfo dirInfo = new DirectoryInfo(path_import);
            if (!dirInfo.Exists) dirInfo.Create();
            FileInfo[] dirList = dirInfo.GetFiles();

            string dir_txt = "Folder importu: "+ path_import+": ";
            string files_txt = string.Empty;

            if (dirList.GetLength(0) == 0)
                files_txt = "<span>Brak plików do importu.</span>";
            else
            {
                files_txt = "Pliki: <span style='color:blue'>";
                foreach (FileInfo fil in dirList)
                {
                    files_txt += fil.Name+" ";
                }
                files_txt += "</span>";
            }

            ViewBag.FolderInfo = dir_txt + files_txt;


            return View();
        }

        public ActionResult GridData(DateTime? dateFrom, DateTime? dateTo, int start = 0, int itemsPerPage = 20, string orderBy = "ImportCzas", bool desc = true, string plik="")
        {

            Response.AppendHeader("X-Total-Row-Count", db.ImportKodPlik.Count().ToString());
            ObjectContext objectContext = ((IObjectContextAdapter)db).ObjectContext;

            ObjectQuery<ImportKodPlik> pliki = objectContext.CreateObjectSet<ImportKodPlik>("ImportKodPlik");

            if (dateFrom.HasValue) 
                pliki = pliki.Where("it.ImportCzas >= @dataFrom", new ObjectParameter("dataFrom", dateFrom.Value));

            if (dateTo.HasValue)
                pliki = pliki.Where("it.ImportCzas <= @dataTo", new ObjectParameter("dataTo", dateTo.Value.AddDays(1)));

            if (!string.IsNullOrEmpty(plik)) 
                pliki = pliki.Where("it.PlikNazwa like '%" + plik + "%'");

            pliki = pliki.OrderBy("it." + orderBy + (desc ? " desc" : ""));

            return PartialView(pliki.Skip(start).Take(itemsPerPage));
        }

        public ActionResult Edit(string id)
        {
            ImportKodPlik ImportKodPlik = db.ImportKodPlik.Single(p => p.ImportKodPlikId == id);
            return PartialView(ImportKodPlik);
        }


        public ActionResult Import()
        {

            Utils.ReadFile();
            return RedirectToAction("Index");
        }

        public ViewResult IndexKod(int start = 0, int itemsPerPage = 20, string orderBy = "ImportKodId", bool desc = false , string id ="")
        {

            ObjectContext objectContext = ((IObjectContextAdapter)db).ObjectContext;
            ObjectQuery<ImportKod> kody = objectContext.CreateObjectSet<ImportKod>("ImportKod");

            if (!string.IsNullOrEmpty(id))
                kody = kody.Where("it.ImportKodPlikId == '"+id+"'");

            ViewBag.Count = kody.Count();
            ViewBag.Start = start;
            ViewBag.ItemsPerPage = itemsPerPage;
            ViewBag.OrderBy = orderBy;
            ViewBag.Desc = desc;
            ViewBag.id = id;

            return View();
        }


        public ActionResult GridDataKod(DateTime? dateFrom, DateTime? dateTo,int start = 0, 
            int itemsPerPage = 20, string orderBy = "ImportKodId", bool desc = false, string id = "", string kodkod = ""   )
        {

            ObjectContext objectContext = ((IObjectContextAdapter)db).ObjectContext;
            ObjectQuery<ImportKod> kody = objectContext.CreateObjectSet<ImportKod>("ImportKod");

            if (!string.IsNullOrEmpty(id))
                    kody=kody.Where("it.ImportKodPlikId=='" + id + "'");

              if (dateFrom.HasValue)
                kody = kody.Where("it.Wydanie >= @dataFrom", new ObjectParameter("dataFrom", dateFrom.Value));

            if (dateTo.HasValue)
                kody = kody.Where("it.Wydanie <= @dataTo", new ObjectParameter("dataTo", dateTo.Value.AddDays(1)));

            if (!string.IsNullOrEmpty(kodkod))
                kody = kody.Where("it.Kod=='" + kodkod + "'");

            kody = kody.OrderBy("it." + orderBy + (desc ? " desc" : ""));

            Response.AppendHeader("X-Total-Row-Count", kody.Count().ToString());
            return PartialView(kody.Skip(start).Take(itemsPerPage));
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

    }
}
