﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ISSG4.Models;

namespace ISSG3.Controllers.Backend
{
    [Authorize(Roles = "Administratorzy,Kolportaż")]
    public class KlientZdarzeniaController : Controller
    {
        private Entities db = new Entities();

        //
        // GET: /Klient/

        public ViewResult Index(int start = 0, int itemsPerPage = 20, string orderBy = "ueh_id", bool desc = true, int userid = 0)
        {
            ObjectContext objectContext = ((IObjectContextAdapter)db).ObjectContext;
            ViewBag.Count = objectContext.CreateObjectSet<UzytkownikZdarzeniaHist>("UzytkownikZdarzeniaHist");

            ViewBag.Start = start;
            ViewBag.ItemsPerPage = itemsPerPage;
            ViewBag.OrderBy = orderBy;
            ViewBag.Desc = desc;

            if (userid > 0)
                ViewBag.ZdarzeniaUserName = db.Users.Where(u => u.ispo_user_id == userid).First().UserName;

            return View();
        }

        //
        // GET: /Klient/GridData/?start=0&itemsPerPage=20&orderBy=ispo_user_id&desc=true

        public ActionResult GridData(int start = 0, int itemsPerPage = 20, string orderBy = "ueh_id", bool desc = true, int userid=0)
        {
            ObjectContext objectContext = ((IObjectContextAdapter)db).ObjectContext;

            ObjectQuery<UzytkownikZdarzeniaHist> userevents = objectContext.CreateObjectSet<UzytkownikZdarzeniaHist>("UzytkownikZdarzeniaHist");

            if (userid > 0)
                userevents = userevents.Where("it.ispo_user_id =" + userid.ToString() );

            userevents = userevents.OrderBy("it." + orderBy + (desc ? " desc" : ""));

            Response.AppendHeader("X-Total-Row-Count", userevents.Count().ToString());

            return PartialView(userevents.Skip(start).Take(itemsPerPage));
        }

        //
        // GET: /Default5/RowData/5

        public ActionResult RowData(int id)
        {
            UzytkownikZdarzeniaHist userevents = db.UzytkownikZdarzeniaHist.Single(u => u.ueh_id == id);
            return PartialView("GridData", new UzytkownikZdarzeniaHist[] { userevents });
        }
        
        //
        // GET: /Klient/Create

        //public ActionResult Create()
        //{
        //    return PartialView("Edit");
        //}

        //
        // POST: /Klient/Create

        //[HttpPost]
        //public ActionResult Create(Users users)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Users.AddObject(users);
        //        db.SaveChanges();
        //        return PartialView("GridData", new Users[] { users });
        //    }

        //    return PartialView("Edit", users);
        //}

        //
        // GET: /Klient/Edit/5

        //public ActionResult Edit(int id)
        //{
        //    Users users = db.Users.Single(u => u.ispo_user_id == id);
        //    return PartialView(users);
        //}

        //
        // POST: /Klient/Edit/5

        //[HttpPost]
        //public ActionResult Edit(Users users)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Users.Attach(users);
        //        db.ObjectStateManager.ChangeObjectState(users, EntityState.Modified);
        //        db.SaveChanges();
        //        return PartialView("GridData", new Users[] { users });
        //    }

        //    return PartialView(users);
        //}

        //
        // POST: /Klient/Delete/5

        //[HttpPost]
        //public void Delete(int id)
        //{
        //    Users users = db.Users.Single(u => u.ispo_user_id == id);
        //    db.Users.DeleteObject(users);
        //    db.SaveChanges();
        //}

        public ActionResult Szczegoly(string uname)
        {
            var userevent = db.UzytkownikZdarzeniaHist.Where(m => m.UserName == uname).Single();

            return PartialView(userevent);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}
