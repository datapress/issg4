﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ISSG4.Models;

namespace ISSG3.Controllers.Backend
{
	[Authorize(Roles = "Administratorzy")]
    public class WydawcaController : Controller
    {
        private Entities db = new Entities();

        //
        // GET: /Wydawca/

        public ViewResult Index(int start = 0, int itemsPerPage = 20, string orderBy = "WydawcaId", bool desc = false)
        {
            ObjectContext objectContext = ((IObjectContextAdapter)db).ObjectContext;
            ViewBag.Count = objectContext.CreateObjectSet<Wydawca>("Wydawca").Count();
            ViewBag.Start = start;
            ViewBag.ItemsPerPage = itemsPerPage;
            ViewBag.OrderBy = orderBy;
            ViewBag.Desc = desc;

            return View();
        }

        //
        // GET: /Wydawca/GridData/?start=0&itemsPerPage=20&orderBy=WydawcaId&desc=true

        public ActionResult GridData(int start = 0, int itemsPerPage = 20, string orderBy = "WydawcaId", bool desc = false)
        {
            Response.AppendHeader("X-Total-Row-Count", db.Wydawca.Count().ToString());
            ObjectContext objectContext = ((IObjectContextAdapter)db).ObjectContext;
            ObjectQuery<Wydawca> wydawca = objectContext.CreateObjectSet<Wydawca>("Wydawca");
            wydawca = wydawca.OrderBy("it." + orderBy + (desc ? " desc" : ""));

            return PartialView(wydawca.Skip(start).Take(itemsPerPage));
        }

        //
        // GET: /Default5/RowData/5

        public ActionResult RowData(int id)
        {
            Wydawca wydawca = db.Wydawca.Single(w => w.WydawcaId == id);
            return PartialView("GridData", new Wydawca[] { wydawca });
        }

        //
        // GET: /Wydawca/Create

        public ActionResult Create()
        {
            return PartialView("Edit");
        }

        //
        // POST: /Wydawca/Create

        [HttpPost]
        public ActionResult Create(Wydawca wydawca)
        {
            if (ModelState.IsValid)
            {
                db.Wydawca.Add(wydawca);
                db.SaveChanges();
                return PartialView("GridData", new Wydawca[] { wydawca });
            }

            return PartialView("Edit", wydawca);
        }

        //
        // GET: /Wydawca/Edit/5

        public ActionResult Edit(int id)
        {
            Wydawca wydawca = db.Wydawca.Single(w => w.WydawcaId == id);
            return PartialView(wydawca);
        }

        //
        // POST: /Wydawca/Edit/5

        [HttpPost]
        public ActionResult Edit(Wydawca wydawca)
        {
            if (ModelState.IsValid)
            {
                db.Wydawca.Attach(wydawca);
                ObjectContext objectContext = ((IObjectContextAdapter)db).ObjectContext;
                objectContext.ObjectStateManager.ChangeObjectState(wydawca, EntityState.Modified);
                db.SaveChanges();
                return PartialView("GridData", new Wydawca[] { wydawca });
            }

            return PartialView(wydawca);
        }

        //
        // POST: /Wydawca/Delete/5

        [HttpPost]
        public void Delete(int id)
        {
            Wydawca wydawca = db.Wydawca.Single(w => w.WydawcaId == id);
            db.Wydawca.Remove(wydawca);
            db.SaveChanges();
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}
