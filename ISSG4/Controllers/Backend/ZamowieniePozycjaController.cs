﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ISSG4.Models;

namespace ISSG3.Controllers.Backend
{
    [Authorize(Roles = "Administratorzy,Kolportaż")]
    public class ZamowieniePozycjaController : Controller
    {
        private Entities db = new Entities();

        //
        // GET: /ZamowieniePozycja/

        public ActionResult Index(int start = 0, int itemsPerPage = 20, string orderBy = "ZamowieniePozycjaId", 
            bool desc = false, int parentId = 0)
        {
            if (parentId == 0) return Content("No id");

            ViewBag.Count = db.ZamowieniePozycja.Where(m => m.ZamowienieId == parentId).Count();
            ViewBag.Start = start;
            ViewBag.ItemsPerPage = itemsPerPage;
            ViewBag.OrderBy = orderBy;
            ViewBag.Desc = desc;

            return PartialView(db.Zamowienie.Where(m => m.ZamowienieId == parentId).Single());
        }

        //
        // GET: /ZamowieniePozycja/GridData/?start=0&itemsPerPage=20&orderBy=ZamowieniePozycjaId&desc=true

        public ActionResult GridData(int start = 0, int itemsPerPage = 20, string orderBy = "ZamowieniePozycjaId", bool desc = false, int parentId = 0)
        {
            ObjectContext objectContext = ((IObjectContextAdapter)db).ObjectContext;

            ObjectQuery<ZamowieniePozycja> zamowieniepozycja =
                 objectContext.CreateObjectSet<ZamowieniePozycja>("ZamowieniePozycja")
                .Include("i3_Cennik")
                .Include("i3_Cennik.i3_Produkt")
                .Include("i3_Cennik.i3_Gazeta")
                .Include("i3_Dodatek")
                .Where("it.ZamowienieId = " + parentId.ToString());

            zamowieniepozycja = zamowieniepozycja.OrderBy("it." + orderBy + (desc ? " desc" : ""));

            Response.AppendHeader("X-Total-Row-Count", zamowieniepozycja.Count().ToString());

            return PartialView(zamowieniepozycja.Skip(start).Take(itemsPerPage));
        }

        //
        // GET: /Default5/RowData/5

        public ActionResult RowData(int id)
        {
            ZamowieniePozycja zamowieniepozycja = db.ZamowieniePozycja.Single(z => z.ZamowieniePozycjaId == id);
            return PartialView("GridData", new ZamowieniePozycja[] { zamowieniepozycja });
        }

        //
        // GET: /ZamowieniePozycja/Create

        public ActionResult Create(int parentId)
        {
            ViewBag.ZamowienieId = parentId;
            ViewBag.CennikId = new SelectList(db.Cennik.Where(m => m.i3_Produkt.i3_ProduktRodzaj.ProduktRodzajReklamacja == true), "CennikId", "i3_Gazeta.GazetaNazwa");
            return PartialView("Edit");
        }

        //
        // POST: /ZamowieniePozycja/Create

        [HttpPost]
        public ActionResult Create(ZamowieniePozycja zamowieniepozycja, int? DodatekId)
        {
            zamowieniepozycja.ZamowieniePozycjaBrutto = 0.00m;
            zamowieniepozycja.ZamowieniePozycjaNetto = 0.00m;
            zamowieniepozycja.ZamowieniePozycjaVat = 0.00m;
            zamowieniepozycja.VatId = "0%";

            if (zamowieniepozycja.ZamowieniePozycjaDo < zamowieniepozycja.ZamowieniePozycjaOd)
                ModelState.AddModelError("ZamowieniePozycjaDo", "Data 'do' nie moze być wczesniej niż data 'od'.");

            if (ModelState.IsValid)
            {
                db.ZamowieniePozycja.Add(zamowieniepozycja);
                db.SaveChanges();
                if (DodatekId.HasValue)
                {
                    Dodatek dodatek = new Dodatek() { DodatekId = DodatekId.Value };
                    db.Dodatek.Attach(dodatek);
                    zamowieniepozycja.i3_Dodatek.Add(dodatek);
                }
                db.SaveChanges();

                return PartialView("GridData", new ZamowieniePozycja[] { zamowieniepozycja });
            }

            ViewBag.ZamowienieId = zamowieniepozycja.ZamowienieId;
            ViewBag.CennikId = new SelectList(db.Cennik.Where(m => m.i3_Produkt.i3_ProduktRodzaj.ProduktRodzajReklamacja == true), "CennikId", "i3_Gazeta.GazetaNazwa");
            return PartialView("Edit", zamowieniepozycja);
        }

        //
        // GET: /ZamowieniePozycja/Edit/5

        //public ActionResult Edit(int id)
        //{
        //    ZamowieniePozycja zamowieniepozycja = db.ZamowieniePozycja.Single(z => z.ZamowieniePozycjaId == id);
        //    ViewBag.ZamowienieId = new SelectList(db.Zamowienie, "ZamowienieId", "ZamowienieId", zamowieniepozycja.ZamowienieId);
        //    ViewBag.CennikId = new SelectList(db.Cennik, "CennikId", "VatId", zamowieniepozycja.CennikId);
        //    return PartialView(zamowieniepozycja);
        //}

        ////
        //// POST: /ZamowieniePozycja/Edit/5

        //[HttpPost]
        //public ActionResult Edit(ZamowieniePozycja zamowieniepozycja)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.ZamowieniePozycja.Attach(zamowieniepozycja);
        //        db.ObjectStateManager.ChangeObjectState(zamowieniepozycja, EntityState.Modified);
        //        db.SaveChanges();
        //        return PartialView("GridData", new ZamowieniePozycja[] { zamowieniepozycja });
        //    }

        //    ViewBag.ZamowienieId = new SelectList(db.Zamowienie, "ZamowienieId", "ZamowienieId", zamowieniepozycja.ZamowienieId);
        //    ViewBag.CennikId = new SelectList(db.Cennik, "CennikId", "VatId", zamowieniepozycja.CennikId);
        //    return PartialView(zamowieniepozycja);
        //}

        //
        // POST: /ZamowieniePozycja/Delete/5

        [HttpPost]
        public void Delete(int id)
        {
            ZamowieniePozycja zamowieniepozycja = db.ZamowieniePozycja.Single(z => z.ZamowieniePozycjaId == id);
            db.ZamowieniePozycja.Remove(zamowieniepozycja);
            db.SaveChanges();
        }

        [HttpPost]
        public ActionResult GetDodatek(int id)
        {
            var dodatki = db.Dodatek.Where(m => m.i3_Gazeta.Any(n => n.i3_Cennik.Any(o => o.CennikId == id)) && m.DodatekAktywny == true).Select(m => new { DodatekId = m.DodatekId, DodatekNazwa = m.DodatekNazwa }).ToList();
            dodatki.Insert(0, new { DodatekId = 0, DodatekNazwa = " " });
            return Json(dodatki);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}
