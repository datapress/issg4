﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ISSG4.Models;
using System.Transactions;

namespace ISSG3.Controllers.Backend
{
    [Authorize(Roles = "Administratorzy,Kolportaż")]
    public class PobranieDarmoweController : Controller
    {
        private Entities db = new Entities();

        //
        // GET: /PobranieDarmowe/

        public ViewResult Index(int start = 0, int itemsPerPage = 20, string orderBy = "PobranieDarmoweId", bool desc = true)
        {
            ObjectContext objectContext = ((IObjectContextAdapter)db).ObjectContext;

            ViewBag.Count = objectContext.CreateObjectSet<PobranieDarmowe>("PobranieDarmowe").Count();
            ViewBag.Start = start;
            ViewBag.ItemsPerPage = itemsPerPage;
            ViewBag.OrderBy = orderBy;
            ViewBag.Desc = desc;

            return View();
        }

        //
        // GET: /PobranieDarmowe/GridData/?start=0&itemsPerPage=20&orderBy=v&desc=true

        public ActionResult GridData(DateTime? dateFrom, DateTime? dateTo, int start = 0, int itemsPerPage = 20, string orderBy = "PobranieDarmoweId", bool desc = true)
        {
            ObjectContext objectContext = ((IObjectContextAdapter)db).ObjectContext;



            ObjectQuery<PobranieDarmowe> pobraniedarmowe = objectContext
                .CreateObjectSet<PobranieDarmowe>("PobranieDarmowe")  
                .Include("i3_Wydanie");

            if (dateFrom.HasValue)
                pobraniedarmowe = pobraniedarmowe.Where("it.PobranieDarmoweCzas >= @dataFrom", new ObjectParameter("dataFrom", dateFrom.Value));

            if (dateTo.HasValue)
                pobraniedarmowe = pobraniedarmowe.Where("it.PobranieDarmoweCzas <= @dataTo", new ObjectParameter("dataTo", dateTo.Value.AddDays(1)));

            pobraniedarmowe = pobraniedarmowe.OrderBy("it." + orderBy + (desc ? " desc" : ""));

            Response.AppendHeader("X-Total-Row-Count", pobraniedarmowe.Count().ToString());

            return PartialView(pobraniedarmowe.Skip(start).Take(itemsPerPage));
        }

        //
        // GET: /Default5/RowData/5

        public ActionResult RowData(int pobranieId)
        {
            PobranieDarmowe pobraniedarmowe = db.PobranieDarmowe.Include("i3_Wydanie").Include("i3_Gazeta").Single(k => k.PobranieDarmoweId == pobranieId);
            return PartialView("GridData", new PobranieDarmowe[] { pobraniedarmowe });
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}
