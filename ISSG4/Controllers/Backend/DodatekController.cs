﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ISSG4.Models;

namespace ISSG3.Controllers.Backend
{
	[Authorize(Roles = "Administratorzy")]
    public class DodatekController : Controller
    {
        private Entities db = new Entities();

        //
        // GET: /Dodatek/

        public ViewResult Index(int start = 0, int itemsPerPage = 20, string orderBy = "DodatekId", bool desc = false)
        {
            ViewBag.Count = db.Dodatek.Count();
            ViewBag.Start = start;
            ViewBag.ItemsPerPage = itemsPerPage;
            ViewBag.OrderBy = orderBy;
            ViewBag.Desc = desc;

            return View();
        }

        //
        // GET: /Dodatek/GridData/?start=0&itemsPerPage=20&orderBy=DodatekId&desc=true

        public ActionResult GridData(int start = 0, int itemsPerPage = 20, string orderBy = "DodatekId", bool desc = false)
        {
            Response.AppendHeader("X-Total-Row-Count", db.Dodatek.Count().ToString());
            ObjectContext objectContext = ((IObjectContextAdapter)db).ObjectContext;

            ObjectQuery<Dodatek> dodatek = objectContext.CreateObjectSet<Dodatek>("Dodatek");
            dodatek = dodatek.OrderBy("it." + orderBy + (desc ? " desc" : ""));

            return PartialView(dodatek.Skip(start).Take(itemsPerPage));
        }

        //
        // GET: /Default5/RowData/5

        public ActionResult RowData(int id)
        {
            Dodatek dodatek = db.Dodatek.Single(d => d.DodatekId == id);
            return PartialView("GridData", new Dodatek[] { dodatek });
        }

        //
        // GET: /Dodatek/Create

        public ActionResult Create()
        {
            return PartialView("Edit");
        }

        //
        // POST: /Dodatek/Create

        [HttpPost]
        public ActionResult Create(Dodatek dodatek)
        {
            if (ModelState.IsValid)
            {
                db.Dodatek.Add(dodatek);
                db.SaveChanges();
                return PartialView("GridData", new Dodatek[] { dodatek });
            }

            return PartialView("Edit", dodatek);
        }

        //
        // GET: /Dodatek/Edit/5

        public ActionResult Edit(int id)
        {
            Dodatek dodatek = db.Dodatek.Single(d => d.DodatekId == id);
            return PartialView(dodatek);
        }

        //
        // POST: /Dodatek/Edit/5

        [HttpPost]
        public ActionResult Edit(Dodatek dodatek)
        {
            if (ModelState.IsValid)
            {
                db.Dodatek.Attach(dodatek);
                ObjectContext objectContext = ((IObjectContextAdapter)db).ObjectContext;

                objectContext.ObjectStateManager.ChangeObjectState(dodatek, EntityState.Modified);
                db.SaveChanges();
                return PartialView("GridData", new Dodatek[] { dodatek });
            }

            return PartialView(dodatek);
        }

        //
        // POST: /Dodatek/Delete/5

        [HttpPost]
        public void Delete(int id)
        {
            Dodatek dodatek = db.Dodatek.Single(d => d.DodatekId == id);
            db.Dodatek.Remove(dodatek);
            db.SaveChanges();
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}
