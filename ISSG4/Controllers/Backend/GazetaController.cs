﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ISSG4.Models;

namespace ISSG3.Controllers.Backend
{
	[Authorize(Roles = "Administratorzy")]
    public class GazetaController : Controller
    {
        private Entities db = new Entities();

        //
        // GET: /Gazeta/

        public ViewResult Index(int start = 0, int itemsPerPage = 20, string orderBy = "GazetaId", bool desc = false)
        {
            ViewBag.Count = db.Gazeta.Count();
            ViewBag.Start = start;
            ViewBag.ItemsPerPage = itemsPerPage;
            ViewBag.OrderBy = orderBy;
            ViewBag.Desc = desc;

            return View();
        }

        //
        // GET: /Gazeta/GridData/?start=0&itemsPerPage=20&orderBy=GazetaId&desc=true

        public ActionResult GridData(int start = 0, int itemsPerPage = 20, string orderBy = "GazetaId", bool desc = false)
        {
            Response.AppendHeader("X-Total-Row-Count", db.Gazeta.Count().ToString());
            ObjectContext objectContext = ((IObjectContextAdapter)db).ObjectContext;

            ObjectQuery<Dodatek> dodatek = objectContext.CreateObjectSet<Dodatek>("Dodatek");

            ObjectQuery<Gazeta> gazeta = objectContext
                .CreateObjectSet<Gazeta>("Gazeta")
                .Include("i3_GazetaKod")
                .Include("i3_GazetaTyp")
                .Include("i3_Wydawca");
            gazeta = gazeta.OrderBy("it." + orderBy + (desc ? " desc" : ""));

            return PartialView(gazeta.Skip(start).Take(itemsPerPage));
        }

        //
        // GET: /Default5/RowData/5

        public ActionResult RowData(int id)
        {
            Gazeta gazeta = db.Gazeta.Single(g => g.GazetaId == id);
            return PartialView("GridData", new Gazeta[] { gazeta });
        }

        //
        // GET: /Gazeta/Create

        public ActionResult Create()
        {
            ViewBag.GazetaKodId = new SelectList(db.GazetaKod, "GazetaKodId", "GazetaKodNumer");
            ViewBag.GazetaTypId = new SelectList(db.GazetaTyp, "GazetaTypId", "GazetaTypNazwa");
            ViewBag.WydawcaId = new SelectList(db.Wydawca, "WydawcaId", "WydawcaNazwa");
            return PartialView("Edit");
        }

        //
        // POST: /Gazeta/Create

        [HttpPost]
        public ActionResult Create(Gazeta gazeta)
        {
            if (ModelState.IsValid)
            {
                db.Gazeta.Add(gazeta);
                db.SaveChanges();
                return PartialView("GridData", new Gazeta[] { gazeta });
            }

            ViewBag.GazetaKodId = new SelectList(db.GazetaKod, "GazetaKodId", "GazetaKodNumer", gazeta.GazetaKodId);
            ViewBag.GazetaTypId = new SelectList(db.GazetaTyp, "GazetaTypId", "GazetaTypNazwa", gazeta.GazetaTypId);
            ViewBag.WydawcaId = new SelectList(db.Wydawca, "WydawcaId", "WydawcaNazwa", gazeta.WydawcaId);
            return PartialView("Edit", gazeta);
        }

        //
        // GET: /Gazeta/Edit/5

        public ActionResult Edit(int id)
        {
            Gazeta gazeta = db.Gazeta.Single(g => g.GazetaId == id);
            ViewBag.GazetaKodId = new SelectList(db.GazetaKod, "GazetaKodId", "GazetaKodNumer", gazeta.GazetaKodId);
            ViewBag.GazetaTypId = new SelectList(db.GazetaTyp, "GazetaTypId", "GazetaTypNazwa", gazeta.GazetaTypId);
            ViewBag.WydawcaId = new SelectList(db.Wydawca, "WydawcaId", "WydawcaNazwa", gazeta.WydawcaId);
            return PartialView(gazeta);
        }

        //
        // POST: /Gazeta/Edit/5

        [HttpPost]
        public ActionResult Edit(Gazeta gazeta)
        {
            if (ModelState.IsValid)
            {
                db.Gazeta.Attach(gazeta);
                ObjectContext objectContext = ((IObjectContextAdapter)db).ObjectContext;

                objectContext.ObjectStateManager.ChangeObjectState(gazeta, EntityState.Modified);

                db.SaveChanges();
                return PartialView("GridData", new Gazeta[] { gazeta });
            }

            ViewBag.GazetaKodId = new SelectList(db.GazetaKod, "GazetaKodId", "GazetaKodNumer", gazeta.GazetaKodId);
            ViewBag.GazetaTypId = new SelectList(db.GazetaTyp, "GazetaTypId", "GazetaTypNazwa", gazeta.GazetaTypId);
            ViewBag.WydawcaId = new SelectList(db.Wydawca, "WydawcaId", "WydawcaNazwa", gazeta.WydawcaId);
            return PartialView(gazeta);
        }

        //
        // POST: /Gazeta/Delete/5

        [HttpPost]
        public void Delete(int id)
        {
            Gazeta gazeta = db.Gazeta.Single(g => g.GazetaId == id);
            db.Gazeta.Remove(gazeta);
            db.SaveChanges();
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}
