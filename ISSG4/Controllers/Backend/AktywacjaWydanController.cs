﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ISSG3.Models;
using ISSG4.Models;

namespace ISSG3.Controllers.Backend
{
    [Authorize(Roles = "Administratorzy,Kolportaż")]
    public class AktywacjaWydanController : Controller
    {
        protected Entities db = new Entities();
        //protected ISSG3.Models.IssgEntities cc = new ISSG3.Models.IssgEntities();

       // protected ISSG3Entities db2 = new ISSG3Entities();

        // GET: /AktywacjaWydan/

        public ViewResult Index(int start = 0, int itemsPerPage = 20, string orderBy = "WydanieId", bool desc = true)
        {
            ViewBag.Count = db.Wydanie.Count();
            ViewBag.Start = start;
            ViewBag.ItemsPerPage = itemsPerPage;
            ViewBag.OrderBy = orderBy;
            ViewBag.Desc = desc;

            return View();
        }

        //
        // GET: /AktywacjaWydan/GridData/?start=0&itemsPerPage=20&orderBy=WydanieId&desc=true

        public ActionResult GridData(int start = 0, int itemsPerPage = 20, string orderBy = "WydanieId", bool desc = true)
        {

            ObjectContext objectContext = ((IObjectContextAdapter)db).ObjectContext;

            Response.AppendHeader("X-Total-Row-Count", db.Wydanie.Count().ToString());

            ObjectQuery<Wydanie> wydanie = objectContext
                .CreateObjectSet<Wydanie>("Wydanie")
                .Include("i3_Dodatek")
                .Include("i3_Gazeta");

            wydanie = wydanie.OrderBy("it." + orderBy + (desc ? " desc" : ""));

            return PartialView(wydanie.Skip(start).Take(itemsPerPage));
        }

        //
        // GET: /Default5/RowData/5

        public ActionResult RowData(int id)
        {
            Wydanie wydanie = db.Wydanie.Single(w => w.WydanieId == id);
            return PartialView("GridData", new Wydanie[] { wydanie });
        }

        //
        // GET: /AktywacjaWydan/Create

        public ActionResult Create()
        {
            ViewBag.DodatekId = new SelectList(db.Dodatek, "DodatekId", "DodatekNazwa");
            ViewBag.GazetaId = new SelectList(db.Gazeta, "GazetaId", "GazetaNazwa");
            return PartialView("Edit");
        }

        //
        // POST: /AktywacjaWydan/Create

        [HttpPost]
        public ActionResult Create(Wydanie wydanie)
        {
            if (ModelState.IsValid)
            {
                ObjectContext objectContext = ((IObjectContextAdapter)db).ObjectContext;
                db.Wydanie.Add(wydanie);
                db.SaveChanges();
                return PartialView("GridData", new Wydanie[] { wydanie });
            }

            ViewBag.DodatekId = new SelectList(db.Dodatek, "DodatekId", "DodatekNazwa", wydanie.DodatekId);
            ViewBag.GazetaId = new SelectList(db.Gazeta, "GazetaId", "GazetaNazwa", wydanie.GazetaId);
            return PartialView("Edit", wydanie);
        }

        //
        // GET: /AktywacjaWydan/Edit/5

        public ActionResult Edit(int id)
        {
            Wydanie wydanie = db.Wydanie.Single(w => w.WydanieId == id);
            ViewBag.DodatekId = new SelectList(db.Dodatek, "DodatekId", "DodatekNazwa", wydanie.DodatekId);
            ViewBag.GazetaId = new SelectList(db.Gazeta, "GazetaId", "GazetaNazwa", wydanie.GazetaId);
            return PartialView(wydanie);
        }

        //
        // POST: /AktywacjaWydan/Edit/5

        [HttpPost]
        public ActionResult Edit(Wydanie wydanie)
        {
            if (ModelState.IsValid)
            {
                db.Wydanie.Attach(wydanie);
                ObjectContext objectContext = ((IObjectContextAdapter)db).ObjectContext;
                objectContext.ObjectStateManager.ChangeObjectState(wydanie, EntityState.Modified);
                db.SaveChanges();
                return PartialView("GridData", new Wydanie[] { wydanie });
            }

            ViewBag.DodatekId = new SelectList(db.Dodatek, "DodatekId", "DodatekNazwa", wydanie.DodatekId);
            ViewBag.GazetaId = new SelectList(db.Gazeta, "GazetaId", "GazetaNazwa", wydanie.GazetaId);
            return PartialView(wydanie);
        }

        //
        // POST: /AktywacjaWydan/Delete/5

        [HttpPost]
        public void Delete(int id)
        {
            Wydanie wydanie = db.Wydanie.Single(w => w.WydanieId == id);
            db.Wydanie.Remove(wydanie);
            db.SaveChanges();
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}
