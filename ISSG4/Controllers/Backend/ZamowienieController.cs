﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ISSG4.Models;
using System.Transactions;

namespace ISSG4.Controllers.Backend
{
    [Authorize(Roles = "Administratorzy,Kolportaż")]
    public class ZamowienieController : Controller
    {
        private Entities db = new Entities();

        //
        // GET: /Zamowienie/

        public ViewResult Index(int start = 0, int itemsPerPage = 20, string orderBy = "ZamowienieId", bool desc = true)
        {
            ObjectContext objectContext = ((IObjectContextAdapter)db).ObjectContext;
            ViewBag.Count = objectContext.CreateObjectSet<Zamowienie>("Zamowienie").Count();
            ViewBag.Start = start;
            ViewBag.ItemsPerPage = itemsPerPage;
            ViewBag.OrderBy = orderBy;
            ViewBag.Desc = desc;

            return View();
        }

        //
        // GET: /Zamowienie/GridData/?start=0&itemsPerPage=20&orderBy=ZamowienieId&desc=true

        public ActionResult GridData(DateTime? dateFrom, DateTime? dateTo, int start = 0, int itemsPerPage = 20,
            string orderBy = "ZamowienieId", bool desc = true, int nr = 0, string user = "")
        {
            ObjectContext objectContext = ((IObjectContextAdapter)db).ObjectContext;
            ObjectQuery<Zamowienie> zamowienie = objectContext
                .CreateObjectSet<Zamowienie>("Zamowienie")
                .Include("Users");

            if (nr > 0)
                zamowienie = zamowienie.Where("it.ZamowienieId == " + nr.ToString());

            if (dateFrom.HasValue)
                zamowienie = zamowienie.Where("it.ZamowienieData >= @dataFrom", new ObjectParameter("dataFrom", dateFrom.Value));

            if (dateTo.HasValue)
                zamowienie = zamowienie.Where("it.ZamowienieData <= @dataTo", new ObjectParameter("dataTo", dateTo.Value.AddDays(1)));

            if (!string.IsNullOrWhiteSpace(user))
                zamowienie = zamowienie.Where("it.Users.Imie like '%" + user + "%' or it.Users.Nazwisko like '%" + user + "%' or it.Users.Email like '%" + user + "%' or it.Users.UserName like '%" + user + "%'");

            zamowienie = zamowienie.OrderBy("it." + orderBy + (desc ? " desc" : ""));

            Response.AppendHeader("X-Total-Row-Count", zamowienie.Count().ToString());

            return PartialView(zamowienie.Skip(start).Take(itemsPerPage));
        }

        //
        // GET: /Default5/RowData/5

        public ActionResult RowData(int id)
        {
            Zamowienie zamowienie = db.Zamowienie.Include("Users").Single(z => z.ZamowienieId == id);
            return PartialView("GridData", new Zamowienie[] { zamowienie });
        }

        //
        // GET: /Zamowienie/Create

        public ActionResult Create()
        {
            return PartialView("Edit");
        }

        //
        // POST: /Zamowienie/Create

        [HttpPost]
        public ActionResult Create(Zamowienie zamowienie)
        {
            if (ModelState.IsValid)
            {
                db.Zamowienie.Add(zamowienie);
                db.SaveChanges();
                return PartialView("GridData", new Zamowienie[] { zamowienie });
            }

            return PartialView("Edit", zamowienie);
        }

        //
        // GET: /Zamowienie/Edit/5

        public ActionResult Edit(int id)
        {
            Zamowienie zamowienie = db.Zamowienie.Single(z => z.ZamowienieId == id);
            return PartialView(zamowienie);
        }

        //
        // POST: /Zamowienie/Edit/5

        [HttpPost]
        public ActionResult Edit(Zamowienie zamowienie)
        {
            if (ModelState.IsValid)
            {
                db.Zamowienie.Attach(zamowienie);
                ObjectContext objectContext = ((IObjectContextAdapter)db).ObjectContext;
                objectContext.ObjectStateManager.ChangeObjectState(zamowienie, EntityState.Modified);
                db.SaveChanges();
                return PartialView("GridData", new Zamowienie[] { zamowienie });
            }

            return PartialView(zamowienie);
        }

        //
        // POST: /Zamowienie/Delete/5

        [HttpPost]
        public void Delete(int id)
        {
            Zamowienie zamowienie = db.Zamowienie.Single(z => z.ZamowienieId == id);
            db.Zamowienie.Remove(zamowienie);
            db.SaveChanges();
        }

        public class RecznaAktywacjaViewModel
        {
            public int ZamowienieId { get; set; }
            public bool Aktywacja { get; set; }
            public string Opis { get; set; }
        }

        public ActionResult RecznaAktywacja(int id)
        {
            Zamowienie zam = db.Zamowienie.Where(m => m.ZamowienieId == id).Single();

            RecznaAktywacjaViewModel model = new RecznaAktywacjaViewModel();
            model.ZamowienieId = id;
            model.Aktywacja = zam.ZamowienieRecznaAktywacja;
            return PartialView(model);
        }

        [HttpPost]
        public ActionResult RecznaAktywacja(RecznaAktywacjaViewModel model)
        {
            ZamowienieLog log = new ZamowienieLog();
            log.ZamowienieId = model.ZamowienieId;
            log.ZamowienieLogData = DateTime.Now;
            log.ZamowienieLogOperacja = model.Aktywacja ? "Aktywowano zamówienie" : "Dezaktywowano zamówienie";
            log.ZamowienieLogOpis = model.Opis;
            log.issg_user_id = (int)System.Web.Security.Membership.GetUser().ProviderUserKey;
            db.ZamowienieLog.Add(log);
            
            Zamowienie zam = db.Zamowienie.Where(m => m.ZamowienieId == model.ZamowienieId).Single();
            zam.ZamowienieRecznaAktywacja = model.Aktywacja;

            db.SaveChanges();

            return Content("true");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}
