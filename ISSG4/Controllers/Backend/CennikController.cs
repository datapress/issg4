﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ISSG3.Models;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using ISSG4.Models;

namespace ISSG3.Controllers.Backend
{
	[Authorize(Roles = "Administratorzy")]
    public class CennikController : Controller
    {
        protected Entities db = new Entities();

        //
        // GET: /Cennik/

        public ActionResult Index(int parentId = 0, int start = 0, int itemsPerPage = 20, string orderBy = "CennikId", bool desc = false)
        {
            if (parentId == 0) return new EmptyResult();

            ViewBag.ParentId = parentId;
            ViewBag.Count = db.Cennik.Where(m => m.CennikNaglowekId == parentId).Count();
            ViewBag.Start = start;
            ViewBag.ItemsPerPage = itemsPerPage;
            ViewBag.OrderBy = orderBy;
            ViewBag.Desc = desc;

            return PartialView();
        }

        //
        // GET: /Cennik/GridData/?start=0&itemsPerPage=20&orderBy=CennikId&desc=true

        public ActionResult GridData(int parentId = 0, int start = 0, int itemsPerPage = 20, string orderBy = "CennikId", bool desc = false)
        {
            if (parentId == 0) return new EmptyResult();

            ObjectContext objectContext = ((IObjectContextAdapter)db).ObjectContext;

            Response.AppendHeader("X-Total-Row-Count", db.Cennik.Count().ToString());

            ObjectQuery<Cennik> cennik = objectContext
                    .CreateObjectSet<Cennik>("Cennik")
                    .Include("i3_CennikNaglowek")
                    .Include("i3_Gazeta")
                    .Include("i3_Produkt")
                    .Where("it.CennikNaglowekId == " + parentId.ToString());
                    //.Where(d => d.CennikNaglowekId == parentId);
            

            cennik = cennik.OrderBy("it." + orderBy + (desc ? " desc" : ""));

            return PartialView(cennik.Skip(start).Take(itemsPerPage));
        }

        //
        // GET: /Default5/RowData/5

        public ActionResult RowData(int id)
        {
            Cennik cennik = db.Cennik.Single(c => c.CennikId == id);
            return PartialView("GridData", new Cennik[] { cennik });
        }

        //
        // GET: /Cennik/Create

        public ActionResult Create(int parentId)
        {
            ViewBag.ParentId = parentId;
            ViewBag.VatId = new SelectList(db.Vat, "VatId", "VatId");
            ViewBag.GazetaId = new SelectList(db.Gazeta, "GazetaId", "GazetaNazwa");
            ViewBag.ProduktId = new SelectList(db.Produkt, "ProduktId", "ProduktNazwa");
            return PartialView("Edit");
        }

        //
        // POST: /Cennik/Create

        [HttpPost]
        public ActionResult Create(Cennik cennik)
        {
            if (ModelState.IsValid)
            {
                db.Cennik.Add(cennik);
                db.SaveChanges();
                return PartialView("GridData", new Cennik[] { cennik });
            }

            ViewBag.VatId = new SelectList(db.Vat, "VatId", "VatId");
            ViewBag.GazetaId = new SelectList(db.Gazeta, "GazetaId", "GazetaNazwa", cennik.GazetaId);
            ViewBag.ProduktId = new SelectList(db.Produkt, "ProduktId", "ProduktNazwa", cennik.ProduktId);
            return PartialView("Edit", cennik);
        }

        //
        // GET: /Cennik/Edit/5

        public ActionResult Edit(int id)
        {
            Cennik cennik = db.Cennik.Single(c => c.CennikId == id);
            ViewBag.CennikNaglowekId = cennik.CennikNaglowekId;
            ViewBag.VatId = new SelectList(db.Vat, "VatId", "VatId", cennik.VatId);
            ViewBag.GazetaId = new SelectList(db.Gazeta, "GazetaId", "GazetaNazwa", cennik.GazetaId);
            ViewBag.ProduktId = new SelectList(db.Produkt, "ProduktId", "ProduktNazwa", cennik.ProduktId);
            return PartialView(cennik);
        }

        //
        // POST: /Cennik/Edit/5

        [HttpPost]
        public ActionResult Edit(Cennik cennik)
        {
            if (ModelState.IsValid)
            {
                db.Cennik.Attach(cennik);
                ObjectContext objectContext = ((IObjectContextAdapter)db).ObjectContext;
                objectContext.ObjectStateManager.ChangeObjectState(cennik, EntityState.Modified);
                db.SaveChanges();
                return PartialView("GridData", new Cennik[] { cennik });
            }

            ViewBag.CennikNaglowekId = cennik.CennikNaglowekId;
            ViewBag.VatId = new SelectList(db.Vat, "VatId", "VatId");
            ViewBag.GazetaId = new SelectList(db.Gazeta, "GazetaId", "GazetaNazwa", cennik.GazetaId);
            ViewBag.ProduktId = new SelectList(db.Produkt, "ProduktId", "ProduktNazwa", cennik.ProduktId);
            return PartialView(cennik);
        }

        //
        // POST: /Cennik/Delete/5

        [HttpPost]
        public void Delete(int id)
        {
            Cennik cennik = db.Cennik.Single(c => c.CennikId == id);
            db.Cennik.Remove(cennik);
            db.SaveChanges();
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}
