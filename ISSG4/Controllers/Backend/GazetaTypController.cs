﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ISSG4.Models;

namespace ISSG3.Controllers.Backend
{
	[Authorize(Roles = "Administratorzy")]
    public class GazetaTypController : Controller
    {
        private Entities db = new Entities();

        //
        // GET: /GazetaTyp/

        public ViewResult Index(int start = 0, int itemsPerPage = 20, string orderBy = "GazetaTypId", bool desc = false)
        {
            ViewBag.Count = db.GazetaTyp.Count();
            ViewBag.Start = start;
            ViewBag.ItemsPerPage = itemsPerPage;
            ViewBag.OrderBy = orderBy;
            ViewBag.Desc = desc;

            return View();
        }

        //
        // GET: /GazetaTyp/GridData/?start=0&itemsPerPage=20&orderBy=GazetaTypId&desc=true

        public ActionResult GridData(int start = 0, int itemsPerPage = 20, string orderBy = "GazetaTypId", bool desc = false)
        {
            Response.AppendHeader("X-Total-Row-Count", db.GazetaTyp.Count().ToString());
            ObjectContext objectContext = ((IObjectContextAdapter)db).ObjectContext;

            ObjectQuery<GazetaTyp> gazetatyp = objectContext.CreateObjectSet<GazetaTyp>("GazetaTyp");
            gazetatyp = gazetatyp.OrderBy("it." + orderBy + (desc ? " desc" : ""));

            return PartialView(gazetatyp.Skip(start).Take(itemsPerPage));
        }

        //
        // GET: /Default5/RowData/5

        public ActionResult RowData(int id)
        {
            GazetaTyp gazetatyp = db.GazetaTyp.Single(g => g.GazetaTypId == id);
            return PartialView("GridData", new GazetaTyp[] { gazetatyp });
        }

        //
        // GET: /GazetaTyp/Create

        public ActionResult Create()
        {
            return PartialView("Edit");
        }

        //
        // POST: /GazetaTyp/Create

        [HttpPost]
        public ActionResult Create(GazetaTyp gazetatyp)
        {
            if (ModelState.IsValid)
            {
                db.GazetaTyp.Add(gazetatyp);
                db.SaveChanges();
                return PartialView("GridData", new GazetaTyp[] { gazetatyp });
            }

            return PartialView("Edit", gazetatyp);
        }

        //
        // GET: /GazetaTyp/Edit/5

        public ActionResult Edit(int id)
        {
            GazetaTyp gazetatyp = db.GazetaTyp.Single(g => g.GazetaTypId == id);
            return PartialView(gazetatyp);
        }

        //
        // POST: /GazetaTyp/Edit/5

        [HttpPost]
        public ActionResult Edit(GazetaTyp gazetatyp)
        {
            if (ModelState.IsValid)
            {
                db.GazetaTyp.Attach(gazetatyp);

                ObjectContext objectContext = ((IObjectContextAdapter)db).ObjectContext;

                objectContext.ObjectStateManager.ChangeObjectState(gazetatyp, EntityState.Modified);
                db.SaveChanges();
                return PartialView("GridData", new GazetaTyp[] { gazetatyp });
            }

            return PartialView(gazetatyp);
        }

        //
        // POST: /GazetaTyp/Delete/5

        [HttpPost]
        public void Delete(int id)
        {
            GazetaTyp gazetatyp = db.GazetaTyp.Single(g => g.GazetaTypId == id);
            db.GazetaTyp.Remove(gazetatyp);
            db.SaveChanges();
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}
