﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ISSG4.Models;

namespace ISSG3.Controllers.Backend
{
	[Authorize(Roles = "Administratorzy")]
    public class GazetaKodController : Controller
    {
        private Entities db = new Entities();

        //
        // GET: /GazetaKod/

        public ViewResult Index(int start = 0, int itemsPerPage = 20, string orderBy = "GazetaKodId", bool desc = false)
        {
            ViewBag.Count = db.GazetaKod.Count();
            ViewBag.Start = start;
            ViewBag.ItemsPerPage = itemsPerPage;
            ViewBag.OrderBy = orderBy;
            ViewBag.Desc = desc;

            return View();
        }

        //
        // GET: /GazetaKod/GridData/?start=0&itemsPerPage=20&orderBy=GazetaKodId&desc=true

        public ActionResult GridData(int start = 0, int itemsPerPage = 20, string orderBy = "GazetaKodId", bool desc = false)
        {
            Response.AppendHeader("X-Total-Row-Count", db.GazetaKod.Count().ToString());
            ObjectContext objectContext = ((IObjectContextAdapter)db).ObjectContext;

            ObjectQuery<GazetaKod> gazetakod = objectContext.CreateObjectSet<GazetaKod>("GazetaKod");
            
            gazetakod = gazetakod.OrderBy("it." + orderBy + (desc ? " desc" : ""));

            return PartialView(gazetakod.Skip(start).Take(itemsPerPage));
        }

        //
        // GET: /Default5/RowData/5

        public ActionResult RowData(int id)
        {
            GazetaKod gazetakod = db.GazetaKod.Single(g => g.GazetaKodId == id);
            return PartialView("GridData", new GazetaKod[] { gazetakod });
        }

        //
        // GET: /GazetaKod/Create

        public ActionResult Create()
        {
            return PartialView("Edit");
        }

        //
        // POST: /GazetaKod/Create

        [HttpPost]
        public ActionResult Create(GazetaKod gazetakod)
        {
            if (ModelState.IsValid)
            {
                db.GazetaKod.Add(gazetakod);
                db.SaveChanges();
                return PartialView("GridData", new GazetaKod[] { gazetakod });
            }

            return PartialView("Edit", gazetakod);
        }

        //
        // GET: /GazetaKod/Edit/5

        public ActionResult Edit(int id)
        {
            GazetaKod gazetakod = db.GazetaKod.Single(g => g.GazetaKodId == id);
            return PartialView(gazetakod);
        }

        //
        // POST: /GazetaKod/Edit/5

        [HttpPost]
        public ActionResult Edit(GazetaKod gazetakod)
        {
            if (ModelState.IsValid)
            {
                ObjectContext objectContext = ((IObjectContextAdapter)db).ObjectContext;
                db.GazetaKod.Attach(gazetakod);
                objectContext.ObjectStateManager.ChangeObjectState(gazetakod, EntityState.Modified);
                db.SaveChanges();
                return PartialView("GridData", new GazetaKod[] { gazetakod });
            }

            return PartialView(gazetakod);
        }

        //
        // POST: /GazetaKod/Delete/5

        [HttpPost]
        public void Delete(int id)
        {
            GazetaKod gazetakod = db.GazetaKod.Single(g => g.GazetaKodId == id);
            db.GazetaKod.Remove(gazetakod);
            db.SaveChanges();
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}
