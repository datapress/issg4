﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ISSG4.Models;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;

namespace ISSG3.Controllers.Backend
{
	[Authorize(Roles = "Administratorzy")]
    public class CennikNaglowekController : Controller
    {
        private Entities db = new Entities();

        //
        // GET: /CennikNaglowek/

        public ViewResult Index(int start = 0, int itemsPerPage = 20, string orderBy = "CennikNaglowekId", bool desc = false)
        {
            ViewBag.Count = db.CennikNaglowek.Count();
            ViewBag.Start = start;
            ViewBag.ItemsPerPage = itemsPerPage;
            ViewBag.OrderBy = orderBy;
            ViewBag.Desc = desc;

            return View();
        }

        //
        // GET: /CennikNaglowek/GridData/?start=0&itemsPerPage=20&orderBy=CennikNaglowekId&desc=true

        public ActionResult GridData(int start = 0, int itemsPerPage = 20, string orderBy = "CennikNaglowekId", bool desc = false)
        {
            Response.AppendHeader("X-Total-Row-Count", db.CennikNaglowek.Count().ToString());

            ObjectContext objectContext = ((IObjectContextAdapter)db).ObjectContext;
            ObjectQuery<CennikNaglowek> cenniknaglowek = objectContext.CreateObjectSet<CennikNaglowek>("CennikNaglowek");
            //ObjectQuery<CennikNaglowek> cenniknaglowek = db.CennikNaglowek;
            cenniknaglowek = cenniknaglowek.OrderBy("it." + orderBy + (desc ? " desc" : ""));

            return PartialView(cenniknaglowek.Skip(start).Take(itemsPerPage));
        }

        //
        // GET: /Default5/RowData/5

        public ActionResult RowData(int id)
        {
            CennikNaglowek cenniknaglowek = db.CennikNaglowek.Single(c => c.CennikNaglowekId == id);
            return PartialView("GridData", new CennikNaglowek[] { cenniknaglowek });
        }

        //
        // GET: /CennikNaglowek/Create

        public ActionResult Create()
        {
            return PartialView("Edit");
        }

        //
        // POST: /CennikNaglowek/Create

        [HttpPost]
        public ActionResult Create(CennikNaglowek cenniknaglowek)
        {
            if (ModelState.IsValid)
            {
                db.CennikNaglowek.Add(cenniknaglowek);
                db.SaveChanges();
                return PartialView("GridData", new CennikNaglowek[] { cenniknaglowek });
            }

            return PartialView("Edit", cenniknaglowek);
        }

        //
        // GET: /CennikNaglowek/Edit/5

        public ActionResult Edit(int id)
        {
            CennikNaglowek cenniknaglowek = db.CennikNaglowek.Single(c => c.CennikNaglowekId == id);
            return PartialView(cenniknaglowek);
        }

        //
        // POST: /CennikNaglowek/Edit/5

        [HttpPost]
        public ActionResult Edit(CennikNaglowek cenniknaglowek)
        {
            if (ModelState.IsValid)
            {
                db.CennikNaglowek.Attach(cenniknaglowek);
                ObjectContext objectContext = ((IObjectContextAdapter)db).ObjectContext;

                objectContext.ObjectStateManager.ChangeObjectState(cenniknaglowek, EntityState.Modified);
                db.SaveChanges();
                return PartialView("GridData", new CennikNaglowek[] { cenniknaglowek });
            }

            return PartialView(cenniknaglowek);
        }

        //
        // POST: /CennikNaglowek/Delete/5

        [HttpPost]
        public void Delete(int id)
        {
            CennikNaglowek cenniknaglowek = db.CennikNaglowek.Single(c => c.CennikNaglowekId == id);
            db.CennikNaglowek.Remove(cenniknaglowek);
            db.SaveChanges();
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}
