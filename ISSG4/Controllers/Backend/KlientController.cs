﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ISSG4.Models;
using DotNetOpenMail;
using DotNetOpenMail.SmtpAuth;
using System.Transactions;
using System.Configuration;
using System.Web.Security;
using NLog;

namespace ISSG3.Controllers.Backend
{
    [Authorize(Roles = "Administratorzy,Kolportaż")]
    public class KlientController : Controller
    {
        private Entities db = new Entities();

        Logger logger = LogManager.GetCurrentClassLogger();

        //
        // GET: /Klient/

        public ViewResult Index(int start = 0, int itemsPerPage = 20, string orderBy = "ispo_user_id", bool desc = false)
        {
            ViewBag.Count = db.Users.Count();
            ViewBag.Start = start;
            ViewBag.ItemsPerPage = itemsPerPage;
            ViewBag.OrderBy = orderBy;
            ViewBag.Desc = desc;

            return View();
        }

        //
        // GET: /Klient/GridData/?start=0&itemsPerPage=20&orderBy=ispo_user_id&desc=true

        public ActionResult GridData(int start = 0, int itemsPerPage = 20, string orderBy = "ispo_user_id", bool desc = false, string user = "")
        {
            ObjectContext objectContext = ((IObjectContextAdapter)db).ObjectContext;
            ObjectQuery<Users> users = objectContext.CreateObjectSet<Users>("Users");

            if (!string.IsNullOrWhiteSpace(user))
                users = users.Where("it.Imie like '%" + user + "%' or it.Nazwisko like '%" + user + "%' or it.Email like '%" + user + "%' or it.UserName like '%" + user + "%'");

            users = users.OrderBy("it." + orderBy + (desc ? " desc" : ""));

            Response.AppendHeader("X-Total-Row-Count", users.Count().ToString());

            return PartialView(users.Skip(start).Take(itemsPerPage));
        }

        //
        // GET: /Default5/RowData/5

        public ActionResult RowData(int id)
        {
            Users users = db.Users.Single(u => u.ispo_user_id == id);
            return PartialView("GridData", new Users[] { users });
        }

        //
        // GET: /Klient/Create

        public ActionResult Create()
        {
            //return PartialView("Edit");
            return View("Edit");
        }

        public ActionResult Przepisz(int userid)
        {

            UserAdminCreatedModel model = new UserAdminCreatedModel();

            string username = Membership.GetUser((object)userid).UserName;
            logger.Debug("Przepisywanie prenumeraty dla usera id:"+userid.ToString()+", username:"+username);

            model.username = username;
            //model.passwd = passwordreturn;
            //model.admin_email = admin_email_address;

            List<viewZamowienieJoined> zamowienia = new List<viewZamowienieJoined>();

            //pokaż prenumeraty zalogowane usera by je potem przepisać.
            using (Entities ie = new Entities())
            {
                if (User.Identity.IsAuthenticated)
                {
                    int userId = (int)Membership.GetUser().ProviderUserKey;
                    zamowienia = ie.viewZamowienieJoined.Where(m => m.issg_user_id == userId).OrderBy(m => m.ZamowienieData).ToList();
                    model.prenumeraty = zamowienia;
                }
            }

            return View(model);
        }

        //
        // POST: /Klient/Create

        [HttpPost]
        public ActionResult Create(Users users)
        {
            string imie ="-";
            string nazwisko = "-";
            string telefon= "-";
            string email;
            string login = "";

            string hash = "";
            string salt = "";
            string password = "";

            string message;

            login = users.UserName;
            email = users.UserName;
            imie = string.IsNullOrEmpty(users.Imie) == true ? "---" : users.Imie;
            nazwisko = string.IsNullOrEmpty(users.Nazwisko) == true ? "---" : users.Nazwisko;
            telefon = string.IsNullOrEmpty(users.telefon) == true ? "---" : users.telefon;
            string passwordreturn;
            
            
            //przrobić na wersję uruchamianą z db. Tam ma być jedna procedura
            // na bazie logiki związanej z issg_users_auto
            // TODO

            List<viewZamowienieJoined> zamowienia = new List<viewZamowienieJoined>();

            using (Entities db = new Entities())
            {
                //ObjectParameter op = new ObjectParameter("haslo",typeof(string));

                logger.Debug("Uruchamiam procedurę sql dodawania usera: "+login);
                
                passwordreturn = db.addUserFromAdmin(login,imie, nazwisko, telefon).Single();
               // password = op.Value.ToString();
            }

            if (!string.IsNullOrEmpty(passwordreturn))
            {
                //ukryte carbon copy dla awaryjnego przekazania hasła.
                string admin_email_address = Membership.GetUser().Email;
                logger.Debug("Mail admina dla wysłania hasło tworzonego konta: "+admin_email_address);

                logger.Debug("UserAdmin created, hasło: " + passwordreturn);
                SendPassByMailForAdminUser(login, passwordreturn, admin_email_address);
                logger.Debug("UserAdmin created, mailed");

                UserAdminCreatedModel model = new UserAdminCreatedModel();
                model.username = login;
                model.passwd = passwordreturn;
                model.admin_email = admin_email_address;

                using (Entities ie = new Entities())
                {
                    if (User.Identity.IsAuthenticated)
                    {
                        int userId = (int)Membership.GetUser().ProviderUserKey;
                        zamowienia = ie.viewZamowienieJoined.Where(m => m.issg_user_id == userId).OrderBy(m => m.ZamowienieData).ToList();
                        model.prenumeraty = zamowienia;
                    }
                }

                return View("Created",model);
            }


            //if (validateLogin(login, out message))
            //{

            //    /* generujemy hasło, oraz pasujące salt i hash */

            //    /* w transakcji należy wpisać do issg_users_auto z odpowiednią grupą. */

            //    /*issg_users_auto iua = new issg_users_auto();
            //    iua.grupa = "ISSG3Backend";
            //    iua.haslo = password;
            //    iua.salt = salt;
            //    iua.haslo = hash;
            //    iua.login = login;*/

            
            //}


            //if (ModelState.IsValid)
            //{
            //    db.Users.AddObject(users);
            //    db.SaveChanges();
            //    return PartialView("GridData", new Users[] { users });
            //}

            //return PartialView("Edit", users);
            return View("Edit", users);
        }

      

        public ActionResult Created(UserAdminCreatedModel model)
        {

            return View(model);
        }

        private bool validateLogin(string login, out string message)
        {
            message = "";

            if (login.Length < 5)
            {
                message = "Minimalna długość loginu to 5 znaków";
            }
            if (login.Length < 5 || login.Length > 224)
            {
                message = "Maksymalna długość loginu to 224 znaki";
            }

            return message.Length == 0 ? true : false;
        }

        //
        // GET: /Klient/Edit/5

        public ActionResult Edit(int id)
        {
            Users users = db.Users.Single(u => u.ispo_user_id == id);
            return PartialView(users);
        }

        //
        // POST: /Klient/Edit/5

        [HttpPost]
        public ActionResult Edit(Users users)
        {
            if (ModelState.IsValid)
            {
                ObjectContext objectContext = ((IObjectContextAdapter)db).ObjectContext;
                db.Users.Attach(users);
                objectContext.ObjectStateManager.ChangeObjectState(users, EntityState.Modified);
                db.SaveChanges();
                return PartialView("GridData", new Users[] { users });
            }

            return PartialView(users);
        }

        //
        // POST: /Klient/Delete/5

        [HttpPost]
        public void Delete(int id)
        {
            Users users = db.Users.Single(u => u.ispo_user_id == id);
            db.Users.Remove(users);
            db.SaveChanges();
        }

        public ActionResult Szczegoly(int id)
        {
            var User = db.Users.Where(m => m.ispo_user_id == id).Single();

            return PartialView(User);
        }

        public void Blokada(int id, bool blokada)
        {
            var usr = db.Users.Where(m => m.ispo_user_id == id).Single();
            usr.czy_zablokowane = (blokada ? "t" : "n" );
            usr.czy_zablokowane_kiedy = DateTime.Now;
            usr.czy_zablokowane_kto = User.Identity.Name;

            db.SaveChanges();
        }

        public ViewResult ZdarzeniaAudyt(int id)
        {
            var User = db.Users.Where(m => m.ispo_user_id == id).Single();

            return View(User);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        public void SendPassByMailForAdminUser(string username, string as_newpass, string admin_email_address)
        {
            Logger logger = LogManager.GetCurrentClassLogger();
            SmtpServer smtpServer;
            AppSettingsReader apr = new AppSettingsReader();

            string ls_mailserver = apr.GetValue("MailServer", typeof(string)).ToString();
            string ls_mailaddress = apr.GetValue("MailSenderAddress", typeof(string)).ToString();
            string ls_mailfrom = apr.GetValue("MailSenderFrom", typeof(string)).ToString();
            string ls_urllink = apr.GetValue("MailLink", typeof(string)).ToString();
            string ls_serverport = apr.GetValue("MailServerPort", typeof(string)).ToString();

            smtpServer = new SmtpServer(ls_mailserver, int.Parse(ls_serverport));

            if (ConfigurationManager.AppSettings["MailAuth"].ToString() == "yes")
            {
                string ls_mailuser = apr.GetValue("MailUser", typeof(string)).ToString();
                string ls_mailpwd = apr.GetValue("MailPass", typeof(string)).ToString();
                smtpServer.SmtpAuthToken = new SmtpAuthToken(ls_mailuser, ls_mailpwd);
            }

            try
            {
                EmailMessage emailMessage = new DotNetOpenMail.EmailMessage();
                emailMessage.HeaderCharSet = System.Text.Encoding.GetEncoding("UTF-8");
                emailMessage.HeaderEncoding = DotNetOpenMail.Encoding.EncodingType.QuotedPrintable;

                emailMessage.FromAddress = new DotNetOpenMail.EmailAddress(ls_mailaddress, ls_mailfrom,
                    DotNetOpenMail.Encoding.EncodingType.QuotedPrintable, System.Text.Encoding.GetEncoding("UTF-8"));
                emailMessage.AddToAddress(new EmailAddress(username));
                //emailMessage.AddCcAddress(new EmailAddress(admin_email_address)); dla testów tylko włączyć
                emailMessage.AddBccAddress(new EmailAddress(admin_email_address)); //po testach ma być BCC
                emailMessage.Subject = "Prenumerata";

                string ls_send_time = DateTime.Now.ToString("dd-MM-yyyy HH:mm");

                String scriptString = "\nDzień dobry! \n";
                scriptString += "\n";
                scriptString += "Dla adresu email " + username + "\n";
                scriptString += "utworzone zostało konto dla prenumeraty Gazety Olsztyńskiej.\n";
                scriptString += "Adres email jest również loginem w serwisie kupgazete.pl\n";
                scriptString += "Do konta zostało wygenerowane hasło: \n";
                scriptString += "\n";
                scriptString += as_newpass;
                scriptString += "\n\nProsimy po zalogowaniu ustawić własne hasło.";

                //emailMessage.BodyText = scriptString;
                TextAttachment ta = new TextAttachment(scriptString);
                ta.CharSet = System.Text.Encoding.GetEncoding("UTF-8");
                emailMessage.TextPart = ta;

                emailMessage.Send(smtpServer);

                logger.Info("Wysłano email z nowym hasła dla użytkownika z admin backend" + username + " na adres: " + username);

            }
            catch (DotNetOpenMail.SmtpException ex)
            {
                logger.Error("SmtpException: " + ex.Message);

            }
            catch (DotNetOpenMail.MailException ex)
            {
                logger.Error("MailException : " + ex.Message);

            }
            catch (Exception ex)
            {
                logger.Error("Exception : " + ex.Message);

            }

        }
    }
}
