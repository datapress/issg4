﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ISSG4.Models;

namespace ISSG3.Controllers.Backend
{
	[Authorize(Roles = "Administratorzy")]
    public class ProduktRodajController : Controller
    {
        private Entities db = new Entities();

        //
        // GET: /ProduktRodaj/

        public ViewResult Index(int start = 0, int itemsPerPage = 20, string orderBy = "ProduktRodzajId", bool desc = false)
        {
            ObjectContext objectContext = ((IObjectContextAdapter)db).ObjectContext;

            ViewBag.Count = objectContext.CreateObjectSet<ProduktRodzaj>("ProduktRodzaj").Count();
            ViewBag.Start = start;
            ViewBag.ItemsPerPage = itemsPerPage;
            ViewBag.OrderBy = orderBy;
            ViewBag.Desc = desc;

            return View();
        }

        //
        // GET: /ProduktRodaj/GridData/?start=0&itemsPerPage=20&orderBy=ProduktRodzajId&desc=true

        public ActionResult GridData(int start = 0, int itemsPerPage = 20, string orderBy = "ProduktRodzajId", bool desc = false)
        {
            Response.AppendHeader("X-Total-Row-Count", db.ProduktRodzaj.Count().ToString());

            ObjectContext objectContext = ((IObjectContextAdapter)db).ObjectContext;

            ObjectQuery<ProduktRodzaj> produktrodzaj = objectContext.CreateObjectSet<ProduktRodzaj>("ProduktRodzaj");
            produktrodzaj = produktrodzaj.OrderBy("it." + orderBy + (desc ? " desc" : ""));

            return PartialView(produktrodzaj.Skip(start).Take(itemsPerPage));
        }

        //
        // GET: /Default5/RowData/5

        public ActionResult RowData(int id)
        {
            ProduktRodzaj produktrodzaj = db.ProduktRodzaj.Single(p => p.ProduktRodzajId == id);
            return PartialView("GridData", new ProduktRodzaj[] { produktrodzaj });
        }

        //
        // GET: /ProduktRodaj/Create

        public ActionResult Create()
        {
            return PartialView("Edit");
        }

        //
        // POST: /ProduktRodaj/Create

        [HttpPost]
        public ActionResult Create(ProduktRodzaj produktrodzaj)
        {
            if (ModelState.IsValid)
            {
                db.ProduktRodzaj.Add(produktrodzaj);
                db.SaveChanges();
                return PartialView("GridData", new ProduktRodzaj[] { produktrodzaj });
            }

            return PartialView("Edit", produktrodzaj);
        }

        //
        // GET: /ProduktRodaj/Edit/5

        public ActionResult Edit(int id)
        {
            ProduktRodzaj produktrodzaj = db.ProduktRodzaj.Single(p => p.ProduktRodzajId == id);
            return PartialView(produktrodzaj);
        }

        //
        // POST: /ProduktRodaj/Edit/5

        [HttpPost]
        public ActionResult Edit(ProduktRodzaj produktrodzaj)
        {
            if (ModelState.IsValid)
            {
                db.ProduktRodzaj.Attach(produktrodzaj);
                ObjectContext objectContext = ((IObjectContextAdapter)db).ObjectContext;
                objectContext.ObjectStateManager.ChangeObjectState(produktrodzaj, EntityState.Modified);
                db.SaveChanges();
                return PartialView("GridData", new ProduktRodzaj[] { produktrodzaj });
            }

            return PartialView(produktrodzaj);
        }

        //
        // POST: /ProduktRodaj/Delete/5

        [HttpPost]
        public void Delete(int id)
        {
            ProduktRodzaj produktrodzaj = db.ProduktRodzaj.Single(p => p.ProduktRodzajId == id);
            db.ProduktRodzaj.Remove(produktrodzaj);
            db.SaveChanges();
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}
