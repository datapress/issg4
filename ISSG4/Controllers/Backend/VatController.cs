﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ISSG4.Models;

namespace ISSG3.Controllers.Backend
{
    [Authorize(Roles = "Administratorzy")]
    public class VatController : Controller
    {
        private Entities db = new Entities();

        public ViewResult Index(int start = 0, int itemsPerPage = 20, string orderBy = "VatId", bool desc = false)
        {
            ViewBag.Count = db.Vat.Count();
            ViewBag.Start = start;
            ViewBag.ItemsPerPage = itemsPerPage;
            ViewBag.OrderBy = orderBy;
            ViewBag.Desc = desc;

            return View();
        }

        //
        // GET: /Vat1/GridData/?start=0&itemsPerPage=20&orderBy=VatId&desc=true

        public ActionResult GridData(int start = 0, int itemsPerPage = 20, string orderBy = "VatId", bool desc = false)
        {
            Response.AppendHeader("X-Total-Row-Count", db.Vat.Count().ToString());
            ObjectContext objectContext = ((IObjectContextAdapter)db).ObjectContext;
            ObjectQuery<Vat> vat = objectContext.CreateObjectSet<Vat>("Vat");
            vat = vat.OrderBy("it." + orderBy + (desc ? " desc" : ""));

            return PartialView(vat.Skip(start).Take(itemsPerPage));
        }

        //
        // GET: /Default5/RowData/5

        public ActionResult RowData(string id)
        {
            Vat vat = db.Vat.Single(v => v.VatId == id);
            return PartialView("GridData", new Vat[] { vat });
        }

        //
        // GET: /Vat1/Create

        public ActionResult Create()
        {
            return PartialView("Edit");
        }

        //
        // POST: /Vat1/Create

        [HttpPost]
        public ActionResult Create(Vat vat)
        {
            if (ModelState.IsValid)
            {
                db.Vat.Add(vat);
                db.SaveChanges();
                return PartialView("GridData", new Vat[] { vat });
            }

            return PartialView("Edit", vat);
        }

        //
        // GET: /Vat1/Edit/5

        public ActionResult Edit(string id)
        {
            Vat vat = db.Vat.Single(v => v.VatId == id);
            return PartialView(vat);
        }

        //
        // POST: /Vat1/Edit/5

        [HttpPost]
        public ActionResult Edit(Vat vat)
        {
            if (ModelState.IsValid)
            {
                db.Vat.Attach(vat);
                ObjectContext objectContext = ((IObjectContextAdapter)db).ObjectContext;
                objectContext.ObjectStateManager.ChangeObjectState(vat, EntityState.Modified);
                db.SaveChanges();
                return PartialView("GridData", new Vat[] { vat });
            }

            return PartialView(vat);
        }

        //
        // POST: /Vat1/Delete/5

        [HttpPost]
        public void Delete(string id)
        {
            Vat vat = db.Vat.Single(v => v.VatId == id);
            db.Vat.Remove(vat);
            db.SaveChanges();
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}
