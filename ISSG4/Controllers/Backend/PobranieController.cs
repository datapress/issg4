﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ISSG4.Models;
using System.Transactions;

namespace ISSG3.Controllers.Backend
{
    [Authorize(Roles = "Administratorzy,Kolportaż")]
    public class PobranieController : Controller
    {
        private Entities db = new Entities();

        //
        // GET: /Pobranie/

        public ViewResult Index(int start = 0, int itemsPerPage = 20, string orderBy = "PobranieId", bool desc = true)
        {
            ViewBag.Count = db.Pobranie.Count();
            ViewBag.Start = start;
            ViewBag.ItemsPerPage = itemsPerPage;
            ViewBag.OrderBy = orderBy;
            ViewBag.Desc = desc;

            return View();
        }

        //
        // GET: /Pobranie/GridData/?start=0&itemsPerPage=20&orderBy=v&desc=true

        public ActionResult GridData(DateTime? dateFrom, DateTime? dateTo, int start = 0, int itemsPerPage = 20, string orderBy = "PobranieId", bool desc = true)
        {

            ObjectContext objectContext = ((IObjectContextAdapter)db).ObjectContext;

            ObjectQuery<Pobranie> pobranie = objectContext
                .CreateObjectSet<Pobranie>("Pobranie")
                .Include("i3_Wydanie").Include("Users");

            if (dateFrom.HasValue)
                pobranie = pobranie.Where("it.PobranieData >= @dataFrom", new ObjectParameter("dataFrom", dateFrom.Value));

            if (dateTo.HasValue)
                pobranie = pobranie.Where("it.PobranieData <= @dataTo", new ObjectParameter("dataTo", dateTo.Value.AddDays(1)));

            pobranie = pobranie.OrderBy("it." + orderBy + (desc ? " desc" : ""));

            Response.AppendHeader("X-Total-Row-Count", pobranie.Count().ToString());

            return PartialView(pobranie.Skip(start).Take(itemsPerPage));
        }

        //
        // GET: /Default5/RowData/5

        public ActionResult RowData(int pobranieId)
        {
            Pobranie pobranie = db.Pobranie.Include("i3_Wydanie").Include("Users").Single(k => k.PobranieId == pobranieId);
            return PartialView("GridData", new Pobranie[] { pobranie });
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}
