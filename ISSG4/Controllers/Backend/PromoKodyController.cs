﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ISSG4.Models;

namespace ISSG3.Controllers.Backend
{
    public class PromoKodyGenViewModel
    {
        [Range(1, 10000, ErrorMessage="Parametr ilość musi mieścić się w granicach: 1-10000")]
        public int ilosc { get; set; }

        public string waznedo { get; set; }
        
        public int typ { get; set; }
    }

    [Authorize(Roles = "Administratorzy,Kolportaż")]
    public class PromoKodyController : Controller
    {
        private Entities db = new Entities();

        //
        // GET: /PromoKody/

        public ViewResult Index(int start = 0, int itemsPerPage = 20, string orderBy = "PromoKodGrupaKiedy", bool desc = true)
        {
            ObjectContext objectContext = ((IObjectContextAdapter)db).ObjectContext;
            ViewBag.Count = objectContext.CreateObjectSet<PromoKodGrupa>("PromoKodGrupa").Count();
            ViewBag.Start = start;
            ViewBag.ItemsPerPage = itemsPerPage;
            ViewBag.OrderBy = orderBy;
            ViewBag.Desc = desc;

            ViewBag.PromoKodTypList = new SelectList(db.PromoKodTyp.ToList(), "PromoKodTypId", "Nazwa");

            return View();
        }

        [HttpPost]
        public ViewResult Index(PromoKodyGenViewModel gen, int start = 0, int itemsPerPage = 20, string orderBy = "PromoKodGrupaKiedy", bool desc = true)
        {
            ViewBag.Count = db.PromoKodGrupa.Count();
            ViewBag.Start = start;
            ViewBag.ItemsPerPage = itemsPerPage;
            ViewBag.OrderBy = orderBy;
            ViewBag.Desc = desc;

            ViewBag.PromoKodTypList = new SelectList(db.PromoKodTyp.ToList(), "PromoKodTypId", "Nazwa");

            if (ModelState.IsValid) {
                DateTime waznedo;
                if (DateTime.TryParse(gen.waznedo, out waznedo)) {
                    db.genPromoKody(gen.typ, (int)System.Web.Security.Membership.GetUser().ProviderUserKey, waznedo, gen.ilosc);
                }
            }

            return View();
        }

        //
        // GET: /PromoKody/GridData/?start=0&itemsPerPage=20&orderBy=PromoKodGrupaId&desc=true

        public ActionResult GridData(int start = 0, int itemsPerPage = 20, string orderBy = "PromoKodGrupaKiedy", bool desc = true)
        {
            Response.AppendHeader("X-Total-Row-Count", db.PromoKodGrupa.Count().ToString());

            ObjectContext objectContext = ((IObjectContextAdapter)db).ObjectContext;

            ObjectQuery<PromoKodGrupa> promokodgrupa = objectContext.CreateObjectSet<PromoKodGrupa>("PromoKodGrupa")
                .Include("Users");

            promokodgrupa = promokodgrupa.OrderBy("it." + orderBy + (desc ? " desc" : ""));
            promokodgrupa = promokodgrupa.Include("i3_PromoKodTyp");

            return PartialView(promokodgrupa.Skip(start).Take(itemsPerPage));
        }


        public ActionResult Kod(int id)
        {
            var model = db.PromoKod.Where(m => m.PromoKodGrupaId == id).ToList();

            return View(model);
        }

        [HttpPost]
        public ActionResult Kod(int id, List<PromoKod> kody)
        {
            var model = db.PromoKod.Where(m => m.PromoKodGrupaId == id);
            
            foreach (var item in model)
            {
                var kod = kody.Where(m => m.PromoKodId == item.PromoKodId).DefaultIfEmpty().SingleOrDefault();
                if (kod != null) item.PromoKodWyslany = kod.PromoKodWyslany;
            }
            db.SaveChanges();

            return View(model.ToList());
        }

        ////
        //// GET: /Default5/RowData/5

        //public ActionResult RowData(int id)
        //{
        //    PromoKodGrupa promokodgrupa = db.PromoKodGrupa.Single(p => p.PromoKodGrupaId == id);
        //    return PartialView("GridData", new PromoKodGrupa[] { promokodgrupa });
        //}

        ////
        //// GET: /PromoKody/Create

        //public ActionResult Create()
        //{
        //    return PartialView("Edit");
        //}

        //
        // POST: /PromoKody/Create

        //[HttpPost]
        //public ActionResult Create(PromoKodGrupa promokodgrupa)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.PromoKodGrupa.AddObject(promokodgrupa);
        //        db.SaveChanges();
        //        return PartialView("GridData", new PromoKodGrupa[] { promokodgrupa });
        //    }

        //    return PartialView("Edit", promokodgrupa);
        //}

        ////
        //// GET: /PromoKody/Edit/5

        //public ActionResult Edit(int id)
        //{
        //    PromoKodGrupa promokodgrupa = db.PromoKodGrupa.Single(p => p.PromoKodGrupaId == id);
        //    return PartialView(promokodgrupa);
        //}

        ////
        //// POST: /PromoKody/Edit/5

        //[HttpPost]
        //public ActionResult Edit(PromoKodGrupa promokodgrupa)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.PromoKodGrupa.Attach(promokodgrupa);
        //        db.ObjectStateManager.ChangeObjectState(promokodgrupa, EntityState.Modified);
        //        db.SaveChanges();
        //        return PartialView("GridData", new PromoKodGrupa[] { promokodgrupa });
        //    }

        //    return PartialView(promokodgrupa);
        //}

        ////
        //// POST: /PromoKody/Delete/5

        //[HttpPost]
        //public void Delete(int id)
        //{
        //    PromoKodGrupa promokodgrupa = db.PromoKodGrupa.Single(p => p.PromoKodGrupaId == id);
        //    db.PromoKodGrupa.DeleteObject(promokodgrupa);
        //    db.SaveChanges();
        //}

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}
