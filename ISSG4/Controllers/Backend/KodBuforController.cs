﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ISSG4.Models;
using System.IO;

namespace ISSG3.Controllers.Backend
{
    public class KodBuforGenViewModel
    {
        [Range(1, 10000, ErrorMessage="Parametr ilość musi mieścić się w granicach: 1-10000")]
        public int ilosc { get; set; }

        public string waznedo { get; set; }
        
    }

    [Authorize(Roles = "Administratorzy,Kolportaż")]
    public class KodBuforController : Controller
    {
        private Entities db = new Entities();

        //
        // GET: /KodBufory/

        public ViewResult Index(int start = 0, int itemsPerPage = 20, string orderBy = "KodBuforGrupaKiedy", bool desc = true)
        {
            ObjectContext objectContext = ((IObjectContextAdapter)db).ObjectContext;
            ViewBag.Count = objectContext.CreateObjectSet<KodBuforGrupa>("KodBuforGrupa").Count();
            ViewBag.Start = start;
            ViewBag.ItemsPerPage = itemsPerPage;
            ViewBag.OrderBy = orderBy;
            ViewBag.Desc = desc;

            //ViewBag.KodBuforTypList = new SelectList(db.KodBuforTyp.ToList(), "KodBuforTypId", "Nazwa");

            return View();
        }

        [HttpPost]
        public ViewResult Index(KodBuforGenViewModel gen, int start = 0, int itemsPerPage = 20, string orderBy = "KodBuforGrupaKiedy", bool desc = true)
        {
            ViewBag.Count = db.KodBuforGrupa.Count();
            ViewBag.Start = start;
            ViewBag.ItemsPerPage = itemsPerPage;
            ViewBag.OrderBy = orderBy;
            ViewBag.Desc = desc;

            //ViewBag.KodBuforTypList = new SelectList(db.KodBuforTyp.ToList(), "KodBuforTypId", "Nazwa");

            if (ModelState.IsValid) {
                DateTime waznedo;
                if (DateTime.TryParse(gen.waznedo, out waznedo)) {
                    db.genKodBufor((int)System.Web.Security.Membership.GetUser().ProviderUserKey, waznedo, gen.ilosc);
                }
            }

            return View();
        }

        //
        // GET: /KodBufory/GridData/?start=0&itemsPerPage=20&orderBy=KodBuforGrupaId&desc=true

        public ActionResult GridData(int start = 0, int itemsPerPage = 20, string orderBy = "KodBuforGrupaKiedy", bool desc = true)
        {
            Response.AppendHeader("X-Total-Row-Count", db.KodBuforGrupa.Count().ToString());

            ObjectContext objectContext = ((IObjectContextAdapter)db).ObjectContext;
            ObjectQuery<KodBuforGrupa> KodBuforgrupa = objectContext
                .CreateObjectSet<KodBuforGrupa>("KodBuforGrupa")
                .Include("Users");
            KodBuforgrupa = KodBuforgrupa.OrderBy("it." + orderBy + (desc ? " desc" : ""));
            //KodBuforgrupa = KodBuforgrupa.Include("i3_KodBuforTyp");

            return PartialView(KodBuforgrupa.Skip(start).Take(itemsPerPage));
        }


        public ActionResult Kod(int id)
        {
            var model = db.KodBufor.Where(m => m.KodBuforGrupaId == id).ToList();

            return View(model);
        }

        public ActionResult Pobierz(int id)
        {
            List<KodBufor> model = db.KodBufor.Where(m => m.KodBuforGrupaId == id).ToList();

            string name = "KodBufor"+id.ToString()+".txt";
            string temp_path = Path.GetTempPath();

            FileInfo info = new FileInfo(temp_path+name);
            if (!info.Exists)
            {
                using (StreamWriter writer = info.CreateText())
                {
                    foreach(var g in model)
                    {
                        string kod = g.Kod;
                        writer.WriteLine(kod);
                    }

                }
            }

            return File(temp_path + name, "text/plain",name);

        }

        //[HttpPost]
        //public ActionResult Kod(int id, List<KodBufor> kody)
        //{
        //    var model = db.KodBufor.Where(m => m.KodBuforGrupaId == id);
            
        //    foreach (var item in model)
        //    {
        //        var kod = kody.Where(m => m.KodBuforId == item.KodBuforId).DefaultIfEmpty().SingleOrDefault();
        //        if (kod != null) item.KodBuforWyslany = kod.KodBuforWyslany;
        //    }
        //    db.SaveChanges();

        //    return View(model.ToList());
        //}

       

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}
