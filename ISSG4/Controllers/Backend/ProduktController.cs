﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ISSG4.Models;

namespace ISSG3.Controllers.Backend
{
	[Authorize(Roles = "Administratorzy")]
    public class ProduktController : Controller
    {
        private Entities db = new Entities();

        //
        // GET: /Produkt/

        public ViewResult Index(int start = 0, int itemsPerPage = 20, string orderBy = "ProduktId", bool desc = false)
        {
            ObjectContext objectContext = ((IObjectContextAdapter)db).ObjectContext;

            ViewBag.Count = objectContext.CreateObjectSet<Produkt>("Produkt").Count();
            ViewBag.Start = start;
            ViewBag.ItemsPerPage = itemsPerPage;
            ViewBag.OrderBy = orderBy;
            ViewBag.Desc = desc;

            return View();
        }

        //
        // GET: /Produkt/GridData/?start=0&itemsPerPage=20&orderBy=ProduktId&desc=true

        public ActionResult GridData(int start = 0, int itemsPerPage = 20, string orderBy = "ProduktId", bool desc = false)
        {
            Response.AppendHeader("X-Total-Row-Count", db.Produkt.Count().ToString());

            ObjectContext objectContext = ((IObjectContextAdapter)db).ObjectContext;

            ObjectQuery<Produkt> produkt = objectContext.CreateObjectSet<Produkt>("Produkt")
                .Include("i3_RodzajOkresu")
                .Include("i3_ProduktRodzaj");
            produkt = produkt.OrderBy("it." + orderBy + (desc ? " desc" : ""));

            return PartialView(produkt.Skip(start).Take(itemsPerPage));
        }

        //
        // GET: /Default5/RowData/5

        public ActionResult RowData(int id)
        {
            Produkt produkt = db.Produkt.Single(p => p.ProduktId == id);
            return PartialView("GridData", new Produkt[] { produkt });
        }

        //
        // GET: /Produkt/Create

        public ActionResult Create()
        {
            ViewBag.RodzajOkresuId = new SelectList(db.RodzajOkresu, "RodzajOkresuId", "RodzajOkresuNazwa");
            ViewBag.ProduktRodzajId = new SelectList(db.ProduktRodzaj, "ProduktRodzajId", "ProduktRodzajNazwa");
            return PartialView("Edit");
        }

        //
        // POST: /Produkt/Create

        [HttpPost]
        public ActionResult Create(Produkt produkt)
        {
            if (ModelState.IsValid)
            {
                db.Produkt.Add(produkt);
                db.SaveChanges();
                return PartialView("GridData", new Produkt[] { produkt });
            }

            ViewBag.RodzajOkresuId = new SelectList(db.RodzajOkresu, "RodzajOkresuId", "RodzajOkresuNazwa", produkt.RodzajOkresuId);
            ViewBag.ProduktRodzajId = new SelectList(db.ProduktRodzaj, "ProduktRodzajId", "ProduktRodzajNazwa", produkt.ProduktRodzajId);
            return PartialView("Edit", produkt);
        }

        //
        // GET: /Produkt/Edit/5

        public ActionResult Edit(int id)
        {
            Produkt produkt = db.Produkt.Single(p => p.ProduktId == id);
            ViewBag.RodzajOkresuId = new SelectList(db.RodzajOkresu, "RodzajOkresuId", "RodzajOkresuNazwa", produkt.RodzajOkresuId);
            ViewBag.ProduktRodzajId = new SelectList(db.ProduktRodzaj, "ProduktRodzajId", "ProduktRodzajNazwa", produkt.ProduktRodzajId);
            return PartialView(produkt);
        }

        //
        // POST: /Produkt/Edit/5

        [HttpPost]
        public ActionResult Edit(Produkt produkt)
        {
            if (ModelState.IsValid)
            {
                db.Produkt.Attach(produkt);
                ObjectContext objectContext = ((IObjectContextAdapter)db).ObjectContext;
                objectContext.ObjectStateManager.ChangeObjectState(produkt, EntityState.Modified);
                db.SaveChanges();
                return PartialView("GridData", new Produkt[] { produkt });
            }

            ViewBag.RodzajOkresuId = new SelectList(db.RodzajOkresu, "RodzajOkresuId", "RodzajOkresuNazwa", produkt.RodzajOkresuId);
            ViewBag.ProduktRodzajId = new SelectList(db.ProduktRodzaj, "ProduktRodzajId", "ProduktRodzajNazwa", produkt.ProduktRodzajId);
            return PartialView(produkt);
        }

        //
        // POST: /Produkt/Delete/5

        [HttpPost]
        public void Delete(int id)
        {
            Produkt produkt = db.Produkt.Single(p => p.ProduktId == id);
            db.Produkt.Remove(produkt);
            db.SaveChanges();
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}
