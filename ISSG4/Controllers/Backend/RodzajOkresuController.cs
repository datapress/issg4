﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ISSG4.Models;

namespace ISSG3.Controllers.Backend
{
	[Authorize(Roles = "Administratorzy")]
    public class RodzajOkresuController : Controller
    {
        private Entities db = new Entities();

        //
        // GET: /RodzajOkresu/

        public ViewResult Index(int start = 0, int itemsPerPage = 20, string orderBy = "RodzajOkresuId", bool desc = false)
        {
            ObjectContext objectContext = ((IObjectContextAdapter)db).ObjectContext;

            ViewBag.Count = objectContext.CreateObjectSet<RodzajOkresu>("RodzajOkresu").Count();
            ViewBag.Start = start;
            ViewBag.ItemsPerPage = itemsPerPage;
            ViewBag.OrderBy = orderBy;
            ViewBag.Desc = desc;

            return View();
        }

        //
        // GET: /RodzajOkresu/GridData/?start=0&itemsPerPage=20&orderBy=RodzajOkresuId&desc=true

        public ActionResult GridData(int start = 0, int itemsPerPage = 20, string orderBy = "RodzajOkresuId", bool desc = false)
        {
            Response.AppendHeader("X-Total-Row-Count", db.RodzajOkresu.Count().ToString());

            ObjectContext objectContext = ((IObjectContextAdapter)db).ObjectContext;

            ObjectQuery<RodzajOkresu> rodzajokresu = objectContext.CreateObjectSet<RodzajOkresu>("RodzajOkresu");
            rodzajokresu = rodzajokresu.OrderBy("it." + orderBy + (desc ? " desc" : ""));

            return PartialView(rodzajokresu.Skip(start).Take(itemsPerPage));
        }

        //
        // GET: /Default5/RowData/5

        public ActionResult RowData(int id)
        {
            RodzajOkresu rodzajokresu = db.RodzajOkresu.Single(r => r.RodzajOkresuId == id);
            return PartialView("GridData", new RodzajOkresu[] { rodzajokresu });
        }

        //
        // GET: /RodzajOkresu/Create

        public ActionResult Create()
        {
            return PartialView("Edit");
        }

        //
        // POST: /RodzajOkresu/Create

        [HttpPost]
        public ActionResult Create(RodzajOkresu rodzajokresu)
        {
            if (ModelState.IsValid)
            {
                db.RodzajOkresu.Add(rodzajokresu);
                db.SaveChanges();
                return PartialView("GridData", new RodzajOkresu[] { rodzajokresu });
            }

            return PartialView("Edit", rodzajokresu);
        }

        //
        // GET: /RodzajOkresu/Edit/5

        public ActionResult Edit(int id)
        {
            RodzajOkresu rodzajokresu = db.RodzajOkresu.Single(r => r.RodzajOkresuId == id);
            return PartialView(rodzajokresu);
        }

        //
        // POST: /RodzajOkresu/Edit/5

        [HttpPost]
        public ActionResult Edit(RodzajOkresu rodzajokresu)
        {
            if (ModelState.IsValid)
            {
                db.RodzajOkresu.Attach(rodzajokresu);

                ObjectContext objectContext = ((IObjectContextAdapter)db).ObjectContext;

                objectContext.ObjectStateManager.ChangeObjectState(rodzajokresu, EntityState.Modified);
                db.SaveChanges();
                return PartialView("GridData", new RodzajOkresu[] { rodzajokresu });
            }

            return PartialView(rodzajokresu);
        }

        //
        // POST: /RodzajOkresu/Delete/5

        [HttpPost]
        public void Delete(int id)
        {
            RodzajOkresu rodzajokresu = db.RodzajOkresu.Single(r => r.RodzajOkresuId == id);
            db.RodzajOkresu.Remove(rodzajokresu);
            db.SaveChanges();
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}
