﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ISSG4.Models;
using System.Transactions;

namespace ISSG3.Controllers.Backend
{
    [Authorize(Roles = "Administratorzy,Kolportaż")]
    public class UserAutoController : Controller
    {
        private Entities db = new Entities();

        //
        // GET: /Zamowienie/

        public ViewResult Index(int start = 0, int itemsPerPage = 20, string orderBy = "lp", bool desc = false)
        {
            ViewBag.CountAll = db.issg_users_auto.Count();
            ViewBag.Start = start;
            ViewBag.ItemsPerPage = itemsPerPage;
            ViewBag.OrderBy = orderBy;
            ViewBag.Desc = desc;

            ViewBag.Count= db.issg_users_auto
                .Where(d=>d.login != null)
                .Count();
            

            return View();
        }

        public ActionResult GridData(int start = 0, int itemsPerPage = 20, string orderBy = "lp", bool desc = false, string login = "", string grupa = "")
        {

            ObjectContext objectContext = ((IObjectContextAdapter)db).ObjectContext;
            ObjectQuery<issg_users_auto> issg_users_auto = objectContext.CreateObjectSet<issg_users_auto>("issg_users_auto")
                .Where("it.login is not null");

            if (!string.IsNullOrEmpty(login))
                issg_users_auto = issg_users_auto.Where("it.login like '%" + login + "%'");

            if (!string.IsNullOrEmpty(grupa))
                issg_users_auto = issg_users_auto.Where("it.grupa == '" + grupa + "'");

            issg_users_auto = issg_users_auto.OrderBy("it." + orderBy + (desc ? " desc" : ""));

            Response.AppendHeader("X-Total-Row-Count", issg_users_auto.Count().ToString());

            ViewBag.Start = start;

            return PartialView(issg_users_auto.Skip(start).Take(itemsPerPage));
        }



        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}
