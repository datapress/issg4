﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ISSG4.Models;
using System.Transactions;

namespace ISSG3.Controllers.Backend
{
    [Authorize(Roles = "Administratorzy,Kolportaż")]
    public class KodController : Controller
    {
        private Entities db = new Entities();

        //
        // GET: /Kod/

        public ViewResult Index(int start = 0, int itemsPerPage = 20, string orderBy = "KodKod", bool desc = true)
        {
            ObjectContext objectContext = ((IObjectContextAdapter)db).ObjectContext;
            ViewBag.Count = objectContext.CreateObjectSet<Kod>("Kod").Count();
            ViewBag.Start = start;
            ViewBag.ItemsPerPage = itemsPerPage;
            ViewBag.OrderBy = orderBy;
            ViewBag.Desc = desc;

            return View();
        }

        //
        // GET: /Kod/GridData/?start=0&itemsPerPage=20&orderBy=KodCzas&desc=true

        public ActionResult GridData(DateTime? dateFrom, DateTime? dateTo, int start = 0, int itemsPerPage = 20, string orderBy = "KodCzas", bool desc = true, string kodkod="")
        {
            ObjectContext objectContext = ((IObjectContextAdapter)db).ObjectContext;
            ObjectQuery<Kod> kod = objectContext.CreateObjectSet<Kod>("Kod")
                .Include("i3_Wydanie");

            if (dateFrom.HasValue)
                kod = kod.Where("it.KodCzas >= @dataFrom", new ObjectParameter("dataFrom", dateFrom.Value));

            if (dateTo.HasValue)
                kod = kod.Where("it.KodCzas <= @dataTo", new ObjectParameter("dataTo", dateTo.Value.AddDays(1)));

            if (!string.IsNullOrEmpty(kodkod))
                kod = kod.Where("it.KodKod == '" + kodkod +"'");

            /* tylko opłacone */
            kod = kod.Where("it.KodStatus == '1'");

            kod = kod.OrderBy("it." + orderBy + (desc ? " desc" : ""));

            Response.AppendHeader("X-Total-Row-Count", kod.Count().ToString());

            return PartialView(kod.Skip(start).Take(itemsPerPage));
        }

        //
        // GET: /Default5/RowData/5

        public ActionResult RowData(string kodstring)
        {
            Kod kod = db.Kod.Include("i3_Wydanie").Single(k => k.KodKod == kodstring);
            return PartialView("GridData", new Kod[] { kod });
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}
