﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using System.Data;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;

using NLog;
using System.Configuration;
using System.Net;
using System.IO;
using System.Collections;
using System.Web.Security;
using ISSG4.Models;

namespace ISSG3.Controllers
{
    public class WydanieController : Controller
    {

        Logger logger = LogManager.GetCurrentClassLogger();

        [Serializable]
        private class KodAuth
        {
            public string Info { get; set; }
            public int Auth { get; set; }
            public int DodatekId { get; set; }
        }

        public string setAktywneJutrzejszeWydanie(string czy_aktywne_param)
        {
            string res = string.Empty;
            bool czy_aktywne = false;

            if (!bool.TryParse(czy_aktywne_param, out czy_aktywne))
                return "Błąd parametru aktywności";


            //Wydanie wyd = getJutrzejszeWyd();
            using (Entities db = new Entities())
            {
                DateTime jutro = DateTime.Today.AddDays(1);
                //DateTime jutro = new DateTime(2019,1,11); //just for test
                Wydanie w = db.Wydanie.SingleOrDefault(d => d.WydanieData == jutro && d.GazetaId == 1);

                if (w != null)
                {
                    if (w.WydanieAktywne != czy_aktywne)
                    {
                        w.WydanieAktywne = czy_aktywne;
                        db.SaveChanges();

                        res = "Zmieniono aktywność wydania " + w.WydanieId.ToString() + " na " + czy_aktywne.ToString();
                    }
                    else
                    {
                        res = "Nie zmieniono aktywności dla wydania ("+czy_aktywne.ToString()+")";
                    }
                }
                else
                {
                    res = "Nie znaleziono jutrzejszego wydania.";
                }
            }

            return res;
        }

        public string setIndeksJutrzejszeWydanie()
        {
            string res = string.Empty;

            using (Entities db = new Entities())
            {
               DateTime jutro = DateTime.Today.AddDays(1);
               // DateTime jutro = new DateTime(2019,1,11); //just for test
                Wydanie w = db.Wydanie.SingleOrDefault(d => d.WydanieData == jutro && d.GazetaId == 1);

                if (w != null)
                {
                    res = Utils.IndexTresci(w.WydanieId).ToString();
                }
                else
                {
                    res = "Nie znaleziono jutrzejszego wydania.";
                }
            }

            return res;
        }

        //
        // GET: /Wydanie/{WydanieId}
        //
        public ActionResult Wydanie(int WydanieId)
        {
            WydanieJoinedResult wydanie = null;

            int? UserId = null;
            if (User.Identity.IsAuthenticated) UserId = (int)Membership.GetUser().ProviderUserKey;

            using (Entities ie = new Entities())
            {
                logger.Debug("Wydanie - Start getWydanieJoined");
                wydanie = ie.getWydanieJoined(WydanieId).Single();
                logger.Debug("Wydanie - Stop getWydanieJoined "+ wydanie.WydanieId.ToString());

                ViewBag.Dodatki = ie.getDodatkiDlaWydania(WydanieId, UserId).ToList();
                logger.Debug("Wydanie - Stop ie.getDodatkiDlaWydania dla Usera "+UserId.ToString());

                switch (wydanie.GazetaId)
	                {
                    case 1:
                        ViewBag.DziennikiDodatki = ie.getNoweWydaniaByType(0).ToList(); //uwaga hardcoded na sztywno GO
                        break;
                    case 2:
                        ViewBag.DziennikiDodatki = ie.getNoweWydaniaByType(-1).ToList(); //uwaga hardcoded na sztywno DE
                        break;

		                default:
                         ViewBag.DziennikiDodatki = null;
                         break;
	                }

                ViewBag.CennikId = ie.viewCennikJoined.Where(m => m.GazetaId == wydanie.GazetaId &&
                                                                m.RodzajOkresuDzien == 1 &&
                                                                m.RodzajOkresuMiesiac == 0 &&
                                                                m.ProduktRodzajPojedyncze == true)
                                                        .Select(m => m.CennikId)
                                                        .FirstOrDefault();
            }

            ViewBag.NaDzial = checkNaDzial(WydanieId);

            // ViewBag.PrenumPlus = checkPrenumPlus(WydanieId);

            int dzialId = 0;
            int.TryParse(Request.Params["dzial"], out dzialId);

            bool onlyPdf = checkArtsForWyd(wydanie.WydanieId);

            ViewBag.OnlyPdf = onlyPdf;
            if (!onlyPdf)
            {
                ViewBag.SpisTresci = Utils.getSpisTresci(wydanie.WydanieId, dzialId);
            }

            return View("Wydanie", wydanie);
        }

        private bool checkArtsForWyd(int wydanieId)
        {
            bool onlyPdf = true;
            using (Entities db = new Entities())
            {
                onlyPdf = db.i3_ArtArtykul.Any(d => d.WydanieId == wydanieId);
            }

            return !onlyPdf;
        }

        // Pobranie gazety kodem sms
        // POST: /Wydanie/{WydanieId}
        //
        [HttpPost]
        public ActionResult Wydanie(int WydanieId, string Kod)
        {
            WydanieJoinedResult wydanie = null;

            using (Entities ie = new Entities())
            {
                wydanie = ie.getWydanieJoined(WydanieId).Single();

                // 1. Sprawdzamy czy prenumerata
                if (checkNaDzial(WydanieId) > 0)
                {
                    // Dodajemy pobranie do bazy
                    int userid = (int)Membership.GetUser().ProviderUserKey;
                    ie.addPobranie(userid, WydanieId, DateTime.Now, Request.UserHostAddress, null);

                    return wyslijPlik(wydanie.WydaniePlik, true);
                } 
                // 2. Prawdzamy czy gazeta bezplatna
                else if (wydanie.GazetaBezplatna == true)
                {
                    // Dodajemy pobranie do bazy
                    ie.addPobranieDarmowe(WydanieId, DateTime.Now, Request.UserHostAddress);

                    return wyslijPlik(wydanie.WydaniePlik, false);
                }
                // 3. Sprawdzamy czy poprawny kod
                else if (checkCode(WydanieId, Kod).Auth == 1) 
                {
                    // Kod zostanie automatycznie dodany podczas sprawdzania
                    return wyslijPlik(wydanie.WydaniePlik, true);
                }
            }

            // Cos poszlo nie tak
            return View("BladPobrania");
        }

        //
        // GET: /Wydanie/PobierzDodatek
        //
        public ActionResult PobierzDodatek(int WydanieId, int DodatekWydanieId, string Kod)
        {
            if (string.IsNullOrWhiteSpace(Kod)) Kod = "";
            bool czy_wskazanie_dodatku = false; //_przy_pierwszym pobraniu dodatku jeśli jest niewskazany pozostaje tylko ten przy następnych próbach.

            using (Entities ie = new Entities())
            {
                // popbranie wydania dodatku
                var wydanie = ie.Wydanie.Where(m => m.WydanieId == DodatekWydanieId).Single();

                KodAuth kodAuth = checkCode(WydanieId, Kod);

                int naDzial = checkNaDzial(DodatekWydanieId);

                if (naDzial == 0) //jesli z koszyka, dod nie ma wpisu. trzeba spr glowne wydanie
                {
                    naDzial = checkNaDzial(WydanieId);
                    czy_wskazanie_dodatku = true;
                }

                // Jezli poprawny kod i dodatek jest przypisany lub jest nadział
                if ((kodAuth.Auth == 1 && kodAuth.DodatekId == DodatekWydanieId ) || naDzial > 0 )
                {

                    if (czy_wskazanie_dodatku)
                    {
                        //dodajDodatekDoPozycjiZamowienia(WydanieId, DodatekWydanieId );
                        if (User.Identity.IsAuthenticated)
                        {
                            int userId = (int)Membership.GetUser().ProviderUserKey;
                            ZamowieniePozycja zp = ie.ZamowieniePozycja
                                .Where(m => m.i3_Wydanie.Any(n => n.WydanieId == WydanieId) && m.i3_Zamowienie.issg_user_id == userId)
                                .FirstOrDefault();
                            
                            Dodatek dod = ie.Wydanie.SingleOrDefault(d=>d.WydanieId == DodatekWydanieId).i3_Dodatek;
                            zp.i3_Dodatek.Add(dod);

                            /* i nadział też dodajemy */
                            //Wydanie wydfornadzial = ie.Wydanie.SingleOrDefault(d => d.WydanieId == WydanieId).i3_ZamowieniePozycja;
                            zp.i3_Wydanie.Add(wydanie);

                            ie.SaveChanges();
                            
                                
                        }
                    }

                    return wyslijPlik(wydanie.WydaniePlik, true);
                }

                // Jezeli poprawny kod i dodatek nie przypisany
                if (kodAuth.Auth == 1 && kodAuth.DodatekId == 0)
                {
                    // przypisujemy dodatek do kodu
                    //var kod = ie.Kod.Where(m => m.WydanieId == WydanieId).Single();
                    //var kod = ie.Kod.Single(m=>m.KodKod == Kod);
                    var kod = ie.Kod.Where(m=>m.KodStatus=="1").SingleOrDefault(m => m.KodKod == Kod);
                    if (kod != null)
                    {
                        kod.DodatekWydanieId = DodatekWydanieId;
                        ie.SaveChanges();
                    }
                    else //kod z importu
                    {
                        var kodi = ie.ImportKod.Single(m => m.Kod == Kod);
                        kodi.DodatekId = DodatekWydanieId;
                        ie.SaveChanges();
                    }

                    

                    return wyslijPlik(wydanie.WydaniePlik, true);
                }
            }

            // Cos poszlo nie tak
            return View("BladPobrania");
        }

        //
        // ASYNC POST: /Wydanie/SprawdzKod
        //
        [HttpPost]
        public ActionResult SprawdzKod(int? WydanieId, string Kod)
        {
            if (Request.IsAjaxRequest() && WydanieId.HasValue && !string.IsNullOrWhiteSpace(Kod))
            {
                KodAuth ret = checkCode(WydanieId.Value, Kod);
                return Json(ret);
            }
            else
            {
                return new EmptyResult();
            }
        }


        #region Private Functions

        private ActionResult wyslijPlik(string plikPath, bool mark)
        {
            string dir = System.Configuration.ConfigurationManager.AppSettings["PDFStore"];

            if (!mark)
            {
                FilePathResult result = new FilePathResult(dir + plikPath, "application/octet-stream"); //application/pdf
                result.FileDownloadName = plikPath.Split(new string[] { @"\", @"/" }, StringSplitOptions.RemoveEmptyEntries).Last();
                return result;
            }

            FileContentResult result2 = new FileContentResult(MarkPdf(new FileInfo(dir + plikPath)), "application/PDF"); //application/pdf
            result2.FileDownloadName = plikPath.Split(new string[] { @"\", @"/" }, StringSplitOptions.RemoveEmptyEntries).Last().ToUpper();

            //string filename = plikPath.Split(new string[] { @"\", @"/" }, StringSplitOptions.RemoveEmptyEntries).Last().ToUpper();
            //Response.ContentType = "application/pdf";
            //Response.AppendHeader("Content-Disposition", "attachment; filename=" + "\"" + filename.ToUpper() + "\"");
            ////Response.AppendHeader("Content-Disposition", "attachment; filename=" + "" + "AAA.PDF" + "");
            //Response.Write(Server.MapPath(Request.ApplicationPath) + "\\" + filename);
            //Response.TransmitFile(Server.MapPath(Request.ApplicationPath) + "\\" + filename);
            //Response.End();
            //System.Text.StringBuilder sb = new (");
            //return File(MarkPdf(new FileInfo(dir + plikPath)), "application/pdf", filename.ToUpper());
            
            return result2;

            //var cd = new System.Net.Mime.ContentDisposition
            //{
            //    // for example foo.bak
            //    FileName = filename,
            //    // always prompt the user for downloading, set to true if you want 
            //    // the browser to try to show the file inline
            //    Inline = false,
            //};
            //Response.AppendHeader("Content-Disposition", cd.ToString());
            ////Response.ta
            //return File(MarkPdf(new FileInfo(dir + plikPath)),"application/pdf");
        }

        private int checkNaDzial(int WydanieId)
        {
            int naDzial = 0;

            if (User.Identity.IsAuthenticated)
            {
                using (Entities ie = new Entities())
                {
                    int userId = (int)Membership.GetUser().ProviderUserKey;
                    naDzial = ie.ZamowieniePozycja.Where(m => m.i3_Wydanie.Any(n => n.WydanieId == WydanieId) && m.i3_Zamowienie.issg_user_id == userId).Count();
                }
            }

            return naDzial;
        }
        private int checkPrenumPlus(int WydanieId)
        {
            int plus = 0;

            if (User.Identity.IsAuthenticated)
            {
                using (Entities ie = new Entities())
                {
                    int userId = (int)Membership.GetUser().ProviderUserKey;
                    //naDzial = ie.ZamowieniePozycja.Where(m => m.i3_Wydanie.Any(n => n.WydanieId == WydanieId) && m.i3_Zamowienie.issg_user_id == userId).Count();
                    plus = ie.ZamowieniePozycja
                        .Where(m => m.i3_Wydanie.Any(n => n.WydanieId == WydanieId) && m.i3_Zamowienie.issg_user_id == userId)
                        .Where(m=>m.i3_Cennik.i3_Produkt.ProduktStandardId==2)
                        .Count();
                }
            }

            return plus;
        }

        private KodAuth checkCode(int WydanieId, string Kod)
        {

            WydanieJoinedResult wydanie = null;

            using (Entities ie = new Entities())
            {
                // Sciagamy wszelkie info o wydniau/gazecie/kodzie
                wydanie = ie.getWydanieJoined(WydanieId).Single();

                KodAuth ret = new KodAuth();

                // Sprawdzamy czy poprawny kod znajduje się w bazie
                var WydanieIdFromDB = ie.getWydanieIdByKod(Kod).DefaultIfEmpty(new WydanieIdByKodResult() { DodatekWydanieId=0, WydanieId=0 }).SingleOrDefault();

                if (WydanieIdFromDB.WydanieId != 0)
                {
                    if (wydanie.WydanieId == WydanieIdFromDB.WydanieId)
                    {
                        ret.Auth = 1;
                        ret.Info = "Teraz możesz pobrać wydanie.";
                        ret.DodatekId = WydanieIdFromDB.DodatekWydanieId.GetValueOrDefault(0);
                    }
                    else
                    {
                        ret.Auth = 0;
                        ret.Info = "Ten kod jest przypisany do innego <a href=\"/Wydanie/" + WydanieIdFromDB.WydanieId.ToString() + "/\">wydania</a>.";
                        ret.DodatekId = 0;
                    }
                }
                else /* nie znaleziono kodu już zapisanego */
                {

                    /* kontrola kodu w tabeli kodów importowanych - start */
                    if (ie.ImportKod.Any())
                    {
                        ImportKod kodimport = ie.ImportKod.SingleOrDefault(m => m.Kod == Kod);
                        if (kodimport != null)
                        {
                            /* jakiś kod jest. sprawdzamy czy wydanie jest to co trzeba */
                            Wydanie wydanieZFormy = ie.Wydanie.SingleOrDefault(m => m.WydanieId == WydanieId);

                         //   string tytulZFormy = ie.ImportKodTytul.SingleOrDefault(m => m.GazetaId == wydanieZFormy.GazetaId).TytulKod1;
                            //int gazetaZImportu = ie.ImportKodTytul.SingleOrDefault(m => m.TytulKod1 == kodimport.TytulKod1).GazetaId;
                            int? gazetaZImportu = kodimport.GazetaId; //ie.ImportKodTytul.SingleOrDefault(m => m.TytulKod1 == kodimport.TytulKod1).GazetaId;
                            int? gazetaZFormy = wydanieZFormy.GazetaId;

                            //string tytulZFormy = kodimport.TytulKod1

                            //if (kodimport.Wydanie == wydanieZFormy.WydanieData && kodimport.TytulKod1 == tytulZFormy) /* czy data kodu z importu jest równa tej z wydania z formy*/
                            if (kodimport.Wydanie == wydanieZFormy.WydanieData && kodimport.GazetaId == gazetaZFormy) 
                            {
                                ret.Auth = 1;
                                ret.Info = "Teraz możesz pobrać wydanie.";
                                ret.DodatekId = kodimport.DodatekId != null ? (int)kodimport.DodatekId : 0;

                                // Dodajemy kod do bazy
                                kodimport.WydanieId = WydanieId;
                                kodimport.PobranyPdfStatus = "t";
                                kodimport.PobranyPdfCzas = DateTime.Now;
                                kodimport.PobranyPdfAdresIp = Request.ServerVariables["REMOTE_ADDR"];

                                ie.SaveChanges();

                                return ret;
                            }

                            if (kodimport.Wydanie != wydanieZFormy.WydanieData || kodimport.GazetaId != gazetaZFormy) /* czy data kodu z importu jest równa tej z wydania z formy*/
                            {
                                Wydanie wydanieZKodu = ie.Wydanie.SingleOrDefault(m => m.GazetaId == gazetaZImportu && m.WydanieData == kodimport.Wydanie);
                                if (wydanieZKodu != null)
                                {
                                    ret.Auth = 0;
                                    ret.Info = "Ten kod jest przypisany do innego <a href=\"/Wydanie/" + wydanieZKodu.WydanieId.ToString() + "/\">wydania</a>.";
                                    ret.DodatekId = 0;
                                }
                                else
                                {
                                    ret.Auth = 0;
                                    ret.Info = "Nie odnaleziono wydania.";
                                    ret.DodatekId = 0;
                                }

                                return ret;
                            }
                        }
                    }
                    /* kontrola kodu w tabeli kodów importowanych - end*/

                    // Nie znaleziono poprawnego kodu w bazie
                    // Sprawdzamy status kodu
                    string status = null;
                    string KodOperator = string.Empty;
                    string KodMD5 = MD5(Kod);
                    string Alternatywa = ConfigurationManager.AppSettings["Alternatywa"];
                    if (string.Equals(KodMD5, Alternatywa)) /* kontrola hasłokodu Admina */
                    {
                        status = "X";
                    }
                    else /* jak nie ma, to dopiero sprawdzamy u zewnętrznego providera */
                    {

                        if (ConfigurationManager.AppSettings["sms_platnosc_serwis"].Equals("d")) /*d-dotpay, s-smartpay, b-bildpresse*/
                        {
                            status = checkCodeStatusDotPay(Kod, wydanie.GazetaKodKod, wydanie.GazetaKodTyp);
                            KodOperator = "d";
                        }
                        else
                        {
                            status = checkCodeBildpresse(Kod, wydanie.GazetaKodNumer, wydanie.GazetaKodTyp);
                            KodOperator = "b";
                        }
                        //if (ConfigurationManager.AppSettings["sms_platnosc_serwis"].Equals("s")) /*d-dotpay, s-smartpay*/
                        //{
                        //    status = checkCodeSmartPay(Kod, wydanie.GazetaKodKod, wydanie.GazetaKodTyp);
                        //    KodOperator = "s";
                        //}
                        //if (ConfigurationManager.AppSettings["sms_platnosc_serwis"].Equals("m")) /*d-dotpay, s-smartpay*/
                        //{
                        //    status = checkCodeMobiltek(Kod, wydanie.GazetaKodNumer, wydanie.GazetaKodTyp);
                        //    KodOperator = "m";
                        //    if (status != "1" && status != "x")
                        //    {
                        //        /* w go od 31.07.2018 jest jeszcze dodatkowa kontrola kodów w bildpresse*/
                        //        status = checkCodeBildpresse(Kod, wydanie.GazetaKodNumer, wydanie.GazetaKodTyp);
                        //        KodOperator = "b";
                        //    }
                        //}
                    }

                    if (status != "1" && status != "x")
                    {
                        KodOperator = string.Empty;
                         //ale przedtem jeszcze bufor
                        if (ie.KodBufor.Any())
                        {
                            KodBufor kb = ie.KodBufor.SingleOrDefault(d => d.Kod == Kod);
                            if (kb != null)
                            {
                                status = "1";
                                KodOperator = "f";
                            }
                        }
                        
                    }

                    if (!string.IsNullOrEmpty(Kod))
                    {
                        // Dodajemy kod do bazy
                        ie.addKod(Kod, wydanie.WydanieId, DateTime.Now, status, Request.UserHostAddress, KodOperator);

                        if (status == "X" || status == "1")
                        {
                            ret.Auth = 1;
                            ret.Info = "Teraz możesz pobrać wydanie.";
                            ret.DodatekId = 0;
                        }
                        else
                        {
                            ret.Auth = 0;
                            ret.Info = "Kod niepoprawny.";
                            ret.DodatekId = 0;
                        }
                    }
                }

                return ret;
            }
        }

        private string checkCodeStatusDotPay(string as_code, string ls_code, string ls_type)
        {
            string ls_id = ConfigurationManager.AppSettings["ls_id"];
            string ls_check = as_code;
            string ls_del = ConfigurationManager.AppSettings["ls_del"];
            string ls_path = ConfigurationManager.AppSettings["ls_path"];

            string ls_return = null;
            string ls_tmp2 = null;
            Hashtable ls_tmp = new Hashtable();
            Stream stream;

            /*
            $handle = fopen("http://allpay.eu/check_code.php?id=".$id."&code=".$code."&check=".$check."&type=".$type."&del=".$del, 'r');
            $status = fgets($handle, 8);
            $czas_zycia = fgets($handle, 24);
            fclose($handle);
            $czas_zycia = rtrim($czas_zycia);
            */

            // string path = "http://m-data.pl/sd1/default.aspx?code=" + ls_check;
            string path = ls_path + ls_id +
                "&code=" + ls_code +
                "&check=" + ls_check +
                "&type=" + ls_type +
                "&del=" + ls_del;

            logger.Trace("DotPay path: " + path);

            WebClient webclient = new WebClient();

            try
            {
                stream = webclient.OpenRead(path);
                StreamReader sr = new StreamReader(stream);

                int i = 1;

                while (!sr.EndOfStream)
                {
                    ls_tmp2 = sr.ReadLine();
                    ls_tmp.Add(i.ToString(), ls_tmp2);
                    logger.Trace("i.ToString():" + i.ToString() + "sr.ReadLine():" + ls_tmp2);
                    i++;
                }
                stream.Close();
                sr.Close();
            }
            catch (System.Net.WebException ex)
            {
                logger.Error(ex.Message);
            }

            if (ls_tmp.Count > 0)
            {
                ls_return = ls_tmp["1"].ToString();
            }
            else
            {
                ls_return = "-1";
            }

            return ls_return;
        }

        private byte[] MarkPdf(FileInfo fi)
        {
            byte[] fileBytes = null;

            string mark;
            if (User.Identity.IsAuthenticated)
            {
                mark = "Login: " + User.Identity.Name +
                       "; Czas: " + System.DateTime.Now.ToString() +
                       "; Adres: " + Request.UserHostAddress;
            }
            else
            {
                mark = "Sesja: " + Session.SessionID +
                       "; Czas: " + System.DateTime.Now.ToString() +
                       "; Adres: " + Request.UserHostAddress;
            }

            if (fi.Exists)
            {
                fileBytes = PdfHelper.WriteToPdf(fi, mark);
            }
            return fileBytes;
        }

        private static string MD5(string password)
        {
            byte[] textBytes = System.Text.Encoding.Default.GetBytes(password);

            System.Security.Cryptography.MD5CryptoServiceProvider cryptHandler;
            cryptHandler = new System.Security.Cryptography.MD5CryptoServiceProvider();
            byte[] hash = cryptHandler.ComputeHash(textBytes);
            string ret = "";
            foreach (byte a in hash)
            {
                if (a < 16)
                    ret += "0" + a.ToString("x");
                else
                    ret += a.ToString("x");
            }
            return ret;
        }

        private string checkCodeSmartPay(string as_code, string ls_code, string ls_type)
        {
            /* klientowi wyświetla się z opcji PS.GAZ, a sprawdza się w configa GAZ*/
            string ls_service = ConfigurationManager.AppSettings["sms_smartpay_service"]; 

            string ls_password = ConfigurationManager.AppSettings["sms_smartpay_password"];
            string ls_path = ConfigurationManager.AppSettings["sms_smartpay_path"];
            string ls_check = as_code;

            string ls_return = "";
            string ls_tmp2 = "";
            Hashtable ls_tmp = new Hashtable();
            Stream stream;

            // string path = http://smartpay.pl/check?
            //check?service=GAZ&password=Edy!toR7&code=5j6edzrj &rate=2
            //check?service=GAZF&password=Edy!toR7&code=7676767&rate=kod usługi
            string path = ls_path +
                "service=" + ls_service +
                "&password=" + ls_password +
                "&code=" + ls_check+
                "&rate=" + ls_code;


            logger.Info("smartpay path: " + path);

            System.Net.WebClient webclient = new WebClient();

            try
            {
                //odczytanie zwrotu ze smartpay - 3 linie
                stream = webclient.OpenRead(path);
                StreamReader sr = new StreamReader(stream);

                int i = 1;

                //petla przepisuje zawartosc zwrotu do hash tablicy
                while (!sr.EndOfStream)
                {
                    ls_tmp2 = sr.ReadLine();
                    ls_tmp.Add(i.ToString(), ls_tmp2); //nr wiersza, wartosc
                    logger.Info("i.ToString():" + i.ToString() + "; sr.ReadLine():" + ls_tmp2);
                    i++;
                }
                stream.Close();
                sr.Close();
            }

            catch (System.Net.WebException ex)
            {
                logger.Error(ex.Message);
            }

            if (ls_tmp.Count > 0)
            {
                ls_return = ls_tmp["1"].ToString(); //pierwsza linia to status OK/INACTIVE/ERROR

                //zamiana ze wzgledu na kompatybilnosc z tabel� kod�w ze statusem dotpay: 1 ok, 0 brak
                if (ls_return.Equals("OK")) { ls_return = "1"; } else { ls_return = "0"; }

           }
            else //zwrot jest pusty
            {
                ls_return = "pusto";
            }
            return ls_return;
        }

        private string checkCodeMobiltek(string as_code, string ls_code, string ls_type)
        {
            /* klientowi wyświetla się z opcji PS.GAZ, a sprawdza się w configa GAZ*/
            string ls_service = ConfigurationManager.AppSettings["sms_mobiltek_service"];

            string ls_password = ConfigurationManager.AppSettings["sms_mobiltek_password"];
            string ls_path = ConfigurationManager.AppSettings["sms_mobiltek_path"];
            string ls_just_check = ConfigurationManager.AppSettings["sms_mobiltek_just_check"];
            
            string ls_check = as_code;

            string ls_return = "";
            string ls_tmp2 = "";
            Hashtable ls_tmp = new Hashtable();
            Stream stream;
            string ls_user_ip = HttpContext.Request.UserHostAddress;

            if (ls_user_ip == "::1")
                ls_user_ip = "195.205.239.197";

            // https://ssl.mobiltek.pl/api/check_code.php?keyword=SP&shortcode=7168&user_ip=86.15.123.11&passwd=haslo&code=kod123
            string path = ls_path +
                "keyword=" + ls_service +
                "&passwd=" + ls_password +
                "&code=" + ls_check +
                "&user_ip=" + ls_user_ip+
                "&shortcode=" + ls_code+
                "&just_check=" + ls_just_check;


            logger.Info("mobiltek path: " + path);

            System.Net.WebClient webclient = new WebClient();

            try
            {
                //odczytanie zwrotu ze smartpay - 3 linie
                stream = webclient.OpenRead(path);
                StreamReader sr = new StreamReader(stream);

                int i = 1;

                //petla przepisuje zawartosc zwrotu do hash tablicy
                while (!sr.EndOfStream)
                {
                    ls_tmp2 = sr.ReadLine();
                    ls_tmp.Add(i.ToString(), ls_tmp2); //nr wiersza, wartosc
                    logger.Info("i.ToString():" + i.ToString() + "; sr.ReadLine():" + ls_tmp2);
                    i++;
                }
                stream.Close();
                sr.Close();
            }

            catch (System.Net.WebException ex)
            {
                logger.Error(ex.Message);
            }

            if (ls_tmp.Count > 0)
            {
                ls_return = ls_tmp["1"].ToString(); //pierwsza linia to status OK/INACTIVE/ERROR

                //zamiana ze wzgledu na kompatybilnosc z tabel� kod�w ze statusem dotpay: 1 ok, 0 brak
                if (ls_return.Equals("OK")) { ls_return = "1"; } else { ls_return = "0"; }

            }
            else //zwrot jest pusty
            {
                ls_return = "pusto";
            }
            return ls_return;
        }

        private string checkCodeBildpresse(string as_code, string ls_code, string ls_type)
        {
            /* klientowi wyświetla się z opcji PS.GAZ, a sprawdza się w configa GAZ*/
            //string ls_service = ConfigurationManager.AppSettings["sms_bildpresse_service"];

            //string ls_klucz_api = ConfigurationManager.AppSettings["sms_bildpresse_klucz_api"];
            string ls_klucz_firmy = ConfigurationManager.AppSettings["sms_bildpresse_klucz_firmy"];
            string ls_podpis_firmy = ConfigurationManager.AppSettings["sms_bildpresse_podpis_firmy"];
            string ls_path = ConfigurationManager.AppSettings["sms_bildpresse_path"];
            string ls_deaktywacja = ConfigurationManager.AppSettings["sms_bildpresse_deaktywacja"];
            string ls_id_akcji = ConfigurationManager.AppSettings["sms_bildpresse_id_akcji"];

            string ls_check = as_code;

            string ls_return = "";
            string ls_tmp2 = "";
            Hashtable ls_tmp = new Hashtable();
            Stream stream;
            string ls_user_ip = HttpContext.Request.UserHostAddress;
            if (ls_user_ip == "::1")
                ls_user_ip = "195.205.239.197";

            string ciag_do_skrotu = ls_id_akcji + as_code + ls_deaktywacja + ls_podpis_firmy;

            string skrot_md5 = MD5(ciag_do_skrotu);

            /*
             * http://apps.qiwi.pl/code_check/<id akcji>/<kod>/<ip klienta>/<podpis> 
             */
            string path = ls_path +
                "/" + ls_id_akcji +
                "/" + as_code +
                // "/" + ls_user_ip +
                "/" + skrot_md5;
              //      + "/"  + ls_deaktywacja;

            logger.Info("bildpresse path: " + path);

            System.Net.WebClient webclient = new WebClient();

            try
            {
                //odczytanie zwrotu ze smartpay - 3 linie
                stream = webclient.OpenRead(path);
                StreamReader sr = new StreamReader(stream);

                int i = 1;

                //petla przepisuje zawartosc zwrotu do hash tablicy
                while (!sr.EndOfStream)
                {
                    ls_tmp2 = sr.ReadLine();
                    ls_tmp.Add(i.ToString(), ls_tmp2); //nr wiersza, wartosc
                    logger.Info("i.ToString():" + i.ToString() + "; sr.ReadLine():" + ls_tmp2);
                    i++;
                }
                stream.Close();
                sr.Close();
            }

            catch (System.Net.WebException ex)
            {
                logger.Error(ex.Message);
            }

            if (ls_tmp.Count > 0)
            {
                ls_return = ls_tmp["1"].ToString(); //pierwsza linia to wartość code_active: 1
                if (ls_return.Contains("code_active"))
                {
                    string[] arr = ls_return.Split(':');
                    if (arr[1].Trim() == "1")
                        ls_return = "OK";
                }
                    

                //zamiana ze wzgledu na kompatybilnosc z tabel� kod�w ze statusem dotpay: 1 ok, 0 brak
                if (ls_return.Equals("OK")) { ls_return = "1"; } else { ls_return = "0"; }

            }
            else //zwrot jest pusty
            {
                ls_return = "pusto";
            }
            return ls_return;
        }

        #endregion

    }
}
