﻿/// <reference path="jquery-1.7.1-vsdoc.js" />
/// <reference path="modernizr-2.0.6-development-only.js" />
/// <reference path="jquery-ui-1.8.18.js" />
/// <reference path="jquery.fancybox-1.3.4.js" />

function ajax(url, dane) {
  
    if (typeof dane === "undefined") dane = {};
  
    console.log(dane);
    return $.ajax({ url: url, dataType: 'json', data: dane, type: "POST" });
  
}

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regexS = "[\\?&]" + name + "=([^&#]*)";
    var regex = new RegExp(regexS);
    var results = regex.exec(window.location.search);
    if (results == null)
        return "";
    else
        return decodeURIComponent(results[1].replace(/\+/g, " "));
}

$(function () {

    wireUpAutoSubmit();

    $("body").delegate(".closeFancybox", "click", function (e) {
        $.fancybox.close();
        e.preventDefault();
    });

    $('form').find('input').bind('keyup blur change', function () {
        var _summary = $('form *[data-valmsg-summary=true]');
        if ($("form").valid()) {
            //if ($(this).valid()) {
            _summary.hide();
        }
        else {
            _summary.show();
        }
    });

    $(".wydanie a#jedynka").fancybox({
        'hideOnContentClick': true
    });

    $(".wydanie .dodatki a.galeria").attr('rel', 'gallery').fancybox();

    $('#Kod').keypress(function (e) {
        if (e.which == 13) {
            sprawdzKod();
            e.preventDefault();
        }
    });

    $("#SubmitKod").click(function (e) {
        e.preventDefault();
        sprawdzKod();
    });

    $("#Zaplac").click(function (e) {
        var url = "/Koszyk/Blokada2";
       
        ajax(url).done(function (data) {
            console.log("ajax po");
            console.log(data);
            if (data.error.length > 0) {
                alert(data.error);
            } else {
                /* zamiast submita formy w T2 trzeba GETem się odwołać. chyba jednak nie */
                    $("#Zaplac").after(data.forma); 
                    $("#FormaDotPay").submit();
             }
        }).fail(function (data) {
            alert(data);
        });
        e.preventDefault();
    });

    $("#artZapisz").click(function (e) {

        var artId = $("#ArtArtykulId").val();
        var dzialId = $("#ArtDzialId").val();

        var url = "/Art/Edit/";

        ajax(url, { artId: artId, dzialId: dzialId })
            .done(function (data) {

                alert(data.status);

            }).fail(function (data) {
                alert("Błąd: "+data.opis);

        });

        e.preventDefault();
    });


    $(".nowaPrenumerata #GazetaId").change(function () {
        ajax("/Prenumerata/GetProdukt", { id: $(this).val() }).done(function (data) {
            $(".nowaPrenumerata #Od").removeAttr("disable");
            var select = $(".nowaPrenumerata #ProduktId").html("");
            $.each(data.Produkty, function (i, item) {
                select.append('<option value="' + item.ProduktId + '">' + item.ProduktNazwa + '</option>');
            });
            var select2 = $(".nowaPrenumerata #DodatekId").html("");
            $.each(data.Dodatki, function (i, item) {
                select2.append('<option value="' + item.DodatekId + '">' + item.DodatekNazwa + '</option>');
            });
        });
        $(".nowaPrenumerata #DodatekId").prop('disabled', '');
        $(".nowaPrenumerata .plus_info_dodatki").remove();
    });

    $(".nowaPrenumerata #ProduktId").change(function () {
        var thisvalue = $(this).find("option:selected").text();
        var dodsy = $(".nowaPrenumerata #DodatekId");
        if (thisvalue.indexOf('Plus') > -1) {
            $(dodsy).prop('disabled', 'disabled');
            $(".nowaPrenumerata .plus_info_dodatki").remove();
            $(dodsy).after("<span class='plus_info_dodatki'></br>W prenumeracie Plus dostępne są wszystkie dodatki.</span>");
        }
        else {
            $(dodsy).prop('disabled', '');
            $(".nowaPrenumerata .plus_info_dodatki").remove();
        }

    });

    function sprawdzKod() {
        var kod = $("#Kod2").val();
        ajax("/Wydanie/SprawdzKod", { WydanieId: $("#WydanieId").val(), Kod: kod }).done(function (data) {
            $("#KodInfo").html(data.Info);
            if (data.Auth == 1) {
                $("#SubmitKod").attr('disabled', 'disabled');
                $("#Kod2").attr('readonly', 'readonly');
                $("#pobierzWydanie").removeClass('d-none');
                
                $("#pobierzWydanie_dis").hide();
                $(".dodatki>div>a:nth-child(2)").show().each(function () {
                    var url = $(this).attr("href");
                    $(this).attr("href", url + '&kod=' + kod);
                });
                if (data.DodatekId > 0) {
                    $(".wydanie .dodatki>div").not(".dodatek" + data.DodatekId).find("a:nth-child(2)").hide();
                }
                /* jeszcze kod należy skopiować do formy dla późniejszego odrzutu pliku */
                $('form').find("#Kod").val(kod);
                
            }
        });
    }

    function wireUpAutoSubmit() {
        $(".autoSubmit").each(function () {
            $(this).filter("select").change(function () { $(this).closest('form').submit(); });
            $(this).filter("input").click(function () { $(this).closest('form').submit(); });
        });
    }

});

function skopiujPren(zp_id, username) {
    var url = "/Prenumerata/Skopiuj";
    var data = { zp_id: zp_id, username: username };

    $.ajax({
        url: url,
        dataType: 'json',
        data: data,
        type: 'POST',
        success: function (res) {
            console.log(res);
            var wynik = res.responseText;
            console.log(wynik);
            alert(wynik);
        },
        done: function () {
            console.log('done');
            //    alert('OK');
        }

    });


}

window.addEventListener("load", function(){
    window.cookieconsent.initialise({
        "palette": {
            "popup": {
                "background": "white",
                "text": "black"
            },
            "button": {
                "background": "#464646"
            }
        },
        "theme": "edgeless",
        "content": {
            "message": "Zgadzam się na przechowywanie moich danych osobowych na urządzeniu, z którego korzystam w formie tzw. plików cookies oraz na przetwarzanie moich danych osobowych pozostawianych w czasie korzystania przeze mnie ze stron internetowych zapisywanych w plikach cookies w celach marketingowych (w tym na profilowanie i w celach analitycznych) przez Kurier Szczeciński Sp. z o.o.",
            "dismiss": "Przejdź do serwisu",
            "link": "Więcej w naszej Polityce Prywatności",
            "href": "/strony-pomocnicze/polityka-prywatnosci/"
        }
    })});
