CREATE TABLE [dbo].[i3_KodBuforGrupa](
	[KodBuforGrupaId] [int] IDENTITY(1,1) NOT NULL,
	[ispo_user_id] [int] NOT NULL,
	[KodBuforGrupaKiedy] [datetime] NOT NULL,
	[KodBuforGrupaIle] [int] NOT NULL,
	[KodBuforGrupaWaznyDo] [datetime] NOT NULL,
 CONSTRAINT [PK_i3_KodBuforGrupa] PRIMARY KEY CLUSTERED 
(
	[KodBuforGrupaId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[i3_KodBuforGrupa]  WITH CHECK ADD  CONSTRAINT [FK_i3_KodBuforGrupa_Users] FOREIGN KEY([ispo_user_id])
REFERENCES [dbo].[Users] ([ispo_user_id])
GO
ALTER TABLE [dbo].[i3_KodBuforGrupa] CHECK CONSTRAINT [FK_i3_KodBuforGrupa_Users]
GO

CREATE TABLE [dbo].[i3_KodBufor](
	[KodBuforId] [int] IDENTITY(1,1) NOT NULL,
	[KodBuforGrupaId] [int] NOT NULL,
	[Kod] [varchar](32) NOT NULL,
 CONSTRAINT [PK_i3_KodBufor] PRIMARY KEY CLUSTERED 
(
	[KodBuforId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[i3_KodBufor]  WITH CHECK ADD  CONSTRAINT [FK_i3_KodBufor_i3_KodBuforGrupa] FOREIGN KEY([KodBuforGrupaId])
REFERENCES [dbo].[i3_KodBuforGrupa] ([KodBuforGrupaId])
GO
ALTER TABLE [dbo].[i3_KodBufor] CHECK CONSTRAINT [FK_i3_KodBufor_i3_KodBuforGrupa]
GO




CREATE PROCEDURE [dbo].[i3_genKodBufor]
	-- Add the parameters for the stored procedure here
	@Kto int
	,@KodBuforWaznyDo datetime
	,@Ilosc int
AS
BEGIN
	SET NOCOUNT ON;

    DECLARE
		 @str nvarchar(58)
		,@maxValue tinyint = 58    
		,@minValue tinyint = 1
		,@KodBuforGrupaId int
	
	DECLARE
		@OutputId table
		(
			id int not null
		)

	SET @str = 'abcdefghijkmnoprstuwxyzABCDEFGHJKLMNPRSTUWXYZ23456789'
	
	BEGIN TRY
		BEGIN TRAN
		
		INSERT INTO [dbo].[i3_KodBuforGrupa] ([ispo_user_id],[KodBuforGrupaKiedy],[KodBuforGrupaIle],[KodBuforGrupaWaznyDo])
		OUTPUT INSERTED.KodBuforGrupaId INTO @OutputId
		VALUES ( @Kto, getdate(), @Ilosc, @KodBuforWaznyDo )
		
		SELECT TOP (1) @KodBuforGrupaId = id FROM @OutputId
		
		INSERT INTO [dbo].[i3_KodBufor] ([Kod], [KodBuforGrupaId])
		SELECT TOP (@Ilosc)
			  substring(@str, ABS(CHECKSUM(NEWID()))%(@MaxValue-@MinValue+1)+@MinValue, 1) 
			+ substring(@str, ABS(CHECKSUM(NEWID()))%(@MaxValue-@MinValue+1)+@MinValue, 1) 
			+ substring(@str, ABS(CHECKSUM(NEWID()))%(@MaxValue-@MinValue+1)+@MinValue, 1) 
			+ substring(@str, ABS(CHECKSUM(NEWID()))%(@MaxValue-@MinValue+1)+@MinValue, 1) 
			+ substring(@str, ABS(CHECKSUM(NEWID()))%(@MaxValue-@MinValue+1)+@MinValue, 1) 
			+ substring(@str, ABS(CHECKSUM(NEWID()))%(@MaxValue-@MinValue+1)+@MinValue, 1) 
			+ substring(@str, ABS(CHECKSUM(NEWID()))%(@MaxValue-@MinValue+1)+@MinValue, 1) 
			+ substring(@str, ABS(CHECKSUM(NEWID()))%(@MaxValue-@MinValue+1)+@MinValue, 1)
			,@KodBuforGrupaId			
			FROM Master.sys.SysColumns	-- Dowolna tabela
		
		COMMIT
	END TRY
	BEGIN CATCH
		PRINT 'Wyst�pi� b��d w genKodBufor - kod�w nie wygenerowano.'
		ROLLBACK
	END CATCH   
END


