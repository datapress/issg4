CREATE TABLE [dbo].[i3_ArtArtykulTresciIndex](
	[ArtArtykulTresciIndexId] [int] NOT NULL IDENTITY(1,1),
	[ArtArtykulId] [int] NOT NULL,
	[ArtAutorId] [int] NULL,
	[ArtDzialId] [smallint] NOT NULL,
	[WydanieId] [int] NOT NULL,
	[ArtArtykulTresc] [nvarchar](50) NOT NULL,
	[DataWpis] [datetime] NOT NULL,
	[DataModyf] [datetime] NOT NULL,
	[PracWpis] [varchar](256) NOT NULL,
	[PracModyf] [varchar](256) NOT NULL,
CONSTRAINT [PK_ArtArtykulTresciIndex] PRIMARY KEY CLUSTERED 
(
	[ArtArtykulTresciIndexId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] 

GO

ALTER TABLE [dbo].[i3_ArtArtykulTresciIndex]  WITH CHECK ADD  CONSTRAINT [FK_ArtArtykulTresciIndex_ArtArtykul] FOREIGN KEY([ArtArtykulId])
REFERENCES [dbo].[i3_ArtArtykul] ([ArtArtykulId])
GO
ALTER TABLE [dbo].[i3_ArtArtykulTresciIndex] CHECK CONSTRAINT [FK_ArtArtykulTresciIndex_ArtArtykul]
GO

ALTER TABLE [dbo].[i3_ArtArtykulTresciIndex]  WITH CHECK ADD  CONSTRAINT [FK_ArtArtykulTresciIndex_ArtAutor] FOREIGN KEY([ArtAutorId])
REFERENCES [dbo].[i3_ArtAutor] ([ArtAutorId])
GO
ALTER TABLE [dbo].[i3_ArtArtykulTresciIndex] CHECK CONSTRAINT [FK_ArtArtykulTresciIndex_ArtAutor]
GO

ALTER TABLE [dbo].[i3_ArtArtykulTresciIndex]  WITH CHECK ADD  CONSTRAINT [FK_ArtArtykulTresciIndex_ArtDzial] FOREIGN KEY([ArtDzialId])
REFERENCES [dbo].[i3_ArtDzial] ([ArtDzialId])
GO
ALTER TABLE [dbo].[i3_ArtArtykulTresciIndex] CHECK CONSTRAINT [FK_ArtArtykulTresciIndex_ArtDzial]
GO

ALTER TABLE [dbo].[i3_ArtArtykulTresciIndex]  WITH CHECK ADD  CONSTRAINT [FK_ArtArtykulTresciIndex_Wydanie] FOREIGN KEY([WydanieId])
REFERENCES [dbo].[i3_Wydanie] ([WydanieId])
GO
ALTER TABLE [dbo].[i3_ArtArtykulTresciIndex] CHECK CONSTRAINT [FK_ArtArtykulTresciIndex_Wydanie]
GO


