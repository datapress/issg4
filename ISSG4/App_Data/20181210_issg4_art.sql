CREATE TABLE [dbo].[i3_ArtDzial](
	[ArtDzialId] [smallint] NOT NULL,
	[ArtDzialNazwa] [nvarchar](100) NOT NULL,
	[ArtDzialAktywny] [bit] NOT NULL,
	[ArtDzialSort] [smallint] NOT NULL,
	[DataWpis] [datetime] NULL,
	[DataModyf] [datetime] NULL,
	[PracWpis] [varchar](256) NULL,
	[PracModyf] [varchar](256) NULL,
 CONSTRAINT [PK_ArtDzial] PRIMARY KEY CLUSTERED 
(
	[ArtDzialId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[i3_ArtAutor](
	[ArtAutorId] [int] NOT NULL,
	[UsrUzytkownikId] [int] NULL,
	[ArtAutorNazwa] [nvarchar](max) NOT NULL,
	[ArtAutorAktywny] [bit] NOT NULL,
	[DataWpis] [datetime] NOT NULL,
	[DataModyf] [datetime] NOT NULL,
	[PracWpis] [varchar](256) NOT NULL,
	[PracModyf] [varchar](256) NOT NULL,
 CONSTRAINT [PK_ArtAutor] PRIMARY KEY CLUSTERED 
(
	[ArtAutorId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[i3_ArtArtykul](
	[ArtArtykulId] [int] NOT NULL,
	[UsrUzytkownikId] [int] NULL,
	[ArtArtykulUtworzono] [datetime] NOT NULL,
	[ArtAutorId] [int] NULL,
	[ArtArtykulNadytul] [nvarchar](max) NULL,
	[ArtArtykulTytul] [nvarchar](max) NOT NULL,
	[ArtArtykulTresc] [nvarchar](max) NOT NULL,
	[ArtArtykulUwagi] [nvarchar](max) NULL,
	[ArtArtykulSlowaKluczowe] [nvarchar](max) NOT NULL,
	[ArtArtykulZnaki] [int] NOT NULL,
	[ArtArtykulLinie] [int] NOT NULL,
	[ArtArtykulTytulLinie] [int] NULL,
	[ArtArtykulFoto] [bit] NOT NULL,
	[ArtArtykulFotoLinie] [int] NOT NULL,
	[SloTytulLamId] [smallint] NOT NULL,
	[DataWpis] [datetime] NOT NULL,
	[DataModyf] [datetime] NOT NULL,
	[PracWpis] [varchar](256) NOT NULL,
	[PracModyf] [varchar](256) NOT NULL,
	[WydanieId] [int] not null,
	[ArtDzialId] [int] not null,
 CONSTRAINT [PK_ArtArtykul] PRIMARY KEY CLUSTERED 
(
	[ArtArtykulId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[i3_ArtArtykul]  WITH CHECK ADD  CONSTRAINT [FK_ArtArtykul_ArtDzial] FOREIGN KEY([ArtDzialId])
REFERENCES [dbo].[i3_ArtDzial] ([ArtDzialId])
GO
ALTER TABLE [dbo].[i3_ArtArtykul] CHECK CONSTRAINT [FK_ArtArtykul_ArtDzial]
GO

ALTER TABLE [dbo].[i3_ArtArtykul]  WITH CHECK ADD  CONSTRAINT [FK_ArtArtykul_ArtAutor] FOREIGN KEY([ArtAutorId])
REFERENCES [dbo].[i3_ArtAutor] ([ArtAutorId])
GO
ALTER TABLE [dbo].[i3_ArtArtykul] CHECK CONSTRAINT [FK_ArtArtykul_ArtAutor]
GO

ALTER TABLE [dbo].[i3_ArtArtykul] WITH CHECK ADD  CONSTRAINT [FK_ArtArtykul_Wydanie] FOREIGN KEY([WydanieId])
REFERENCES [dbo].[i3_Wydanie] ([WydanieId])
GO
ALTER TABLE [dbo].[i3_ArtArtykul] CHECK CONSTRAINT [FK_ArtArtykul_Wydanie]
GO

CREATE TABLE [dbo].[i3_ArtFoto](
	[ArtFotoId] [int] NOT NULL,
	[ArtArtykulId] [int] NOT NULL,
	[FotoSKT_FotoId] [int] NULL,
	[SloFotoLamId] [smallint] NOT NULL,
	[ArtFotoLinie] [int] NULL,
	[ArtFotoPodpis] [nvarchar](max) NULL,
	[ArtFotoUwagi] [nvarchar](max) NOT NULL,
	[DataWpis] [datetime] NOT NULL,
	[DataModyf] [datetime] NOT NULL,
	[PracWpis] [varchar](256) NOT NULL,
	[PracModyf] [varchar](256) NOT NULL,
	[ArtFotoFilePath] nvarchar(1024),
 CONSTRAINT [PK_ArtFoto] PRIMARY KEY CLUSTERED 
(
	[ArtFotoId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[i3_ArtFoto]  WITH CHECK ADD  CONSTRAINT [FK_ArtFoto_ArtArtykul] FOREIGN KEY([ArtArtykulId])
REFERENCES [dbo].[i3_ArtArtykul] ([ArtArtykulId])
GO
ALTER TABLE [dbo].[i3_ArtFoto] CHECK CONSTRAINT [FK_ArtFoto_ArtArtykul]
GO

alter table i3_Kod add KodOperator varchar(1)
go

alter PROCEDURE [dbo].[i3_AddKod]
@crkod nvarchar (20) ,
@issgwydaniaid int ,
@crczas datetime ,
@status char (1),
@crip nvarchar(15),
@operator nvarchar(1)
AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO dbo.i3_Kod (KodKod, WydanieId, KodCzas, KodStatus, KodIP, KodOperator)
	VALUES(@crkod, @issgwydaniaid, @crczas, @status, @crip, @operator)

END
GO

alter table i3_ArtArtykul add ArtPierwsza bit;
go
alter table i3_ArtArtykul add ArtJedynka bit;
go

alter table i3_ArtArtykul add ArtZdjeciaExists bit;
go





