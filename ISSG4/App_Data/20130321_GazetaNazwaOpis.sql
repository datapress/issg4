﻿alter table i3_Gazeta add GazetaNazwaOpis varchar(255)
go

ALTER PROCEDURE [dbo].[i3_getNoweWydaniaByType]
	-- Add the parameters for the stored procedure here
	@GazetaTypId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	select g.GazetaNazwa, w.WydanieData, w.WydanieOpis, w.GazetaId, w.WydanieId, w.WydanieJedynka, g.GazetaNazwaOpis
	from i3_Wydanie as w
	inner join (
		SELECT GazetaId, MAX(WydanieData) AS WydanieData 
		FROM i3_Wydanie 
		WHERE (WydanieAktywne = 1) 
		GROUP BY GazetaId) AS n ON w.GazetaId = n.GazetaId AND w.WydanieData = n.WydanieData
	inner join
		i3_Gazeta as g on w.GazetaId = g.GazetaId
	where (g.GazetaTypId = @GazetaTypId)
	order by g.GazetaId
END
go

ALTER PROCEDURE [dbo].[i3_getWydanieJoined]
	-- Add the parameters for the stored procedure here
	@WydanieId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	select w.*, g.GazetaNazwa, g.GazetaSkrot, g.GazetaTypId, g.WydawcaId, g.GazetaBezplatna, 
	g.GazetaKodId, k.GazetaKodNumer, k.GazetaKodSms, k.GazetaKodCenaInfo, k.GazetaKodTyp, k.GazetaKodKod,
	g.GazetaNazwaOpis
	from i3_Wydanie as w
		inner join i3_Gazeta as g on w.GazetaId = g.GazetaId
		left outer join i3_GazetaKod as k on g.GazetaKodId = k.GazetaKodId
	where
		w.WydanieAktywne = 1
		and w.WydanieId = @WydanieId

END

go