alter table [i3_Wydanie] add ApiDpsProductId varchar(256);
go

--update i3_wydanie set ApiDpsProductId = 'Kurier.Szczecinski.'+CONVERT ( varchar(10) ,wydaniedata,104 )  
--select 'Kurier.Szczecinski.'+CONVERT ( varchar(10) ,wydaniedata,104 ) from i3_wydanie order by wydanieid desc


CREATE TABLE [dbo].[api_tokens](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[username] [nvarchar](512) NOT NULL,
	[authToken] [nvarchar](512) NOT NULL,
	[data_wpis] [datetime] NOT NULL,
 CONSTRAINT [PK_api_tokens] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

/*
alter table users add tkr_id int
go 
alter table users add ag_czy_szabl_plan varchar(1)

*/


alter table [i3_Wydanie] add WydanieNr smallint;
go
declare @i int  = (SELECT ISNULL(MAX(WydanieNr),-1) + 1 FROM [i3_Wydanie] where year(WydanieData)=2016)
update [i3_Wydanie]
set WydanieNr  = @i , @i = @i + 1
where WydanieNr is null and year(WydanieData)=2016 and GazetaId = 1
go

alter TRIGGER [dbo].[iAddWydanieNrAndProductId]
   ON  [dbo].[i3_Wydanie]
   AFTER INSERT
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	declare @i int  = (SELECT ISNULL(MAX(WydanieNr),0) FROM [i3_Wydanie] where year(WydanieData) = year(getdate()))

	update [i3_Wydanie]
	set WydanieNr  = @i , @i = @i + 1
	where WydanieNr is null and year(wydaniedata)= year(getdate())

	update [i3_Wydanie]
	set ApiDpsProductId = 'pl.kurierszczecinski.'+convert(varchar,year(WydanieData),4)+'.'+convert(varchar,WydanieNr,3)
	where ApiDpsProductId is null and wydanieid in (select wydanieid from inserted)
	
END

GO



