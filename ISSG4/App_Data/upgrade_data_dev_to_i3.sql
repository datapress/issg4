BEGIN TRAN

-- Baza docelowa:
-- [ISPO2GO_20120711].

-- Baza �r�d�owa:
-- [ISPO2GO_gazeta].

--------------------------------------------------------------------------------------
-- [i3_AudytNazwa]

SET IDENTITY_INSERT [ISPO2GO_20120711].[dbo].[i3_AudytNazwa] ON

INSERT INTO [ISPO2GO_20120711].[dbo].[i3_AudytNazwa]
		(audyt_nazwa_id
	   , audyt_nazwa_typ
	   , tabela_nazwa
	   , kolumna_nazwa
	   , audyt_nazwa)
	SELECT *
		FROM [ISPO2GO_gazeta].[dbo].[i3_AudytNazwa]

SET IDENTITY_INSERT [ISPO2GO_20120711].[dbo].[i3_AudytNazwa] OFF
DBCC checkident ('[ISPO2GO_20120711].[dbo].[i3_AudytNazwa]')

--------------------------------------------------------------------------------------
-- [i3_Wydawca]

SET IDENTITY_INSERT [ISPO2GO_20120711].[dbo].[i3_Wydawca] ON

INSERT INTO [ISPO2GO_20120711].[dbo].[i3_Wydawca]
		(WydawcaId
	   , WydawcaNazwa)
	SELECT *
		FROM [ISPO2GO_gazeta].[dbo].[i3_Wydawca]

SET IDENTITY_INSERT [ISPO2GO_20120711].[dbo].[i3_Wydawca] OFF
DBCC checkident ('[ISPO2GO_20120711].[dbo].[i3_Wydawca]')

--------------------------------------------------------------------------------------
-- [i3_GazetaTyp]

SET IDENTITY_INSERT [ISPO2GO_20120711].[dbo].[i3_GazetaTyp] ON

INSERT INTO [ISPO2GO_20120711].[dbo].[i3_GazetaTyp]
		(GazetaTypId
	   , GazetaTypNazwa)
	SELECT *
		FROM [ISPO2GO_gazeta].[dbo].[i3_GazetaTyp]

SET IDENTITY_INSERT [ISPO2GO_20120711].[dbo].[i3_GazetaTyp] OFF
DBCC checkident ('[ISPO2GO_20120711].[dbo].[i3_GazetaTyp]')

--------------------------------------------------------------------------------------
-- [i3_GazetaKod]

SET IDENTITY_INSERT [ISPO2GO_20120711].[dbo].[i3_GazetaKod] ON

INSERT INTO [ISPO2GO_20120711].[dbo].[i3_GazetaKod]
		(GazetaKodId
	   , GazetaKodNumer
	   , GazetaKodSms
	   , GazetaKodCenaInfo
	   , GazetaKodTyp
	   , GazetaKodKod)
	SELECT *
		FROM [ISPO2GO_gazeta].[dbo].[i3_GazetaKod]

SET IDENTITY_INSERT [ISPO2GO_20120711].[dbo].[i3_GazetaKod] OFF
DBCC checkident ('[ISPO2GO_20120711].[dbo].[i3_GazetaKod]')

--------------------------------------------------------------------------------------
-- [i3_Gazeta]

SET IDENTITY_INSERT [ISPO2GO_20120711].[dbo].[i3_Gazeta] ON

INSERT INTO [ISPO2GO_20120711].[dbo].[i3_Gazeta]
		(GazetaId
	   , GazetaNazwa
	   , GazetaSkrot
	   , GazetaTypId
	   , WydawcaId
	   , GazetaBezplatna
	   , GazetaKodId)
	SELECT *
		FROM [ISPO2GO_gazeta].[dbo].[i3_Gazeta]

SET IDENTITY_INSERT [ISPO2GO_20120711].[dbo].[i3_Gazeta] OFF
DBCC checkident ('[ISPO2GO_20120711].[dbo].[i3_Gazeta]')

IF OBJECT_ID('[ISPO2GO_20120711].[dbo].[i3_map_GazetaId]') IS NOT NULL 
BEGIN
DELETE
	FROM [ISPO2GO_20120711].[dbo].[i3_map_GazetaId]
END
ELSE 
BEGIN
    CREATE TABLE [ISPO2GO_20120711].[dbo].[i3_map_GazetaId] ( OldGazetaId int, NewGazetaId int )
END

INSERT INTO [ISPO2GO_20120711].[dbo].[i3_map_GazetaId]
		(OldGazetaId
	   , NewGazetaId)
	SELECT o.issg_gazeta_id
		 , n.GazetaId
		FROM
			[ISPO2GO_20120711].[dbo].[issg_gazeta] AS o
			INNER JOIN [ISPO2GO_20120711].[dbo].[i3_Gazeta] AS n
				ON n.GazetaSkrot = o.issg_gazeta_short

--------------------------------------------------------------------------------------
-- [i3_RodzajOkresu]

SET IDENTITY_INSERT [ISPO2GO_20120711].[dbo].[i3_RodzajOkresu] ON

INSERT INTO [ISPO2GO_20120711].[dbo].[i3_RodzajOkresu]
		(RodzajOkresuId
	   , RodzajOkresuNazwa
	   , RodzajOkresuMiesiac
	   , RodzajOkresuDzien)
	SELECT *
		FROM [ISPO2GO_gazeta].[dbo].[i3_RodzajOkresu]

SET IDENTITY_INSERT [ISPO2GO_20120711].[dbo].[i3_RodzajOkresu] OFF
DBCC checkident ('[ISPO2GO_20120711].[dbo].[i3_RodzajOkresu]')

--------------------------------------------------------------------------------------
-- [i3_ProduktRodzaj]

SET IDENTITY_INSERT [ISPO2GO_20120711].[dbo].[i3_ProduktRodzaj] ON

INSERT INTO [ISPO2GO_20120711].[dbo].[i3_ProduktRodzaj]
		(ProduktRodzajId
	   , ProduktRodzajNazwa
	   , ProduktRodzajBiezace
	   , ProduktRodzajArchiwalne
	   , ProduktRodzajReklamacja
	   , ProduktRodzajPojedyncze)
	SELECT *
		FROM [ISPO2GO_gazeta].[dbo].[i3_ProduktRodzaj]

SET IDENTITY_INSERT [ISPO2GO_20120711].[dbo].[i3_ProduktRodzaj] OFF
DBCC checkident ('[ISPO2GO_20120711].[dbo].[i3_ProduktRodzaj]')

--------------------------------------------------------------------------------------
-- [i3_Produkt]

SET IDENTITY_INSERT [ISPO2GO_20120711].[dbo].[i3_Produkt] ON

INSERT INTO [ISPO2GO_20120711].[dbo].[i3_Produkt]
		(ProduktId
	   , RodzajOkresuId
	   , ProduktIlosc
	   , ProduktNazwa
	   , ProduktRodzajId)
	SELECT *
		FROM [ISPO2GO_gazeta].[dbo].[i3_Produkt]

SET IDENTITY_INSERT [ISPO2GO_20120711].[dbo].[i3_Produkt] OFF
DBCC checkident ('[ISPO2GO_20120711].[dbo].[i3_Produkt]')

IF OBJECT_ID('[ISPO2GO_20120711].[dbo].[i3_map_iProduktId_i3CennikId]') IS NOT NULL 
BEGIN
DELETE
	FROM [ISPO2GO_20120711].[dbo].[i3_map_iProduktId_i3CennikId]
END
ELSE 
BEGIN
    CREATE TABLE [ISPO2GO_20120711].[dbo].[i3_map_iProduktId_i3CennikId] ( issg_ProduktId int, i3_CennikId int )
END

INSERT INTO [ISPO2GO_20120711].[dbo].[i3_map_iProduktId_i3CennikId]
		(issg_ProduktId
	   , i3_CennikId)
	SELECT
		p.issg_produkt_id
	  , i3c.CennikId
		FROM
			[ISPO2GO_20120711].[dbo].[issg_produkt] AS p
			INNER JOIN
				[ISPO2GO_20120711].[dbo].[issg_produkt_rodzaj] AS r
				ON p.issg_produkt_rodzaj_id = r.issg_produkt_rodzaj_id
			INNER JOIN
				[ISPO2GO_gazeta].[dbo].[i3_Cennik] AS i3c
				ON i3c.GazetaId = p.issg_gazeta_id
			INNER JOIN
				[ISPO2GO_gazeta].[dbo].[i3_Produkt] AS i3p
				ON i3c.ProduktId = i3p.ProduktId
			INNER JOIN
				[ISPO2GO_gazeta].[dbo].[i3_RodzajOkresu] AS i3ro
				ON i3p.RodzajOkresuId = i3ro.RodzajOkresuId
				AND r.issg_produkt_rodzaj_czas_ilosc = i3ro.RodzajOkresuMiesiac
		WHERE
			r.issg_produkt_rodzaj_nazwa != 'wydanie archiwalne'

--------------------------------------------------------------------------------------
-- [i3_CennikNaglowek]

SET IDENTITY_INSERT [ISPO2GO_20120711].[dbo].[i3_CennikNaglowek] ON

INSERT INTO [ISPO2GO_20120711].[dbo].[i3_CennikNaglowek]
		(CennikNaglowekId
	   , CennikNaglowekNazwa
	   , CennikNaglowekWaznyOd
	   , CennikNaglowekAktywny)
	SELECT *
		FROM [ISPO2GO_gazeta].[dbo].[i3_CennikNaglowek]

SET IDENTITY_INSERT [ISPO2GO_20120711].[dbo].[i3_CennikNaglowek] OFF
DBCC checkident ('[ISPO2GO_20120711].[dbo].[i3_CennikNaglowek]')

--------------------------------------------------------------------------------------
-- [i3_Vat]

INSERT INTO [ISPO2GO_20120711].[dbo].[i3_Vat]
		(VatId
	   , VatProc
	   , VatAktywny
	   , VatFiskalKod)
	SELECT *
		FROM [ISPO2GO_gazeta].[dbo].[i3_Vat]
		
IF not exists
(SELECT *
	 FROM [ISPO2GO_20120711].[dbo].[i3_Vat]
	 WHERE VatId = '22%')
BEGIN
INSERT INTO [ISPO2GO_20120711].[dbo].[i3_Vat]
		(VatId
	   , VatProc
	   , VatAktywny
	   , VatFiskalKod)
	VALUES
		('22%'
		, 0.22
		, 1
		, '')
END

--------------------------------------------------------------------------------------
-- [i3_Cennik]

SET IDENTITY_INSERT [ISPO2GO_20120711].[dbo].[i3_Cennik] ON

INSERT INTO [ISPO2GO_20120711].[dbo].[i3_Cennik]
		(CennikId
	   , GazetaId
	   , ProduktId
	   , CennikNaglowekId
	   , CennikNetto
	   , CennikBrutto
	   , CennikVat
	   , VatId)
	SELECT *
		FROM [ISPO2GO_gazeta].[dbo].[i3_Cennik]

SET IDENTITY_INSERT [ISPO2GO_20120711].[dbo].[i3_Cennik] OFF
DBCC checkident ('[ISPO2GO_20120711].[dbo].[i3_Cennik]')

--------------------------------------------------------------------------------------
-- [i3_Dodatek]

SET IDENTITY_INSERT [ISPO2GO_20120711].[dbo].[i3_Dodatek] ON

INSERT INTO [ISPO2GO_20120711].[dbo].[i3_Dodatek]
		(DodatekId
	   , DodatekNazwa
	   , DodatekSkrot
	   , DodatekAktywny)
	SELECT *
		FROM [ISPO2GO_gazeta].[dbo].[i3_Dodatek]

SET IDENTITY_INSERT [ISPO2GO_20120711].[dbo].[i3_Dodatek] OFF
DBCC checkident ('[ISPO2GO_20120711].[dbo].[i3_Dodatek]')

IF OBJECT_ID('[ISPO2GO_20120711].[dbo].[i3_map_DodatekId]') IS NOT NULL 
BEGIN
DELETE
	FROM [ISPO2GO_20120711].[dbo].[i3_map_DodatekId]
END
ELSE 
BEGIN
    CREATE TABLE [ISPO2GO_20120711].[dbo].[i3_map_DodatekId] ( OldDodatekId int, NewDodatekId int )
END

INSERT INTO [ISPO2GO_20120711].[dbo].[i3_map_DodatekId]
		(OldDodatekId
	   , NewDodatekId)
	SELECT o.issg_gazeta_mutacja_id
		 , n.DodatekId
		FROM
			[ISPO2GO_20120711].[dbo].[issg_gazeta_mutacja] AS o
			INNER JOIN [ISPO2GO_20120711].[dbo].[i3_Dodatek] AS n
				ON n.DodatekSkrot = o.issg_gazeta_mutacja_short

--------------------------------------------------------------------------------------
-- [i3_GazetaDodatek]

INSERT INTO [ISPO2GO_20120711].[dbo].[i3_GazetaDodatek]
		(GazetaId
	   , DodatekId)
	SELECT *
		FROM [ISPO2GO_gazeta].[dbo].[i3_GazetaDodatek]

--------------------------------------------------------------------------------------
-- [i3_PromoKodTyp]

SET IDENTITY_INSERT [ISPO2GO_20120711].[dbo].[i3_PromoKodTyp] ON

INSERT INTO [ISPO2GO_20120711].[dbo].[i3_PromoKodTyp]
		(PromoKodTypId
	   , Wartosc
	   , Nazwa)
	SELECT *
		FROM [ISPO2GO_gazeta].[dbo].[i3_PromoKodTyp]

SET IDENTITY_INSERT [ISPO2GO_20120711].[dbo].[i3_PromoKodTyp] OFF
DBCC checkident ('[ISPO2GO_20120711].[dbo].[i3_PromoKodTyp]')

--------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------
-- [i3_Zamowienie]

SET IDENTITY_INSERT [ISPO2GO_20120711].[dbo].[i3_Zamowienie] ON

INSERT INTO [ISPO2GO_20120711].[dbo].[i3_Zamowienie]
		(ZamowienieId
	   , issg_user_id
	   , ZamowienieData
	   , ZamowienieZablokowane
	   , ZamowienieOplacone
	   , ZamowienieRecznaAktywacja
	   , ZamowienieNetto
	   , ZamowienieBrutto
	   , ZamowienieVat
	   , ZamowieniePromoKodId
	   , ZamowieniePromoKodTypNazwa
	   , ZamowieniePromoKodKod
	   , ZamowieniePrzedRd
	   , ZamowienieProcRd
	   , ZamowienieWartoscRd)
	SELECT
		issg_zam_id --ZamowienieId
	  , issg_user_id --issg_user_id
	  , issg_zam_data_wpisu --ZamowienieData
	  , CASE
			WHEN issg_zam_czy_zablokowane = 't' THEN
				1
			ELSE
				0
		END --ZamowienieZablokowane
	  , CASE
			WHEN issg_zam_czy_oplacone = 't' THEN
				1
			ELSE
				0
		END --ZamowienieOplacone
	  , 0 --ZamowienieRecznaAktywacja
	  , issg_zam_netto --ZamowienieNetto
	  , issg_zam_brutto --ZamowienieBrutto
	  , issg_zam_vat --ZamowienieVat
	  , NULL --ZamowieniePromoKodId
	  , NULL --ZamowieniePromoKodTypNazwa
	  , NULL --ZamowieniePromoKodKod
	  , 0 --ZamowieniePrzedRd
	  , 0 --ZamowienieProcRd
	  , 0 --ZamowienieWartoscRd
		FROM [ISPO2GO_20120711].[dbo].[issg_zamowienia]

SET IDENTITY_INSERT [ISPO2GO_20120711].[dbo].[i3_Zamowienie] OFF
DBCC checkident ('[ISPO2GO_20120711].[dbo].[i3_Zamowienie]')

--------------------------------------------------------------------------------------
-- [i3_Zamowienie]

SET IDENTITY_INSERT [ISPO2GO_20120711].[dbo].[i3_ZamowieniePozycja] ON

INSERT INTO [ISPO2GO_20120711].[dbo].[i3_ZamowieniePozycja]
		(ZamowieniePozycjaId
	   , CennikId
	   , ZamowienieId
	   , ZamowieniePozycjaOd
	   , ZamowieniePozycjaDo
	   , ZamowieniePozycjaPrzedRD
	   , ZamowieniePozycjaProcRD
	   , ZamowieniePozycjaWartoscRD
	   , ZamowieniePozycjaNetto
	   , ZamowieniePozycjaBrutto
	   , ZamowieniePozycjaVat
	   , VatId)
	SELECT
		z.issg_zam_id --ZamowieniePozycjaId
	  , c.i3_CennikId --CennikId
	  , z.issg_zam_id --ZamowienieId
	  , z.issg_zam_data_start --ZamowieniePozycjaOd
	  , z.issg_zam_data_stop --ZamowieniePozycjaDo
	  , z.issg_zam_netto --ZamowieniePozycjaPrzedRD
	  , 0 --ZamowieniePozycjaProcRD
	  , 0 --ZamowieniePozycjaWartoscRD
	  , z.issg_zam_netto --ZamowieniePozycjaNetto
	  , z.issg_zam_brutto --ZamowieniePozycjaBrutto
	  , z.issg_zam_vat --ZamowieniePozycjaVat
	  , CASE
			WHEN z.issg_zam_vat_id = 23 THEN
				'23%'
			WHEN z.issg_zam_vat_id = 22 THEN
				'22%'
			WHEN z.issg_zam_vat_id = 8 THEN
				'8%'
			WHEN z.issg_zam_vat_id = 7 THEN
				'8%'
			WHEN z.issg_zam_vat_id = 5 THEN
				'5%'
			ELSE
				'0%'
		END --VatId
		FROM [ISPO2GO_20120711].[dbo].[issg_zamowienia] AS z
			INNER JOIN [ISPO2GO_20120711].[dbo].[i3_map_iProduktId_i3CennikId] AS c
				ON z.issg_produkt_id = c.issg_ProduktId

SET IDENTITY_INSERT [ISPO2GO_20120711].[dbo].[i3_ZamowieniePozycja] OFF
DBCC checkident ('[ISPO2GO_20120711].[dbo].[i3_ZamowieniePozycja]')

--------------------------------------------------------------------------------------
-- [i3_ZamowienieDodatek]

INSERT INTO [ISPO2GO_20120711].[dbo].[i3_ZamowienieDodatek]
		(ZamowieniePozycjaId
	   , DodatekId)
	SELECT
		issg_zam_id
	  , issg_gazeta_mutacja_id
		FROM [ISPO2GO_20120711].[dbo].[issg_zamowienia_dodatek]

--------------------------------------------------------------------------------------
-- [i3_Wydanie]

SET IDENTITY_INSERT [ISPO2GO_20120711].[dbo].[i3_Wydanie] ON

INSERT INTO [ISPO2GO_20120711].[dbo].[i3_Wydanie]
		(WydanieId
	   , WydanieOpis
	   , WydaniePlik
	   , WydanieAktywne
	   , WydanieData
	   , WydanieJedynka
	   , GazetaId
	   , DodatekId)
	SELECT
		w.issg_wydania_id --WydanieId
	  , w.wydanie_opis --WydanieOpis
	  , w.wydanie_file_name --WydaniePlik
	  , CASE
			WHEN w.issg_wydania_aktywne = 't' THEN
				1
			ELSE
				0
		END --WydanieAktywne
	  , w.wydanie_data --WydanieData
	  , replace(w.wydanie_file_name, '.pdf', '.jpg') --WydanieJedynka
	  , w.gazeta_id --GazetaId
	  , NULL --DodatekId
		FROM [ISPO2GO_20120711].[dbo].[issg_wydania] AS w

INSERT INTO [ISPO2GO_20120711].[dbo].[i3_Wydanie]
		(WydanieId
	   , WydanieOpis
	   , WydaniePlik
	   , WydanieAktywne
	   , WydanieData
	   , WydanieJedynka
	   , GazetaId
	   , DodatekId)
	SELECT
		3000 + wd.issg_wydania_dodatek_id --WydanieId
	  , '' --WydanieOpis
	  , wd.issg_wydania_dodatek_plik --WydaniePlik
	  , wd.issg_wydania_dodatek_aktywny --WydanieAktywne
	  , w.wydanie_data --WydanieData
	  , replace(wd.issg_wydania_dodatek_plik, '.pdf', '.jpg') --WydanieJedynka
	  , NULL --GazetaId
	  , wd.issg_gazeta_mutacja_id --DodatekId
		FROM [ISPO2GO_20120711].[dbo].[issg_wydania_dodatek] AS wd
			INNER JOIN [ISPO2GO_20120711].[dbo].[issg_wydania] AS w
				ON wd.issg_wydania_id = w.issg_wydania_id

SET IDENTITY_INSERT [ISPO2GO_20120711].[dbo].[i3_Wydanie] OFF
DBCC checkident ('[ISPO2GO_20120711].[dbo].[i3_Wydanie]', reseed, 10000)

--------------------------------------------------------------------------------------
-- [i3_Kod]

INSERT INTO [ISPO2GO_20120711].[dbo].[i3_Kod]
		(KodKod
	   , KodCzas
	   , KodStatus
	   , KodIP
	   , WydanieId
	   , DodatekWydanieId)
	SELECT
		kod --KodKod
	  , czas_operacji --KodCzas
	  , [status] --KodStatus
	  , ip --KodIP
	  , issg_wydania_id --WydanieId
	  , 3000 + issg_wydania_dodatek_id --DodatekWydanieId
		FROM [ISPO2GO_20120711].[dbo].[issg_kody]

--------------------------------------------------------------------------------------
-- [i3_Platnosc]

SET IDENTITY_INSERT [ISPO2GO_20120711].[dbo].[i3_Platnosc] ON

INSERT INTO [ISPO2GO_20120711].[dbo].[i3_Platnosc]
		(PlatnoscId
	   , ispo_user_id
	   , ZamowienieId
	   , PlatnoscCzas
	   , PlatnoscOpis
	   , PlatnoscTransId
	   , PlatnoscKwota
	   , PlatnoscStatus)
	SELECT
		 issg_platnosc_id --PlatnoscId
	   , issg_platnosc_user_id --ispo_user_id
	   , issg_platnosc_zam_id --ZamowienieId
	   , issg_platnosc_czas --PlatnoscCzas
	   , issg_platnosc_opis --PlatnoscOpis
	   , issg_platnosc_t_id --PlatnoscTransId
	   , issg_platnosc_kwota --PlatnoscKwota
	   , issg_platnosc_status --PlatnoscStatus
		FROM [ISPO2GO_20120711].[dbo].[issg_platnosci]

SET IDENTITY_INSERT [ISPO2GO_20120711].[dbo].[i3_Platnosc] OFF
DBCC checkident ('[ISPO2GO_20120711].[dbo].[i3_Platnosc]')

ROLLBACK