
/*  MarcinB, 2012-07-04
	1. klucz z pobran do usera 
	2. tabela do rejestracji pobra� tytu��w bezp�atnych 
	3. tabela dla zdarze� u�ytkownika (logowania, resety has�a, zmiany has�a)
*/

ALTER TABLE [dbo].[i3_Pobranie]  WITH CHECK ADD  CONSTRAINT [FK_i3_Pobranie_Users] FOREIGN KEY([ispo_user_id])
REFERENCES [dbo].[Users] ([ispo_user_id])
GO
ALTER TABLE [dbo].[i3_Pobranie] CHECK CONSTRAINT [FK_i3_Pobranie_Users]
GO

CREATE TABLE [dbo].[i3_PobranieDarmowe](
	[PobranieDarmoweId] [int] IDENTITY(1,1) NOT NULL,
	[PobranieDarmoweCzas] [datetime] NOT NULL,
	[WydanieId] [int] NOT NULL,
	[PobranieDarmoweIp] [nvarchar](20) NOT NULL,
 CONSTRAINT [PK_PobranieDarmowe] PRIMARY KEY CLUSTERED 
(
	[PobranieDarmoweId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[i3_PobranieDarmowe]  WITH CHECK ADD  CONSTRAINT [FK_i3_PobranieDarmowe_Wydanie] FOREIGN KEY([WydanieId])
REFERENCES [dbo].[i3_Wydanie] ([WydanieId])
GO
ALTER TABLE [dbo].[i3_PobranieDarmowe] CHECK CONSTRAINT [FK_i3_PobranieDarmowe_Wydanie]
GO


CREATE TABLE [dbo].[Users_event_hist](
	[ueh_id] [int] IDENTITY(1,1) NOT NULL,
	[ueh_czas] [datetime] NOT NULL,
	[UserName] [varchar] (255) NOT NULL,
	[ueh_name] [nvarchar](255) NOT NULL,
	[ueh_description] [nvarchar](255),
 CONSTRAINT [PK_Users_event_hist] PRIMARY KEY CLUSTERED 
(
	[ueh_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[Users_event_hist]  WITH CHECK ADD  CONSTRAINT [FK_Users_event_Users] FOREIGN KEY([UserName])
REFERENCES [dbo].[Users] ([UserName])
GO
ALTER TABLE [dbo].[Users_event_hist] CHECK CONSTRAINT [FK_Users_event_Users]
GO