BEGIN TRAN

DECLARE
	@zamowieniePozycjaId int
	,@gazetaId int
	,@pozycjaOd datetime
	,@pozycjaDo datetime

declare NaDzialCursor cursor for
SELECT zP.ZamowieniePozycjaId
	 , zP.ZamowieniePozycjaOd
	 , zP.ZamowieniePozycjaDo
	 , c.GazetaId
	FROM i3_ZamowieniePozycja AS zP
		INNER JOIN i3_Cennik AS c
			ON zp.CennikId = c.CennikId
		INNER JOIN i3_Zamowienie AS z
			ON z.ZamowienieId = zP.ZamowienieId
	WHERE
		(z.ZamowienieOplacone = 1
		OR z.ZamowienieRecznaAktywacja = 1)
		AND z.ZamowienieZablokowane = 1

open NaDzialCursor
fetch next from NaDzialCursor
into @zamowieniePozycjaId, @pozycjaOd, @pozycjaDo, @gazetaId
while @@FETCH_STATUS = 0
begin
-- wydania
INSERT INTO i3_NaDzial
		(WydanieId
	   , ZamowieniePozycjaId)
	SELECT WydanieId
		 , @zamowieniePozycjaId
		FROM i3_Wydanie
		WHERE GazetaId = @gazetaId
			AND (WydanieData BETWEEN @pozycjaOd AND @pozycjaDo)

-- dodatki
INSERT INTO i3_NaDzial
		(WydanieId
	   , ZamowieniePozycjaId)
	SELECT wy.WydanieId
		 , @zamowieniePozycjaId
		FROM i3_ZamowienieDodatek AS zaD
			INNER JOIN i3_Wydanie AS wy
				ON zaD.DodatekId = wy.DodatekId
		WHERE zaD.ZamowieniePozycjaId = @zamowieniePozycjaId
			AND wy.WydanieData BETWEEN @pozycjaOd AND @pozycjaDo
				
	fetch next from NaDzialCursor
	into @zamowieniePozycjaId, @pozycjaOd, @pozycjaDo, @gazetaId
end
close NaDzialCursor	
deallocate NaDzialCursor

ROLLBACK