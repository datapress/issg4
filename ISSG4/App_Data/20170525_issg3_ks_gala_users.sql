CREATE TABLE [dbo].[typ_raport_filtr](
	[typ_raport_filtr_id] [int] IDENTITY(1,1) NOT NULL,
	[typ_raport_filtr_nazwa] [nvarchar](100) NOT NULL,
	[data_wpis] [datetime] NOT NULL,
	[data_modyf] [datetime] NOT NULL,
	[prac_wpis] [varchar](256) NOT NULL,
	[prac_modyf] [varchar](256) NOT NULL,
 CONSTRAINT [PK_typ_raport_filtr] PRIMARY KEY CLUSTERED 
(
	[typ_raport_filtr_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[typ_raport_filtr_gazeta](
	[typ_raport_filtr_id] [int] NOT NULL,
	[typ_gazeta_id] [varchar](2) NOT NULL,
	[data_wpis] [datetime] NOT NULL,
	[data_modyf] [datetime] NOT NULL,
	[prac_wpis] [varchar](256) NOT NULL,
	[prac_modyf] [varchar](256) NOT NULL,
 CONSTRAINT [PK_typ_raport_filtr_gazeta] PRIMARY KEY CLUSTERED 
(
	[typ_raport_filtr_id] ASC,
	[typ_gazeta_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


ALTER TABLE [dbo].[typ_raport_filtr_gazeta]  WITH CHECK ADD  CONSTRAINT [FK_typ_raport_filtr_gazeta_typ_gazeta] FOREIGN KEY([typ_gazeta_id])
REFERENCES [dbo].[typ_gazeta] ([typ_gazeta_id])
GO
ALTER TABLE [dbo].[typ_raport_filtr_gazeta] CHECK CONSTRAINT [FK_typ_raport_filtr_gazeta_typ_gazeta]
GO

ALTER TABLE [dbo].[typ_raport_filtr_gazeta]  WITH CHECK ADD  CONSTRAINT [FK_typ_raport_filtr_gazeta_typ_raport_filtr] FOREIGN KEY([typ_raport_filtr_id])
REFERENCES [dbo].[typ_raport_filtr] ([typ_raport_filtr_id])
GO
ALTER TABLE [dbo].[typ_raport_filtr_gazeta] CHECK CONSTRAINT [FK_typ_raport_filtr_gazeta_typ_raport_filtr]
GO



