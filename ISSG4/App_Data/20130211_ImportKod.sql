﻿CREATE TABLE [dbo].[i3_ImportKodPlik](
	[ImportKodPlikId] [varchar](50) NOT NULL,
	[PlikNazwa] [varchar](255) NOT NULL,
	[ImportCzas] [datetime] NOT NULL,
	[ImportPlikContent] [text] NOT NULL,
 CONSTRAINT [PK_i3_ImportKodPlik] PRIMARY KEY CLUSTERED 
(
	[ImportKodPlikId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

CREATE TABLE [dbo].[i3_ImportKod](
	[ImportKodId] [int] IDENTITY(1,1) NOT NULL,
	[ImportKodPlikId] [varchar](50) NOT NULL,
	[Kod] [varchar](32) NOT NULL,
	[Wydanie] [datetime] NOT NULL,
	[TytulKod1] [varchar](32) NOT NULL,
	[TytulKod2] [varchar](32) NULL,
	[Email] [varchar](255) NULL,
	[SprawdzanyStatus] [varchar](1) NOT NULL,
	[SprawdzanyAdresIp] [varchar](24) NULL,
	[SprawdzanyCzas] [datetime] NULL,
	[SprawdzanyWynik] [varchar](24) NULL,
	[PobranyPdfStatus] [varchar](1) NOT NULL,
	[PobranyPdfAdresIp] [varchar](24) NULL,
	[PobranyPdfCzas] [datetime] NULL,
	[WydanieId] [int] NULL,
 CONSTRAINT [PK_i3_ImportKod] PRIMARY KEY CLUSTERED 
(
	[ImportKodId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[i3_ImportKod]  WITH CHECK ADD  CONSTRAINT [FK_i3_ImportKod_i3_ImportKodPlik1] FOREIGN KEY([ImportKodPlikId])
REFERENCES [dbo].[i3_ImportKodPlik] ([ImportKodPlikId])
GO
ALTER TABLE [dbo].[i3_ImportKod] CHECK CONSTRAINT [FK_i3_ImportKod_i3_ImportKodPlik1]
GO
ALTER TABLE [dbo].[i3_ImportKod] ADD  CONSTRAINT [DF__i3_Import__Spraw__363DB7A5]  DEFAULT ('n') FOR [SprawdzanyStatus]
GO
ALTER TABLE [dbo].[i3_ImportKod] ADD  CONSTRAINT [DF__i3_Import__Pobra__3731DBDE]  DEFAULT ('n') FOR [PobranyPdfStatus]
GO

CREATE TABLE [dbo].[i3_ImportKodTytul](
	[TytulKod1] [varchar](32) NOT NULL,
	[TytulKod2] [varchar](32) NULL,
	[GazetaId] [int] NOT NULL,
	[DodatekId] [int] NULL,
 CONSTRAINT [PK_i3_ImportKodTytul] PRIMARY KEY CLUSTERED 
(
	[TytulKod1] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE VIEW [dbo].[i3_view_rpt_1005_eImportowane]
AS
SELECT
dbo.i3_ImportKod.ImportKodId, 
dbo.i3_ImportKod.Kod, 
dbo.i3_ImportKod.TytulKod1, 
dbo.i3_ImportKod.TytulKod2, 
dbo.i3_ImportKod.Email, 
dbo.i3_ImportKod.PobranyPdfStatus, 
dbo.i3_ImportKod.PobranyPdfAdresIp, 
dbo.i3_ImportKod.PobranyPdfCzas, 
dbo.i3_Wydanie.WydanieId ,
dbo.i3_Wydanie.WydanieOpis, 
dbo.i3_Wydanie.WydaniePlik,
dbo.i3_Wydanie.WydanieAktywne,
dbo.i3_Wydanie.WydanieData,
dbo.i3_Wydanie.WydanieJedynka, 
dbo.i3_Wydanie.DodatekId, 
dbo.i3_GazetaTyp.GazetaTypNazwa , 
dbo.i3_GazetaTyp.GazetaTypId , 
dbo.i3_Gazeta.GazetaId,
dbo.i3_Gazeta.GazetaNazwa,
dbo.i3_Gazeta.GazetaSkrot 
FROM
dbo.i3_Wydanie INNER JOIN
dbo.i3_Gazeta ON dbo.i3_Wydanie.GazetaId = dbo.i3_Gazeta.GazetaId INNER JOIN
dbo.i3_GazetaTyp ON dbo.i3_Gazeta.GazetaTypId = dbo.i3_GazetaTyp.GazetaTypId INNER JOIN
dbo.i3_ImportKod ON dbo.i3_Wydanie.WydanieId = dbo.i3_ImportKod.WydanieId
GO

