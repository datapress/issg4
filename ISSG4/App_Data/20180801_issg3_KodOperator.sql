alter table [i3_Kod] add KodOperator varchar(1)
go

update i3_kod set KodOperator = 'm' where KodStatus = '1' 
go

ALTER PROCEDURE [dbo].[i3_AddKod]
@crkod nvarchar (20) ,
@issgwydaniaid int ,
@crczas datetime ,
@status char (1),
@crip nvarchar(15),
@operator nvarchar(1)
AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO dbo.i3_Kod (KodKod, WydanieId, KodCzas, KodStatus, KodIP, KodOperator)
	VALUES(@crkod, @issgwydaniaid, @crczas, @status, @crip, @operator)

END
GO

ALTER VIEW [dbo].[i3_view_rpt_1001_eKomisy]
AS
SELECT	dbo.i3_Kod.KodCzas,
		CONVERT(VARCHAR(10),dbo.i3_Kod.KodCzas,105) as KodData,
		dbo.i3_Kod.KodKod,
		dbo.i3_Gazeta.GazetaNazwa,
		dbo.i3_Wydanie.WydanieId, 
		dbo.i3_Wydanie.WydanieData,
		dbo.i3_Wydanie.WydanieOpis, 
		dbo.i3_Kod.KodIP,
		dbo.i3_Kod.KodOperator
FROM	dbo.i3_Kod INNER JOIN
		dbo.i3_Wydanie ON dbo.i3_Kod.WydanieId = dbo.i3_Wydanie.WydanieId  and i3_Kod.KodStatus='1' INNER JOIN
		dbo.i3_Gazeta ON dbo.i3_Wydanie.GazetaId = dbo.i3_Gazeta.GazetaId
GO



