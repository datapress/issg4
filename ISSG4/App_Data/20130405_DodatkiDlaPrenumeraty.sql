﻿ALTER PROCEDURE [dbo].[i3_getDodatkiDlaWydania] 
	@WydanieId int,
	@UserId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

if (@UserId is null) begin

	SELECT     wd.WydanieId, wd.WydanieOpis, wd.WydaniePlik, wd.WydanieAktywne,
			   wd.WydanieData, wd.WydanieJedynka, wd.GazetaId, d.DodatekId, 
			   d.DodatekNazwa, d.DodatekSkrot, d.DodatekAktywny, 0 as wNaDzial
		FROM      i3_Wydanie AS wd INNER JOIN
				  i3_Dodatek AS d ON wd.DodatekId = d.DodatekId INNER JOIN
				  i3_GazetaDodatek AS gd ON d.DodatekId = gd.DodatekId INNER JOIN
				  i3_Gazeta AS g ON g.GazetaId = gd.GazetaId INNER JOIN
				  i3_Wydanie AS w ON w.GazetaId = g.GazetaId AND wd.WydanieData = w.WydanieData
		WHERE     (w.WydanieId = @WydanieId) AND (wd.WydanieAktywne = 1)
	
end else begin

	SELECT     wd.WydanieId, wd.WydanieOpis, wd.WydaniePlik, wd.WydanieAktywne,
			   wd.WydanieData, wd.WydanieJedynka, wd.GazetaId, d.DodatekId, 
			   d.DodatekNazwa, d.DodatekSkrot, d.DodatekAktywny,
			   (
					/*select count(n.WydanieId)
						from i3_NaDzial as n
							inner join i3_ZamowieniePozycja as zp on zp.ZamowieniePozycjaId = n.ZamowieniePozycjaId
							inner join i3_Zamowienie as z on z.ZamowienieId = zp.ZamowienieId
						where WydanieId = wd.WydanieId and
							z.issg_user_id = @UserId*/
						select count(n.ZamowieniePozycjaId)
						from i3_NaDzial as n
							inner join i3_ZamowieniePozycja as zp on zp.ZamowieniePozycjaId = n.ZamowieniePozycjaId
							inner join i3_ZamowienieDodatek as zd on zd.ZamowieniePozycjaId = zp.ZamowieniePozycjaId
							inner join i3_Zamowienie as z on z.ZamowienieId = zp.ZamowienieId and d.DodatekId = zd.DodatekId
						where WydanieId = wd.WydanieId and	z.issg_user_id = @UserId
							
			   ) as wNaDzial
		FROM      i3_Wydanie AS wd INNER JOIN
				  i3_Dodatek AS d ON wd.DodatekId = d.DodatekId INNER JOIN
				  i3_GazetaDodatek AS gd ON d.DodatekId = gd.DodatekId INNER JOIN
				  i3_Gazeta AS g ON g.GazetaId = gd.GazetaId INNER JOIN
				  i3_Wydanie AS w ON w.GazetaId = g.GazetaId AND wd.WydanieData = w.WydanieData
		WHERE     (w.WydanieId = @WydanieId) AND (wd.WydanieAktywne = 1)

end
	
	
END

