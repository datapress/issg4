/* oznaczenie prenumeraty plus - prenumeraty z dostepem do wszystkich dodatk�w no-limit*/

CREATE TABLE [dbo].[i3_ProduktStandard](
	[ProduktStandardId] [int] IDENTITY(1,1) NOT NULL,
	[ProduktStandardNazwa] [nvarchar](100) NOT NULL,
 CONSTRAINT [PK_ProduktStandard] PRIMARY KEY CLUSTERED 
(
	[ProduktStandardId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

alter table i3_produkt add ProduktStandardId int 
go

update i3_produkt set ProduktStandardId = 1
go


--Standard
--Plus

--10	1	0	Prenumerata miesi�czna Plus	1	2

ALTER TABLE [dbo].i3_produkt  WITH CHECK ADD  CONSTRAINT [FK_i3_Produkt_i3_ProduktStandard] FOREIGN KEY([ProduktStandardId])
REFERENCES [dbo].[i3_ProduktStandard] ([ProduktStandardId])
GO
ALTER TABLE [dbo].i3_produkt CHECK CONSTRAINT [FK_i3_Produkt_i3_ProduktStandard]
GO



ALTER VIEW [dbo].[i3_viewCennikJoined]
AS
SELECT     TOP (100) PERCENT dbo.i3_Cennik.CennikNetto, dbo.i3_Cennik.CennikBrutto, dbo.i3_Cennik.CennikVat, dbo.i3_CennikNaglowek.CennikNaglowekNazwa, 
                      dbo.i3_CennikNaglowek.CennikNaglowekAktywny, dbo.i3_CennikNaglowek.CennikNaglowekWaznyOd, dbo.i3_Gazeta.GazetaNazwa, dbo.i3_Gazeta.GazetaSkrot, 
                      dbo.i3_RodzajOkresu.RodzajOkresuNazwa, dbo.i3_RodzajOkresu.RodzajOkresuMiesiac, dbo.i3_RodzajOkresu.RodzajOkresuDzien, dbo.i3_Produkt.ProduktNazwa, 
                      dbo.i3_ProduktRodzaj.ProduktRodzajArchiwalne, dbo.i3_ProduktRodzaj.ProduktRodzajBiezace, dbo.i3_ProduktRodzaj.ProduktRodzajNazwa, 
                      dbo.i3_Produkt.ProduktIlosc, dbo.i3_Gazeta.GazetaBezplatna, dbo.i3_Gazeta.GazetaId, dbo.i3_RodzajOkresu.RodzajOkresuId, dbo.i3_Produkt.ProduktId, 
                      dbo.i3_ProduktRodzaj.ProduktRodzajId, dbo.i3_CennikNaglowek.CennikNaglowekId, dbo.i3_Cennik.CennikId, dbo.i3_Vat.VatProc, dbo.i3_Vat.VatAktywny, 
                      dbo.i3_Vat.VatFiskalKod, dbo.i3_Vat.VatId, dbo.i3_ProduktRodzaj.ProduktRodzajPojedyncze,
					  dbo.i3_ProduktRodzaj.ProduktRodzajReklamacja, dbo.i3_ProduktStandard.ProduktStandardNazwa
FROM         dbo.i3_Cennik INNER JOIN
                      dbo.i3_CennikNaglowek ON dbo.i3_Cennik.CennikNaglowekId = dbo.i3_CennikNaglowek.CennikNaglowekId INNER JOIN
                      dbo.i3_Gazeta ON dbo.i3_Cennik.GazetaId = dbo.i3_Gazeta.GazetaId and dbo.i3_Gazeta.GazetaAktywny = 1 JOIN
                      dbo.i3_Produkt ON dbo.i3_Cennik.ProduktId = dbo.i3_Produkt.ProduktId INNER JOIN
                      dbo.i3_RodzajOkresu ON dbo.i3_Produkt.RodzajOkresuId = dbo.i3_RodzajOkresu.RodzajOkresuId INNER JOIN
                      dbo.i3_ProduktRodzaj ON dbo.i3_Produkt.ProduktRodzajId = dbo.i3_ProduktRodzaj.ProduktRodzajId INNER JOIN
					  dbo.i3_ProduktStandard ON dbo.i3_produkt.ProduktStandardId = dbo.i3_ProduktStandard.ProduktStandardId INNER JOIN
                      dbo.i3_Vat ON dbo.i3_Cennik.VatId = dbo.i3_Vat.VatId
WHERE     (dbo.i3_CennikNaglowek.CennikNaglowekWaznyOd =
                          (SELECT     MAX(CennikNaglowekWaznyOd) AS Expr1
                            FROM          dbo.i3_CennikNaglowek AS i3_CennikNaglowek_1
                            WHERE      (CennikNaglowekAktywny = 1)))
ORDER BY dbo.i3_CennikNaglowek.CennikNaglowekWaznyOd
go




ALTER PROCEDURE [dbo].[i3_getDodatkiDlaWydania] 
	@WydanieId int,
	@UserId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

if (@UserId is null) begin

	SELECT     wd.WydanieId, wd.WydanieOpis, wd.WydaniePlik, wd.WydanieAktywne,
			   wd.WydanieData, wd.WydanieJedynka, wd.GazetaId, d.DodatekId, 
			   d.DodatekNazwa, d.DodatekSkrot, d.DodatekAktywny, 0 as wNaDzial
		FROM      i3_Wydanie AS wd INNER JOIN
				  i3_Dodatek AS d ON wd.DodatekId = d.DodatekId INNER JOIN
				 -- i3_GazetaDodatek AS gd ON d.DodatekId = gd.DodatekId INNER JOIN
				 -- i3_Gazeta AS g ON g.GazetaId = gd.GazetaId INNER JOIN
				  i3_Wydanie AS w ON wd.WydanieData = w.WydanieData INNER JOIN
				  i3_Gazeta AS g ON w.GazetaId = g.GazetaId AND  g.GazetaId = W.GazetaId 
		WHERE     (w.WydanieId = @WydanieId) AND (wd.WydanieAktywne = 1)
	
end else begin

	SELECT     wd.WydanieId, wd.WydanieOpis, wd.WydaniePlik, wd.WydanieAktywne,
			   wd.WydanieData, wd.WydanieJedynka, wd.GazetaId, d.DodatekId, 
			   d.DodatekNazwa, d.DodatekSkrot, d.DodatekAktywny,
			   (
			    select dbo.getDodatekNadzial (@WydanieId, @UserId, d.DodatekId)
 			   ) as wNaDzial
		FROM      i3_Wydanie AS wd INNER JOIN
				  i3_Dodatek AS d ON wd.DodatekId = d.DodatekId INNER JOIN
				 -- i3_GazetaDodatek AS gd ON d.DodatekId = gd.DodatekId INNER JOIN
				 -- i3_Gazeta AS g ON g.GazetaId = gd.GazetaId INNER JOIN
				  i3_Wydanie AS w ON wd.WydanieData = w.WydanieData INNER JOIN
				  i3_Gazeta AS g ON w.GazetaId = g.GazetaId AND  g.GazetaId = W.GazetaId 
		WHERE     (w.WydanieId = @WydanieId) AND (wd.WydanieAktywne = 1)

end

END
go


ALTER function [dbo].[getDodatekNadzial](
 @WydanieId int,
 @UserID integer,
 @DodatekId integer
  ) 
returns integer
AS
begin
  declare @li_start integer
  declare @li_temp integer
  declare @li_inny_dod integer
  declare @li_prenum_plus integer;
  
  /* najpierw sprawdzamy czy jest prenum plus, wtedy nadzial dodatku jest jeden */
  select @li_prenum_plus = COUNT(n.WydanieId) from i3_NaDzial n 
  inner join i3_zamowieniepozycja zp on n.ZamowieniePozycjaId = zp.zamowieniepozycjaid
  inner join i3_Zamowienie z on zp.ZamowienieId = z.ZamowienieId
  inner join i3_Cennik c on c.CennikId = zp.CennikId
  inner join i3_Produkt p on p.ProduktId = c.ProduktId
  where p.produktstandardid=2 and n.WydanieId = @WydanieId and z.issg_user_id = @UserID;
  if @li_prenum_plus > 0 
  begin
	return 1;
  end
  /*-----------------------------*/
  --c wybrany dodatek, jest nadzia� dodatku
  select @li_start = count(n.ZamowieniePozycjaId)
			from i3_NaDzial as n
							inner join i3_ZamowieniePozycja as zp on zp.ZamowieniePozycjaId = n.ZamowieniePozycjaId
							inner join i3_ZamowienieDodatek as zd on zd.ZamowieniePozycjaId = zp.ZamowieniePozycjaId
							inner join i3_dodatek d on zd.dodatekid = d.dodatekid
							inner join i3_Zamowienie as z on z.ZamowienieId = zp.ZamowienieId 
							inner join i3_wydanie w on w.wydanieid=n.wydanieid --and w.dodatekid = zd.dodatekid
		where z.issg_user_id = @UserId and d.dodatekid = @DodatekId and n.WydanieId = @WydanieId;
  
  --a brak nadzia�u dodatku, sprawdz czy jest nadzial dla innego dodatku w pozycji zlecenia. jesli jest, to 0 jesli brak to 1
  if @li_start = 0 
  begin
	select @li_temp = count(n.ZamowieniePozycjaId) -- czy jest dla wydania nadzial
			from i3_NaDzial as n
							inner join i3_ZamowieniePozycja as zp on zp.ZamowieniePozycjaId = n.ZamowieniePozycjaId
							--inner join i3_ZamowienieDodatek as zd on zd.ZamowieniePozycjaId = zp.ZamowieniePozycjaId
							--inner join i3_dodatek d on zd.dodatekid = d.dodatekid
							inner join i3_Zamowienie as z on z.ZamowienieId = zp.ZamowienieId 
							inner join i3_wydanie w on w.wydanieid=n.wydanieid --and w.dodatekid = zd.dodatekid
		where z.issg_user_id = @UserId and n.WydanieId = @WydanieId;
		
		select @li_inny_dod = count(n.ZamowieniePozycjaId) -- czy innny dodatek ma nadzial
			from i3_NaDzial as n
							inner join i3_ZamowieniePozycja as zp on zp.ZamowieniePozycjaId = n.ZamowieniePozycjaId
							inner join i3_ZamowienieDodatek as zd on zd.ZamowieniePozycjaId = zp.ZamowieniePozycjaId
							inner join i3_dodatek d on zd.dodatekid = d.dodatekid
							inner join i3_Zamowienie as z on z.ZamowienieId = zp.ZamowienieId 
							inner join i3_wydanie w on w.wydanieid=n.wydanieid --and w.dodatekid = zd.dodatekid
		where z.issg_user_id = @UserId and d.dodatekid <> @DodatekId and n.WydanieId = @WydanieId;
  end
    
  if @li_start = 0 and @li_temp > 0 and @li_inny_dod = 0
  begin 
	set @li_start = 1
  end
  
							 
  --b niewybrany dodatek
if @li_start is null
   
  begin 
	set @li_start = 0
  end
  
  
  return @li_start
end
go

