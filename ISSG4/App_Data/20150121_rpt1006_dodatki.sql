CREATE VIEW [dbo].[i3_view_rpt_1006_ReczneAktywacje]
AS
SELECT	dbo.i3_Gazeta.GazetaNazwa,
		dbo.i3_Zamowienie.ZamowienieId, 
		dbo.i3_ZamowieniePozycja.ZamowieniePozycjaId, 
		dbo.i3_ZamowieniePozycja.ZamowieniePozycjaOd, 
		dbo.i3_ZamowieniePozycja.ZamowieniePozycjaDo, 
		dbo.i3_ZamowieniePozycja.ZamowieniePozycjaNetto, 
		dbo.i3_ZamowieniePozycja.ZamowieniePozycjaBrutto, 
		dbo.i3_ZamowieniePozycja.ZamowieniePozycjaVat, 
        Users.UserName,   
        Users.Imie,   
        Users.Nazwisko,   
        Users.salt,   
        Users.Adres,   
        Users.Miejscowosc,   
        Users.Kod,   
        Users.Email,   
        Users.czy_firma,   
        Users.ulica,   
        Users.budynek,   
        Users.mieszkanie,   
        Users.telefon,   
        Users.kraj,   
        Users.nip,   
        Users.nazwa_firmy,   
        Users.kod_external,   
        Users.klient_czy_kredyt,   
        Users.czy_faktura,   
        c_faktura_username = CASE Users.czy_faktura when 1 then Users.UserName when 0 then '<bez faktury>' END,
        getdate() as PlatnoscCzas,
        dbo.i3_Produkt.ProduktNazwa,
        CONVERT(VARCHAR(10),getdate(),105) as PlatnoscData,
        convert ( varchar(128), dbo.i3_Produkt.ProduktNazwa ) +  
        convert ( varchar(128), dbo.i3_ZamowieniePozycja.ZamowieniePozycjaOd ) +
        convert ( varchar(128), dbo.i3_ZamowieniePozycja.ZamowieniePozycjaDo ) as Grupa4,
        RodzajOkresuMiesiac,
        RodzajOkresuDzien
        ,dbo.i3_Dodatek.DodatekNazwa
        ,dbo.i3_Dodatek.DodatekSkrot
FROM	(dbo.i3_Gazeta INNER JOIN dbo.i3_Cennik ON dbo.i3_Gazeta.GazetaId = dbo.i3_Cennik.GazetaId INNER JOIN
		dbo.i3_ZamowieniePozycja ON dbo.i3_Cennik.CennikId = dbo.i3_ZamowieniePozycja.CennikId  INNER JOIN
		dbo.i3_Zamowienie ON dbo.i3_Zamowienie.ZamowienieId = dbo.i3_ZamowieniePozycja.ZamowienieId  INNER JOIN
		dbo.Users ON dbo.Users.ispo_user_id = dbo.i3_Zamowienie.issg_user_id 
		--INNER JOIN dbo.i3_Platnosc on dbo.i3_Platnosc.ZamowienieId = dbo.i3_Zamowienie.ZamowienieId 
		INNER JOIN dbo.i3_Produkt on dbo.i3_Cennik.ProduktId = dbo.i3_Produkt.ProduktId INNER JOIN
		dbo.i3_RodzajOkresu on dbo.i3_Produkt.RodzajOkresuId = dbo.i3_RodzajOkresu.RodzajOkresuId) 
		
		left outer join 
		( dbo.i3_Dodatek inner join dbo.i3_ZamowienieDodatek on dbo.i3_Dodatek.DodatekId = dbo.i3_ZamowienieDodatek.DodatekId) 
		 on dbo.i3_ZamowieniePozycja.ZamowieniePozycjaId = dbo.i3_ZamowienieDodatek.ZamowieniePozycjaId
		 where zamowienierecznaaktywacja=1

GO

USE [ISSG3_KS]
GO

/****** Object:  UserDefinedFunction [dbo].[getDodatekNadzial]    Script Date: 01/21/2015 13:11:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE function [dbo].[getDodatekNadzial](
 @WydanieId int,
 @DodatekWydanieId int,
 @ZpId integer,
 @ZId integer,
 @UserID integer,
 @DodatekId integer
  ) 
returns integer
AS
begin
  declare @li_start integer
  declare @li_temp integer
  --c wybrany dodatek, jest nadzia� dodatku
  select @li_start = count(n.ZamowieniePozycjaId)
			from i3_NaDzial as n
							inner join i3_ZamowieniePozycja as zp on zp.ZamowieniePozycjaId = n.ZamowieniePozycjaId
							inner join i3_ZamowienieDodatek as zd on zd.ZamowieniePozycjaId = zp.ZamowieniePozycjaId
							inner join i3_dodatek d on zd.dodatekid = d.dodatekid
							inner join i3_Zamowienie as z on z.ZamowienieId = zp.ZamowienieId 
							inner join i3_wydanie w on w.wydanieid=n.wydanieid --and w.dodatekid = zd.dodatekid
		where z.issg_user_id = @UserId and w.dodatekid = @DodatekId 
  
  --a brak nadzia�u dodatku, sprawdz czy jest nadzial dla innego dodatku w pozycji zlecenia. jesli jest, to 0 jesli brak to 1
  if @li_start = 0 
  begin
	select @li_temp = count(n.ZamowieniePozycjaId)
			from i3_NaDzial as n
							inner join i3_ZamowieniePozycja as zp on zp.ZamowieniePozycjaId = n.ZamowieniePozycjaId
							inner join i3_ZamowienieDodatek as zd on zd.ZamowieniePozycjaId = zp.ZamowieniePozycjaId
							inner join i3_dodatek d on zd.dodatekid = d.dodatekid
							inner join i3_Zamowienie as z on z.ZamowienieId = zp.ZamowienieId 
							inner join i3_wydanie w on w.wydanieid=n.wydanieid --and w.dodatekid = zd.dodatekid
		where z.issg_user_id = @UserId --and w.dodatekid = @DodatekId
  end
  
  if @li_start = 0 and @li_temp = 0
   
  begin 
	set @li_start = 1
  end
							 
  --b niewybrany dodatek

  
  
  return @li_start
end
GO

ALTER PROCEDURE [dbo].[i3_getDodatkiDlaWydania] 
	@WydanieId int,
	@UserId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

if (@UserId is null) begin

	SELECT     wd.WydanieId, wd.WydanieOpis, wd.WydaniePlik, wd.WydanieAktywne,
			   wd.WydanieData, wd.WydanieJedynka, wd.GazetaId, d.DodatekId, 
			   d.DodatekNazwa, d.DodatekSkrot, d.DodatekAktywny, 0 as wNaDzial
		FROM      i3_Wydanie AS wd INNER JOIN
				  i3_Dodatek AS d ON wd.DodatekId = d.DodatekId INNER JOIN
				  i3_GazetaDodatek AS gd ON d.DodatekId = gd.DodatekId INNER JOIN
				  i3_Gazeta AS g ON g.GazetaId = gd.GazetaId INNER JOIN
				  i3_Wydanie AS w ON w.GazetaId = g.GazetaId AND wd.WydanieData = w.WydanieData
		WHERE     (w.WydanieId = @WydanieId) AND (wd.WydanieAktywne = 1)
	
end else begin

	SELECT     wd.WydanieId, wd.WydanieOpis, wd.WydaniePlik, wd.WydanieAktywne,
			   wd.WydanieData, wd.WydanieJedynka, wd.GazetaId, d.DodatekId, 
			   d.DodatekNazwa, d.DodatekSkrot, d.DodatekAktywny,
			   (
						select dbo.getDodatekNadzial (@WydanieId, d.DodatekId, zp.ZamowieniePozycjaId,z.zamowienieid, @UserId,d.dodatekid)
						from i3_NaDzial as n
							inner join i3_ZamowieniePozycja as zp on zp.ZamowieniePozycjaId = n.ZamowieniePozycjaId
							inner join i3_Zamowienie as z on z.ZamowienieId = zp.ZamowienieId
						where @WydanieId = n.WydanieId and
							z.issg_user_id = @UserId
							
							
			   ) 
			   as wNaDzial
		FROM      i3_Wydanie AS wd INNER JOIN
				  i3_Dodatek AS d ON wd.DodatekId = d.DodatekId INNER JOIN
				  i3_GazetaDodatek AS gd ON d.DodatekId = gd.DodatekId INNER JOIN
				  i3_Gazeta AS g ON g.GazetaId = gd.GazetaId INNER JOIN
				  i3_Wydanie AS w ON w.GazetaId = g.GazetaId AND wd.WydanieData = w.WydanieData
		WHERE     (w.WydanieId = @WydanieId) AND (wd.WydanieAktywne = 1)

end

	
END


GO







