
/*2012-07-05 Marcin
	Procdury do wpis�w rejestracji pobra� */

CREATE PROCEDURE [dbo].[i3_addPobranie]
@ispouserid int,
@wydanieid int ,
@pobraniedata datetime ,
@pobranieip nvarchar(45),
@zamowieniepozycjaid int
AS
BEGIN

INSERT INTO dbo.i3_Pobranie
           ([WydanieId]
           ,[ispo_user_id]
           ,[PobranieData]
           ,[PobranieIp]
           ,[ZamowieniePozycjaId])
     VALUES
           (@wydanieid
           ,@ispouserid
           ,@pobraniedata
           ,@pobranieip
           ,@zamowieniepozycjaid)
END
GO

CREATE PROCEDURE [dbo].[i3_addPobranieDarmowe]
@wydanieid int ,
@pobranieczas datetime ,
@pobranieip nvarchar(45)
AS
BEGIN
INSERT INTO dbo.i3_PobranieDarmowe
           ([WydanieId]
           ,[PobranieDarmoweCzas]
           ,[PobranieDarmoweIp])
     VALUES
           (@wydanieid
           ,@pobranieczas
           ,@pobranieip)
END
GO




