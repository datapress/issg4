
CREATE TABLE [dbo].[i3_Post2](
	[PostId] [bigint] IDENTITY(1,1) NOT NULL,
	[id] [nvarchar](50) NULL, -- nasze id w dotpay
	[operation_number] [nvarchar](24) NULL, --^M\d{4,5}\-\d{4,5}$ Przyk�ad: operation_number = M1234-56789
	[operation_type] [nvarchar](24) NULL, -- payment
	[operation_status] [nvarchar](24) NULL, -- completed/rejected
	[operation_amount] [nvarchar](24) NULL, --177.27 z kropk�
	[operation_currency] [nvarchar](3) NULL,  --PLN
	[operation_original_amount] [nvarchar](128) NULL, --177.27
	[operation_original_currency] [nvarchar](128) NULL, --PLN
	[control] [nvarchar](128) NULL, --z submita
	[email] [nvarchar](256) NULL, --email p�ac�cego
	[signature] [nvarchar](256) NULL, -- wyliczony podpis
	[description] [nvarchar](128) NULL, -- opis transakcji
	[wpis_error] [nvarchar](512) NULL, -- z kontrolera
	[wpis_dataczas] [datetime] NULL, -- czas inserta
 CONSTRAINT [PK_i3_Post2] PRIMARY KEY CLUSTERED 
(
	[PostId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE TRIGGER [dbo].[iSetZaplaconoControl2] ON [dbo].[i3_Post2]
FOR INSERT
AS
declare @zamid int;
declare @zamid_n numeric;
declare @desc nvarchar(4000);
declare @control nvarchar(2000);
declare @operation_number nvarchar(4000);
declare @status_string nvarchar(24);
declare @kwota numeric (18,2);
declare @kwota_string nvarchar (20);
declare @zam_payment_control nvarchar(2000);
declare @ispo_user_id int;
--
select @control = control, @desc = description, @operation_number = operation_number, @kwota_string = operation_amount, @status_string = operation_status  from inserted;
select @kwota = cast ( @kwota_string as numeric (18,2) );

select	@zam_payment_control	= PaymentControl,		--pole control
		@zamid					= ZamowienieId,		--id ogl albo koszyka
		@ispo_user_id			= ispo_user_id		--id usera
from	dbo.i3_Payment
where	PaymentControl = @control

if @zam_payment_control is null --brak controla - nie ma co zapisywac??
begin
	return;
end;

if @status_string = 'completed'
begin
	update i3_Zamowienie
	set ZamowienieOplacone = 1
	where ZamowienieId = @zamid;
	
	--
	insert into i3_Platnosc (ispo_user_id, ZamowienieId, PlatnoscCzas, PlatnoscOpis, PlatnoscTransId, PlatnoscKwota, PlatnoscStatus)
	values ( @ispo_user_id ,  @zamid , CURRENT_TIMESTAMP, @desc, @operation_number, @kwota, 'Wykonana' )
end

-- 3 to status odmowna z allpaja
if @status_string = 'rejected'
begin
	--zdjecie platnosci
	update i3_Zamowienie
	set ZamowienieOplacone = 0
	where ZamowienieId = @zamid;
	
	-- insert
	insert into i3_Platnosc (ispo_user_id, ZamowienieId, PlatnoscCzas, PlatnoscOpis, PlatnoscTransId, PlatnoscKwota, PlatnoscStatus)
	values ( @ispo_user_id ,  @zamid , CURRENT_TIMESTAMP, @desc, @operation_number, @kwota, 'Odmowa' )
end

-- 1 to status nowa z allpaja
if @status_string = 'new'
begin
	update i3_Zamowienie
	set ZamowienieOplacone = 0
	where ZamowienieId = @zamid;
	
	-- insert
	insert into i3_Platnosc (ispo_user_id, ZamowienieId, PlatnoscCzas, PlatnoscOpis, PlatnoscTransId, PlatnoscKwota, PlatnoscStatus)
	values ( @ispo_user_id ,  @zamid , CURRENT_TIMESTAMP, @desc, @operation_number, @kwota, 'Nowa' )
end


GO





