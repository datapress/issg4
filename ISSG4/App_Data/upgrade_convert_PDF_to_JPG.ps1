# PowerShell example to list .dll in Windows folder and sub-folders
Write-Host `n "Waiting ...."
$Files = gci "D:\IssgPdfStore\" -recurse | Where {$_.extension -eq ".pdf"}
#$Files = gci "D:\TestStore\" -recurse | Where {$_.extension -eq ".pdf"}
Clear-Host
#$Files | Format-Table Name, CreationTime, Length -auto
$count = $Files.count
$i = 0
foreach ($File in $Files) {
	$i += 1
	$newFile = $File.fullname -replace '\.[pP][dD][fF]', '.jpg'
	$pdfFile = $File.fullname
	[int]$proc = ($i / $count) * 100
	#write-host "$proc% [$i/$count] convert -density 300x300 -resize x1200 -quality 60% $pdfFile[0] $newFile"
	write-host "$proc%`t[$i/$count]`t$pdfFile -> .jpg"
	if (Test-Path $newFile){
		write-host "`t`t`tPomijam. Plik ju� istnieje." -foregroundcolor "yellow"
	} else {
		$cmd1 = "convert -density 300x300 " + $pdfFile + "[0] " + $newFile
		$cmd2 = "convert -resize x1200 -quality 80% " + $newFile + " " + $newFile
		Invoke-Expression "$cmd1" | out-Null
		Invoke-Expression "$cmd2" | out-Null
		write-host "`t`t`tOK" -foregroundcolor "green"
	}
}
