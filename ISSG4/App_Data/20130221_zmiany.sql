alter table i3_Gazeta add GazetaAktywny bit not null default 1
go 

ALTER PROCEDURE [dbo].[i3_getNoweWydaniaByType]
	-- Add the parameters for the stored procedure here
	@GazetaTypId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	select g.GazetaNazwa, w.WydanieData, w.WydanieOpis, w.GazetaId, w.WydanieId, w.WydanieJedynka
	from i3_Wydanie as w
	inner join (
		SELECT GazetaId, MAX(WydanieData) AS WydanieData 
		FROM i3_Wydanie 
		WHERE (WydanieAktywne = 1) 
		GROUP BY GazetaId) AS n ON w.GazetaId = n.GazetaId AND w.WydanieData = n.WydanieData
	inner join
		i3_Gazeta as g on w.GazetaId = g.GazetaId
	where (g.GazetaTypId = @GazetaTypId and g.GazetaAktywny=1)
	order by g.GazetaId
END
go

/* nasza warmia */
update i3_Dodatek set DodatekAktywny = 0 where DodatekId=1
go


update i3_Gazeta set GazetaAktywny = 0 where GazetaId = 12
go


