CREATE TABLE [dbo].[webapi_action](
	[action_id] [int] IDENTITY(1,1) NOT NULL,
	[ispo_user_id] [int] NULL,
	[action_typ_id] [int] NOT NULL,
	[aw_user_id] [nvarchar](255) NULL,
	[action_xml_content] [nvarchar](2048) NOT NULL,
	[action_timestamp] [datetime] NOT NULL,
	[action_system] [varchar](255) NOT NULL,
	[action_source_ip] [varchar](255) NULL,
	[action_confirmed] [bit] NULL,
	[action_info] [varchar](2048) NULL,
	[action_status] [varchar](50) NULL,
	[action_parent_id] [int] NULL,
 CONSTRAINT [PK_webapi_action] PRIMARY KEY CLUSTERED 
(
	[action_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = OFF, ALLOW_PAGE_LOCKS  = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[webapi_action]  WITH CHECK ADD  CONSTRAINT [FK_webapi_action_typ] FOREIGN KEY([action_typ_id])
REFERENCES [dbo].[webapi_action_typ] ([action_typ_id])
GO

ALTER TABLE [dbo].[webapi_action] CHECK CONSTRAINT [FK_webapi_action_typ]
GO

CREATE TABLE [dbo].[webapi_action_response](
	[ar_id] [int] IDENTITY(1,1) NOT NULL,
	[action_id] [int] NOT NULL,
	[ar_xml_content] [nvarchar](2048) NOT NULL,
	[ar_timestamp] [datetime] NOT NULL,
 CONSTRAINT [PK_webapi_action_response] PRIMARY KEY CLUSTERED 
(
	[ar_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[webapi_action_response]  WITH CHECK ADD  CONSTRAINT [FK_webapi_action_response] FOREIGN KEY([action_id])
REFERENCES [dbo].[webapi_action] ([action_id])
GO

ALTER TABLE [dbo].[webapi_action_response] CHECK CONSTRAINT [FK_webapi_action_response]
GO

CREATE TABLE [dbo].[webapi_action_typ](
	[action_typ_id] [int] NOT NULL,
	[action_typ_nazwa] [varchar](50) NOT NULL,
 CONSTRAINT [PK_webapi_action_typ] PRIMARY KEY CLUSTERED 
(
	[action_typ_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[webapi_dotpay_post](
	[wpis_id] [bigint] IDENTITY(1,1) NOT NULL,
	[id] [nvarchar](50) NULL,
	[status] [nvarchar](6) NULL,
	[control] [nvarchar](128) NULL,
	[t_id] [nvarchar](128) NULL,
	[amount] [nvarchar](128) NULL,
	[email] [nvarchar](256) NULL,
	[service] [nvarchar](128) NULL,
	[code] [nvarchar](128) NULL,
	[username] [nvarchar](128) NULL,
	[password] [nvarchar](128) NULL,
	[t_status] [nvarchar](4) NULL,
	[description] [nvarchar](128) NULL,
	[md5] [nvarchar](128) NULL,
	[wpis_error] [nvarchar](512) NULL,
	[wpis_dataczas] [datetime] NULL,
 CONSTRAINT [PK_webapi_dotpay_post] PRIMARY KEY CLUSTERED 
(
	[wpis_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


CREATE TABLE [dbo].[webapi_nopayment](
	[np_id] [int] IDENTITY(1,1) NOT NULL,
	[ispo_user_id] [int] NOT NULL,
	[aw_np_id] [nvarchar](255) NOT NULL,
	[aw_user_id] [nvarchar](255) NOT NULL,
	[np_timestamp] [datetime] NOT NULL,
	[aw_conf_urlc] [varchar](255) NULL,
	[action_confirmed] [bit] NULL,
	[action_status] [varchar](255) NULL,
	[action_info] [varchar](2048) NULL,
 CONSTRAINT [PK_webapi_nopayment] PRIMARY KEY CLUSTERED 
(
	[np_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


CREATE TABLE [dbo].[webapi_nopayment_row](
	[np_row_id] [int] IDENTITY(1,1) NOT NULL,
	[np_id] [int] NOT NULL,
	[wap_produkt_id] [int] NOT NULL,
	[wap_produkt_ilosc_jm] [int] NOT NULL,
	[wap_produkt_wartosc_jm] [decimal](10, 2) NOT NULL,
	[wap_produkt_link] [nvarchar](255) NULL,
	[wap_produkt_link_opis] [nvarchar](255) NOT NULL,
	[ogl_id] [int] NULL,
 CONSTRAINT [PK_webapi_nopayment_row] PRIMARY KEY CLUSTERED 
(
	[np_row_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[webapi_nopayment_row]  WITH CHECK ADD  CONSTRAINT [FK_webapi_nopayment_row_produkt] FOREIGN KEY([wap_produkt_id])
REFERENCES [dbo].[webapi_produkt] ([wap_produkt_id])
GO

ALTER TABLE [dbo].[webapi_nopayment_row] CHECK CONSTRAINT [FK_webapi_nopayment_row_produkt]
GO

ALTER TABLE [dbo].[webapi_nopayment_row]  WITH CHECK ADD  CONSTRAINT [FK_webapi_nopayment_row_webapi_nopayment] FOREIGN KEY([np_id])
REFERENCES [dbo].[webapi_nopayment] ([np_id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[webapi_nopayment_row] CHECK CONSTRAINT [FK_webapi_nopayment_row_webapi_nopayment]
GO



CREATE TABLE [dbo].[webapi_payment](
	[wap_id] [int] IDENTITY(1,1) NOT NULL,
	[ispo_user_id] [int] NOT NULL,
	[aw_payment_id] [nvarchar](255) NOT NULL,
	[aw_user_id] [nvarchar](255) NOT NULL,
	[wap_xml_content] [nvarchar](2048) NOT NULL,
	[wap_timestamp] [datetime] NOT NULL,
	[wap_control] [uniqueidentifier] NOT NULL,
	[wap_kwota] [decimal](10, 2) NULL,
	[aw_conf_url] [varchar](255) NULL,
	[aw_conf_urlc] [varchar](255) NULL,
 CONSTRAINT [PK_webapi_payment] PRIMARY KEY CLUSTERED 
(
	[wap_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[webapi_payment]  WITH CHECK ADD  CONSTRAINT [FK_webapi_payment_users] FOREIGN KEY([ispo_user_id])
REFERENCES [dbo].[Users] ([ispo_user_id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[webapi_payment] CHECK CONSTRAINT [FK_webapi_payment_users]
GO


CREATE TABLE [dbo].[webapi_payment_conf](
	[wap_conf_id] [int] IDENTITY(1,1) NOT NULL,
	[wap_id] [int] NOT NULL,
	[ispo_user_id] [int] NOT NULL,
	[aw_payment_id] [nvarchar](255) NOT NULL,
	[aw_user_id] [nvarchar](255) NOT NULL,
	[wap_conf_timestamp] [datetime] NOT NULL,
	[dotpay_id] [nvarchar](50) NULL,
	[dotpay_status] [nvarchar](6) NULL,
	[dotpay_control] [nvarchar](128) NULL,
	[dotpay_t_id] [nvarchar](128) NULL,
	[dotpay_amount] [nvarchar](128) NULL,
	[dotpay_email] [nvarchar](256) NULL,
	[dotpay_t_status] [nvarchar](4) NULL,
	[dotpay_description] [nvarchar](128) NULL,
	[dotpay_md5] [nvarchar](128) NULL,
	[dotpay_wpis_error] [nvarchar](512) NULL,
	[dotpay_wpis_dataczas] [datetime] NULL,
	[aw_conf_urlc] [varchar](255) NULL,
	[action_confirmed] [bit] NULL,
	[action_status] [varchar](255) NULL,
	[action_info] [varchar](2048) NULL,
 CONSTRAINT [PK_webapi_payment_conf] PRIMARY KEY CLUSTERED 
(
	[wap_conf_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[webapi_payment_conf]  WITH CHECK ADD  CONSTRAINT [FK_webapi_payment] FOREIGN KEY([wap_id])
REFERENCES [dbo].[webapi_payment] ([wap_id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[webapi_payment_conf] CHECK CONSTRAINT [FK_webapi_payment]
GO



CREATE TABLE [dbo].[webapi_payment_row](
	[wap_row_id] [int] IDENTITY(1,1) NOT NULL,
	[wap_id] [int] NOT NULL,
	[wap_produkt_id] [int] NOT NULL,
	[wap_produkt_ilosc_jm] [int] NOT NULL,
	[wap_produkt_wartosc_jm] [decimal](10, 2) NOT NULL,
	[wap_produkt_link] [nvarchar](255) NULL,
	[wap_produkt_link_opis] [nvarchar](255) NOT NULL,
	[ogl_id] [int] NULL,
	[wap_produkt_eksport_ispo_mp2] [char](1) NOT NULL,
 CONSTRAINT [PK_webapi_payment_row] PRIMARY KEY CLUSTERED 
(
	[wap_row_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


ALTER TABLE [dbo].[webapi_payment_row]  WITH CHECK ADD  CONSTRAINT [FK_webapi_payment_row_produkt] FOREIGN KEY([wap_produkt_id])
REFERENCES [dbo].[webapi_produkt] ([wap_produkt_id])
GO

ALTER TABLE [dbo].[webapi_payment_row] CHECK CONSTRAINT [FK_webapi_payment_row_produkt]
GO

ALTER TABLE [dbo].[webapi_payment_row]  WITH CHECK ADD  CONSTRAINT [FK_webapi_payment_row_webapi_payment] FOREIGN KEY([wap_id])
REFERENCES [dbo].[webapi_payment] ([wap_id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[webapi_payment_row] CHECK CONSTRAINT [FK_webapi_payment_row_webapi_payment]
GO

ALTER TABLE [dbo].[webapi_payment_row] ADD  DEFAULT ('t') FOR [wap_produkt_eksport_ispo_mp2]
GO



CREATE TABLE [dbo].[webapi_produkt](
	[wap_produkt_id] [int] NOT NULL,
	[wap_produkt_wartosc_jm] [decimal](10, 2) NOT NULL,
	[wap_produkt_link_default] [nvarchar](255) NULL,
	[wap_produkt_link_opis_default] [nvarchar](255) NULL,
	[wap_produkt_aktywny] [bit] NULL,
	[wap_produkt_nazwa] [nvarchar](255) NOT NULL,
	[wap_produkt_vat] [int] NULL,
	[wap_produkt_ispo_map] [nvarchar](10) NULL,
	[wap_produkt_eksport_ispo_mp2] [char](1) NOT NULL,
 CONSTRAINT [PK_webapi_produkt] PRIMARY KEY CLUSTERED 
(
	[wap_produkt_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


ALTER TABLE [dbo].[webapi_produkt] ADD  CONSTRAINT [DF__webapi_pr__wap_p__271082AE]  DEFAULT ((1)) FOR [wap_produkt_aktywny]
GO

ALTER TABLE [dbo].[webapi_produkt] ADD  CONSTRAINT [DF__webapi_pr__wap_p__4FDD8E17]  DEFAULT ('t') FOR [wap_produkt_eksport_ispo_mp2]
GO

CREATE TABLE [dbo].[webapi_sms_payment](
	[wap_sms_id] [int] IDENTITY(1,1) NOT NULL,
	[ispo_user_id] [int] NOT NULL,
	[aw_payment_sms_id] [nvarchar](255) NOT NULL,
	[aw_user_id] [nvarchar](255) NOT NULL,
	[wap_xml_content_request] [nvarchar](2048) NOT NULL,
	[wap_xml_content_response] [nvarchar](2048) NOT NULL,
	[wap_timestamp] [datetime] NOT NULL,
	[service] [varchar](10) NOT NULL,
	[code] [varchar](50) NOT NULL,
	[rate] [decimal](10, 2) NOT NULL,
	[produkt_id] [int] NOT NULL,
	[produkt_link] [varchar](255) NULL,
	[produkt_link_opis] [varchar](255) NULL,
	[ogl_id] [int] NULL,
	[user_ip] [varchar](128) NULL,
	[shortcode] [varchar](128) NULL,
 CONSTRAINT [PK_webapi_sms_payment] PRIMARY KEY CLUSTERED 
(
	[wap_sms_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[webapi_sms_payment]  WITH CHECK ADD  CONSTRAINT [FK_webapi_sms_payment_users] FOREIGN KEY([ispo_user_id])
REFERENCES [dbo].[Users] ([ispo_user_id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[webapi_sms_payment] CHECK CONSTRAINT [FK_webapi_sms_payment_users]
GO

CREATE TABLE [dbo].[OPCJE](
	[opcje_nazwa] [nvarchar](25) NOT NULL,
	[opcje_wartosc] [nvarchar](256) NOT NULL,
	[opcje_czy_useredit] [char](1) NOT NULL,
	[opcje_czy_uservisible] [char](1) NOT NULL,
	[opcje_info] [nvarchar](255) NULL,
 CONSTRAINT [PK_OPCJE] PRIMARY KEY CLUSTERED 
(
	[opcje_nazwa] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


ALTER TABLE [dbo].[OPCJE] ADD  DEFAULT ('n') FOR [opcje_czy_useredit]
GO

ALTER TABLE [dbo].[OPCJE] ADD  DEFAULT ('n') FOR [opcje_czy_uservisible]
GO


