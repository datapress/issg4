﻿alter table i3_zamowieniepozycja add ZamowieniePozycjaWidoczna char(1) not null default 't'
go
ALTER VIEW [dbo].[i3_viewZamowienieJoined]
AS
SELECT     dbo.i3_Gazeta.GazetaNazwa, dbo.i3_Produkt.ProduktNazwa, dbo.i3_ZamowieniePozycja.ZamowieniePozycjaOd, dbo.i3_ZamowieniePozycja.ZamowieniePozycjaDo, 
                      dbo.i3_Zamowienie.issg_user_id, dbo.i3_Zamowienie.ZamowienieData, dbo.i3_Zamowienie.ZamowienieZablokowane, dbo.i3_Zamowienie.ZamowienieOplacone, 
                      dbo.i3_Zamowienie.ZamowienieRecznaAktywacja, dbo.i3_Zamowienie.ZamowienieNetto, dbo.i3_Zamowienie.ZamowienieBrutto, dbo.i3_Zamowienie.ZamowienieVat, 
                      dbo.i3_Zamowienie.ZamowienieId, dbo.i3_ZamowieniePozycja.ZamowieniePozycjaNetto, dbo.i3_ZamowieniePozycja.ZamowieniePozycjaBrutto, 
                      dbo.i3_ZamowieniePozycja.ZamowieniePozycjaVat, dbo.i3_Gazeta.GazetaId, dbo.i3_ZamowieniePozycja.ZamowieniePozycjaWidoczna,
                      dbo.i3_ZamowieniePozycja.ZamowieniePozycjaId
FROM         dbo.i3_Cennik INNER JOIN
                      dbo.i3_Gazeta ON dbo.i3_Cennik.GazetaId = dbo.i3_Gazeta.GazetaId INNER JOIN
                      dbo.i3_Produkt ON dbo.i3_Cennik.ProduktId = dbo.i3_Produkt.ProduktId INNER JOIN
                      dbo.i3_ZamowieniePozycja ON dbo.i3_Cennik.CennikId = dbo.i3_ZamowieniePozycja.CennikId INNER JOIN
                      dbo.i3_Zamowienie ON dbo.i3_ZamowieniePozycja.ZamowienieId = dbo.i3_Zamowienie.ZamowienieId

GO