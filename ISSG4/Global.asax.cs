﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using NLog;
using System.Web.Security;
using System.Configuration;

namespace ISSG3
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            //filters.Add(new HandleErrorAttribute());
        }

        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                "Przycisk", // Route name
                "Przycisk/{Text}", // URL with parameters
                new { controller = "Przycisk", action = "Index" } // Parameter defaults
            );

            routes.MapRoute(
              "PrzyciskYellow", // Route name
              "Przycisk/Yellow/{Text}", // URL with parameters
              new { controller = "Przycisk", action = "Yellow" } // Parameter defaults
          );

            routes.MapRoute(
                "Wydanie", // Route name
                "Wydanie/{WydanieId}", // URL with parameters
                new { controller = "Wydanie", action = "Wydanie" }, // Parameter defaults
                new { WydanieId = @"\d+" }
            );

            routes.MapRoute(
                "Default", // Route name
                "{controller}/{action}/{id}", // URL with parameters
                new { controller = "Home", action = "Index", id = UrlParameter.Optional } // Parameter defaults
            );

            routes.MapRoute(
            name: "Static404", // Route name
            url:  "Static404/{action}/{id}", // URL with parameters
            defaults:   new { controller = "Error", action = "Static404", id = UrlParameter.Optional } // Parameter defaults
           );


        }

        protected void Application_Start()
        {
            DataAnnotationsModelValidatorProvider.RegisterAdapter(typeof(PasswordStrengthAttribute), typeof(RegularExpressionAttributeAdapter));

            AreaRegistration.RegisterAllAreas();

            RegisterGlobalFilters(GlobalFilters.Filters);
            RegisterRoutes(RouteTable.Routes);

            ModelBinders.Binders.DefaultBinder = new ISSG3.Helpers.DateFiexedCultureModelBinder();

            ViewEngines.Engines.Add(new MvcXaml.XamlImageViewEngine());

            AppSettingsReader apr = new AppSettingsReader();
            string futer = apr.GetValue("layout_footer", typeof(string)).ToString();
            string layout_version = apr.GetValue("layout_version", typeof(string)).ToString();

            Application["layout_footer"] = futer;
            Application["layout_version"] = layout_version;

        }

        protected void Application_Error(object sender, EventArgs e)
        {
            Exception exToCheck = Server.GetLastError();

            if (exToCheck != null)
            {
                Exception ex = exToCheck.GetBaseException();

                HttpException hex = ex as HttpException;
                if (hex != null)
                {
                    int code = hex.GetHttpCode();
                    if (code == 404)
                    {
                        return;
                    }
                }
                
                logger.FatalException("Application Error", ex);
            }
        }

        void Session_Start(object sender, EventArgs e)
        {
            HttpContext.Current.Session.Add("__SessionTracking", string.Empty);
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            
             //Funkcja do debugowania routingu
            /* ISSG2 ROUTE MAP */
            if (Request.Url.ToString().ToLower().Contains("darmowa.aspx") ||
                Request.Url.ToString().ToLower().Contains("archiwalne.aspx") ||
                Request.Url.ToString().ToLower().Contains("gazeta.aspx"))
            {
                if (Request.QueryString["gazeta"] != null)
                {
                    int gaz = 1;
                    if (int.TryParse(Request.QueryString["gazeta"], out gaz))
                    {
                        Context.Response.StatusCode = 301;
                        Context.Response.Redirect("/Home/Archiwum/" + gaz.ToString());
                    }
                    else
                    {
                        Context.Response.StatusCode = 301;
                        Context.Response.Redirect("/");
                    }
                }
                else
                {
                    Context.Response.StatusCode = 301;
                    Context.Response.Redirect("/");
                }
            }

            if (Request.Url.ToString().ToLower().Contains("start.aspx") ||
                Request.Url.ToString().ToLower().Contains("welcome.aspx") ||
                Request.Url.ToString().ToLower().Contains("kolportaz.aspx"))
            {
                Context.Response.StatusCode = 301;
                Context.Response.Redirect("/");
            }

            if (Request.Url.ToString().ToLower().Contains("cennik.aspx") )
            {
                Context.Response.StatusCode = 301;
                Context.Response.Redirect("/Home/Cennik");
            }

            if (Request.Url.ToString().ToLower().Contains("kontakty.aspx"))
            {
                Context.Response.StatusCode = 301;
                Context.Response.Redirect("/Home/Kontakt");
            }

            if (Request.Url.ToString().ToLower().Contains("login.aspx"))
            {
                Context.Response.StatusCode = 301;
                Context.Response.Redirect("/Konto/Zaloguj");
            }


        }

        public override string GetVaryByCustomString(HttpContext context, string custom)
        {

            // User
            if (custom.ToLower() == "user")
            {
                if (User.Identity.IsAuthenticated)
                    return User.Identity.Name;
            }

            return base.GetVaryByCustomString(context, custom);
        }

        //protected void Application_EndRequest()
        //{
        //    if (Context.Response.StatusCode == 404)
        //    {
        //        Response.Clear();

        //        var rd = new RouteData();
        //        rd.DataTokens["area"] = "AreaName"; // In case controller is in another area
        //        rd.Values["controller"] = "Errors";
        //        rd.Values["action"] = "NotFound";

        //        IController c = new ErrorsController();
        //        c.Execute(new RequestContext(new HttpContextWrapper(Context), rd));
        //    }
        //}

    }
}